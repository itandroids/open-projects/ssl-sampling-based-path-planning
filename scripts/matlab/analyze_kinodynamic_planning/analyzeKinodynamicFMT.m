clear all
close all
clc
format long

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

color = ['b' 'r' 'g' 'm'];
colorCiInterval = ['b' 'r' 'g' 'm'];

resultsObstacles = containers.Map;
resultsConvergence = containers.Map;

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";
savePath = "/home/felipe/Desktop/ITA/Mestrado/tese/Cap5/";

fileConvergence = "results_analysis/convergenceTestFMTK";
filePerformance = "results_analysis/performanceTestFMTK";
fileSucProb = "results_analysis/successProbabilityFMTK";


withObstacles = "_WithObstacles";
withOptimization = "_optimizing";


%data = load(path + "bootstrappSpeedTest.txt");

%alpha = 0.05;
%cimean = bootci(100, @mean, data); % Average value confidence interval
%cistd = bootci(100, @std, data); % Standard deviation confidence interval

% Initializing variable for success probability results
sucProbCurve = [];
ciClopperPearson = [];



% Reading results for success probability
dataSucProb = load(path + fileSucProb + ".txt");
sucProbCurve(:,1) = dataSucProb(:,2)./dataSucProb(:,3);
[phat,pci] = binofit(dataSucProb(:,2),dataSucProb(:,3));
ciClopperPearson = cat(3,ciClopperPearson,pci);
dataSucProb = load(path + fileSucProb + withOptimization + ".txt");
sucProbCurve(:,2) = dataSucProb(:,2)./dataSucProb(:,3);
[phat,pci] = binofit(dataSucProb(:,2),dataSucProb(:,3));
ciClopperPearson = cat(3,ciClopperPearson,pci);
dataSucProb = load(path + fileSucProb + withObstacles + ".txt");
sucProbCurve(:,3) = dataSucProb(:,2)./dataSucProb(:,3);
[phat,pci] = binofit(dataSucProb(:,2),dataSucProb(:,3));
ciClopperPearson = cat(3,ciClopperPearson,pci);
dataSucProb = load(path + fileSucProb + withOptimization + withObstacles + ".txt");
sucProbCurve(:,4) = dataSucProb(:,2)./dataSucProb(:,3);
[phat,pci] = binofit(dataSucProb(:,2),dataSucProb(:,3));
ciClopperPearson = cat(3,ciClopperPearson,pci);





% Reading results for performance
data = load(path + filePerformance + ".txt");
cimeanSpeed(1,:) = bootci(100, @mean, data);
meanVSpeed(1,:) = mean(data);
stdVSpeed(1,:) = std(data);
cistdSpeed(1,:) = bootci(100, @std, data);
data = load(path + filePerformance + withOptimization + ".txt");
cimeanSpeed(2,:) = bootci(100, @mean, data);
meanVSpeed(2,:) = mean(data);
stdVSpeed(2,:) = std(data);
cistdSpeed(2,:) = bootci(100, @std, data);
data = load(path + filePerformance + withObstacles +".txt");
cimeanSpeed(3,:) = bootci(100, @mean, data);
meanVSpeed(3,:) = mean(data);
stdVSpeed(3,:) = std(data);
cistdSpeed(3,:) = bootci(100, @std, data);
data = load(path + filePerformance + withOptimization + withObstacles +  ".txt");
cimeanSpeed(4,:) = bootci(100, @mean, data);
meanVSpeed(4,:) = mean(data);
stdVSpeed(4,:) = std(data);
cistdSpeed(4,:) = bootci(100, @std, data);



% Generating Latex table
digits(4);
latex_table = latex(sym(vpa([meanVSpeed cimeanSpeed])));

%Reading results for convergence
dataConvergence = [];
dataConvergenceOpt = [];
dataConvergenceObst = [];
dataConvergenceOptObst = [];

filename = "";

convergenceTime = 10:10:120;
cimean = zeros(length(convergenceTime),2,4);
meanV = zeros(length(convergenceTime),1,4);
stdV = zeros(length(convergenceTime),1,4);
cistd = zeros(length(convergenceTime),2,4);


for i=1:length(convergenceTime)
    % Reading results for performance
    filename= path + fileConvergence + "Samples_" + convergenceTime(i) + ".txt";
    data = load(filename);
    cimean(i,:,1) = bootci(100, @mean, data(:,1));
    meanV(i,1,1) = mean(data(:,1));
    stdV(i,1,1) = std(data(:,1));
    cistd(i,:,1) = bootci(100, @std, data(:,1));
    filename = path + fileConvergence + withOptimization+ "Samples_" + convergenceTime(i) + ".txt";
    data = load(filename);
    cimean(i,:,2) = bootci(100, @mean, data(:,1));
    meanV(i,1,2) = mean(data(:,1));
    stdV(i,1,2) = std(data(:,1));
    cistd(i,:,2) = bootci(100, @std, data(:,1));
    filename = path + fileConvergence +  withObstacles+ "Samples_" + convergenceTime(i) + ".txt";
    data = load(filename);
    cimean(i,:,3) = bootci(100, @mean, data(:,1));
    cistd(i,:,3) = bootci(100, @std, data(:,1));
    meanV(i,1,3) = mean(data(:,1));
    stdV(i,1,3) = std(data(:,1));
    filename = path + fileConvergence + withOptimization+ withObstacles+ "Samples_" + convergenceTime(i) + ".txt";
    data = load(filename);
    cimean(i,:,4) = bootci(100, @mean, data(:,1));
    stdV(i,1,4) = std(data(:,1));
    cistd(i,:,4) = bootci(100, @std, data(:,1));
    meanV(i,1,4) = mean(data(:,1));
end


% Plotting results
 f1 = figure();
 
 color = ['b' 'r' 'g' 'm'];
 colorCiInterval = ['b' 'r' 'g' 'm'];

 hold on;
 for i = 1:4
    plot(convergenceTime, meanV(:,:,i), 'color' ,color(i),'Linewidth',2); 
    plot(convergenceTime, cimean(:,1,i), 'Marker', '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
    plot(convergenceTime, cimean(:,2,i), 'Marker' , '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
 end
 grid on;
legend("NO/NOb","O/NOb","NO/Obs","O/Obs",'Location','Best');
xlabel("Number of samples");
ylabel("Average path cost (s)");
ylim([0 20]);
saveas(f1,savePath + "convergenceTestFMTK","epsc");

% Plotting results
 f2 = figure();
 


 hold on;
 for i = 1:4
    plot(convergenceTime, stdV(:,:,i), 'color' ,color(i),'Linewidth',2);
        plot(convergenceTime, cistd(:,1,i), 'Marker', '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
    plot(convergenceTime, cistd(:,2,i), 'Marker' , '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
 end
 grid on;
legend("NO/NOb","O/NOb","NO/Obs","O/Obs",'Location','Best');
xlabel("Number of samples");
ylabel("Standard deviation (s)");
saveas(f2,savePath + "convergenceTestFMTKstd","epsc");

% Plotting success probability results
fSucProb = figure();

hold on
grid on
plot(dataSucProb(2:end,1),sucProbCurve(2:end,1),'Linewidth',3,'color' ,color(1));
plot(dataSucProb(2:end,1),sucProbCurve(2:end,2),'Linewidth',2,'color' ,color(2));
plot(dataSucProb(2:end,1),sucProbCurve(2:end,3),'Linewidth',2,'color' ,color(3));
plot(dataSucProb(2:end,1),sucProbCurve(2:end,4),'Linewidth',2,'color' ,color(4));
legend("NO/NOb","O/NOb","NO/Obs","O/Obs",'Location','Best');
xlabel("Planning time (s)");
ylabel("Success probability");
ylim([0 1]);
yticks([0:0.1:1]);

 for i = 1:4
    plot(dataSucProb(2:end,1),ciClopperPearson(2:end,1,i), 'Marker', '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
    plot(dataSucProb(2:end,1),ciClopperPearson(2:end,2,i), 'Marker' , '.' ,'color', colorCiInterval(i),'HandleVisibility','off');
 end
saveas(fSucProb,savePath + "successProbabilityFMTK","epsc");
legend("NO/NOb","O/NOb","NO/Obs","O/Obs",'Location','Best');


