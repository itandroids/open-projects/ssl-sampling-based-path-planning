clear all
close all
clc

resultsObstacles = containers.Map;
resultsConvergence = containers.Map;

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";

fileConvergence = "convergenceTestFMTK";
filePerformance = "performanceTestFMTK";


withObstacles = "_WithObstacles";
withOptimization = "_optimizing";


%data = load(path + "bootstrappSpeedTest.txt");

%alpha = 0.05;
%cimean = bootci(100, @mean, data); % Average value confidence interval
%cistd = bootci(100, @std, data); % Standard deviation confidence interval


% Reading results for performance
dataPerformance = load(path + filePerformance + ".txt");
dataPerformanceOpt = load(path + filePerformance + withOptimization + ".txt");
dataPerformanceObst = load(path + filePerformance + withObstacles +".txt");
dataPerformanceOptObst = load(path + filePerformance + withOptimization + withObstacles +  ".txt");



%Reading results for convergence
dataConvergence = [];
dataConvergenceOpt = [];
dataConvergenceObst = [];
dataConvergenceOptObst = [];

filename = "";


samplesVector  = [1;3;10;30;100;300;1000;3000];
cimean = zeros(length(samplesVector),2);
cistd = zeros(length(samplesVector),2);


for i=1:length(samplesVector)
    % Reading results for performance
    filename= path + fileConvergence + "Samples_" + samplesVector(i) + ".txt";
    data = load(filename);
    cimean(i,:) = bootci(100, @mean, data);
    cistd(i,:) = bootci(100, @std, data);
    filename = path + fileConvergence + withOptimization+ "Samples_" + samplesVector(i) + ".txt";
    data = load(filename);
    cimean(i,:) = bootci(100, @mean, data);
    cistd(i,:) = bootci(100, @std, data);
    filename = path + fileConvergence +  withObstacles+ "Samples_" + samplesVector(i) + ".txt";
    data = load(filename);
    cimean(i,:) = bootci(100, @mean, data);
    cistd(i,:) = bootci(100, @std, data);
    filename = path + fileConvergence + withOptimization+ withObstacles+ "Samples_" + samplesVector(i) + ".txt";
    data = load(filename);
    cimean(i,:) = bootci(100, @mean, data);
    cistd(i,:) = bootci(100, @std, data);
end


