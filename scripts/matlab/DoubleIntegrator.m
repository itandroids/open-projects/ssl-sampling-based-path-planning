close all;
clear all;
clc;

%Double Integrator system definition
A = [0 0 1 0;0 0 0 1;0 0 0 0;0 0 0 0];
B = [0 0;0 0;1 0;0 1];
C = [1 0 0 0;0 1 0 0];
D = 0;

sysc = ss(A,B,C,D);

sampleTime = 0.001;

sysd = c2d(sysc,sampleTime);