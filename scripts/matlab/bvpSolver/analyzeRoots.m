clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

% Initial state
%initialState = [-3.85862; 1.76404; -1.84721; 1.47269];
initialState = [0; 1.0; 0.0; 0.25];

% Final state
%finalState = [-3.90772; 0.918184; -0.616304; 1.56635];
finalState = [0.25; 1.0; 0.25; 0.25];


%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.1;
maxAcceleration = 4.0;
maxVelocity = 3.0;
maxIterations = 1000;
timeFinal = 0.0;
maxIterationsSimul = 10000;
it = 0.0;
dss = [];
[alphaFinal,betaFinal,timeFinal, ux,uy] = decoupledSearch(x(2),finalState(2),x(1),finalState(1),x(4),finalState(4),x(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
alphaFinal = [0.01:0.0001:pi/2];
timeX = [];
timeY = [];
for i=1:size(alphaFinal,2)
    [ux,tx,phase] = optimalBVP1D(x(1:2),finalState(1:2),maxVelocity*cos(betaFinal),maxAcceleration*cos(alphaFinal(i)),1);
    [uy,ty,phase] = optimalBVP1D(x(3:4),finalState(3:4),maxVelocity*sin(betaFinal),maxAcceleration*sin(alphaFinal(i)),1);
    timeX = [timeX;tx];
    timeY = [timeY;ty];
end

plot(alphaFinal,timeX,'linewidth',2);
hold on
grid on
plot(alphaFinal,timeY,'linewidth',2);
legend('$t_x$','$t_y$');

figure()
plot(alphaFinal,abs(timeX-timeY),'linewidth',2);
grid on
legend('$|t_x-t_y|$');

figure()
plot(alphaFinal,max(timeX,timeY),'linewidth',2);
grid on
legend('$max\{t_x,t_y\}$');



function saturatedX = saturateVelocity(state,maxVelocity,alpha)
%     alpha = atan2(state(4),state(2));
%     norm = sqrt(state(2)^2+state(4)^2);
%     if(norm > maxVelocity)
%         norm = maxVelocity;
%     end
%     state(2) = norm*cos(alpha);
%     state(4) = norm*sin(alpha);
%     saturatedX = state;
      if(state(2) > maxVelocity*cos(alpha))
          state(2) = maxVelocity*cos(alpha);
      end
      if(state(2) < -maxVelocity*cos(alpha))
          state(2) = -maxVelocity*cos(alpha);
     end
      if(state(4) > maxVelocity*sin(alpha))
          state(4) = maxVelocity*sin(alpha);
      end
      if(state(4) < -maxVelocity*sin(alpha))
          state(4) = -maxVelocity*sin(alpha);
      end
      saturatedX = state;
        
end
