function [x1,x2] = getPoints(xi,xf,r)
%GETPOINTS Summary of this function goes here
%   Detailed explanation goes here

a = 0.0;
if(xf(2) ~= xi(2))
    a = (xf(4)-xi(4))/(xf(2)-xi(2));
end

b = xf(4)-a*xf(2);

x1 =zeros(2,1);
x2 = zeros(2,1);

delta = sqrt(a^2*r^2-b^2+r^2);

x1(1) = (-a*b+delta)/(a^2+1);
x1(2) = a*x1(1)+b;
x2(1) = (-a*b-delta)/(a^2+1);
x2(2) = a*x2(1)+b;


end

