clc

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";

data = load(path + 'bvpSolution.txt');

% Defined in C++ BVP Unit Test
sampleTime = 0.001;

n = length(data);

timeAxis = (0:1:(n-1))*sampleTime;


figure()
hold on;
grid on;
plot(timeAxis,data(:,1),'Linewidth',2);
plot(timeAxis,data(:,2),'Linewidth',2);
plot(timeAxis,data(:,3),'Linewidth',2);
plot(timeAxis,data(:,4),'Linewidth',2);

legend('x','v','u');
xlabel('time(s)');

figure()
hold on;
grid on;
plot(timeAxis,data(:,1)-dss(:,1),'Linewidth',2);
plot(timeAxis,data(:,2)-dss(:,2),'Linewidth',2);
plot(timeAxis,data(:,3)-dss(:,3),'Linewidth',2);
plot(timeAxis,data(:,4)-curveSwitch,'Linewidth',2);