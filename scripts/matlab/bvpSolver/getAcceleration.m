function accel = getAcceleration(x,xr,xm,time,minAccel,maxAccel)
%OPTIMALBVP1D Summary of this function goes here
%   Detailed explanation goes here
accel = (minAccel+maxAccel)/2.0;
timeObtained = -1.0;
epsilon = 0.0000001;
maxIterations = 100;
it = 0;
while(it<maxIterations)
[u,timeObtained,phase] = optimalBVP1D(x,xr,xm,accel,1.0);
    
if(abs(time-timeObtained)<epsilon)
    break;
elseif time > timeObtained
    maxAccel = accel;
else minAccel = accel;
end

accel = (minAccel+maxAccel)/2.0;
    
it = it+1;
end

end

