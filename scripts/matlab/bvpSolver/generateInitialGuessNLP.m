function [path,totalTime] = generateInitialGuess(x1,x2,timesteps,maxVelocity,maxAcceleration)
% Generate initial solution guess for given BVP problem

maxIterations = 20;

% Numerical Optimization

x0 = zeros(2,1);
% Numerical Optimization
if(x1(2) == 0 && x1(4) == 0)
    x0(1) = atan2(x2(3)-x1(3),x2(1)-x1(1));
else x0(1) = atan2(x1(4),x1(2));
end
if (x2(2) == 0 && x2(4) == 0)
    x0(2) = atan2(x2(3)-x1(3),x2(1)-x1(1));
else x0(2) = atan2(x2(4),x2(2));
end

f = @(x) getTimeFromFrame(x1,x2,x(1),x(2),maxVelocity, maxAcceleration, maxIterations);
xOpt = fminsearch(f,x0);

% Getting optimal time
totalTime = f(xOpt);

% Getting sample time
sampleTime = totalTime/timesteps;
path = zeros(4,timesteps);

% Constructing system's matrices
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];


% Creating rectangle
r1 = [maxVelocity*cos(xOpt(1)); maxVelocity*sin(xOpt(1))];
r2 = [maxVelocity*cos(xOpt(2));maxVelocity*sin(xOpt(2))];
rm = (r1+r2)/2.0;
beta = atan2(rm(2),rm(1));
theta = acos(sqrt(rm'*rm)/maxVelocity);
maxVelocityX = maxVelocity*abs(cos(theta));
maxVelocityY = maxVelocity*abs(sin(theta));
[maxAccelX, maxAccelY, timeFinal, ux,uy, rotation] = decoupledSynchronization(x1(2),x2(2),x1(1),x2(1),x1(4),x2(4),x1(3),x2(3),maxAcceleration, maxVelocityX,maxVelocityY,maxIterations,beta);


it = 0;
x = x1;
rotatedState = zeros(4,1);
rotatedFinal = zeros(4,1);
x([1 3]) = rotation*x([1 3]);
x([2 4]) = rotation*x([2 4]);
x2([1 3]) = rotation*x2([1 3]);
x2([2 4]) = rotation*x2([2 4]);
while(it < timesteps)
    [ux,tx,phase] = optimalBVP1D(x(1:2),x2(1:2),maxVelocityX,maxAccelX,1);
    [uy,ty,phase] = optimalBVP1D(x(3:4),x2(3:4),maxVelocityY,maxAccelY,1);
    u = [ux;uy];
    x = saturateVelocity(x,maxVelocity,theta);

    x = A*x+B*u;
    rotatedState([1 3]) = rotation'*x([1 3]);
    rotatedState([2 4]) = rotation'*x([2 4]);
    path(:,it+1) = rotatedState; 
    x = saturateVelocity(x,maxVelocity,theta);
    it = it+1;
end

end

function saturatedX = saturateVelocity(state,maxVelocity,alpha)
%     alpha = atan2(state(4),state(2));
%     norm = sqrt(state(2)^2+state(4)^2);
%     if(norm > maxVelocity)
%         norm = maxVelocity;
%     end
%     state(2) = norm*cos(alpha);
%     state(4) = norm*sin(alpha);
%     saturatedX = state;
      if(state(2) > abs(maxVelocity*cos(alpha)))
          state(2) = abs(maxVelocity*cos(alpha));
      end
      if(state(2) < -abs(maxVelocity*cos(alpha)))
          state(2) = -abs(maxVelocity*cos(alpha));
     end
      if(state(4) > abs(maxVelocity*sin(alpha)))
          state(4) = abs(maxVelocity*sin(alpha));
      end
      if(state(4) < -abs(maxVelocity*sin(alpha)))
          state(4) = -abs(maxVelocity*sin(alpha));
      end
      saturatedX = state;
        
end

