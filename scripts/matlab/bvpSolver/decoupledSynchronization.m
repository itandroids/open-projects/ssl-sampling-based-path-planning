function [maxAccelerationX, maxAccelerationY, time, ux,uy, rotation] = decoupledSearchFrame(v0x,vfx, x0, xf,v0y,vfy, y0, yf,maxAcceleration, maxVelocityX, maxVelocityY, maxIterations, theta)
%GETCONTROLCOMMAND Function that synchronizes with any given velocity
%rectangle
%   Detailed explanation goes here


alpha = pi/4;
lowerBoundAlpha = 0.0; upperBoundAlpha = pi/2;
it = 0;
%epsilon = 0.0001;
epsilon = 0.001;
rotation = [cos(theta) sin(theta);-sin(theta) cos(theta)];
v0 = rotation*[v0x; v0y];
vf = rotation*[vfx; vfy];
pos = rotation*[x0; y0];
finalPos = rotation*[xf; yf];
minDiff = 999999;
alphaFound = pi/4;
while(it < maxIterations)
   [uxit,tx,phase] = optimalBVP1D([pos(1) v0(1)],[finalPos(1) vf(1)],maxVelocityX,maxAcceleration*cos(alpha),1);
   [uyit,ty,phase] = optimalBVP1D([pos(2) v0(2)],[finalPos(2) vf(2)],maxVelocityY,maxAcceleration*sin(alpha),1);
   if(ty < tx && minDiff > abs(tx-ty))
      alphaFound = alpha;
      minDiff=abs(tx-ty);
   end
   alphaFinal = alpha;
   time = max(tx,ty);
   ux = uxit;
   uy = uyit;
   if(abs(tx-ty)<epsilon)
       break;
   elseif(tx>ty)
       upperBoundAlpha = alpha;
   else lowerBoundAlpha = alpha;
   end
   alpha = (upperBoundAlpha+lowerBoundAlpha)/2.0;
   it = it+1; 
end

maxAccelerationX = maxAcceleration*cos(alphaFinal);
maxAccelerationY = maxAcceleration*sin(alphaFinal);

%accelerationEps = 0.000005;
if(abs(tx-ty)>epsilon && ty > 0)
    alphaFinal = alphaFound;
    maxAccelerationX = maxAcceleration*cos(alphaFinal);
    maxAccelerationY = maxAcceleration*sin(alphaFinal);
    
    [uxit,tx,phase] = optimalBVP1D([pos(1) v0(1)],[finalPos(1) vf(1)],maxVelocityX,maxAccelerationX,1);
    maxAccelerationY = getAcceleration([pos(2) v0(2)],[finalPos(2) vf(2)],maxVelocityY,tx,0,maxAccelerationY);
    [uyit,ty,phase] = optimalBVP1D([pos(2) v0(2)],[finalPos(2) vf(2)],maxVelocityY,maxAccelerationY,1);
    time =tx;   
end

if abs(tx-ty) > 10*epsilon
    time = inf;
end


end