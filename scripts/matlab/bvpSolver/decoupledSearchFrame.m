function [maxAccelerationX, maxAccelerationY, betaFinal, time, ux,uy, rotation, betaLimit] = decoupledSearchFrame(v0x,vfx, x0, xf,v0y,vfy, y0, yf,maxAcceleration, maxVelocity, maxIterations)
%GETCONTROLCOMMAND Summary of this function goes here
%   Detailed explanation goes here
gamma = 0.0;

if(yf~=y0 || xf~=x0)
    gamma = atan2(yf-y0,xf-x0);
end

if( v0x == 0 && v0y == 0)
    initBeta = wrapTo2Pi(gamma);
else
    initBeta = wrapTo2Pi(atan2(v0y,v0x));
end

if( vfx == 0 && vfy == 0)
    finalBeta = wrapTo2Pi(gamma);
else
    finalBeta = wrapTo2Pi(atan2(vfy,vfx));
end

if( v0x == 0 && v0y == 0 )
    beta = finalBeta;
    betaFinal = abs(finalBeta);
    betaLimit = finalBeta;
elseif(vfx == 0 && vfy == 0)
    beta = initBeta;
    betaFinal = abs(initBeta);
    betaLimit = initBeta;
else
    beta = (initBeta+finalBeta)/2.0;
    betaFinal = abs(beta-initBeta);
    betaLimit = abs(finalBeta-initBeta)/2.0;
end

beta = (initBeta+finalBeta)/2.0;
betaFinal = abs(beta-initBeta);
betaLimit = abs(finalBeta-initBeta)/2.0;

if abs(finalBeta-initBeta) > pi
    beta = beta+pi;
    betaFinal = abs(beta-initBeta);
end

alpha = pi/4;
lowerBoundAlpha = 0.0; upperBoundAlpha = pi/2;
it = 0;
epsilon = 0.0001;
rotation = [cos(beta) sin(beta);-sin(beta) cos(beta)];
v0 = rotation*[v0x; v0y];
vf = rotation*[vfx; vfy];
pos = rotation*[x0; y0];
finalPos = rotation*[xf; yf];
minDiff = 999999;
alphaFound = pi/4;
while(it < maxIterations)
   [uxit,tx,phase] = optimalBVP1D([pos(1) v0(1)],[finalPos(1) vf(1)],abs(maxVelocity*cos(betaFinal)),maxAcceleration*cos(alpha),1);
   [uyit,ty,phase] = optimalBVP1D([pos(2) v0(2)],[finalPos(2) vf(2)],abs(maxVelocity*sin(betaFinal)),maxAcceleration*sin(alpha),1);
   if(ty < tx && minDiff > abs(tx-ty))
      alphaFound = alpha;
      minDiff=abs(tx-ty);
   end
   alphaFinal = alpha;
   time = max(tx,ty);
   ux = uxit;
   uy = uyit;
   if(abs(tx-ty)<epsilon)
       break;
   elseif(tx>ty)
       upperBoundAlpha = alpha;
   else lowerBoundAlpha = alpha;
   end
   alpha = (upperBoundAlpha+lowerBoundAlpha)/2.0;
   it = it+1; 
end

maxAccelerationX = maxAcceleration*cos(alphaFinal);
maxAccelerationY = maxAcceleration*sin(alphaFinal);

accelerationEps = 0.000005;
if(abs(tx-ty)>epsilon && ty > 0)
    alphaFinal = alphaFound;
    maxAccelerationX = maxAcceleration*cos(alphaFinal);
    maxAccelerationY = maxAcceleration*sin(alphaFinal);
    
    [uxit,tx,phase] = optimalBVP1D([pos(1) v0(1)],[finalPos(1) vf(1)],maxVelocity*cos(betaFinal),maxAccelerationX,1);
    maxAccelerationY = getAcceleration([pos(2) v0(2)],[finalPos(2) vf(2)],maxVelocity*sin(betaFinal),tx,0,maxAccelerationY);
    time =tx;
    
    
end


end