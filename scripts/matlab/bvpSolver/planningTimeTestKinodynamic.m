clear all
close all
clc

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";

data = load(path + 'bootstrappSpeedTest.txt');

alpha = 0.05;
cimean = bootci(100, @mean, data); % Average value confidence interval
cistd = bootci(100, @std, data); % Standard deviation confidence interval
