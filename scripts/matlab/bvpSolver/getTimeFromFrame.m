function time = getTimeFromFrame(initialState,finalState,thetai,thetaj, maxVelocity, maxAcceleration, maxIterations)
%GETTIMEFROMFRAME Summary of this function goes here
%   Detailed explanation goes here
ri = [maxVelocity*cos(thetai); maxVelocity*sin(thetai)];
rj = [maxVelocity*cos(thetaj);maxVelocity*sin(thetaj)];
if(thetai < -pi || thetai > pi || thetaj < -pi || thetaj > pi)
    time = inf;
    return
end
rm = (ri+rj)/2.0;
beta = atan2(rm(2),rm(1));
theta = acos(sqrt(rm'*rm)/maxVelocity);
maxVelocityX = maxVelocity*abs(cos(theta));
maxVelocityY = maxVelocity*abs(sin(theta));
[maxAccelerationX, maxAccelerationY, time, ux,uy, rotation] = decoupledSynchronization(initialState(2),finalState(2),initialState(1),finalState(1),initialState(4),finalState(4),initialState(3),finalState(3),maxAcceleration, maxVelocityX,maxVelocityY,maxIterations,beta);


end

