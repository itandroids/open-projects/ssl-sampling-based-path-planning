clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.0001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

fieldXsize = 4.5;
fieldYsize = 3.0;

% Defining limits
maxAcceleration = 3.0;
maxVelocity = 3.0;
maxIterations = 20;
timeFinal = 0.0;
numberOfAttempts = 100000;
timeVector = [];
successCost = 5;
probSuccess = 0;
states = zeros(numberOfAttempts,2,4);
for i = 1:numberOfAttempts

    % Initial state
    initX = fieldXsize*(2*rand-1);
    initY = fieldYsize*(2*rand-1);

    randInitAngle = 2*pi*rand;
    randInitAngle = 2*pi*rand;
    randInitVelocity = maxVelocity*rand;
    initXdot = randInitVelocity*cos(randInitAngle);
    initYdot = randInitVelocity*sin(randInitAngle);
    initialState = [initX; initXdot; initY; initYdot];
    %initialState = [3.1972;-1.3552;-0.6350;2.4717];

    % Final state
    endX = fieldXsize*(2*rand-1);
    endY = fieldYsize*(2*rand-1);

    randEndAngle = 2*pi*rand;
    randEndVelocity = maxVelocity*rand;
    endXdot = randEndVelocity*cos(randEndAngle);
    endYdot = randEndVelocity*sin(randEndAngle);
    endState = [endX; endXdot; endY; endYdot];
    finalState = endState;
    %finalState = [0.9942;-2.8163;0.0629;0.1183];


    %Iteration
    x = initialState;
    error = sqrt((finalState-x)'*(finalState-x));
    epsilon = 0.9;


    it = 0.0;
    dss = [];
    [maxAccelX, maxAccelY,betaFinal,timeFinal, ux,uy, rotation, betaLimit] = decoupledSearchFrame(x(2),finalState(2),x(1),finalState(1),x(4),finalState(4),x(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
    rotatedState = zeros(4,1);
    rotatedFinal = zeros(4,1);
    x([1 3]) = rotation*x([1 3]);
    x([2 4]) = rotation*x([2 4]);
    maxIterationsSimul = timeFinal/sampleTime;
    finalState([1 3]) = rotation*finalState([1 3]);
    finalState([2 4]) = rotation*finalState([2 4]);
    maxIterationsSimul = timeFinal/sampleTime;
    if(timeFinal < successCost)
        probSuccess = probSuccess+1;
    end
    timeVector = [timeVector;timeFinal];
    states(i,1,:) = initialState;
    states(i,2,:) = endState;
end

plot(timeVector);
probSuccess = probSuccess/numberOfAttempts;



figure()
subplot(2,1,1);
%%Fit a distribution using a kernel smoother
myFit = fitdist(timeVector, 'kernel')
%%Visualize the resulting fit
index = linspace(0, 50, 100000);
plot(index, pdf(myFit, index), 'Linewidth', 2);
grid on
%%Inspect the complete set of methods for myFit
methods(myFit)
xlabel('$t(s)$');
ylabel('$f(t)$');
subplot(2,1,2); 
cumulativeTime = 0:0.01:100;
cumTime = cdf(myFit,cumulativeTime);
plot(cumulativeTime,cumTime, 'Linewidth', 2);
grid on
xlabel('$t(s)$');
ylabel('$F(t)$');


