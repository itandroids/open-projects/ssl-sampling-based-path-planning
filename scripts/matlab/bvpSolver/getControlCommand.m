function [alphaFinal, time, ux,uy] = getControlCommand(v0x,vfx, x0, xf,v0y,vfy, y0, yf,maxAcceleration, maxVelocity, maxIterations)
%GETCONTROLCOMMAND Summary of this function goes here
%   Detailed explanation goes here
alpha = pi/4;
lowerBound = 0.0; upperBound = pi/2;
it = 0;
epsilon = 0.0001;
while(it < maxIterations)
   [uxit,tx,phase] = optimalBVP1D([x0 v0x],[xf vfx],maxVelocity*cos(alpha),maxAcceleration*cos(alpha),1);
   [uyit,ty,phase] = optimalBVP1D([y0 v0y],[yf vfy],maxVelocity*sin(alpha),maxAcceleration*sin(alpha),1);
   alphaFinal = alpha;
   time = max(tx,ty);
   ux = uxit;
   uy = uyit;
   if(abs(tx-ty)<epsilon)
       break;
   elseif(tx>ty)
       upperBound = alpha;
   else lowerBound = alpha;
   end
   alpha = (upperBound+lowerBound)/2.0;
   it = it+1; 
end

end

