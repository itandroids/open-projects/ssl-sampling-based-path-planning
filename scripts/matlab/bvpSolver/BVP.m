function [time, accel] = BVP(v0, vc, vf, maxAcceleration, x0, xf)
if(xf >= x0)
    
    [time, accel]= trivialBVP(v0,vc,vf,maxAcceleration,x0,xf);
% %     if(v0 >= 0 && vf >= 0)
% %         d = xf-x0;
% %         minD = (vf*vf-v0*v0)/(2*maxAcceleration);
% %         [time, accel] = trivialBVP(v0,vc,vf,maxAcceleration,x0,xf);
% %         if(minD < d && minD > -d)
% %             [time, accel] = trivialBVP(v0,vc,vf,maxAcceleration,x0,xf);
% %         %else
% %          %   [time1, accel1] = trivialBVP(v0,vc,0,maxAcceleration,x0,x0+v0*v0/(2*maxAcceleration));
% %          %   [time2, accel2] = trivialBVP(0,vc,0,maxAcceleration,x0+v0*v0/(2*maxAcceleration),xf-vf*vf/(2*maxAcceleration));
% %          %   [time3, accel3] = trivialBVP(0,vc,vf,maxAcceleration,xf-vf*vf/(2*maxAcceleration),xf);
% %          %   time = time1+time2+time3;
% %          %   accel = accel1;
% %         end
% %     elseif(v0 <= 0 && vf >= 0)
% %         [time, accel] = trivialBVP(v0,vc,0 ,maxAcceleration,x0,xf-vf*vf/(2*maxAcceleration));
% %         %if(x0 - v0*v0/(2*maxAcceleration) < xf - vf*vf/(2*maxAcceleration))
% %         %    [time1, accel1] = trivialBVP(-v0,vc,0,maxAcceleration,x0,x0-v0*v0/(2*maxAcceleration));
% %         %    [time2, accel2] = trivialBVP(0,vc,vf,maxAcceleration,x0-v0*v0/(2*maxAcceleration),xf);
% %         %    time = time1+time2;
% %         %    accel = accel1;
% %         %else
% %         %    [time1,accel1] = trivialBVP(-v0,vc,0,maxAcceleration,x0,xf-vf*vf/(2*maxAcceleration));
% %         %    [time2,accel2] = trivialBVP(0,vc,vf,maxAcceleration,xf-vf*vf/(2*maxAcceleration),xf);
% %         %    time = time1+time2;
% %         %    accel = accel1;
% %         %end
% %     elseif(v0 <= 0 && vf <= 0)
% %         [time1, accel1] = trivialBVP(-v0,vc,0,maxAcceleration,x0,x0-v0*v0/(2*maxAcceleration));
% %         [time2, accel2] = trivialBVP(0,vc,0,maxAcceleration,x0-v0*v0/(2*maxAcceleration),xf+vf*vf/(2*maxAcceleration));
% %         [time3, accel3] = trivialBVP(0,vc,-vf,maxAcceleration,xf+vf*vf/(2*maxAcceleration),xf);
% %         time = time1+time2+time3;
% %         accel = accel1;
% %     else %v0 > 0 and vf < 0
% %         %[time, accel] = trivialBVP(v0,vc,-vf,maxAcceleration,x0,xf);
% %         [time, accel] = trivialBVP(v0,vc,0,maxAcceleration,x0,xf+vf*vf/(2*maxAcceleration));
% %         
% %         %if(x0 + v0*v0/(2*maxAcceleration) > xf + vf*vf/(2*maxAcceleration))
% %         %    [time1, accel1] = trivialBVP(v0,vc,0,maxAcceleration,x0,x0+v0*v0/(2*maxAcceleration));
% %         %    [time2, accel2] = trivialBVP(0,vc,-vf,maxAcceleration,x0+v0*v0/(2*maxAcceleration),xf);
% %         %    time = time1+time2;
% %         %    accel = accel1;
% %         %else
% %         %    [time1, accel1] = trivialBVP(v0,vc,0,maxAcceleration,x0,xf+vf*vf/(2*maxAcceleration));
% %         %    [time2, accel2] = trivialBVP(0,vc,-vf,maxAcceleration,xf+vf*vf/(2*maxAcceleration),xf);
% %         %    time = time1+time2;
% %         %    accel = accel1;
% %         %end
% %     end
else [time1,accel1] = BVP(-v0,vc,-vf,maxAcceleration,-x0,-xf);
    time = time1;
    accel = -accel1;
end
        
end

