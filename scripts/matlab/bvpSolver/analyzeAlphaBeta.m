clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

% Initial state
initialState = [-3.85862; 1.76404; -1.84721; 1.47269];
initialState = [0; 1.0; 0.0; 1.0];

% Final state
finalState = [-3.90772; 0.918184; -0.616304; 1.56635];
finalState = [0.25; 1.5; 0.25; 1.5];


%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.1;
maxAcceleration = 3.0;
maxVelocity = 3.0;
maxIterations = 1000;
timeFinal = 0.0;
maxIterationsSimul = 10000;
it = 0.0;
dss = [];
[alphaFinal,betaFinal,timeFinal, ux,uy] = decoupledSearch(x(2),finalState(2),x(1),finalState(1),x(4),finalState(4),x(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
alphaFinal = [0.001:0.01:pi/2-0.05];
betaFinal = [0.001:0.01:pi/2-0.05];
timeX = zeros(size(alphaFinal,2),size(betaFinal,2));
timeY = zeros(size(alphaFinal,2),size(betaFinal,2));
for i=1:size(alphaFinal,2)
    for j=1:size(betaFinal,2)
    [ux,tx,phase] = optimalBVP1D(x(1:2),finalState(1:2),maxVelocity*cos(betaFinal(j)),maxAcceleration*cos(alphaFinal(i)),1);
    [uy,ty,phase] = optimalBVP1D(x(3:4),finalState(3:4),maxVelocity*sin(betaFinal(j)),maxAcceleration*sin(alphaFinal(i)),1);
    if(tx > 200)
        tx = 0;
    end
    if(ty > 200)
        ty = 0;
    end
    timeX(i,j) = tx;
    timeY(i,j) = ty;
    end
end

h = surf(alphaFinal,betaFinal,timeX);
colorbar
hold on
grid on
xlabel('$\alpha$');
ylabel('$\beta$');
zlabel('$t_x$');
%plot(alphaFinal,timeY,'linewidth',2);
%legend('$t_x$','$t_y$');

figure()

h = surf(alphaFinal,betaFinal,timeY);
colorbar
hold on
grid on
xlabel('$\alpha$');
ylabel('$\beta$');
zlabel('$t_y$');
%plot(alphaFinal,abs(timeX-timeY),'linewidth',2);
%grid on
%legend('$|t_x-t_y|$');

%figure()
%plot(alphaFinal,max(timeX,timeY),'linewidth',2);
%grid on
%legend('$max\{t_x,t_y\}$');



function saturatedX = saturateVelocity(state,maxVelocity,alpha)
%     alpha = atan2(state(4),state(2));
%     norm = sqrt(state(2)^2+state(4)^2);
%     if(norm > maxVelocity)
%         norm = maxVelocity;
%     end
%     state(2) = norm*cos(alpha);
%     state(4) = norm*sin(alpha);
%     saturatedX = state;
      if(state(2) > maxVelocity*cos(alpha))
          state(2) = maxVelocity*cos(alpha);
      end
      if(state(2) < -maxVelocity*cos(alpha))
          state(2) = -maxVelocity*cos(alpha);
     end
      if(state(4) > maxVelocity*sin(alpha))
          state(4) = maxVelocity*sin(alpha);
      end
      if(state(4) < -maxVelocity*sin(alpha))
          state(4) = -maxVelocity*sin(alpha);
      end
      saturatedX = state;
        
end
