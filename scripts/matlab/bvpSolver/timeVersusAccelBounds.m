clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
% System matrices
sampleTime = 0.001;
A = [1 sampleTime;0 1];
B = [0;sampleTime];

% Initial state
initialState = [0; 1.0];

% Final state
finalState = [1.0;3.0];


%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.01;
maxAcceleration = 5;
maxVelocity = 6;
maxIterations = 30;
it = 0;
maxIterationsSimul = 10000;
dss = [];
xm = maxVelocity;
totalTime = 0.0;
phaseTotal = 'a';

accelerationBounds = [1:0.01:maxAcceleration];
timeResult = [];
for i = 1:length(accelerationBounds)
    [u,time,phase] = optimalBVP1D(x,finalState,maxVelocity,accelerationBounds(i),1);
    timeResult = [timeResult;time];
end

plot(accelerationBounds,timeResult,'LineWidth',2);
grid on
xlabel('$a_{max}$');
ylabel('$t(s)$');


