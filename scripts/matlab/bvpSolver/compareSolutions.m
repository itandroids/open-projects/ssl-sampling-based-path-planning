%This script compares the solution of using the optimal rectangle with the
%bisector solution. It generates two different probability distributions to
%compare them.

clear all;
close all;
clc;

path = "/home/felipe/Desktop/ITA/Mestrado/Artigos/bvp_solver/ieeeconf/";
path2 = "/home/felipe/Desktop/ITA/Mestrado/tese/Cap4/";

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.0001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

fieldXsize = 4.5;
fieldYsize = 3.0;

% Defining limits
maxAcceleration = 2.7747;
maxVelocity = 3.8651;
maxIterations = 20;
timeFinal = 0.0;
numberOfAttempts = 10000;
successCost = 5.0;
probSuccess = 0;
probSuccessSub = 0;
states = zeros(numberOfAttempts,2,4);

% Defining all rectangles
numUpper = 100;
numDown = 100;
thetaAbove = linspace(-pi/2,pi/2,numUpper);
thetaDown = linspace(-pi/2, pi/2,numDown);

optimalTime = 0.0;
subOptimalTime = 0.0;
optimalTimeArr = zeros(numberOfAttempts,1);
optimalTimeArrNM = zeros(numberOfAttempts,1);
subOptimalTimeArr = zeros(numberOfAttempts,1);
errorArr = zeros(numberOfAttempts,1);

% Stop verbosity of optimizers
optionsunc = optimoptions('fminunc','Display','off');
optionsfmin = optimset('Display','off');

timeNM = 0;
timeBFGS = 0;
timeBSC = 0;

for k = 1:numberOfAttempts

    % Initial state
    initX = fieldXsize*(2*rand-1);
    initY = fieldYsize*(2*rand-1);

    randInitAngle = 2*pi*rand;
    randInitAngle = 2*pi*rand;
    randInitVelocity = maxVelocity*sqrt(rand);
    initXdot = randInitVelocity*cos(randInitAngle);
    initYdot = randInitVelocity*sin(randInitAngle);
    initialState = [initX; initXdot; initY; initYdot];
    %initialState = [3.1972;-1.3552;-0.6350;2.4717];

    % Final state
    endX = fieldXsize*(2*rand-1);
    endY = fieldYsize*(2*rand-1);

    randEndAngle = 2*pi*rand;
    randEndVelocity = maxVelocity*sqrt(rand);
    endXdot = randEndVelocity*cos(randEndAngle);
    endYdot = randEndVelocity*sin(randEndAngle);
    endState = [endX; endXdot; endY; endYdot];
    finalState = endState;
    %finalState = [0.9942;-2.8163;0.0629;0.1183];
    [maxAccelX, maxAccelY,betaFinal,subOptimalTime, ux,uy, rotation, betaLimit] = decoupledSearchFrame(initialState(2),finalState(2),initialState(1),finalState(1),initialState(4),finalState(4),initialState(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
    
    %timeVector = zeros(numUpper,numDown);
    %for i = 1:length(thetaAbove)
    %    for j = 1:length(thetaDown)
    %        ri = [maxVelocity*cos(thetaAbove(i)); maxVelocity*sin(thetaAbove(i))];
    %        rj = [maxVelocity*cos(thetaDown(j));maxVelocity*sin(thetaDown(j))];
    %        rm = (ri+rj)/2.0;
    %        beta = atan2(rm(2),rm(1));
    %        theta = acos(sqrt(rm'*rm)/maxVelocity);
    %        maxVelocityX = maxVelocity*abs(cos(theta));
    %        maxVelocityY = maxVelocity*abs(sin(theta));
    %        [maxAccelerationX, maxAccelerationY, time, ux,uy, rotation] = decoupledSynchronization(initialState(2),finalState(2),initialState(1),finalState(1),initialState(4),finalState(4),initialState(3),finalState(3),maxAcceleration, maxVelocityX,maxVelocityY,maxIterations,beta);
    %        if(time < inf)
    %            timeVector(i,j) = time;
    %        else
    %            timeVector(i,j) = inf;
    %        end
    %    end
    %end
    
     %Numerical Optimization
    x0 = [atan2(initialState(4),initialState(2)); atan2(finalState(4),finalState(2))];
    f = @(x) getTimeFromFrame(initialState,finalState,x(1),x(2),maxVelocity, maxAcceleration, maxIterations);
    tic
    xOpt =fminunc(f,x0,optionsunc);
    optimalTime = f(xOpt);
    if(f(xOpt) > f(x0))
        optimalTime = f(x0);
    end
    timeBFGS = timeBFGS + toc;
    
    tic
    xOptnm = fminsearch(f,x0,optionsfmin);
    optimalTimeNM = f(xOptnm);
    timeNM = timeNM + toc;
    if(optimalTime < successCost)
        probSuccess = probSuccess+1;
    end
    if (subOptimalTime < successCost)
        probSuccessSub = probSuccessSub + 1;
    end
    errorArr(k) = subOptimalTime-optimalTime;
    subOptimalTimeArr(k) = subOptimalTime;
    optimalTimeArrNM(k) = optimalTimeNM;
    optimalTimeArr(k) = optimalTime;
    states(k,1,:) = initialState;
    states(k,2,:) = endState;
end

timeNM = timeNM/numberOfAttempts;
timeBFGS = timeBFGS/numberOfAttempts;

probSuccess = probSuccess/numberOfAttempts;
probSuccessSub = probSuccessSub/numberOfAttempts;



figure()
subplot(2,1,1);
%%Fit a distribution using a kernel smoother
myFit = fitdist(errorArr, 'kernel');
%%Visualize the resulting fit
index = linspace(0, 50, 100000);
plot(index, pdf(myFit, index), 'Linewidth', 2);
grid on
xlabel('$t(s)$');
ylabel('$f(t)$');
subplot(2,1,2); 
cumulativeTime = 0:0.01:100;
cumTime = cdf(myFit,cumulativeTime);
plot(cumulativeTime,cumTime, 'Linewidth', 2);
grid on
xlabel('$t(s)$');
ylabel('$F(t)$');


f2 = figure()
subplot(2,1,1);
%%Fit a distribution using a kernel smoother
hold on
optimalDist = fitdist(optimalTimeArr, 'kernel');
optimalDistNM = fitdist(optimalTimeArrNM, 'kernel');
subOptimalDist = fitdist(subOptimalTimeArr, 'kernel');
%%Visualize the resulting fit
index = linspace(0, 20, 100);
plot(index, pdf(subOptimalDist, index), 'Linewidth', 2);
plot(index, pdf(optimalDist, index), 'Linewidth', 2);
plot(index, pdf(optimalDistNM, index), 'Linewidth', 2);
grid on
title("(A) - Probability Distribution");
xlabel('$t(s)$');
ylabel('$f(t)$');
legend("Bisector method","BFGS","Nelder-Mead","Location","best");
subplot(2,1,2); 
hold on
cumulativeTime = 0:0.01:20;
cumTimeOptimal = cdf(optimalDist,cumulativeTime);
cumTimeOptimalNM = cdf(optimalDistNM,cumulativeTime);
cumTimeSubOptimal = cdf(subOptimalDist,cumulativeTime);
plot(cumulativeTime,cumTimeSubOptimal,'Linewidth', 2);
plot(cumulativeTime,cumTimeOptimal, 'Linewidth', 2);
plot(cumulativeTime,cumTimeOptimalNM, 'Linewidth', 2);
grid on
title("(B) - Cumulative Probability Distribution");
xlabel('$t(s)$');
ylabel('$F(t)$');
legend("Bisector method","BFGS","Nelder-Mead","Location","best");
saveas(f2,path + "comparison_two_solutions","epsc");
saveas(f2,path2 + "comparison_two_solutions","epsc");

f3 = figure()
hold on
subplot(3,1,1);
histogramInterval = [0:0.5:10];
tickInterval = [0:500:2000];
histogram(optimalTimeArrNM, histogramInterval,  'EdgeColor', 'blue', 'FaceColor',  'blue' );
title('Nelder-Mead optimization')
ylim([0 2000]);
grid on
xlabel('$t$ (s)')
ylim([0 2000]);
yticks(tickInterval);
grid on


subplot(3,1,2);
histogram(optimalTimeArr, histogramInterval,  'EdgeColor', 'red', 'FaceColor',  'red');
xlabel('$t$ (s)')
title('BFGS optimization')
ylim([0 2000]);
grid on
ylim([0 2000]);
yticks(tickInterval);


subplot(3,1,3);
grid on
histogram(subOptimalTimeArr, histogramInterval, 'EdgeColor', [0.8500, 0.3250, 0.0980], 'FaceColor',  [0.8500, 0.3250, 0.0980]);
xlabel('$t$ (s)')
title('Bisector method')
ylim([0 2000]);
yticks(tickInterval)
grid on

saveas(f3,path + "comparison_two_solutions_histogram","epsc");
saveas(f3,path2 + "comparison_two_solutions_histogram","epsc");




