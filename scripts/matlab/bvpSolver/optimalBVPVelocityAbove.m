function [u, time,phase] = optimalBVPVelocityAbove(x,xr,xm,k1,k2)
%OPTIMALBVP1D This function solves the BVP considering speeds above the
%limit
%   Detailed explanation goes here
epsilon = 0.001;
safeguard = 0.03;
epsilonState = 0.000001;

if (x-xr)'*(x-xr) < epsilonState
    time = 0;
    u = 0;
    phase = 'nan';
    return 
end

if(x(2)> xm)
    u = -k1;
elseif (x(2) < -xm)
    u = k1;
elseif ((x(2) >= -xm) && (x(2) <= xm) && (x(2) > xr(2)) && abs(x(1) -(-k2/(2*k1)*(x(2)^2-xr(2)^2) +xr(1)) )<epsilon)
    u = -k1;
elseif((x(2) > -xm) && (x(2) < xm )&& (x(1) < k2/(2*k1)*sign(-x(2)+xr(2))*(x(2)^2-xr(2)^2) +xr(1))) || ((x(2) >= -xm) && (x(2) <= xm) && (x(2) < xr(2)) && abs(x(1) -(k2/(2*k1)*(x(2)^2-xr(2)^2) +xr(1)))<epsilon)
    u = k1;
    
elseif((x(2) > -xm) && (x(2) < xm )&& (x(1) > k2/(2*k1)*sign(-x(2)+xr(2))*(x(2)^2-xr(2)^2) +xr(1))) 
    u = -k1;
else
    u = 0;
end
time=0;


B = k2/(2*k1)*sign(-x(2)+xr(2))*(x(2)^2-xr(2)^2)+xr(1);
C = k2/(2*k1)*(x(2)^2+xr(2)^2)-k2/k1*xm^2+xr(1);
D = -k2/(2*k1)*(x(2)^2+xr(2)^2)+k2/k1*xm^2+xr(1);
A = (xr(2) <= xm && xr(2) >= -xm && k1 >= 0 && x(2) <= xm && x(2) >= -xm);

if A && (x(1) <= B && x(1) >= C)
   time = (-k2*(x(2)+xr(2))+2*sqrt(k2^2*x(2)^2-k1*k2*(k2/(2*k1)*(x(2)^2-xr(2)^2)+x(1)-xr(1))))/(k1*k2);
   phase = 'a';
elseif A && (x(1) <= B && x(1) < C)
    time = (xm-x(2)-xr(2))/k1+(x(2)^2+xr(2)^2)/(2*k1*xm)+(xr(1)-x(1))/(k2*xm);
    phase = 'b';
elseif A && (x(1)>B && x(1) <= D)
    time = (k2*(x(2)+xr(2))+2*sqrt(k2^2*x(2)^2+k1*k2*(k2/(2*k1)*(-x(2)^2+xr(2)^2)+x(1)-xr(1))))/(k1*k2);
    phase = 'c';
elseif A && (x(1)>B && x(1) > D)
    time = (xm+x(2)+xr(2))/k1+(x(2)^2+xr(2)^2)/(2*k1*xm)-(xr(1)-x(1))/(k2*xm);
    phase = 'd';
else
    [~,time,~] = optimalBVPVelocityAbove([x(1),xr,xm,k1,k2);
    deltaTime = abs(xm-x(2))/k1;
    finalPos = x(1) + x(2)*deltaTime + u*deltaTime^2/2.0;
    nextState = [finalPos; sign(x(2))*xm];
    time =  deltaTime +  optimalBVPVelocityAbove(nextState,xr,xm,k1,k2) ;
    phase = 'nan';
end




end

