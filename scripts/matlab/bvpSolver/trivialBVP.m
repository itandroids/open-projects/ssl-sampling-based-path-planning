function [time, acceleration] = trivialBVP(v0, vc, vf, maxAcceleration, x0, xf)
%TRIVIALBVP Summary of this function goes here
% x0 initial position
% xf final position
% maxAcceleration maximum allowed acceleration
% v0 initial velocity
% vc cruise velocity
% vf final velocity
% returns time and acceleration
d = abs(xf-x0);
d1 = (vc*vc-v0*v0)/(2*maxAcceleration);
d2 = (vc*vc-vf*vf)/(2*maxAcceleration);
dArrival = 0.0;
time = 0.0;
if(v0 >= 0 && vf >= 0)
    dArrival = (vf*vf-v0*v0)/(2*maxAcceleration);
elseif((v0 >= 0 && vf <= 0)||(v0 <= 0 && vf >= 0))
    dArrival = (vf*vf+v0*v0)/(2*maxAcceleration);
else
    dArrival = (vf*vf+v0*v0)/(2*maxAcceleration);
end
sign = 1;

if d1==0
    sign = 0;
end

if d1+d2 <= d
    time = (2*vc-v0-vf)/maxAcceleration + (d-d1-d2)/vc;
    acceleration = maxAcceleration*sign;
elseif dArrival >= d
    time = (vf-v0)/maxAcceleration;
    acceleration = maxAcceleration;
elseif dArrival <= -d
    time = (v0-vf)/maxAcceleration;
    acceleration = -maxAcceleration;
else
    v = sqrt((v0*v0+vf*vf+2*maxAcceleration*d)/2.0);
    time = (2*v-v0-vf)/maxAcceleration;
    acceleration = maxAcceleration;
    
end


if v0 >= 0 && vf >= 0
    if vf^2/(2*maxAcceleration) -v0^2/(2*maxAcceleration) <= xf-x0
        acceleration = maxAcceleration;
    else acceleration = -maxAcceleration;
    end
elseif v0 >= 0 && vf <= 0
    if x0-v0^2/(2*maxAcceleration) < xf+vf^2/(2*maxAcceleration) 
        acceleration = maxAcceleration;
    else acceleration = -maxAcceleration;
    end
elseif v0 <= 0 && vf >= 0
    if x0+v0^2/(2*maxAcceleration) < xf-vf^2/(2*maxAcceleration) 
    acceleration = maxAcceleration;
    else acceleration = -maxAcceleration;
    end
elseif v0 <= 0 && vf <= 0
    if x0+v0^2/(2*maxAcceleration) < xf+vf^2/(2*maxAcceleration) 
    acceleration = -maxAcceleration;
    else acceleration = +maxAcceleration;
    end
end
    





end

