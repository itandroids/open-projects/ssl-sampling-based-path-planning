% Script to visualize path done by planner.

clear all
close all
clc




set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
%ax = axes;
%c = colorbar;
%ax.FontName = 'cmr12'; %installation file: cmunsl.ttf
%c.FontName = 'cmr12';

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";

savePath = "/home/felipe/Desktop/ITA/Mestrado/tese/Cap5/";
planners = ["RRTK" "FMTK"];
fileSuffix = "_path_example";
goalSize = 0.70;
goalDepth = 0.50;
maxXsize = 4.5;
maxYsize = 3.0;

fieldCoordinatesX = [-maxXsize maxXsize maxXsize maxXsize+goalDepth maxXsize+goalDepth maxXsize  maxXsize -maxXsize -maxXsize -maxXsize-goalDepth -maxXsize-goalDepth -maxXsize -maxXsize];
fieldCoordinatesY = [-maxYsize -maxYsize -goalSize/2.0 -goalSize/2.0 goalSize/2.0  goalSize/2.0 maxYsize maxYsize goalSize/2.0 goalSize/2.0 -goalSize/2.0  -goalSize/2.0   -maxYsize ];


for fileNumber = 1:3
    for plannerNumber = 1:2
        data = load(path + planners(plannerNumber) + "narrowPhaseAnalysis_" + fileNumber + ".txt");

        
        
        % Get initial information for setup
        initialMessage = data(1,:);
        numberOfStates = initialMessage(1);
        numberOfObstacles = initialMessage(2);
        obstacleRadius = initialMessage(3);



        % The first lines of the data may represent the position of each static
        % obstacle
        obstacles = data(2:2+numberOfObstacles-1,:);


        % Jumping the initial lines, we reach the full path across the two
        % different states
        pathExecuted = data(2+numberOfObstacles:end,:);

        maxVelocity = 3.8651;
        obstacleCheck = [];
        distanceCheck = 0.0;
        for  i =1:length(obstacles)
            for j = i:length(obstacles)
               if(i ~= j)
                  distanceCheck = (obstacles(i,:)-obstacles(j,:))*(obstacles(i,:)-obstacles(j,:))';
                  if(distanceCheck < (obstacleRadius)^2)
                      disp('obstacle too close');
                  end
               end

            end
        end

        f = figure()
        x0=10;
        y0=10;
        width=800;
        height=800;
        set(gcf,'position',[x0,y0,width,height])
        axis equal

        hold on

        sizeVec = 1:size(pathExecuted,1);
        mp = hsv;
        mp = flip(mp(1:43,:));



        for i=1:length(sizeVec)
            val = (sqrt(pathExecuted(sizeVec(i),2)^2+pathExecuted(sizeVec(i),4)^2)/maxVelocity);
            plot(pathExecuted(sizeVec(i),1), pathExecuted(sizeVec(i),3),'o', 'color', mp(ceil(val*size(mp,1)),:));
        end
        colormap(mp);
        caxis([0 maxVelocity]);
        colorbarObject  = colorbar('southoutside');
        set(colorbarObject,'TickLabelInterpreter','latex');

        %plot(pathExecuted(1:200:end,1),pathExecuted(1:200:end,3),'o' ,'Linewidth', 2);
        plot(fieldCoordinatesX,fieldCoordinatesY, 'Linewidth', 2, 'color', 'k');
        plot(obstacles(:,1),obstacles(:,3), 'k.');
        grid on
        for i = 1:size(obstacles,1)
            viscircles([obstacles(i,1) obstacles(i,3)],obstacleRadius/2.0, 'Linewidth', 2, 'color','k');
        end
        xlim([-5 5]);
        ylim([-5 5]);
        xticks([-5:1:5]);
        yticks([-6:1:6]);
        f.Renderer='Painters';
        saveas(f,savePath + planners(plannerNumber) +  fileSuffix + "_" + fileNumber,"epsc");
    end
end


