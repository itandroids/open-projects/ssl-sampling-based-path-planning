% Test using bisector change of frame

clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

path = "/home/felipe/Desktop/ITA/Mestrado/Artigos/bvp_solver/ieeeconf/";
path2 = "/home/felipe/Desktop/ITA/Mestrado/tese/Cap4/";

% System matrices
sampleTime = 0.001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

% Initial state
%initialState = [0.0; 0.0; 0.0; 2.99];
initialState = [4.0; 1.0; 3.0; 0.0];
%initialState = [-3.871875552227694; 3.6038635982183647;2.495202577326368; -0.063753127525774722]
%initialState = [-1.9354; 1.4546;0.4951; 1.2116];


% Final state
%finalState = [0.0; -2.99; -1.0; 0.0];
finalState = [-3.0; 0.0; -4.0; 1.0];
%finalState = [3.387910740260561;  0.13380679685309474; 0.84873020223523543; 1.5269727112744635]


%finalState = [0.6601; -0.1774; -2.5062; -0.1636];

%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.9;
%Robot data: maxAcceleration = 2.7747;
%Robot data: maxVelocity = 3.8651;
maxAcceleration = 2.7747;
maxVelocity = 3.8651;
maxIterations = 20;
timeFinal = 0.0;

it = 0.0;
dss = [];
rss = [];
[maxAccelX, maxAccelY,betaFinal,timeFinal, ux,uy, rotation, betaLimit] = decoupledSearchFrame(x(2),finalState(2),x(1),finalState(1),x(4),finalState(4),x(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
rotatedState = zeros(4,1);
rotatedFinal = zeros(4,1);
x([1 3]) = rotation*x([1 3]);
x([2 4]) = rotation*x([2 4]);
maxIterationsSimul = timeFinal/sampleTime;
finalState([1 3]) = rotation*finalState([1 3]);
finalState([2 4]) = rotation*finalState([2 4]);
maxIterationsSimul = timeFinal/sampleTime;
broadPhaseRec = zeros(2,4);
maxXrec = max(x(1)+x(2)^2/(2*maxAccelX), finalState(1)+finalState(2)^2/(2*maxAccelX)); 
maxYrec = max(x(3)+x(4)^2/(2*maxAccelY), finalState(3)+finalState(4)^2/(2*maxAccelY)); 
minXrec = min(x(1)-x(2)^2/(2*maxAccelX), finalState(1)-finalState(2)^2/(2*maxAccelX)); 
minYrec = min(x(3)-x(4)^2/(2*maxAccelY), finalState(3)-finalState(4)^2/(2*maxAccelY)); 
broadPhaseRec(:,1) = rotation'*[maxXrec; maxYrec];
broadPhaseRec(:,2) = rotation'*[minXrec; maxYrec];
broadPhaseRec(:,3) = rotation'*[minXrec; minYrec];
broadPhaseRec(:,4) = rotation'*[maxXrec; minYrec];
while(it < maxIterationsSimul)
    [ux,tx,phase] = optimalBVP1D(x(1:2),finalState(1:2),abs(maxVelocity*cos(betaFinal)),maxAccelX,1);
    [uy,ty,phase] = optimalBVP1D(x(3:4),finalState(3:4),abs(maxVelocity*sin(betaFinal)),maxAccelY,1);
    u = [ux;uy];
    x = saturateVelocity(x,maxVelocity,betaFinal);
    
    x = A*x+B*u;
    rotatedState([1 3]) = rotation'*x([1 3]);
    rotatedState([2 4]) = rotation'*x([2 4]);
    rstate = [x;u];
    dstate = [rotatedState' (rotation'*u)'];
    dss = [dss;dstate];
    rss = [rss;rstate'];
    x = saturateVelocity(x,maxVelocity,betaFinal);
    error = sqrt((finalState-x)'*(finalState-x));
    it = it+1;
end

timeVec = sampleTime*[0:1:it-1];

figure()
hold on
plot(timeVec,rss(:,1:2),'LineWidth',2);
plot(timeVec,rss(:,5),'LineWidth',2);
xlabel('$t$ (s)');
grid on 
legend("$x'$ (m)","$v_{x'}$ (m/s)","$u_{x'}$ (m/s$^2$)", 'Location', 'Best');
saveas(gcf,path + "full_loop_rotated_bisector_x","epsc");
saveas(gcf,path2 + "full_loop_rotated_bisector_x","epsc");

figure()
hold on
plot(timeVec,rss(:,3:4),'LineWidth',2);
plot(timeVec,rss(:,6),'LineWidth',2);
xlabel('$t$ (s)');
grid on
legend("$y'$ (m)","$v_{y'}$ (m/s)","$u_{y'}$ (m/s$^2$)", 'Location', 'Best');
saveas(gcf,path + "full_loop_rotated_bisector_y","epsc");
saveas(gcf,path2 + "full_loop_rotated_bisector_y","epsc");

figure()
plot(timeVec,rss,'LineWidth',2);
xlabel('$t$ (s)');
grid on
legend("$x'$","$v_{x'}$","$y'$","$v_{y'}$","$u_{x'}$","$u_{y'}$", 'Location', 'Best');
saveas(gcf,path + "full_loop_rotated_bisector","epsc");
saveas(gcf,path2 + "full_loop_rotated_bisector","epsc");

figure()
plot(timeVec,dss,'LineWidth',2);
xlabel('$t$ (s)');
grid on
legend('$x$','$v_x$','$y$','$v_y$','$u_x$','$u_y$', 'Location', 'Best');
saveas(gcf,path + "full_loop_bisector","epsc");
saveas(gcf,path2 + "full_loop_bisector","epsc");


figure()
xlabel('$q$');
ylabel('$\dot{q}$');
hold on
grid on
plot(dss(:,1),dss(:,2),'LineWidth',2);
plot(dss(:,3),dss(:,4),'LineWidth',2);
legend('$x$','$y$', 'Location', 'Best');
saveas(gcf,path + "phase_space_bisector","epsc");
saveas(gcf,path2 + "phase_space_bisector","epsc");

figure()
hold on
grid on
xlabel('$x$');
ylabel('$y$');
plot(dss(:,1),dss(:,3),'LineWidth',2);
xlim([-6,6]);
ylim([-6,6]);
plot(broadPhaseRec(1,1:2),broadPhaseRec(2,1:2),'g','Linewidth',2);
plot(broadPhaseRec(1,2:3),broadPhaseRec(2,2:3),'g','Linewidth',2);
plot(broadPhaseRec(1,3:4),broadPhaseRec(2,3:4),'g','Linewidth',2);
plot(broadPhaseRec(1,[4 1]),broadPhaseRec(2,[4 1]),'g','Linewidth',2);
legend('$\pi(x(t),u(t))$','$R_b$', 'Location', 'Best');
saveas(gcf,path + "trajectory_bisector","epsc");
saveas(gcf,path2 + "trajectory_bisector","epsc");


function saturatedX = saturateVelocity(state,maxVelocity,alpha)
%     alpha = atan2(state(4),state(2));
%     norm = sqrt(state(2)^2+state(4)^2);
%     if(norm > maxVelocity)
%         norm = maxVelocity;
%     end
%     state(2) = norm*cos(alpha);
%     state(4) = norm*sin(alpha);
%     saturatedX = state;
      if(state(2) > abs(maxVelocity*cos(alpha)))
          state(2) = abs(maxVelocity*cos(alpha));
      end
      if(state(2) < -abs(maxVelocity*cos(alpha)))
          state(2) = -abs(maxVelocity*cos(alpha));
     end
      if(state(4) > abs(maxVelocity*sin(alpha)))
          state(4) = abs(maxVelocity*sin(alpha));
      end
      if(state(4) < -abs(maxVelocity*sin(alpha)))
          state(4) = -abs(maxVelocity*sin(alpha));
      end
      saturatedX = state;
        
end
