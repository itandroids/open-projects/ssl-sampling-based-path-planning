%File to analyze convergence of kinodynamic path planners when the number
%of samples increase.

clear all
close all
clc

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

path = "/home/felipe/Desktop/ITA/Mestrado/ssl-sampling-based-path-planning/binaries/";

data = load(path + 'KFMTConvergenceTest.txt');

% Get initial information for setup
figure()
loglog(data(:,3),data(:,1),'Linewidth',2);
grid on
xlabel('Number of samples');
ylabel('Average time of arrival(s)');

figure()
loglog(data(:,3),data(:,2),'Linewidth',2);
grid on
xlabel('Number of samples');
ylabel('Variance');