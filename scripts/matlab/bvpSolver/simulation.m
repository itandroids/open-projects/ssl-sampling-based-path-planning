clear all;
close all;
clc;


% System matrices
sampleTime = 0.0001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

% Initial state
initialState = [-3.85862; 1.76404; -1.84721; 1.47269];

% Final state
finalState = [-3.90772; 0.918184; -0.616304; 1.56635];


%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.1;
maxAcceleration = 3.0;
maxVelocity = 3.0;
maxIterations = 1000;
timeFinal = 0.0;
maxIterationsSimul = 50000;
it = 0.0;
dss = [];
[alphaFinal,betaFinal,timeFinal, ux,uy] = decoupledSearch(x(2),finalState(2),x(1),finalState(1),x(4),finalState(4),x(3),finalState(3),maxAcceleration, maxVelocity,maxIterations);
while(error > epsilon && it < maxIterationsSimul)
    [ux,tx,phase] = optimalBVP1D(x(1:2),finalState(1:2),maxVelocity*cos(betaFinal),maxAcceleration*cos(alphaFinal),1);
    [uy,ty,phase] = optimalBVP1D(x(3:4),finalState(3:4),maxVelocity*sin(betaFinal),maxAcceleration*sin(alphaFinal),1);
    u = [ux;uy];
    x = saturateVelocity(x,maxVelocity,betaFinal);
    
    x = A*x+B*u;
    dstate = [x' u' alphaFinal];
    dss = [dss;dstate];
    x = saturateVelocity(x,maxVelocity,betaFinal);
    error = sqrt((finalState-x)'*(finalState-x));
    it = it+1;
end

timeVec = sampleTime*[0:1:it-1];
plot(timeVec,dss);
legend('x','vx','y','vy','ux','uy','alpha');


function saturatedX = saturateVelocity(state,maxVelocity,alpha)
%     alpha = atan2(state(4),state(2));
%     norm = sqrt(state(2)^2+state(4)^2);
%     if(norm > maxVelocity)
%         norm = maxVelocity;
%     end
%     state(2) = norm*cos(alpha);
%     state(4) = norm*sin(alpha);
%     saturatedX = state;
      if(state(2) > maxVelocity*cos(alpha))
          state(2) = maxVelocity*cos(alpha);
      end
      if(state(2) < -maxVelocity*cos(alpha))
          state(2) = -maxVelocity*cos(alpha);
     end
      if(state(4) > maxVelocity*sin(alpha))
          state(4) = maxVelocity*sin(alpha);
      end
      if(state(4) < -maxVelocity*sin(alpha))
          state(4) = -maxVelocity*sin(alpha);
      end
      saturatedX = state;
        
end
