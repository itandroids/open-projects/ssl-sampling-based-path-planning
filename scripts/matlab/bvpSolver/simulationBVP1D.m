clear all;
close all;
clc;


set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.001;
A = [1 sampleTime;0 1];
B = [0;sampleTime];

% Initial state
initialState = [0; 0.7654];
initialState = [0; 1.8478];
initialState = [2.92746 -0.285881]';

% Final state
finalState = [0.5412; 1.3066];
finalState = [-1.3066; 0.5412];
finalState =  [3.59808 2.55272]';

%Iteration
x = initialState;
error = sqrt((finalState-x)'*(finalState-x));
epsilon = 0.005;
maxAcceleration = 3.0;
maxVelocity =  3.0;
maxIterations = 30;
it = 0;
maxIterationsSimul = 50000;
dss = [];
xm = maxVelocity;
totalTime = 0.0;
phaseTotal = 'a';
[u0,totalTime,phase] = optimalBVP1D(x,finalState,maxVelocity,maxAcceleration,1);
maxIterationsSimul = totalTime/sampleTime;
curveSwitch = [];
while(it < maxIterationsSimul)
    [u,time,phase] = optimalBVP1D(x,finalState,maxVelocity,maxAcceleration,1);
    curveSwitch = [curveSwitch; 1.0/(2*maxAcceleration)*sign(finalState(2)-x(2))*(x(2)^2-finalState(2)^2) + finalState(1)-x(1)];
    if(it == 0)
        totalTime = time;
        phaseTotal = phase; 
        maxIterationsSimul  = totalTime/sampleTime;
    end
    x(1) = x(1) + x(2)*0.5*sampleTime;
    x(2) = x(2) + u*sampleTime;
    x = saturateVelocity(x,maxVelocity);
    
    %x = A*x+B*u;
    x(1) = x(1) + 0.5*x(2)*sampleTime;
    
    dstate = [x' u ];
    dss = [dss;dstate];
    x = saturateVelocity(x,maxVelocity);
    error = sqrt((finalState-x)'*(finalState-x))
    it = it+1;
end

timeVec = sampleTime*(0:1:it-1)';
figure()
plot(timeVec,dss,'LineWidth',2);
hold on
plot(timeVec,curveSwitch,'LineWidth',2);
grid on
xlabel('time(s)');
legend('$x$','$v_x$','$a_x$');

figure()
plot(dss(:,1),dss(:,2),'LineWidth',2);
grid on
xlabel('$x$');
ylabel('$v_x$');


function saturatedX = saturateVelocity(state,maxVelocity)
    if(state(2) > maxVelocity)
        state(2) = maxVelocity;
    elseif state(2) < -maxVelocity
        state(2)=-maxVelocity;
    end
    saturatedX = state;
    
end
