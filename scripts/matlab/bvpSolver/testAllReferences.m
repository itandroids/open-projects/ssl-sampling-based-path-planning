%This script attempts to try all possible rectangles, getting the best
%rectangle. It also includes in the end a numerical optimization to get
%optimal rectangle.

clear all;
close all;
clc;

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

% System matrices
sampleTime = 0.0001;
A = [1 sampleTime 0 0;0 1 0 0; 0 0 1 sampleTime; 0 0 0 1];
B = [sampleTime^2/2 0;sampleTime 0;sampleTime^2/2 0; 0 sampleTime];

fieldXsize = 4.5;
fieldYsize = 3.0;

fieldXsize = 1.0;
fieldYsize = 1.0;

% Defining limits
maxAcceleration = 3.0;
maxVelocity = 3.0;
maxIterations = 20;
timeFinal = 0.0;
numberOfAttempts = 10000;
timeVector = [];
successCost = 5;
probSuccess = 0;
states = zeros(numberOfAttempts,2,4);

% Defining all rectangles
numUpper = 200;
numDown = 200;
thetaAbove = linspace(-pi/2,pi/2,numUpper);
thetaDown = linspace(-pi/2, pi/2,numDown);

% Initial state
%initialState = [4.0; 1.0; 3.0; 0.0];
%initialState = [0.4470; 0.9144; -1.6866; -0.1558];

initialState = [0.0; 1.0; 0.0; 0.0];


% Final state
%finalState = [3.0; 1.0; 4.0; 1.0];
%finalState = [4.0162; 0.9282; -0.5366; 0.0604];
finalState = [1.0; -1.0; 1.0; 0.0];

timeVector = zeros(numUpper,numDown);

for i = 1:length(thetaAbove)
    for j = 1:length(thetaDown)
        ri = [maxVelocity*cos(thetaAbove(i)); maxVelocity*sin(thetaAbove(i))];
        rj = [maxVelocity*cos(thetaDown(j));maxVelocity*sin(thetaDown(j))];
        rm = (ri+rj)/2.0;
        beta = atan2(rm(2),rm(1));
        theta = acos(sqrt(rm'*rm)/maxVelocity);
        maxVelocityX = maxVelocity*abs(cos(theta));
        maxVelocityY = maxVelocity*abs(sin(theta));
        [maxAccelerationX, maxAccelerationY, time, ux,uy, rotation] = decoupledSynchronization(initialState(2),finalState(2),initialState(1),finalState(1),initialState(4),finalState(4),initialState(3),finalState(3),maxAcceleration, maxVelocityX,maxVelocityY,maxIterations,beta);
        if(time < 10)
            timeVector(i,j) = time;
        else
            timeVector(i,j) = 10;
        end
    end
end
minMatrix = min(timeVector(:));
[row,col] = find(timeVector==minMatrix);
r1 = [maxVelocity*cos(thetaAbove(row(1))); maxVelocity*sin(thetaAbove(row(1)))];
r2 = [maxVelocity*cos(thetaDown(col(1)));maxVelocity*sin(thetaDown(col(1)))];
r3 = [maxVelocity*cos(thetaAbove(row(1))+pi);maxVelocity*sin(thetaAbove(row(1))+pi)];
r4 = [maxVelocity*cos(thetaDown(col(1))+pi);maxVelocity*sin(thetaDown(col(1))+pi)];
r = [r1 r2 r3 r4 r1];
rm = (r1+r2)/2.0;
betaMin = atan2(rm(2),rm(1));



 surf(thetaAbove,thetaDown,timeVector);
 
 figure()
 numAngles = 100;
 angles = linspace(0,2*pi,numAngles);
 [x1,x2] = getPoints(initialState,finalState,maxVelocity);
 radius = maxVelocity;
 plot(radius*cos(angles),radius*sin(angles),'linewidth',2);
 grid on
 hold on
 plot([0 initialState(2)],[0 initialState(4)],'k','linewidth',2);
 plot([0 finalState(2)],[0 finalState(4)],'k','linewidth',2);
 plot([x1(1) x2(1)],[x1(2) x2(2)],'m','linewidth',2);
 plot(r(1,:),r(2,:),'r','linewidth',2);
 
 figure()
 rotation = [cos(betaMin) sin(betaMin);-sin(betaMin) cos(betaMin)];
 plot(radius*cos(angles),radius*sin(angles),'linewidth',2);
 grid on
 hold on
 rotatedInitial = rotation*initialState([2 4]);
 rotatedFinal = rotation* finalState([2 4]);
 plot([0 rotatedInitial(1)],[0 rotatedInitial(2)],'k','linewidth',2);
 plot([0 rotatedFinal(1)],[0 rotatedFinal(2)],'k','linewidth',2);
 rotatedRec = rotation*r;
 plot(rotatedRec(1,:),rotatedRec(2,:),'r','linewidth',2);
 
 %Numerical Optimization
 x0 = [atan2(initialState(4),initialState(2)); atan2(finalState(4),finalState(2))];
 f = @(x) getTimeFromFrame(initialState,finalState,x(1),x(2),maxVelocity, maxAcceleration, maxIterations);
 xOpt =fminsearch(f,x0);
 f(x0);
 

 
 