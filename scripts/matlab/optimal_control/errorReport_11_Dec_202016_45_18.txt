Report generated on: 11-Dec-2020 16:45:18

Error message:
The path function "@robotConstraint" requires 3 input(s), while the problem provides 2 input(s). Please check the function and problem consistency.

Error stack:
   - Stack error no. 1: in PathFunction.CheckConsistency at 0
   - Stack error no. 2: in Phase.CheckConsistency at 0
   - Stack error no. 3: in Problem.CheckConsistency at 0
   - Stack error no. 4: in Problem.Bake at 0
   - Stack error no. 5: in optimalControlSolution at 70

Falcon version information:
    - Version: n/a
    - Valid:   n/a

Release information:
    - Used Matlab Version:              9.3.0.713579 (R2017b)
    - Used Matlab Version Release Date: September 14, 2017

Installed Toolboxes:
    - "Aerospace Blockset" as Version "3.20"
    - "Aerospace Toolbox" as Version "2.20"
    - "Automated Driving System Toolbox" as Version "1.1"
    - "Communications System Toolbox" as Version "6.5"
    - "Computer Vision System Toolbox" as Version "8.0"
    - "Control System Toolbox" as Version "10.3"
    - "Curve Fitting Toolbox" as Version "3.5.6"
    - "DSP System Toolbox" as Version "9.5"
    - "Embedded Coder" as Version "6.13"
    - "Fixed-Point Designer" as Version "6.0"
    - "Fuzzy Logic Toolbox" as Version "2.3"
    - "GPU Coder" as Version "1.0"
    - "GUI Layout Toolbox" as Version "2.3.1"
    - "Global Optimization Toolbox" as Version "3.4.3"
    - "Image Acquisition Toolbox" as Version "5.3"
    - "Image Processing Toolbox" as Version "10.1"
    - "Instrument Control Toolbox" as Version "3.12"
    - "MATLAB" as Version "9.3"
    - "MATLAB Coder" as Version "3.4"
    - "Mapping Toolbox" as Version "4.5.1"
    - "Neural Network Toolbox" as Version "11.0"
    - "Optimization Toolbox" as Version "8.0"
    - "Parallel Computing Toolbox" as Version "6.11"
    - "RF Blockset" as Version "6.1"
    - "RF Toolbox" as Version "3.3"
    - "Robotics System Toolbox" as Version "1.5"
    - "Robust Control Toolbox" as Version "6.4"
    - "Signal Processing Toolbox" as Version "7.5"
    - "Simscape" as Version "4.3"
    - "Simscape Driveline" as Version "2.13"
    - "Simscape Electronics" as Version "2.12"
    - "Simscape Fluids" as Version "2.3"
    - "Simscape Multibody" as Version "5.1"
    - "Simscape Power Systems" as Version "6.8"
    - "Simulink" as Version "9.0"
    - "Simulink 3D Animation" as Version "7.8"
    - "Simulink Coder" as Version "8.13"
    - "Simulink Control Design" as Version "5.0"
    - "Simulink Design Optimization" as Version "3.3"
    - "Stateflow" as Version "9.0"
    - "Statistics and Machine Learning Toolbox" as Version "11.2"
    - "Symbolic Math Toolbox" as Version "8.0"
    - "System Identification Toolbox" as Version "9.7"
    - "Text Analytics Toolbox" as Version "1.0"
    - "Tracking and Sensor Fusion Toolbox" as Version "1.0"

Basic FALCON.m toolboxes installed and licensed:
   - MATLAB Coder..............Installed
   - MATLAB Coder..............Licensed
   - Symbolic Math Toolbox.....Installed
   - Symbolic Math Toolbox.....Licensed

Compiler:
    - Selected C Compiler:   "gcc"
    - Selected C++ Compiler: "g++"

