function [statesDot,yOut] = sourceRobot(states,controls)

% Extract states
x = states(1);
vx = states(2);
y = states(3);
vy = states(4);

% Extract controls
ax = controls(1);
ay = controls(2);

%-------------------------------%
%   implement the model here    %
%-------------------------------%


% implement state derivatives here
xDot = vx;
vxDot = ax;
yDot = vy;
vyDot = ay;

statesDot = [xDot;vxDot;yDot;vyDot];
% Specify outputs
yOut = [ x; vx; y; vy];
%yOut = [];

end

