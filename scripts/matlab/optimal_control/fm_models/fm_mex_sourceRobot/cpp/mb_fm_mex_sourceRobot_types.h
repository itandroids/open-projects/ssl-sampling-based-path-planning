/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * mb_fm_mex_sourceRobot_types.h
 *
 * Code generation for function 'mb_fm_mex_sourceRobot'
 *
 */

#ifndef MB_FM_MEX_SOURCEROBOT_TYPES_H
#define MB_FM_MEX_SOURCEROBOT_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (mb_fm_mex_sourceRobot_types.h) */
