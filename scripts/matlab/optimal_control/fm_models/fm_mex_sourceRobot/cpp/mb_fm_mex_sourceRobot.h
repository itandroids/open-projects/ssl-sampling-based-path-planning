/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * mb_fm_mex_sourceRobot.h
 *
 * Code generation for function 'mb_fm_mex_sourceRobot'
 *
 */

#ifndef MB_FM_MEX_SOURCEROBOT_H
#define MB_FM_MEX_SOURCEROBOT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "mb_fm_mex_sourceRobot_types.h"

/* Function Declarations */
extern void mb_fm_mex_sourceRobot(double states[4], double controls[2], double
  statesdot[4], double outputs[4], double j_statesdot[24], double j_outputs[24]);
extern void mb_fm_mex_sourceRobot_initialize();
extern void mb_fm_mex_sourceRobot_terminate();

#endif

/* End of code generation (mb_fm_mex_sourceRobot.h) */
