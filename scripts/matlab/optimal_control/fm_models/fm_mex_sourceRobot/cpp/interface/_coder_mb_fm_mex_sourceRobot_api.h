/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * _coder_mb_fm_mex_sourceRobot_api.h
 *
 * Code generation for function '_coder_mb_fm_mex_sourceRobot_api'
 *
 */

#ifndef _CODER_MB_FM_MEX_SOURCEROBOT_API_H
#define _CODER_MB_FM_MEX_SOURCEROBOT_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_mb_fm_mex_sourceRobot_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void mb_fm_mex_sourceRobot(real_T states[4], real_T controls[2], real_T
  statesdot[4], real_T outputs[4], real_T j_statesdot[24], real_T j_outputs[24]);
extern void mb_fm_mex_sourceRobot_api(const mxArray *prhs[2], const mxArray
  *plhs[4]);
extern void mb_fm_mex_sourceRobot_atexit(void);
extern void mb_fm_mex_sourceRobot_initialize(void);
extern void mb_fm_mex_sourceRobot_terminate(void);
extern void mb_fm_mex_sourceRobot_xil_terminate(void);

#endif

/* End of code generation (_coder_mb_fm_mex_sourceRobot_api.h) */
