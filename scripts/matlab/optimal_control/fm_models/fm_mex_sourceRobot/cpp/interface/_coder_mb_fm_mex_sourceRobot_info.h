/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * _coder_mb_fm_mex_sourceRobot_info.h
 *
 * Code generation for function 'mb_fm_mex_sourceRobot'
 *
 */

#ifndef _CODER_MB_FM_MEX_SOURCEROBOT_INFO_H
#define _CODER_MB_FM_MEX_SOURCEROBOT_INFO_H
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo();
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties();

#endif
/* End of code generation (_coder_mb_fm_mex_sourceRobot_info.h) */
