/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * _coder_mb_fm_mex_sourceRobot_mex.cpp
 *
 * Code generation for function '_coder_mb_fm_mex_sourceRobot_mex'
 *
 */

/* Include files */
#include "_coder_mb_fm_mex_sourceRobot_api.h"
#include "_coder_mb_fm_mex_sourceRobot_mex.h"

/* Function Declarations */
static void c_mb_fm_mex_sourceRobot_mexFunc(int32_T nlhs, mxArray *plhs[4],
  int32_T nrhs, const mxArray *prhs[2]);

/* Function Definitions */
static void c_mb_fm_mex_sourceRobot_mexFunc(int32_T nlhs, mxArray *plhs[4],
  int32_T nrhs, const mxArray *prhs[2])
{
  const mxArray *inputs[2];
  const mxArray *outputs[4];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 2) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 2, 4,
                        21, "mb_fm_mex_sourceRobot");
  }

  if (nlhs > 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 21,
                        "mb_fm_mex_sourceRobot");
  }

  /* Temporary copy for mex inputs. */
  if (0 <= nrhs - 1) {
    memcpy((void *)&inputs[0], (void *)&prhs[0], (uint32_T)(nrhs * (int32_T)
            sizeof(const mxArray *)));
  }

  /* Call the function. */
  mb_fm_mex_sourceRobot_api(inputs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);

  /* Module termination. */
  mb_fm_mex_sourceRobot_terminate();
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  mexAtExit(mb_fm_mex_sourceRobot_atexit);

  /* Initialize the memory manager. */
  /* Module initialization. */
  mb_fm_mex_sourceRobot_initialize();

  /* Dispatch the entry-point. */
  c_mb_fm_mex_sourceRobot_mexFunc(nlhs, plhs, nrhs, prhs);
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_mb_fm_mex_sourceRobot_mex.cpp) */
