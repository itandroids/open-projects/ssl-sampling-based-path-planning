/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * main.cpp
 *
 * Code generation for function 'main'
 *
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include files */
#include "mb_fm_mex_sourceRobot.h"
#include "main.h"

/* Function Declarations */
static void argInit_2x1_real_T(double result[2]);
static void argInit_4x1_real_T(double result[4]);
static double argInit_real_T();
static void main_mb_fm_mex_sourceRobot();

/* Function Definitions */
static void argInit_2x1_real_T(double result[2])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 2; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

static void argInit_4x1_real_T(double result[4])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 4; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

static double argInit_real_T()
{
  return 0.0;
}

static void main_mb_fm_mex_sourceRobot()
{
  double dv0[4];
  double dv1[2];
  double statesdot[4];
  double outputs[4];
  double j_statesdot[24];
  double j_outputs[24];

  /* Initialize function 'mb_fm_mex_sourceRobot' input arguments. */
  /* Initialize function input argument 'states'. */
  /* Initialize function input argument 'controls'. */
  /* Call the entry-point 'mb_fm_mex_sourceRobot'. */
  argInit_4x1_real_T(dv0);
  argInit_2x1_real_T(dv1);
  mb_fm_mex_sourceRobot(dv0, dv1, statesdot, outputs, j_statesdot, j_outputs);
}

int main(int, const char * const [])
{
  /* Initialize the application.
     You do not need to do this more than one time. */
  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_mb_fm_mex_sourceRobot();

  /* Terminate the application.
     You do not need to do this more than one time. */
  mb_fm_mex_sourceRobot_terminate();
  return 0;
}

/* End of code generation (main.cpp) */
