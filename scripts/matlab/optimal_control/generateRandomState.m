function state = generateRandomState(maxPosition,maxVelocity)

state = zeros(4,1);

randAngle = 2*pi*rand();
state(1) = 2*maxPosition(1)*rand()-maxPosition(1);
velocity = maxVelocity*rand();
state(2) = velocity*cos(randAngle);
state(3) = 2*maxPosition(2)*rand()-maxPosition(2);
state(4) = velocity*sin(randAngle);

end

