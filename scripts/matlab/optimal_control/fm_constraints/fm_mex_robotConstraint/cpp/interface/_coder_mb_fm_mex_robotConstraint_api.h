/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * _coder_mb_fm_mex_robotConstraint_api.h
 *
 * Code generation for function '_coder_mb_fm_mex_robotConstraint_api'
 *
 */

#ifndef _CODER_MB_FM_MEX_ROBOTCONSTRAINT_API_H
#define _CODER_MB_FM_MEX_ROBOTCONSTRAINT_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_mb_fm_mex_robotConstraint_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void mb_fm_mex_robotConstraint(real_T outputs[4], real_T states[4],
  real_T controls[2], real_T constraintvalue[2], real_T j_constraintvalue[20]);
extern void mb_fm_mex_robotConstraint_api(const mxArray *prhs[3], const mxArray *
  plhs[2]);
extern void mb_fm_mex_robotConstraint_atexit(void);
extern void mb_fm_mex_robotConstraint_initialize(void);
extern void mb_fm_mex_robotConstraint_terminate(void);
extern void mb_fm_mex_robotConstraint_xil_terminate(void);

#endif

/* End of code generation (_coder_mb_fm_mex_robotConstraint_api.h) */
