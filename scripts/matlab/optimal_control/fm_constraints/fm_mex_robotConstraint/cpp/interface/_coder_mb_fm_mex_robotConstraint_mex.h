/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * _coder_mb_fm_mex_robotConstraint_mex.h
 *
 * Code generation for function '_coder_mb_fm_mex_robotConstraint_mex'
 *
 */

#ifndef _CODER_MB_FM_MEX_ROBOTCONSTRAINT_MEX_H
#define _CODER_MB_FM_MEX_ROBOTCONSTRAINT_MEX_H

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_mb_fm_mex_robotConstraint_api.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);
extern emlrtCTX mexFunctionCreateRootTLS(void);

#endif

/* End of code generation (_coder_mb_fm_mex_robotConstraint_mex.h) */
