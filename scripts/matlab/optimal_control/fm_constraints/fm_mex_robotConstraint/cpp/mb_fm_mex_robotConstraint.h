/*
 * Sponsored License - for use in support of a program or activity
 * sponsored by MathWorks.  Not for government, commercial or other
 * non-sponsored organizational use.
 *
 * mb_fm_mex_robotConstraint.h
 *
 * Code generation for function 'mb_fm_mex_robotConstraint'
 *
 */

#ifndef MB_FM_MEX_ROBOTCONSTRAINT_H
#define MB_FM_MEX_ROBOTCONSTRAINT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "mb_fm_mex_robotConstraint_types.h"

/* Function Declarations */
extern void mb_fm_mex_robotConstraint(const double outputs[4], double states[4],
  double controls[2], double constraintvalue[2], double j_constraintvalue[20]);
extern void mb_fm_mex_robotConstraint_initialize();
extern void mb_fm_mex_robotConstraint_terminate();

#endif

/* End of code generation (mb_fm_mex_robotConstraint.h) */
