function [ constraintvalue, j_constraintvalue ] = mb_fm_mex_robotConstraint( outputs, states, controls )
%mb_fm_mex_robotConstraint
% File automatically generated by FALCON.m

%=== Extract Data From Input ==============================================
xOut = outputs(1);
vxOut = outputs(2);
yOut = outputs(3);
vyOut = outputs(4);
x = states(1);
vX = states(2);
y = states(3);
vY = states(4);
aX = controls(1);
aY = controls(2);

%=== Jacobians and Hessians ===============================================
j_xOut = zeros(1,10);
j_xOut(:,1) = eye(1);
j_vxOut = zeros(1,10);
j_vxOut(:,2) = eye(1);
j_yOut = zeros(1,10);
j_yOut(:,3) = eye(1);
j_vyOut = zeros(1,10);
j_vyOut(:,4) = eye(1);
j_x = zeros(1,10);
j_x(:,5) = eye(1);
j_vX = zeros(1,10);
j_vX(:,6) = eye(1);
j_y = zeros(1,10);
j_y(:,7) = eye(1);
j_vY = zeros(1,10);
j_vY(:,8) = eye(1);
j_aX = zeros(1,10);
j_aX(:,9) = eye(1);
j_aY = zeros(1,10);
j_aY(:,10) = eye(1);

% Combine Variables to outputs
outputs = [xOut; vxOut; yOut; vyOut];
j_outputs = [j_xOut(1,:); j_vxOut(1,:); j_yOut(1,:); j_vyOut(1,:)];

% Combine Variables to states
states = [x; vX; y; vY];
j_states = [j_x(1,:); j_vX(1,:); j_y(1,:); j_vY(1,:)];

% Combine Variables to controls
controls = [aX; aY];
j_controls = [j_aX(1,:); j_aY(1,:)];

%=== Write Constants ======================================================

%=== Call sys_5c56430ae8152e60b8293b5405d768ac ============================
[constraintvaluetmp, j_constraintvaluetmp] = sys_5c56430ae8152e60b8293b5405d768ac(outputs, states, controls);

% Hessian Jacobian for sys_5c56430ae8152e60b8293b5405d768ac
tmp_j_input_sys_5c56430ae8152e60b8293b5405d768ac = [j_outputs; j_states; j_controls];

% Calculation of Jacobian with respect to function global input for sys_5c56430ae8152e60b8293b5405d768ac
j_constraintvaluetmp = j_constraintvaluetmp * tmp_j_input_sys_5c56430ae8152e60b8293b5405d768ac;

% Combine Variables to constraintvalue
constraintvalue = [constraintvaluetmp];
j_constraintvalue = [j_constraintvaluetmp(1,:); j_constraintvaluetmp(2,:)];

end
