clear all
clc

%% Defining max and min velocity, and acceleration
maxVelocity = 3.0;
maxAcceleration = 3.0;
maxXPosition = 4.5;
maxYPosition = 3.0;

addpath('../bvpSolver');

numberOfIterations = 2000;
totalTimeNM = zeros(numberOfIterations,1);
totalTimeNLP = zeros(numberOfIterations,1);
resultsDiff = zeros(numberOfIterations,1);

echo off;
for i =1:numberOfIterations

    %% Define States Controls and Parameter
    x_vec = [...
        falcon.State('x',       -maxXPosition,    maxXPosition, 0.001);...
        falcon.State('vX',       -maxVelocity,     maxVelocity, 0.001);...
        falcon.State('y',       -maxYPosition,    maxYPosition, 0.001);...
        falcon.State('vY', -maxVelocity,   maxVelocity, 0.001)];

    u_vec = [falcon.Control('aX'  ,-maxAcceleration,maxAcceleration, 0.001);...
        falcon.Control('aY',-maxAcceleration,maxAcceleration, 0.001)];

    tf = falcon.Parameter('FinalTime', 5, 0, 10, 0.001);

    %% Define Optimal Control Problem
    % Create new Problem Instance (Main Instance)
    problem = falcon.Problem('SmallSize');

    % Specify Discretization
    timesteps = 1001;
    tau = linspace(0,1,timesteps);

    % Add a new Phase
    phase = problem.addNewPhase(@sourceRobot, x_vec, tau, 0, tf);
    phase.addNewControlGrid(u_vec, tau);
    phase.Model.setModelOutputs([falcon.Output('xOut'); 
        falcon.Output('vxOut');
        falcon.Output('yOut');
        falcon.Output('vyOut');]);

    % Set Boundary Condition
    initialState = generateRandomState([maxXPosition; maxYPosition], maxVelocity);
    finalState = generateRandomState([maxXPosition; maxYPosition], maxVelocity);

    phase.setInitialBoundaries(initialState);
    phase.setFinalBoundaries(finalState);

    % Set initial guess
    [initGuess, totalTime] = generateInitialGuessNLP(initialState,finalState,timesteps,maxVelocity,maxAcceleration);

    phase.StateGrid.setValues(tau,initGuess);

    % Path constraint builder
    % % pconMdl = falcon.PathConstraintBuilder('CarPCon', [], x_vec(3),...
    % %     u_vec(2), [], @source_path_reduced);
    % % pconMdl.Build();

    % Path Constraint
    pathconstraints = [
        falcon.Constraint('speedub', -inf, 0);
        falcon.Constraint('accelerationub', -inf, 0)];
    phase.addNewPathConstraint(@robotConstraint, pathconstraints,tau);


    % Add Cost Function
    problem.addNewParameterCost(tf);

    % apply post-processing to each phase
    % problem.addPostProcessingStep(function_handle, state/control/output
    % objects, debug value object(s))
    %problem.addPostProcessingStep(@(x) x+1, {x_vec(3)}, falcon.Value('Vp1'));
    %problem.addPostProcessingStep(@postProcessFcn, {x_vec(4),x_vec(3)}, [falcon.Value('yDOT'),falcon.Value('V_square')]);

    % Prepare problem for solving
    problem.Bake();

    % Solve problem
    solver = falcon.solver.ipopt(problem);
    solver.Options.MajorIterLimit = 500;
    solver.Options.MajorFeasTol   = 1e-5;
    solver.Options.MajorOptTol    = 1e-5;

    [z_opt, F_opt, status, lambda, mu, zl, zu] = solver.Solve();
    diff = totalTime - phase.RealTime(end);
    totalTimeNM(i) = totalTime;
    totalTimeNLP(i) = totalTime;
    if(strcmp(status,'successful'))
        results(i)= diff;   
        results(i) = phase.RealTime(end);
    end
    
end
echo on;

