function [constraints] = robotConstraint(outputs,states,controls)

maxVelocity = 3.0;
maxAcceleration = 3.0;


upperBoundVelocity = states(2)^2 + states(4)^2 - maxVelocity*maxVelocity;
upperBoundAcceleration = controls(1)^2 + controls(2)^2-maxAcceleration*maxAcceleration;

constraints = [
    upperBoundVelocity;...
    upperBoundAcceleration];
end

