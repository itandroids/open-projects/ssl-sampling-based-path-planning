clear all
clc

%% Defining max and min velocity, and acceleration
maxVelocity = 3.0;
maxAcceleration = 3.0;
maxXPosition = 4.5;
maxYPosition = 3.0;

addpath('../bvpSolver');



%% Define States Controls and Parameter
x_vec = [...
    falcon.State('x',       -maxXPosition,    maxXPosition, 0.001);...
    falcon.State('vX',       -maxVelocity,     maxVelocity, 0.001);...
    falcon.State('y',       -maxYPosition,    maxYPosition, 0.001);...
    falcon.State('vY', -maxVelocity,   maxVelocity, 0.001)];

u_vec = [falcon.Control('aX'  ,-maxAcceleration,maxAcceleration, 0.001);...
    falcon.Control('aY',-maxAcceleration,maxAcceleration, 0.001)];

tf = falcon.Parameter('FinalTime', 5, 0, 10, 0.001);

%% Define Optimal Control Problem
% Create new Problem Instance (Main Instance)
problem = falcon.Problem('SmallSize');

% Specify Discretization
timesteps = 101;
tau = linspace(0,1,timesteps);

% Add a new Phase
phase = problem.addNewPhase(@sourceRobot, x_vec, tau, 0, tf);
phase.addNewControlGrid(u_vec, tau);
phase.Model.setModelOutputs([falcon.Output('xOut'); 
    falcon.Output('vxOut');
    falcon.Output('yOut');
    falcon.Output('vyOut');]);

% Set Boundary Condition
initialState = generateRandomState([maxXPosition; maxYPosition], maxVelocity);
finalState = generateRandomState([maxXPosition; maxYPosition], maxVelocity);

phase.setInitialBoundaries(initialState);
phase.setFinalBoundaries(finalState);

% Set initial guess
[initGuess, totalTime] = generateInitialGuessNLP(initialState,finalState,timesteps,maxVelocity,maxAcceleration);

phase.StateGrid.setValues(tau,initGuess);

% Path constraint builder
% % pconMdl = falcon.PathConstraintBuilder('CarPCon', [], x_vec(3),...
% %     u_vec(2), [], @source_path_reduced);
% % pconMdl.Build();

% Path Constraint
pathconstraints = [
    falcon.Constraint('speedub', -inf, 0);
    falcon.Constraint('accelerationub', -inf, 0)];
phase.addNewPathConstraint(@robotConstraint, pathconstraints,tau);


% Add Cost Function
problem.addNewParameterCost(tf);

% apply post-processing to each phase
% problem.addPostProcessingStep(function_handle, state/control/output
% objects, debug value object(s))
%problem.addPostProcessingStep(@(x) x+1, {x_vec(3)}, falcon.Value('Vp1'));
%problem.addPostProcessingStep(@postProcessFcn, {x_vec(4),x_vec(3)}, [falcon.Value('yDOT'),falcon.Value('V_square')]);

% Prepare problem for solving
problem.Bake();

% Solve problem
solver = falcon.solver.ipopt(problem);
solver.Options.MajorIterLimit = 100;
solver.Options.MajorFeasTol   = 1e-5;
solver.Options.MajorOptTol    = 1e-5;

solver.Solve();

diff = totalTime - phase.RealTime(end);


%% Plot
try
    figure
    subplot(2,4,[1,2,5,6]); grid on; hold on; xlabel('xpos'); ylabel('ypos'); title('Trajectory');
    plot(phase.StateGrid.Values(1,:), phase.StateGrid.Values(3,:), 'x-');
    
    subplot(2,4,3); grid on; hold on; xlabel('time'); ylabel('speedX');
    plot(phase.RealTime, phase.StateGrid.Values(2,:), 'x-');
    subplot(2,4,4); grid on; hold on; xlabel('time'); ylabel('speedY');
    plot(phase.RealTime, phase.StateGrid.Values(4,:), 'x-');
    
    subplot(2,4,7); grid on; hold on; xlabel('time'); ylabel('speed x dot cmd');
    plot(phase.RealTime, phase.ControlGrids.Values(1,:), 'x-');
    subplot(2,4,8); grid on; hold on; xlabel('time'); ylabel('speed y dot cmd');
    plot(phase.RealTime, phase.ControlGrids.Values(2,:), 'x-');
catch
    % plotting is optional and depending on Matlab version
end

%% Open the plot gui
try
    falcon.gui.plot.show(problem,'AskSaveOnClose',false);
catch
    fprintf('Please check your graphics driver and Java setting for the FALCON.m plot GUI to work.\n');
end

