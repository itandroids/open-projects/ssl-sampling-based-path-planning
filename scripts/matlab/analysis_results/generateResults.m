clear all
close all
clc

set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');

path = "/home/felipe/Desktop/ITA/Mestrado/Artigos/lars_pathplanners/ieeeconf/"

%BIT star
bestCostBIT = load('resultsBestCostBITstar.txt');
successProbBIT = load('resultsSuccessProbabilityBITstar.txt');
trackingBIT = load('resultsTrackingBITstar.txt');

%FMT star
bestCostFMT = load('resultsBestCostFMTstar.txt');
successProbFMT = load('resultsSuccessProbabilityFMTstar.txt');
trackingFMT = load('resultsTrackingFMT.txt');

%RRT star
bestCostRRT = load('resultsBestCostRRTstar.txt');
successProbRRT = load('resultsSuccessProbabilityRRTstar.txt');
trackingRRT = load('resultsTrackingRRTstar.txt');


%RRT
bestCostRRTstd = load('resultsBestCostRRT.txt');
successProbRRTstd = load('resultsSuccessProbabilityRRT.txt');
trackingRRTstd = load('resultsTrackingRRT.txt');

%PRM star
bestCostPRM = load('resultsBestCostPRMstar.txt');
successProbPRM = load('resultsSuccessProbabilityPRMstar.txt');
trackingPRM = load('resultsTrackingPRMstar.txt');

%ERRT
trackingERRT = load('resultsTrackingERRT.txt');


%Generate plots
f1 = figure
plot(bestCostBIT(:,3),bestCostBIT(:,2)*100,'ro','linewidth',1.5);
ylabel("Success probability (\%)");
xlabel("Samples per batch");
grid on


f2 = figure
plot(successProbBIT(:,1)*1000,successProbBIT(:,2)*100,'linewidth',1.5);
grid on
ylabel("Success probability (\%)");
xlabel("Planning time(ms)");

f3 = figure
plot(bestCostFMT(:,3),bestCostFMT(:,2)*100,'ro','linewidth',1.5);
ylabel("Success probability (\%)");
xlabel("Graph samples");
grid on


f4 = figure
plot(successProbFMT(:,1)*1000,successProbFMT(:,2)*100,'linewidth',1.5);
grid on
ylabel("Success probability (\%)");
xlabel("Planning time(ms)");

f5 = figure
plot(bestCostRRT(:,3),bestCostRRT(:,2)*100,'b','linewidth',1.5);
ylabel("Success probability (\%)",'interpreter','latex');
xlabel("Goal bias",'interpreter','latex');
grid on
ylim([0 100]) 
saveas(f5,path + "goalBias","epsc");


f6 = figure
plot(successProbRRT(:,1)*1000,successProbRRT(:,2)*100,'linewidth',1.5);
grid on
ylabel("Success probability (\%)");
xlabel("Planning time (ms)");


 %Evaluating confidence intervals
N = 10000;
confidence = 0.95;
ts = tinv([(1-confidence)/2  (1+confidence)/2],N-1);      % T-Score
lowerBIT = successProbBIT(:,4) + ts(1)*successProbBIT(:,5)/sqrt(N);
upperBIT = successProbBIT(:,4) + ts(2)*successProbBIT(:,5)/sqrt(N);
lowerFMT = successProbFMT(:,4) + ts(1)*successProbFMT(:,5)/sqrt(N);
upperFMT = successProbFMT(:,4) + ts(2)*successProbFMT(:,5)/sqrt(N);
lowerRRT = successProbRRT(:,4) + ts(1)*successProbRRT(:,5)/sqrt(N);
upperRRT = successProbRRT(:,4) + ts(2)*successProbRRT(:,5)/sqrt(N);
lowerPRM = successProbPRM(:,4) + ts(1)*successProbPRM(:,5)/sqrt(N);
upperPRM = successProbPRM(:,4) + ts(2)*successProbPRM(:,5)/sqrt(N);
lowerRRTstd= successProbRRTstd(:,4) + ts(1)*successProbRRTstd(:,5)/sqrt(N);
upperRRTstd = successProbRRTstd(:,4) + ts(2)*successProbRRTstd(:,5)/sqrt(N);

%Variance confidence interval
extremes = chi2inv([(1-confidence)/2  (1+confidence)/2],N-1);
lowerStdBIT = sqrt((N-1)*successProbBIT(:,5).^2/extremes(2));
upperStdBIT = sqrt((N-1)*successProbBIT(:,5).^2/extremes(1));
lowerStdFMT = sqrt((N-1)*successProbFMT(:,5).^2/extremes(2));
upperStdFMT = sqrt((N-1)*successProbFMT(:,5).^2/extremes(1));
lowerStdRRT = sqrt((N-1)*successProbRRT(:,5).^2/extremes(2));
upperStdRRT = sqrt((N-1)*successProbRRT(:,5).^2/extremes(1));
lowerStdPRM = sqrt((N-1)*successProbPRM(:,5).^2/extremes(2));
upperStdPRM = sqrt((N-1)*successProbPRM(:,5).^2/extremes(1));
lowerStdRRTstd = sqrt((N-1)*successProbRRTstd(:,5).^2/extremes(2));
upperStdRRTstd = sqrt((N-1)*successProbRRTstd(:,5).^2/extremes(1));



f7 = figure
hold on
%errorbar(successProbBIT(:,1)*1000,successProbBIT(:,4),successProbBIT(:,5),'linewidth',1.5);
plot(successProbBIT(:,1)*1000,successProbBIT(:,4), 'LineWidth', 1.5,'Color','b');
plot(successProbFMT(:,1)*1000,successProbFMT(:,4), 'LineWidth', 1.5,'Color','r');
plot(successProbRRT(:,1)*1000,successProbRRT(:,4), 'LineWidth', 1.5,'Color','g');
%plot(successProbPRM(:,1)*1000,successProbPRM(:,4),'k','linewidth',1.5);
plot(successProbRRTstd(:,1)*1000,successProbRRTstd(:,4),'m','linewidth',1.5);
plot(successProbBIT(:,1)*1000,lowerBIT,'--b' , 'LineWidth', 1.5);
plot(successProbBIT(:,1)*1000,upperBIT, '--b','LineWidth', 1.5);
plot(successProbRRT(:,1)*1000,lowerRRT, '--g','LineWidth', 1.5);
plot(successProbRRT(:,1)*1000,upperRRT, '--g','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,lowerFMT, '--r','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,upperFMT, '--r','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,lowerRRTstd, '--m','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,upperRRTstd, '--m','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,lowerPRM, '--k','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,upperPRM, '--k','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,lowerRRTstd, '--m','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,upperRRTstd, '--m','LineWidth', 1.5);

%xlim([0.1 1]) 
%ylim([6 8])

grid on
leg9 = legend("BIT*","FMT*","RRT*","RRT");
set(leg9,'Interpreter','latex','Location','best');
ylabel("Average path length (m)",'Interpreter','latex');
xlabel("Planning time (ms)",'Interpreter','latex');
saveas(f7,path + "avgPathLength","epsc")

f8 = figure 
hold on
plot(successProbBIT(:,1)*1000,successProbBIT(:,2)*100,'b','linewidth',1.5);
plot(successProbFMT(:,1)*1000,successProbFMT(:,2)*100,'r','linewidth',1.5);
plot(successProbRRT(:,1)*1000,successProbRRT(:,2)*100,'g','linewidth',1.5);
%plot(successProbPRM(:,1)*1000,successProbPRM(:,2)*100,'k','linewidth',1.5);
plot(successProbRRTstd(:,1)*1000,successProbRRTstd(:,2)*100,'m','linewidth',1.5);
grid on
leg8 = legend("BIT*","FMT*","RRT*","RRT");
set(leg8,'Interpreter','latex','Location','best');
ylabel("Success probability (\%)",'interpreter','latex');
xlabel("Planning time (ms)",'interpreter','latex');
saveas(f8,path + "successProb","epsc")


f9 = figure
hold on
plot(bestCostBIT(:,3),bestCostBIT(:,2)*100,'bo','linewidth',1.5);
plot(bestCostFMT(:,3),bestCostFMT(:,2)*100,'ro','linewidth',1.5);
leg9 = legend("BIT*","FMT*");
set(leg9,'Interpreter','latex');
xlabel("RGG samples",'interpreter','latex');
ylabel("Success probability (\%)",'interpreter','latex');
grid on
saveas(f9,path + "sampleTuning","epsc");


f10 = figure
hold on
plot(successProbBIT(:,1)*1000,successProbBIT(:,5), 'b','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,successProbFMT(:,5), 'r','LineWidth', 1.5);
plot(successProbRRT(:,1)*1000,successProbRRT(:,5), 'g','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,successProbPRM(:,5),'k','linewidth',1.5);
plot(successProbRRTstd(:,1)*1000,successProbRRTstd(:,5),'m','linewidth',1.5);
plot(successProbBIT(:,1)*1000,lowerStdBIT,'--b' , 'LineWidth', 1.5);
plot(successProbBIT(:,1)*1000,upperStdBIT, '--b','LineWidth', 1.5);
plot(successProbRRT(:,1)*1000,lowerStdRRT, '--g','LineWidth', 1.5);
plot(successProbRRT(:,1)*1000,upperStdRRT, '--g','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,lowerStdFMT, '--r','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,upperStdFMT, '--r','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,lowerStdRRTstd, '--m','LineWidth', 1.5);
plot(successProbFMT(:,1)*1000,upperStdRRTstd, '--m','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,lowerStdPRM, '--k','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,upperStdPRM, '--k','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,lowerStdRRTstd, '--m','LineWidth', 1.5);
%plot(successProbPRM(:,1)*1000,upperStdRRTstd, '--m','LineWidth', 1.5);
leg9 = legend("BIT*","FMT*","RRT*","RRT");
set(leg9,'Interpreter','latex','Location','best');
xlabel("Planning time (ms)",'interpreter','latex');
ylabel("Path length standard deviation (m)",'interpreter','latex');
grid on
saveas(f10,path + "variance","epsc");


f11 = figure
plot(successProbPRM(:,1)*1000,successProbPRM(:,2)*100,'linewidth',1.5);
grid on
ylabel("Success probability (\%)");
xlabel("Planning time (ms)");


f12 = figure
hold on
grid on
plot(trackingBIT(:,1), 'b','LineWidth', 1.5);
plot(trackingFMT(:,1),'r','LineWidth', 1.5);
plot(trackingRRT(:,1),'g','LineWidth', 1.5);
plot(trackingRRTstd(:,1),'m','linewidth',1.5);
plot(trackingERRT(:,1),'k','linewidth',1.5);
leg12 = legend("BIT*","FMT*","RRT*","RRT","ERRT");
xlim([1 1000])
ylabel("Average distance to goal (m)");
xlabel("Replanning iteration");
saveas(f12,path + "trackingComparison","epsc");


