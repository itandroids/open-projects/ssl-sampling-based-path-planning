/*
 * SplineInterpolator.cpp
 *
 *  Created on: Oct 16, 2015
 *      Author: root
 */

#include "SplineInterpolator.h"
#include <iostream>
namespace itandroids_lib {
namespace math {

SplineInterpolator::SplineInterpolator(
		std::vector<itandroids_lib::math::Vector2<double> > points) :
		points(points), phi(2, 0), diff(1, 0) {
	// TODO Auto-generated constructor stub
	std::vector<Vector2<double> >::iterator iter;
	std::vector<double> diffVector;
	std::vector<double> a;
	std::vector<double> b;
	std::vector<double> c;
	std::vector<double> d;
	for (iter = points.begin(); iter + 1 != points.end(); iter++) {
		diffVector.push_back((*(iter + 1)).x - iter->x);
		diff.push_back((*(iter + 1)).x - iter->x);
	}
	std::vector<double>::iterator auxIter;

	for (int i = 0; i < diffVector.size() - 1; i++) {
		a.push_back(diffVector[i]);
		b.push_back(2 * (diffVector[i] + (diffVector[i + 1])));
		c.push_back(diffVector[i + 1]);
		d.push_back(
				6 * (points[i + 2].y - points[i + 1].y) / diffVector[i + 1]
						- 6 * (points[i + 1].y - points[i].y) / diffVector[i]);
	}
	a[0] = 0;
	c[c.size() - 1] = 0;
	std::vector<double> auxVector = MathUtils::tridiagonalSolution(a, b, c, d);

	phi.insert(phi.begin() + 1, auxVector.begin(), auxVector.end());

}

SplineInterpolator::~SplineInterpolator() {
	// TODO Auto-generated destructor stub
}

double SplineInterpolator::interpolate(double position) {
	double returnValue;

	for (int i = 1; i < points.size(); i++) {
		if (points[i].x > position || i == points.size() - 1) {
			returnValue =
					((phi[i - 1] * pow((points[i].x - position), 3))
							/ (6 * diff[i]))
							+ (phi[i] * pow(position - points[i - 1].x, 3)
									/ (6 * diff[i]))
							+ (((points[i].y / diff[i]) - (diff[i] * phi[i] / 6))
									* (position - points[i - 1].x))
							+ (((points[i - 1].y / diff[i])
									- (diff[i] * phi[i - 1] / 6))
									* (points[i].x - position));
			break;
		}
	}
	return returnValue;

}

} /* namespace math */
} /* namespace itandroids_lib */
