//
// Created by mmaximo on 2/26/17.
//

#include "EigenUtils.h"

#include "MathUtils.h"

namespace itandroids_lib {
namespace math {

Eigen::MatrixXd EigenUtils::concatenateHorizontally(const Eigen::MatrixXd &left, const Eigen::MatrixXd &right) {
    assert(left.rows() == right.rows());

    Eigen::MatrixXd result(left.rows(), left.cols() + right.cols());
    result << left, right;

    return result;
}

Eigen::MatrixXd EigenUtils::concatenateVertically(const Eigen::MatrixXd &top, const Eigen::MatrixXd &bottom) {
    assert(top.cols() == bottom.cols());

    Eigen::MatrixXd result(top.rows() + bottom.rows(), top.cols());
    result << top, bottom;

    return result;
}

Eigen::MatrixXd EigenUtils::blockDiagonal(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2) {
    Eigen::MatrixXd result = Eigen::MatrixXd::Zero(m1.rows() + m2.rows(), m1.cols() + m2.cols());

    result.block(0, 0, m1.rows(), m1.cols()) = m1;
    result.block(m1.rows(), m1.cols(), m2.rows(), m2.cols()) = m2;

    return result;
}

Eigen::RowVectorXd EigenUtils::sequence(int start, int end) {
    assert(end >= start);

    Eigen::RowVectorXd result(end - start + 1);
    for (int i = 0; i < result.cols(); ++i)
        result(i) = start + i;

    return result;
}

Eigen::RowVectorXd EigenUtils::sequence(int start, int stride, int end) {
    int numElements = std::floor(std::abs(static_cast<double>(end - start + 1) / static_cast<double>(stride)));

    int accumulator = start;
    Eigen::RowVectorXd result(numElements);
    for (int i = 0; i < numElements; ++i, accumulator += stride)
        result(i) = accumulator;

    return result;
}

Eigen::RowVectorXd EigenUtils::sequence(double start, double stride, double end) {
    int numElements = std::floor(std::abs((end - start + 1) / stride));

    double accumulator = start;
    Eigen::RowVectorXd result(numElements);
    for (int i = 0; i < numElements; ++i, accumulator += stride)
        result(i) = accumulator;

    return result;
}

} // namespace math
} // namespace itandroids_lib
