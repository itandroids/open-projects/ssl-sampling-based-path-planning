//
// Created by mmaximo on 2/26/17.
//

#ifndef ITANDROIDS_LIB_EIGENUTILS_H
#define ITANDROIDS_LIB_EIGENUTILS_H

#include <Eigen/Core>

namespace itandroids_lib {
namespace math {

class EigenUtils {
public:
    static Eigen::MatrixXd concatenateHorizontally(const Eigen::MatrixXd &left, const Eigen::MatrixXd &right);

    static Eigen::MatrixXd concatenateVertically(const Eigen::MatrixXd &top, const Eigen::MatrixXd &bottom);

    static Eigen::MatrixXd blockDiagonal(const Eigen::MatrixXd &m1, const Eigen::MatrixXd &m2);

    static Eigen::RowVectorXd sequence(int start, int end);

    static Eigen::RowVectorXd sequence(int start, int stride, int end);

    static Eigen::RowVectorXd sequence(double start, double stride, double end);

};

} // namespace math
} // namespace itandroids_lib

#endif //ITANDROIDS_LIB_EIGENUTILS_H
