//
// Created by felipe on 20/09/20.
//

#ifndef SIMULATOR_MINIMUMTIMEOBJECTIVE_H
#define SIMULATOR_MINIMUMTIMEOBJECTIVE_H
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <controllers/tracking/MinimumTime2DController.h>


namespace objective {
    class MinimumTimeObjective : public ompl::base::StateCostIntegralObjective
    {
    public:
        MinimumTimeObjective(const ompl::base::SpaceInformationPtr& si, double epsilon);

        ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const;

        controller::MinimumTime2DController controller;

    private:
    public:
        void setMaxVelocity(double maxVelocity);

        void setMaxAcceleration(double maxAcceleration);

    private:
        double maxVelocity = 1000;
        double maxAcceleration = 1000;
    };
}



#endif //SIMULATOR_MINIMUMTIMEOBJECTIVE_H
