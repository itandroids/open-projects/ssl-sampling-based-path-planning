//
// Created by felipe on 28/04/20.
//

#include "DoubleIntegrator.h"
#include <math/MathUtils.h>
#include <math.h>


namespace robot_model {
    DoubleIntegrator::DoubleIntegrator(const Eigen::VectorXd& initialState, path_planning::PathPlannerBase* pathPlanner,
                                       const Eigen::VectorXd& accelerationConstraints, const Eigen::VectorXd& speedConstraints,
                                       controller::SpeedController& robotController, double sampleTime)
            :RobotModelBase(initialState, pathPlanner, {6,6}, {6,3}, sampleTime),
             accelerationConstraints(accelerationConstraints), speedConstraints(speedConstraints), rotationMatrix(2,2),
             robotController(robotController)
    {
        /// Defining state matrix
        A << 1.0,  0.0, 0.0, sampleTime, 0.0, 0.0,
             0.0, 1.0, 0.0, 0.0, sampleTime, 0.0,
             0.0, 0.0, 1.0, 0.0, 0.0, sampleTime,
             0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 1.0;

        /// Defining control matrix
        B << sampleTime*sampleTime/2.0, 0.0, 0.0,
             0.0, sampleTime*sampleTime/2.0, 0.0,
             0.0, 0.0, sampleTime*sampleTime/2.0,
             sampleTime, 0.0, 0.0,
             0.0, sampleTime, 0.0,
             0.0, 0.0, sampleTime;

        /// Initializing rotationMatrix with initial Yaw value
        fillRotationMatrix();
    }

    void DoubleIntegrator::fillRotationMatrix() {
        rotationMatrix << cos(robotState(2)), sin(robotState(2)),
                         -sin(robotState(2)),cos(robotState(2));
    }

    void DoubleIntegrator::positionUpdate(const Eigen::VectorXd& controlInput){
        Eigen::VectorXd effectiveSpeed = robotState.segment(3,3) + B.block(0,0,3,3)*controlInput/sampleTime;

        effectiveSpeed = cylinderSaturation(effectiveSpeed,speedConstraints);

        /// Position update
        robotState.segment(0,3) += effectiveSpeed*sampleTime;
    }

    double DoubleIntegrator::saturateValue(double value, double upperBound, double lowerBound) {
        if(value > upperBound)
            value = upperBound;
        else if(value < lowerBound)
            value = lowerBound;
        return value;
    }

    Eigen::VectorXd DoubleIntegrator::cylinderSaturation(Eigen::VectorXd vector, Eigen::VectorXd& constraint) {
        /// Saturating angular speed
        vector(2) = saturateValue(vector(2),constraint(2),-constraint(2));

        /// Rotation vector according to robot's frame
        Eigen::VectorXd rotatedVector = rotationMatrix*vector.segment(0,2);
        double norm2 = rotatedVector(0)*rotatedVector(0) + rotatedVector(1)*rotatedVector(1);
        double angle = atan2(rotatedVector(1),rotatedVector(0));
        double maxNorm2 = 1/(cos(angle)*cos(angle)/(constraint(0)*constraint(0)) + sin(angle)*sin(angle)/(constraint(1)*constraint(1)));


        if(norm2 > maxNorm2)
        {
            double maxNorm = sqrt(maxNorm2);
            rotatedVector(0) = maxNorm*cos(angle);
            rotatedVector(1) = maxNorm*sin(angle);
            vector.segment(0,2) = rotationMatrix.transpose()*rotatedVector;
        }

        return vector;
    }

    Eigen::VectorXd DoubleIntegrator::applyControlConstraints(Eigen::VectorXd controlInput)
    {
        /// Saturating control inputs conserving moving direction

        /// Saturating linear acceleration
        double linearAcceleration = sqrt(controlInput(0)*controlInput(0) + controlInput(1)*controlInput(1));
        double direction = atan2(controlInput(0), controlInput(1));
        double relativeAngle = robotState(2)-direction;
        double maxAcceleration =
                1/sqrt(cos(relativeAngle)*cos(relativeAngle)/(accelerationConstraints(0)*accelerationConstraints(0))
                       + sin(relativeAngle)*sin(relativeAngle)/(accelerationConstraints(1)*accelerationConstraints(1)));

        if(linearAcceleration > maxAcceleration)
        {
            controlInput(0) = maxAcceleration*cos(direction);
            controlInput(1) = maxAcceleration*sin(direction);
        }

        /// Saturating angular acceleration
        controlInput(2) = saturateValue(controlInput(2),accelerationConstraints(2),-accelerationConstraints(2));

        return controlInput;
    }

    Eigen::VectorXd DoubleIntegrator::applySpeedConstraints(Eigen::VectorXd speed) {

        /// Saturating according to saturation parallelepiped

        /// Saturation of angular velocity

        speed(2) = saturateValue(speed(2), speedConstraints(2), -speedConstraints(2));

        /// Saturation of linear speeds considering frame rotation
        speed.segment(0,2) = rotationMatrix*speed.segment(0,2);

        speed(0) = saturateValue(speed(0),speedConstraints(0), -speedConstraints(0));
        speed(1) = saturateValue(speed(1),speedConstraints(1), -speedConstraints(1));

        /// Inverse mapping
        speed.segment(0,2) = rotationMatrix.transpose()*speed.segment(0,2);


        return speed;

    }

    void DoubleIntegrator::executeControlLoop(Eigen::VectorXd controlInput)
    {
        controlInput = applyControlConstraints(controlInput);

        Eigen::VectorXd effectiveInput = B*controlInput;

        /// Position update with pre saturation
        positionUpdate(controlInput);

        /// Speed update

        robotState.segment(3,3) = A.block(3,3,3,3)*robotState.segment(3,3) + B.block(3,0,3,3)*controlInput;

        /// Wrapping angle to pi
        robotState(2) = itandroids_lib::math::MathUtils::wrapToPi(robotState(2));

        /// Rotation matrix update
        fillRotationMatrix();

        /// Speed saturation

        robotState.segment(3,3) = cylinderSaturation(robotState.segment(3,3), speedConstraints);
    }

    void DoubleIntegrator::track(const Eigen::VectorXd& reference) {
        robotController.setReference(reference);
        executeControlLoop(robotController.getControlInput(robotState));
    }

    Eigen::VectorXd DoubleIntegrator::getNextState() {
        throw;
    }

    const controller::SpeedController &DoubleIntegrator::getRobotController() const {
        return robotController;
    }

}

