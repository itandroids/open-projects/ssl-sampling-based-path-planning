//
// Created by felipe on 20/09/20.
//

#ifndef SIMULATOR_STATECOST_H
#define SIMULATOR_STATECOST_H
#include <ompl/base/objectives/StateCostIntegralObjective.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <controllers/tracking/MinimumTime2DController.h>


class StateCost {

};

class MinimumTimeObjective : public ompl::base::StateCostIntegralObjective
{
public:
    MinimumTimeObjective(const ompl::base::SpaceInformationPtr& si, double maxAcceleration, double epsilon) :
            ompl::base::StateCostIntegralObjective(si, true)
    {
    }

    ompl::base::Cost stateCost(const ompl::base::State* s) const
    {

        s->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        return ompl::base::Cost(1 / si_->getStateValidityChecker()->clearance(s));
    }

    ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const
    {
        double x0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vx0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double y0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vy0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];

        double xf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vxf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double yf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vyf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];

        return ompl::base::Cost();
    }
};
#endif //SIMULATOR_STATECOST_H
