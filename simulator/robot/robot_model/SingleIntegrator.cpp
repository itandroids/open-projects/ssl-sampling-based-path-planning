//
// Created by felipe on 15/07/20.
//

#include "SingleIntegrator.h"
#include <math/MathUtils.h>


namespace robot_model {
    SingleIntegrator::SingleIntegrator(const Eigen::VectorXd &initialState,
                                                    const Eigen::VectorXd &initialControllerState,
                                                    path_planning::PathPlannerBase *pathPlanner,
                                                    const Eigen::VectorXd &accelerationConstraints,
                                                    const Eigen::VectorXd &speedConstraints,
                                                    double sampleTime,
                                                    double wn,
                                                    double csi):RobotModelBase(initialState, pathPlanner, {6,6}, {3,3}, sampleTime),
                                                                speedController(initialControllerState,speedConstraints,
                                                                                accelerationConstraints, wn, csi, sampleTime), rotationMatrix(2,2)
    {
        A << 1, 0, 0, sampleTime, 0, 0,
             0, 1, 0, 0, sampleTime, 0,
             0, 0, 1, 0, 0, sampleTime,
             0, 0, 0, 1, 0, 0,
             0, 0, 0, 0, 1, 0,
             0, 0, 0, 0, 0, 1;

        B << 0, 0, 0,
             0, 0, 0,
             0, 0, 0;

        fillRotationMatrix();

    }

    void SingleIntegrator::track(const Eigen::VectorXd &reference) {
        speedController.simulate(reference,rotationMatrix);

        executeStateLoop();

        robotState(2) = itandroids_lib::math::MathUtils::wrapToPi(robotState(2));

        fillRotationMatrix();
    }

    void SingleIntegrator::executeStateLoop() {
        robotState.segment(3,3) = speedController.getState().segment(0,3);
        robotState = A*robotState;
    }

    void SingleIntegrator::fillRotationMatrix() {
        rotationMatrix << cos(robotState(2)), sin(robotState(2)),
                -sin(robotState(2)),cos(robotState(2));
    }
}