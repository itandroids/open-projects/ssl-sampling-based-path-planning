//
// Created by felipe on 20/09/20.
//

#include "MinimumTimeObjective.h"


namespace objective {

    MinimumTimeObjective::MinimumTimeObjective(const ompl::base::SpaceInformationPtr &si, double epsilon):ompl::base::StateCostIntegralObjective(si, true),controller(epsilon)
    {
    }


    ompl::base::Cost MinimumTimeObjective::motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const
    {
        double x0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vx0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double y0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vy0 = s1->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];

        double xf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vxf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double yf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vyf = s2->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];

        return ompl::base::Cost(controller.getArrivalTime(x0,xf,vx0,vxf,y0,yf,vy0,vyf,maxAcceleration,maxVelocity));
    }

    void MinimumTimeObjective::setMaxVelocity(double maxVelocity) {
        this->maxVelocity = maxVelocity;
    }

    void MinimumTimeObjective::setMaxAcceleration(double maxAcceleration) {
        this->maxAcceleration = maxAcceleration;
    }


};
