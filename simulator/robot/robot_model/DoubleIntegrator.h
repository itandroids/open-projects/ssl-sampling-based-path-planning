//
// Created by felipe on 28/04/20.
//

#ifndef SIMULATOR_DOUBLEINTEGRATOR_H
#define SIMULATOR_DOUBLEINTEGRATOR_H

#include <Eigen/Dense>
#include <controllers/speed_controller/SpeedController.h>
#include "RobotModelBase.h"

namespace robot_model {

    class DoubleIntegrator: public RobotModelBase {

        public:
        /**
         * Constructor for DoubleIntegrator class. State for double
         * integrator is defined as [x, y, yaw, \dot{x}, \dot{y} , \dot{yaw}]^T
         * @param initialState initial state
         * @param pathPlanner planner to be used
         * @param accelerationConstraints contraints for control inputs
         * @param speedConstraints constraints for speed states
         * @param controller to emulate robot's control loop
         * @param sampleTime system's sample time
         */
        DoubleIntegrator(const Eigen::VectorXd& initialState, path_planning::PathPlannerBase* pathPlanner
                , const Eigen::VectorXd& accelerationConstraints, const Eigen::VectorXd& speedConstraints,
                         controller::SpeedController& robotController, double sampleTime);

        ~DoubleIntegrator() = default;

        Eigen::VectorXd getNextState();

        /**
        * Start tracking to a reference state
         * @param reference state
        */
        void track(const Eigen::VectorXd& reference);

        /**
         * Gets controller class used
         * @return robot's controller
         */
        const controller::SpeedController &getRobotController() const;

        private:

        /**
         * Saturates given value to a interval [lowerBound, upperBound]
         * @param value to be saturated
         * @param upperBound supremum
         * @param lowerBound infimum
         * @return saturated value
         */
        static double saturateValue(double value,double upperBound, double lowerBound);

        /**
         * Calculates next state for robot given its control input
         * @param controlInput control input for system
         */
        void executeControlLoop(Eigen::VectorXd controlInput);

        Eigen::VectorXd cylinderSaturation(Eigen::VectorXd vector, Eigen::VectorXd& constraint);

        /**
         * Pre saturates effective speed to emulate state prediction and updates
         * robot's position.
         * @param controlInput system control input
         */
        void positionUpdate(const Eigen::VectorXd& controlInput);

        Eigen::VectorXd applyControlConstraints(Eigen::VectorXd controlInput);

        /**
         * Function to apply speed constraints
         * @param speed [speed_x speed_y omega]^T
         * @return saturated speed according to current rotation matrix frame
         */
        Eigen::VectorXd applySpeedConstraints(Eigen::VectorXd speed);

        void fillRotationMatrix();

        /**
         * Acceleration constraints. The constraints used reflect a cylinder shape saturation.
         * [ \alpha_max a_max]^T
         */
        Eigen::VectorXd accelerationConstraints;

        /**
         * Speed constraints. The constraints used reflect a rectangle shape saturation.
         * [ maxSpeed_x maxSpeed_y maxSpeed_z ]^T
         */
        Eigen::VectorXd speedConstraints;

        /**
         * Rotation matrix to move coordinate system to rotated frame according to robot's yaw.
         * [ cos(yaw) sin(yaw) ]
         * [ -sin(yaw) cos(yaw)]
         * The matrix belongs to SO^2, therefore its inverse coincides with its transpose.
         */
        Eigen::MatrixXd rotationMatrix;

        /// Controller to implement speed controller used.

        controller::SpeedController robotController;



    };
}




#endif //SIMULATOR_DOUBLEINTEGRATOR_H
