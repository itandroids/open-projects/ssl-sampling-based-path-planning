//
// Created by felipe on 15/07/20.
//

#ifndef SIMULATOR_SINGLEINTEGRATOR_H
#define SIMULATOR_SINGLEINTEGRATOR_H

#include "Eigen/Dense"
#include "path_planning/PathPlanning.h"
#include "SecondOrderSystem.h"
#include "RobotModelBase.h"

namespace robot_model {
    class SingleIntegrator : public RobotModelBase {
    public:
        /**
         * Constructor for SingleIntegrator class. State for double
         * integrator is defined as [x, y, yaw, \dot{x}, \dot{y} , \dot{yaw}]^T. This system uses a
         * Second order dynamics for speed state.
         * @param initialState initial state
         * @param initialControllerState initial second order system state
         * @param pathPlanner planner to be used
         * @param accelerationConstraints contraints for control inputs
         * @param speedConstraints constraints for speed states
         * @param sampleTime system's sample time
         * @param wn second order system natural frequency
         * @param csi second order system damping factor
         */
        SingleIntegrator(const Eigen::VectorXd& initialState, const Eigen::VectorXd& initialControllerState, path_planning::PathPlannerBase* pathPlanner
                , const Eigen::VectorXd& accelerationConstraints, const Eigen::VectorXd& speedConstraints,
                         double sampleTime, double wn, double csi);

        ~SingleIntegrator() = default;

        /**
        * Start tracking to a reference speed state
         * @param speed reference [v_x v_y \omega]^T
        */
        void track(const Eigen::VectorXd& reference);




    private:
        void executeStateLoop();

        /// Fills rotation matrix with the current yaw angle
        void fillRotationMatrix();

        system_type::SecondOrderSystem speedController;

        /// Change of basis matrix to robot's rotated frame

        Eigen::MatrixXd rotationMatrix;

    };
}



#endif //SIMULATOR_SINGLEINTEGRATOR_H
