//
// Created by felipe on 13/04/20.
//

#ifndef SIMULATOR_ROBOTMODELBASE_H
#define SIMULATOR_ROBOTMODELBASE_H


#include <Eigen/Dense>
#include "path_planning/PathPlanning.h"

namespace robot_model {

    class RobotModelBase {

    public:
        /**
         * Constructor for RobotModelBase base class
         * @param initialState Initial robot state
         * @param pathPlanner Path planner to be used
         * @param Adimensions vector with matrix A dimensions
         * @param Bdimensions vector with matrix B dimensions
         * @param sampleTime system's sample time
         */
        RobotModelBase(const Eigen::VectorXd& initialState, path_planning::PathPlannerBase* pathPlanner,
                       std::vector<int> Adimensions, std::vector<int> Bdimensions, double sampleTime);

        virtual ~RobotModelBase() = default;

        /**
         * Gets robot current state
         * @return Robot's state
         */
        virtual Eigen::VectorXd getRobotState() const;

        /**
         * Gets next robot state
         */
        virtual Eigen::VectorXd getNextState();


        /**
         * Tracks robot to a reference state
         * @return
         */
        virtual void track(const Eigen::VectorXd& reference) = 0;

        double getSampleTime() const;

        path_planning::PathPlannerBase &getPlanner();

    protected:
        double sampleTime;
        Eigen::VectorXd robotState;
        Eigen::MatrixXd A;
        Eigen::MatrixXd B;
        path_planning::PathPlannerBase* planner;

        /**
         * Executes robot control loop
         * @param controlInput
         */
        virtual void executeControlLoop(Eigen::VectorXd controlInput);

    };
}

#endif //SIMULATOR_ROBOTMODELBASE_H