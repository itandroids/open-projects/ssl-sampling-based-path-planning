//
// Created by felipe on 13/07/20.
//

#include "SecondOrderSystem.h"


namespace system_type {
    SecondOrderSystem::SecondOrderSystem(Eigen::VectorXd initialState, const Eigen::VectorXd &speedConstraints,
                                         const Eigen::VectorXd &accelerationConstraints, double wn, double csi, double sampleTime):
            k2(2*csi*wn), k1(wn/(2*csi)), speedConstraints(speedConstraints), accelerationConstraints(accelerationConstraints), state(initialState), sampleTime(sampleTime)
    {

    }

    void SecondOrderSystem::simulate(const Eigen::VectorXd &input, const Eigen::MatrixXd& rotationMatrix) {
        Eigen::VectorXd jerk = k2*(k1*(input-state.segment(0,3))-state.segment(3,3));
        Eigen::VectorXd effectiveAcceleration = state.segment(3,3) + sampleTime*jerk/2.0;

        /// Evaluating pre saturation
        preSaturation(effectiveAcceleration,rotationMatrix);

        /// Speed update
        state.segment(0,3) = state.segment(0,3) + sampleTime*effectiveAcceleration;

        /// Speed saturation
        state.segment(0,3) = cylinderSaturation(state.segment(0,3),speedConstraints,rotationMatrix);

        /// Acceleration update
        state.segment(3,3) = state.segment(3,3) + sampleTime*jerk;

        /// Acceleration saturation
        state.segment(3,3) = cylinderSaturation(state.segment(3,3),accelerationConstraints,rotationMatrix);
    }

    void SecondOrderSystem::preSaturation(Eigen::VectorXd &effectiveAcceleration, const Eigen::MatrixXd& rotationMatrix) {
        effectiveAcceleration = cylinderSaturation(effectiveAcceleration, accelerationConstraints, rotationMatrix);
    }

    double SecondOrderSystem::saturateValue(double value, double upperBound, double lowerBound) {
        if(value > upperBound)
            value = upperBound;
        else if(value < lowerBound)
            value = lowerBound;
        return value;
    }

    Eigen::VectorXd SecondOrderSystem::cylinderSaturation(Eigen::VectorXd vector, Eigen::VectorXd& constraint,const Eigen::MatrixXd& rotationMatrix) {
        /// Saturating angular speed
        vector(2) = saturateValue(vector(2),constraint(2),-constraint(2));

        /// Rotation vector according to robot's frame
        Eigen::VectorXd rotatedVector = rotationMatrix*vector.segment(0,2);
        double norm2 = rotatedVector(0)*rotatedVector(0) + rotatedVector(1)*rotatedVector(1);
        double angle = atan2(rotatedVector(1),rotatedVector(0));
        double maxNorm2 = 1/(cos(angle)*cos(angle)/(constraint(0)*constraint(0)) + sin(angle)*sin(angle)/(constraint(1)*constraint(1)));


        if(norm2 > maxNorm2)
        {
            double maxNorm = sqrt(maxNorm2);
            rotatedVector(0) = maxNorm*cos(angle);
            rotatedVector(1) = maxNorm*sin(angle);
            vector.segment(0,2) = rotationMatrix.transpose()*rotatedVector;
        }

        return vector;
    }

    Eigen::VectorXd SecondOrderSystem::getState(){
        return state;
    }
}