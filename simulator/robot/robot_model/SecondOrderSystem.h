//
// Created by felipe on 13/07/20.
//

#ifndef SIMULATOR_SECONDORDERSYSTEM_H
#define SIMULATOR_SECONDORDERSYSTEM_H

#include "Eigen/Dense"

namespace system_type {
    class SecondOrderSystem {
    public:
        /**
         * Constructs second order system class.
         * @param initialState initial robot state
         * @param speedConstraints speed constraints
         * @param accelerationConstraints acceleration constraints
         * @param wn second order system wn parameter
         * @param csi second order system csi parameter
         * @param sampleTime system's sample time
         */
        SecondOrderSystem(Eigen::VectorXd initialState,
                          const Eigen::VectorXd& speedConstraints, const Eigen::VectorXd& accelerationConstraints,
                          double wn, double csi, double sampleTime);

        /**
         * Emulates next step of second order state system
         * @param input reference speed input vector
         * @param rotationMatrix change of basis matrix to robot's rotated frame
         */
        void simulate(const Eigen::VectorXd& input, const Eigen::MatrixXd& rotationMatrix);


        /**
         * Pre saturates effective acceleration
         * @param effectiveAcceleration effective acceleration. At step k it is given by a_{eff}(k) = a(k) + j(k)*T/2
         * @param rotationMatrix change of basis matrix to robot's rotated frame
         */
        void preSaturation(Eigen::VectorXd& effectiveAcceleration, const Eigen::MatrixXd& rotationMatrix);


        Eigen::VectorXd getState();

    private:

        /**
         * Saturates given value to a interval [lowerBound, upperBound]
         * @param value to be saturated
         * @param upperBound supremum
         * @param lowerBound infimum
         * @return saturated value
         */
        double saturateValue(double value, double upperBound, double lowerBound);

        /**
         * Saturates vector values.
         * @param vector to be saturated.
         * @param constraint maximum allowed value for each component.
         * @param rotationMatrix to robot's frame
         * @return vector saturated.
         */
        Eigen::VectorXd cylinderSaturation(Eigen::VectorXd vector, Eigen::VectorXd& constraint, const Eigen::MatrixXd& rotationMatrix);

        Eigen::VectorXd state;
        Eigen::VectorXd speedConstraints;
        Eigen::VectorXd accelerationConstraints;
        double k1;
        double k2;
        double sampleTime;

    };
}



#endif //SIMULATOR_SECONDORDERSYSTEM_H
