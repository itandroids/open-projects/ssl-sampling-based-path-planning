//
// Created by felipe on 17/04/20.
//

#include <Eigen/Dense>
#include "RobotModelBase.h"

namespace robot_model
{
    RobotModelBase::RobotModelBase(const Eigen::VectorXd& initialState, path_planning::PathPlannerBase* pathPlanner,
                                   std::vector<int> Adimensions, std::vector<int> Bdimensions, double sampleTime):
            A(Adimensions[0],Adimensions[1]), B(Bdimensions[0], Bdimensions[1]), planner(pathPlanner), robotState(initialState),
            sampleTime(sampleTime)
    {
    }

    Eigen::VectorXd RobotModelBase::getRobotState() const
    {
        return robotState;
    }

    Eigen::VectorXd RobotModelBase::getNextState()
    {
        throw;
    }

    void RobotModelBase::executeControlLoop(Eigen::VectorXd controlInput)
    {
        robotState = A*robotState + B*controlInput;
    }

    double RobotModelBase::getSampleTime() const {
        return sampleTime;
    }

    path_planning::PathPlannerBase &RobotModelBase::getPlanner() {
        return *planner;
    }


}



