//
// Created by felipe on 29/04/20.
//

#include <ompl/base/Planner.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/SE2StateSpace.h"
#include <ompl/geometric/PathGeometric.h>
#include "constants/Constants.h"
#include "OMPLPlanning.h"
#include <list>


namespace path_planning {
    CollisionCheckerClass::CollisionCheckerClass(const ompl::base::SpaceInformationPtr &spaceInfo) :
            ompl::base::StateValidityChecker(spaceInfo), spaceInfo(spaceInfo)
    {
        if(spaceInfo->getStateSpace()->isCompound())
        {
            ompl::base::CompoundStateSpace* compoundSpace = spaceInfo->getStateSpace()->as<ompl::base::CompoundStateSpace>();
            int subspaceCount = compoundSpace->getSubspaceCount();

            for(int i=0;i<subspaceCount;i++)
            {
                if(compoundSpace->getSubspace(i)->getType() == ompl::base::STATE_SPACE_REAL_VECTOR)
                    realSpaceDimensions = compoundSpace->getSubspace(i)->getDimension();
            }
        }
        else realSpaceDimensions = spaceInfo->getStateDimension();
    }

    bool CollisionCheckerClass::belongsToFree(std::vector<double>& state ,double obstacleRadius) const {
        double sqrdistance, diff;
        double sqrObstacleRadius = obstacleRadius*obstacleRadius;
        for(std::vector<double> obstacle : obstacles)
        {
            sqrdistance = 0.0;
            diff = 0.0;
            for(int i=0; i< state.size(); i++)
            {
                diff = obstacle[i]-state[i];
                sqrdistance += diff*diff;
            }

            if(sqrdistance < sqrObstacleRadius)
                return false;
        }

        return true;
    }

    bool CollisionCheckerClass::isValid(const ompl::base::State *state) const {
        double* realPosition = state->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        std::vector<double> realValues(realPosition, realPosition+realSpaceDimensions);

        return belongsToFree(realValues, robot_constants::OBSTACLE_RADIUS_CONSERVATIVE);
    }

    void CollisionCheckerClass::setObstacles(const std::vector<std::vector<double>> &obstacles) {
        this->obstacles = obstacles;
    }

    bool CollisionCheckerClass::belongsToFree(std::vector<double>& state) const {
        return belongsToFree(state, robot_constants::OBSTACLE_RADIUS_CONSERVATIVE);
    }

    const std::vector<std::vector<double>> &CollisionCheckerClass::getObstacles() const {
        return obstacles;
    }

    OmplPlanning::OmplPlanning(const std::shared_ptr<ompl::base::Planner>& planner,
                               const std::shared_ptr<ompl::base::SpaceInformation>& spaceInformation) :
    PathPlannerBase(), planner(planner), spaceInformation(spaceInformation), startState(spaceInformation), goalState(spaceInformation),
    space(spaceInformation->getStateSpace())
    {
        //Evaluate number of state dimensions
        stateDimensions = spaceInformation->getStateDimension();

        //Create collision checking class
        auto collisionCheckerPtr(std::make_shared<CollisionCheckerClass>(spaceInformation));

        this->collisionCheckerPtr = collisionCheckerPtr;

        //Set collision checker to space information
        spaceInformation->setStateValidityChecker(collisionCheckerPtr);

        //Set state validity checking resolution in %
        spaceInformation->setStateValidityCheckingResolution(0.03);

        //Setup space information
        spaceInformation->setup();

        //Deactivate ompl log messages
        ompl::msg::noOutputHandler();
    }

    OmplPlanning::~OmplPlanning(){
        //Destructor
    }


    bool OmplPlanning::solveProblem(double planningTime, bool clearPlanner) {
        if(clearPlanner)
        {
            planner->clear();
        }
        defineProblem(); //Set problem definition
        bool solved = false;
        planner->setup();
        ompl::base::PlannerStatus result = planner->solve(planningTime);
        if(result == ompl::base::PlannerStatus::EXACT_SOLUTION)
            solved = true;
        if(result)
            cachePathSolution(result);
        return result;
    }

    bool OmplPlanning::solveProblem(double planningTime, std::mutex& mutex, bool clearPlanner){
        if(clearPlanner)
        {
            planner->clear();
        }
        defineProblem(); //Set problem definition
        bool result = planner->solve(planningTime);
        if(result)
        {
            std::lock_guard<std::mutex> guard(mutex);
            cachePathSolution(result);
        }
        return result;
    }

    bool OmplPlanning::getSolvingTime(double planningTime, tools::Clock& clock, double& time, bool clearPlanner) {
        if(clearPlanner)
        {
            planner->clear();
        }
        defineProblem(); //Set problem definition
        time = clock.getTime();
        bool solved = false;
        planner->setup();
        ompl::base::PlannerStatus result = planner->solve(planningTime);
        if(result == ompl::base::PlannerStatus::EXACT_SOLUTION)
            solved = true;
        time = clock.getTime()-time;
        cachePathSolution(result);
        return solved;
    }

    bool OmplPlanning::getSolvingTimeApproximate(double planningTime, tools::Clock &clock, double &time,
                                                 bool clearPlanner) {
        if(clearPlanner)
        {
            planner->clear();
        }
        defineProblem(); //Set problem definition
        time = clock.getTime();
        bool solved = false;
        planner->setup();
        ompl::base::PlannerStatus result = planner->solve(planningTime);
        if(result == ompl::base::PlannerStatus::EXACT_SOLUTION ||
                result==ompl::base::PlannerStatus::APPROXIMATE_SOLUTION ||
                result== ompl::base::PlannerStatus::TIMEOUT ||
                result== ompl::base::PlannerStatus::INVALID_START)
            solved = true;
        time = clock.getTime()-time;
        cachePathSolution(result);
        return solved;
    }

    std::vector<std::vector<double>> OmplPlanning::getWaypoint(int index)
    {
        ompl::base::State* state;

        if(index < pathSize)
            state = path->getState(index);
        else state = path->getState(pathSize-1);

        std::vector<std::vector<double>> waypoint (2,std::vector<double>());

        if(space->getType() == ompl::base::STATE_SPACE_SE2)
        {
            waypoint[WaypointCoordinates::REAL_COMPONENTS].push_back(state->as<ompl::base::SE2StateSpace::StateType>()->getX());
            waypoint[WaypointCoordinates::REAL_COMPONENTS].push_back(state->as<ompl::base::SE2StateSpace::StateType>()->getY());
            waypoint[WaypointCoordinates::SO2_COMPONENTS].push_back(state->as<ompl::base::SE2StateSpace::StateType>()->getYaw());
        }
        else {
            for(int i=0; i<space->getDimension(); i++)
            {
                waypoint[WaypointCoordinates::REAL_COMPONENTS].push_back(state->as<ompl::base::RealVectorStateSpace::StateType>()->values[i]);
            }

        }

        return waypoint;
    }

    void OmplPlanning::cachePathSolution(bool cache)
    {
        if(!cache)
            return;
        path = std::dynamic_pointer_cast<ompl::geometric::PathGeometric>(problem->getSolutionPath());
        pathSize = path->getStateCount();
        ompl::base::State* state;


        /// Caching path basing on which type of space
        pathWaypoints = std::queue<std::vector<double>>();

        /// For SE^2 State Space
        if(space->getType() == ompl::base::STATE_SPACE_SE2)
        {

            for(int i=0; i< pathSize; i++)
            {
                state = path->getState(i);
                std::vector<double> waypoint = {state->as<ompl::base::SE2StateSpace::StateType>()->getX(),
                                                state->as<ompl::base::SE2StateSpace::StateType>()->getY(),
                                                state->as<ompl::base::SE2StateSpace::StateType>()->getYaw()};
                pathWaypoints.push(waypoint);
            }

        }
            /// For R^2 state space.
        else {
            for(int i=0; i<pathSize; i++)
            {
                state = path->getState(i);
                std::vector<double> waypoint = {state->as<ompl::base::RealVectorStateSpace::StateType>()->values[0],
                                                state->as<ompl::base::RealVectorStateSpace::StateType>()->values[1],
                                                yawReference};
                pathWaypoints.push(waypoint);
            }

        }

    }

    void OmplPlanning::defineProblem() {
        problem = std::make_shared<ompl::base::ProblemDefinition>(spaceInformation);
        problem->setStartAndGoalStates(startState, goalState);
        planner->setProblemDefinition(problem);
    }

    void OmplPlanning::setObstaclePositions(std::vector<std::vector<double>> &obstacles) {
        collisionCheckerPtr->setObstacles(obstacles);
    }

    double OmplPlanning::distanceToWaypoint2(const Eigen::VectorXd &state) {
        if(pathWaypoints.empty())
            return 0;
        double deltaX = state(0)-pathWaypoints.front()[0];
        double deltaY = state(1)-pathWaypoints.front()[1];
        return deltaX*deltaX+deltaY*deltaY;
    }

    void OmplPlanning::setStartAndGoal(std::vector<double> start, std::vector<double> goal) {
        std::vector<double> stateVector = {start[0],start[1]};
        startState = stateVector;
        stateVector = {goal[0],goal[1]};
        goalState = stateVector;

        /// Update yaw reference
        if(goal.size() > 2)
            yawReference = goal[2];
    }

    double OmplPlanning::getPlanningTime() const {
        return planningTime;
    }

    const ompl::base::ScopedState<> &OmplPlanning::getGoalState() const {
        return goalState;
    }

    const ompl::base::ScopedState<> &OmplPlanning::getStartState() const {
        return startState;
    }

    const std::shared_ptr<ompl::base::SpaceInformation> &OmplPlanning::getSpace() const {
        return spaceInformation;
    }

    const std::shared_ptr<ompl::geometric::PathGeometric> &OmplPlanning::getPath() const {
        return path;
    }

    int OmplPlanning::getStateDimensions() const {
        return stateDimensions;
    }

    int OmplPlanning::getPathSize() const {
        return pathSize;
    }

    const std::shared_ptr<ompl::base::Planner> &OmplPlanning::getPlanner() const {
        return planner;
    }

    const std::shared_ptr<ompl::base::ProblemDefinition> &OmplPlanning::getProblem() const {
        return problem;
    }

    bool OmplPlanning::belongsToFree(std::vector<double>& state) const {
        collisionCheckerPtr->belongsToFree(state);
    }

    double OmplPlanning::getPathLength() const {
        return path->length();
    }

    void OmplPlanning::printPath(std::ostream &out) const {
        problem->getSolutionPath()->print(out);
    }

    void OmplPlanning::clearPathPlanner() {
        planner->clear();
    }


}