//
// Created by felipe on 29/04/20.
//

#ifndef SIMULATOR_OMPLPLANNING_H
#define SIMULATOR_OMPLPLANNING_H

#include <ompl/base/Planner.h>
#include <ompl/base/StateSampler.h>
#include <clock/Clock.h>
#include <mutex>
#include <queue>
#include "PathPlanning.h"

namespace path_planning {
    /**
     * StateValidityChecker class
     */
    class CollisionCheckerClass : public ompl::base::StateValidityChecker
    {
    public:
        CollisionCheckerClass(const ompl::base::SpaceInformationPtr &spaceInfo);

        ~CollisionCheckerClass() = default;

        virtual bool isValid(const ompl::base::State *state) const;

        /**
         * Checks if state belongs to free space
         * @param obstacleRadius
         * @return true or false
         */
        bool belongsToFree(std::vector<double>& state, double obstacleRadius) const;

        /**
         * Checks if state belongs to free space using a default radius
         * @param state
         * @return
         */
        bool belongsToFree(std::vector<double>& state) const;

        /**
         * Function to set circular obstacles' positions
         * @param obstacles
         */
        void setObstacles(const std::vector<std::vector<double>> &obstacles);

        /**
         * @return obstacles vector
         */
        const std::vector<std::vector<double>> &getObstacles() const;

    private:
        // State space dimension
        int realSpaceDimensions;

        //Space information wrapper
        ompl::base::SpaceInformationPtr spaceInfo;

        //Obstacle list
        std::vector<std::vector<double>> obstacles;
    };

/**
 * Class to encapsulate OMPL planners
 */
    class OmplPlanning : public PathPlannerBase {
    private:

        /**
         * Function to cache path solution
         */
        void cachePathSolution(bool);

        /**
         * Defines path planning problem to be solved
         */

        void defineProblem();
        /**
         * Defines maximum planning time
         */
        double planningTime;

        /**
        * Defines goal state
        */
        ompl::base::ScopedState<> goalState;

        /**
        * Defines start state
        */
        ompl::base::ScopedState<> startState;

        /**
        * State space information used for planning
        */
        std::shared_ptr<ompl::base::SpaceInformation> spaceInformation;

        /**
        * State space used for planning
        */
        std::shared_ptr<ompl::base::StateSpace> space;

        /**
        * Path solution obtained once the problem is solved
        */
        std::shared_ptr<ompl::geometric::PathGeometric> path;


        int stateDimensions;

        /**
         * Planner used to solve the problem
         */
        std::shared_ptr<ompl::base::Planner> planner;

        /**
         * Path planning problem definition
         */
        std::shared_ptr<ompl::base::ProblemDefinition> problem;

        /**
         * Collision checker class
         */
        std::shared_ptr<CollisionCheckerClass> collisionCheckerPtr;

        /**
         * Yaw reference for robot to follow at the end of path
         */
        double yawReference = 0.0;


    public:
        /**
         * Constructor for OmplPlanning class
         * @param planner OMPL path planner
         * @param spaceInformation Path planning navigation space
         */
        OmplPlanning(const std::shared_ptr<ompl::base::Planner>& planner,
                     const std::shared_ptr<ompl::base::SpaceInformation>& spaceInformation);

        ~OmplPlanning();

        /**
         * Function to solve path planning problem. The path will be cached in memory for future
         * queries to waypoints.
         * @param planningTime to set the maximum allowed planning time in seconds
         * @param clearPlanner clears the planner's datastructures if true
         * @return boolean telling whether the problem was solved or not
         */
        bool solveProblem(double planningTime, bool clearPlanner=true);

        /**
         * Function to solve path planning problem. The path will be cached in memory for future
         * queries to waypoints. This is a thread safe function to protect the path writing.
         * @param planningTime maximum allowed planning time in seconds
         * @param mutex
         * @return boolean telling whether the problem was solved or not
         */
        bool solveProblem(double planningTime, std::mutex& mutex, bool clearPlanner=true);

        bool getSolvingTime(double planningTime, tools::Clock& clock, double& time, bool clearPlanner=true);

        bool getSolvingTimeApproximate(double planningTime, tools::Clock& clock, double& time, bool clearPlanner=true);

        /**
         * Set start and goal
         * @param start
         * @param goal
         */
        void setStartAndGoal(std::vector<double> start, std::vector<double> goal);

        /**
         * Get waypoint W_{index}
         * @param index waypoint index
         * @return Waypoint matrix. First dimension is R^n components.
         * Second dimension is SO^2 components (SSL specific)
         */
        std::vector<std::vector<double>> getWaypoint(int index);

        /**
         * Function to set positions of obstacles for further plannings
         * @param obstacles
         */
        void setObstaclePositions(std::vector<std::vector<double>>& obstacles);

        /**
         * Check whether a given state belongs to free space
         * @param state 2D state in its dimensions
         * @return boolean validity check
         */
        bool belongsToFree(std::vector<double>& state) const;

        double getPathLength() const;

        double getPlanningTime() const;

        const ompl::base::ScopedState<> &getGoalState() const;

        const ompl::base::ScopedState<> &getStartState() const;

        const std::shared_ptr<ompl::base::SpaceInformation> &getSpace() const;

        const std::shared_ptr<ompl::geometric::PathGeometric> &getPath() const;

        int getStateDimensions() const;

        int getPathSize() const;

        const std::shared_ptr<ompl::base::Planner> &getPlanner() const;

        const std::shared_ptr<ompl::base::ProblemDefinition> &getProblem() const;

        double distanceToWaypoint2(const Eigen::VectorXd &state);

        void printPath(std::ostream &out) const;
        
        void clearPathPlanner();
    };
}




#endif //SIMULATOR_OMPLPLANNING_H
