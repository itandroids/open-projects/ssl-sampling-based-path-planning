//
// Created by felipe on 01/04/20.
//

#ifndef SIMULATOR_PATHPLANNING_H
#define SIMULATOR_PATHPLANNING_H



#include <iostream>
#include <vector>
#include <utility>
#include <list>
#include <clock/Clock.h>
#include <mutex>
#include <Eigen/Dense>
#include <queue>


namespace path_planning
{
    /**
    * Enum to define waypoint components
    */
    enum WaypointCoordinates {
        REAL_COMPONENTS = 0,

        SO2_COMPONENTS = 1,
    };


    /**
     * Base class to encapsulate path planners
     */
    class PathPlannerBase
    {

        public:
            /**
            * Constructor for class PathPlanner
            */
            PathPlannerBase();

            /**
             * Destructor for class PathPlanner
             */
            virtual ~PathPlannerBase() = default;

            /**
             * Function to solve path planning problem. The path will be cached in memory for future
             * queries to waypoints. This is a thread safe function to protect the path writing.
             * @param planningTime maximum allowed planning time in seconds
             * @param mutex
             * @param clear the planner's datastructures if true
             * @return boolean telling whether the problem was solved or not
             */
            virtual bool solveProblem(double planningTime, std::mutex& mutex, bool clearPlanner=true) = 0;

            /**
            * Function to solve the path planning problem given problem definition
            * @param planningTime to set the maximum allowed planning time in seconds
            * @param clear the planner's datastructures if true
            * @return boolean telling whether the problem was solved or not
            */
            virtual bool solveProblem(double planningTime, bool clearPlanner=true) = 0;

            /**
            * Function to solve the path planning problem given problem definition, returning the solving time
            * @param planningTime to set the maximum allowed planning time in seconds
            * @param clock to make time measurements
            * @param clearPlanner boolean parameter to clear planner datastructures
            * @return boolean telling whether the problem was solved or not
            */
            virtual bool getSolvingTime(double planningTime, tools::Clock& clock, double& time, bool clearPlanner=true);

            /**
            * Function to solve (complete or approximately) the path planning problem given problem definition, returning the solving time
            * @param planningTime to set the maximum allowed planning time in seconds
            * @param clock to make time measurements
            * @param clearPlanner boolean parameter to clear planner datastructures
            * @return boolean telling whether the problem was solved or not
            */
            virtual bool getSolvingTimeApproximate(double planningTime, tools::Clock& clock, double& time, bool clearPlanner=true);

            /**
             * Set start and goal for path planning problem
             * @param start coordinates
             * @param goal coordinates
             */
            virtual void setStartAndGoal(std::vector<double> start, std::vector<double> goal) = 0;

            /**
             * Set start and goal for path planning problem. Thread safe operation.
             * @param start coordinates
             * @param goal coordinates
             * @param mutex for multithreading applications
             */
            void setStartAndGoal(std::vector<double> start, std::vector<double> goal, std::mutex& mutex);

            /**
             * Set obstacle positions
             * @param obstacles
             * @return
             */
            virtual void setObstaclePositions(std::vector<std::vector<double>> &obstacles);

            /**
            * Get waypoint W_{index}
            * @param index waypoint index
            * @return Waypoint matrix. First dimension is R^n components.
            * Second dimension is SO^2 components (SSL specific)
            */
            virtual std::vector<std::vector<double>> getWaypoint(int index);

             /**
             * Get waypoint W_{index}
             * @param index waypoint index
             * @param mutex to thread safe read operations
             * @return Waypoint matrix. First dimension is R^n components.
             * Second dimension is SO^2 components (SSL specific)
             */
            std::vector<std::vector<double>> getWaypoint(int index, std::mutex& mutex);

            /**
             * Get tracking waypoint increasing tracking index
             * @param mutex to create thread safe read operations
             * @param currentState current robot state to evaluate current waypoint
             * @param diskRadius disk radius to perform waypoint clearance
             * @return waypoint to be followed.
             * Dimensions: [x y yaw]^T
             */
            std::vector<double> getTrackingWaypoint(std::mutex& mutex,
                                                                 const Eigen::VectorXd& currentState,
                                                                 double diskRadius);

             /**
             * Get tracking waypoint increasing tracking index
             * @param currentState current robot state to evaluate current waypoint
             * @param diskRadius disk radius to perform waypoint clearance
             * @return waypoint to be followed.
             * Dimensions: [x y yaw]^T
             */
             std::vector<double> getTrackingWaypoint(const Eigen::VectorXd& currentState,
                                                double diskRadius);

            /**
             * Get current distance to given waypoint.
             * @param state to evaluate distance squared.
             */
            virtual double distanceToWaypoint2(const Eigen::VectorXd& state);


            /**
             * Gets path size value
             * @param mutex used to be a thread safe operation
             * @return path size value
             */
            int getPathSize(std::mutex& mutex) const;

            /**
             * Check whether a given state belongs to free space
             * @param state 2D state in its dimensions
             * @return boolean validity check
             */
            virtual bool belongsToFree(std::vector<double>& state) const;

            /**
             * Returns best cost
             */
            virtual double bestCost();

            /**
             * Returns path size
             */
            virtual double getPathLength() const;

            /**
             * Prints path found
             * @param out output stream
             */
            virtual void printPath(std::ostream &out) const = 0;
            
            /**
             * Clears path planner data structures.
             */
            virtual void clearPathPlanner();



        protected:
            std::vector<double> start;
            std::vector<double> goal;

            /// Current path size obtained
            int pathSize;


            /**
             * Set of waypoints
             * state [x y yaw]^T
             */
            std::queue<std::vector<double>> pathWaypoints;
    };

}

#endif //SIMULATOR_PATHPLANNING_H