//
// Created by felipe on 29/04/20.
//

#include "PathPlanning.h"
#include <list>
#include <clock/Clock.h>


namespace path_planning{

    PathPlannerBase::PathPlannerBase() : pathSize(0)
    {

    }

    std::vector<std::vector<double>> PathPlannerBase::getWaypoint(int index) {
        throw;
    }

    std::vector<std::vector<double>> PathPlannerBase::getWaypoint(int index, std::mutex& mutex) {
        // Locking to protect read operation
        std::lock_guard<std::mutex> guard(mutex);

        return getWaypoint(index);
    }

    std::vector<double> PathPlannerBase::getTrackingWaypoint(const Eigen::VectorXd &currentState, double diskRadius) {
        std::vector<double> emptyVector;

        while(!pathWaypoints.empty()){
            // The last element should never be popped out
            if(pathWaypoints.size() > 1 && distanceToWaypoint2(currentState) < diskRadius*diskRadius)
                pathWaypoints.pop();
            else {
                return pathWaypoints.front();
            }
        }

        return emptyVector;
    }

    std::vector<double> PathPlannerBase::getTrackingWaypoint(std::mutex &mutex, const Eigen::VectorXd& currentState,
                                                             double diskRadius){
        // Locking to protect read operation
        std::lock_guard<std::mutex> guard(mutex);
        return getTrackingWaypoint(currentState,diskRadius);
    }

    void PathPlannerBase::setObstaclePositions(std::vector<std::vector<double>> &obstacles) {
        return;
    }

    bool PathPlannerBase::solveProblem(double planningTime, bool clearPlanner) {
        return true;
    }

    bool PathPlannerBase::solveProblem(double planningTime, std::mutex& mutex, bool clearPlanner) {
        return true;
    }

    bool PathPlannerBase::getSolvingTime(double planningTime, tools::Clock &clock, double &time, bool clearPlanner) {
        return true;
    }

    bool PathPlannerBase::getSolvingTimeApproximate(double planningTime, tools::Clock &clock, double &time,
                                                    bool clearPlanner) {
        return true;
    }

    int PathPlannerBase::getPathSize(std::mutex &mutex) const {
        std::lock_guard<std::mutex> guard(mutex);
        return pathSize;
    }

    double PathPlannerBase::distanceToWaypoint2(const Eigen::VectorXd &state) {
        return 0.0;
    }

    void PathPlannerBase::setStartAndGoal(std::vector<double> start, std::vector<double> goal,
                                          std::mutex &mutex) {
        std::lock_guard<std::mutex> guard(mutex);
        setStartAndGoal(start,goal);
    }

    bool PathPlannerBase::belongsToFree(std::vector<double>& state) const {
        /// Base class free space coincides with state space.
        return true;
    }

    double PathPlannerBase::bestCost() {
        return 0.0;
    }

    double PathPlannerBase::getPathLength() const {
        return 0.0;
    }
    
    void PathPlannerBase::clearPathPlanner() {
        return;
    }


}