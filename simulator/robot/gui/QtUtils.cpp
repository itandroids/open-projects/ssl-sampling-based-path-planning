//
// Created by mmaximo on 02/02/2020.
//

#include "QtUtils.h"

#include <QApplication>
#include <QStyleFactory>

namespace itandroids_lib {
namespace gui {

void QtUtils::setDarkTheme(QApplication &application) {
    application.setStyle(QtUtils::getFusionStyle());
    application.setPalette(QtUtils::getDarkPalette());
    application.setStyleSheet(QtUtils::getDarkStyleSheet());
}

QStyle* QtUtils::getFusionStyle() {
    return QStyleFactory::create("Fusion");
}

QPalette QtUtils::getDarkPalette() {
    QColor darkGray(53, 53, 53);
    QColor gray(128, 128, 128);
    QColor black(25, 25, 25);
    QColor blue(42, 130, 218);

    // To enable the dark theme. Taken from: https://gist.github.com/QuantumCD/6245215
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(53,53,53));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(25,25,25));
    darkPalette.setColor(QPalette::AlternateBase, QColor(53,53,53));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(53,53,53));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);

    // Workaround for disabled widgets (see post in the link above)
    darkPalette.setColor(QPalette::Active, QPalette::Button, gray.darker());
    darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::WindowText, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::Text, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::Light, darkGray);

    return darkPalette;
}

QString QtUtils::getDarkStyleSheet() {
    return QString("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
}

void QtUtils::drawPose(QPainter &painter, const itandroids_lib::math::Pose2D &pose, double size) {
    QPointF begin(pose.translation.x, pose.translation.y);
    double displacementX = size - ARROW_HEAD_PROPORTION * size * std::sqrt(3.0) / 2.0;
    double leftDisplacementY = ARROW_HEAD_PROPORTION * size / 2.0;
    double rightDisplacementY = -ARROW_HEAD_PROPORTION * size / 2.0;
    QPointF lineEnd(pose.translation.x + displacementX * cos(pose.rotation),
                    pose.translation.y + displacementX * sin(pose.rotation));
    QPointF arrowEnd(pose.translation.x + size * cos(pose.rotation),
                     pose.translation.y + size * sin(pose.rotation));
    QPointF left(pose.translation.x + displacementX * cos(pose.rotation) - leftDisplacementY * sin(pose.rotation),
                 pose.translation.y + displacementX * sin(pose.rotation) + leftDisplacementY * cos(pose.rotation));
    QPointF right(pose.translation.x + displacementX * cos(pose.rotation) - rightDisplacementY * sin(pose.rotation),
                  pose.translation.y + displacementX * sin(pose.rotation) + rightDisplacementY * cos(pose.rotation));
    QPolygonF arrowHead;
    arrowHead << arrowEnd << left << right << arrowEnd;
    painter.drawLine(begin, lineEnd);
    painter.drawPolygon(arrowHead);
}

double QtUtils::normalizeAngleQt(double angle) {
    while (angle < 0.0)
        angle += 2.0 * M_PI;
    while (angle > 2.0 * M_PI)
        angle -= 2.0 * M_PI;
}

QColor QtUtils::toQColor(const utils::Color &color) {
    return QColor(color.r, color.g, color.b);
}

}
}