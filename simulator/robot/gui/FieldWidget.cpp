//
// Created by mmaximo on 01/02/2020.
//

#include "FieldWidget.h"

#include <mutex>
#include <utility>
#include <world_model/Team.h>
#include <gui/draw_request/DrawSSLTeamRequest.h>

#include "QtUtils.h"
#include "draw_request/DrawRequest.h"


namespace itandroids_lib {
namespace gui {

using utils::Color;

FieldDrawingParams::FieldDrawingParams() : fieldColor(Color::DARKGREEN), fieldLineColor(Color::WHITE),
                                           boundaryLineColor(Color::BLACK), leftGoalColor(Color::WHITE),
                                           rightGoalColor(Color::WHITE) {
}

FieldDrawingParams FieldDrawingParams::getSLLParams() {
    using utils::Color;
    FieldDrawingParams params;
    params.fieldColor = Color::DARKGREEN;
    params.fieldLineColor = Color::WHITE;
    params.boundaryLineColor = Color::BLACK;
    params.leftGoalColor = Color::BLUE;
    params.rightGoalColor = Color::YELLOW;
    return params;
}

FieldGeometryParams FieldGeometryParams::getSSLParams() {
    FieldGeometryParams params;
    params.fieldLineWidth = 0.01; // from SSL 2019 rules
    params.goalLineWidth = 0.02; // from SSL 2019 rules
    params.boundaryLineWidth = 0.03; // not specified in SSL 2019 rules, chosen arbitrarily
    params.centerCircleRadius = 1.0 / 2.0; // from SSL 2019 rules
    return params;
}

void FieldWidget::paintEvent(QPaintEvent *event) {
    draw(painter);
}

void FieldWidget::wheelEvent(QWheelEvent *event) {
    // These numbers are arbitrary and were tuned by trial and error
    scale += event->delta() / 10.0;
    if (scale > 400.0)
        scale = 400.0;
    if (scale < 50.0)
        scale = 50.0;
    repaint();
}

void FieldWidget::mouseMoveEvent(QMouseEvent *event) {
    if (event->buttons() & Qt::RightButton) {
        QPoint cursorPosition = this->mapFromGlobal(QCursor::pos());
        QPoint deltaPosition = cursorPosition - previousCursorPosition;
        screenCenter.x += deltaPosition.x() / scale;
        screenCenter.y -= deltaPosition.y() / scale;
        previousCursorPosition = cursorPosition;
    }
    repaint();
}

void FieldWidget::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::MiddleButton) {
        fitToScreen();
    }
    if (event->button() == Qt::RightButton) {
        previousCursorPosition = this->mapFromGlobal(QCursor::pos());
        this->setCursor(Qt::ClosedHandCursor);
    }
    repaint();
}

void FieldWidget::mouseReleaseEvent(QMouseEvent *event) {
    this->setCursor(Qt::ArrowCursor);
}

const char *FieldWidget::DEFAULT_FONT = "Times";

FieldWidget::FieldWidget(const FieldDrawingParams &drawingParams, const FieldGeometryParams &geometryParams,
                         const std::vector<geometry::Line2D> &fieldLines,
                         const std::vector<geometry::Line2D> &leftGoalLines,
                         const std::vector<geometry::Line2D> &rightGoalLines,
                         const std::vector<geometry::Line2D> &boundaryLines) : drawingParams(drawingParams),
                                                                               geometryParams(geometryParams),
                                                                               fieldLines(fieldLines),
                                                                               leftGoalLines(leftGoalLines),
                                                                               rightGoalLines(rightGoalLines),
                                                                               boundaryLines(boundaryLines),
                                                                               font(QString(DEFAULT_FONT), 12),
                                                                               preferredSize(
                                                                                       DEFAULT_PREFERRED_WIDTH,
                                                                                       DEFAULT_PREFERRED_HEIGHT),
                                                                               scale(1.0) {
    configurePensAndBrushes();
    fitToScreen();
    setMouseTracking(true);
}

void FieldWidget::startAnimation(double sampleTime,
                                 const std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>>& requestsSequence) {
    timestep = 0;
    this->requestsSequence = requestsSequence;
    connect(&timer, &QTimer::timeout, this, &FieldWidget::updateField);
    timer.start(static_cast<int>(1000*sampleTime));
}


void FieldWidget::updateField(){
    if(timestep < requestsSequence.size())
    {
        updateDrawRequests(requestsSequence[timestep]);
        this->repaint();
        timestep++;
    }
}

void FieldWidget::setSizeHint(int preferredWidth, int preferredHeight) {
    this->preferredSize = QSize(preferredWidth, preferredHeight);
}

QSize FieldWidget::sizeHint() const {
    return preferredSize;
}

void FieldWidget::configurePensAndBrushes() {
    textPen = QPen(QtUtils::DEFAULT_PEN_COLOR, QtUtils::DEFAULT_PEN_SIZE);
    fieldLinePen = QPen(QtUtils::toQColor(drawingParams.fieldLineColor), geometryParams.fieldLineWidth);
    leftGoalLinePen = QPen(QtUtils::toQColor(drawingParams.leftGoalColor), geometryParams.goalLineWidth);
    rightGoalLinePen = QPen(QtUtils::toQColor(drawingParams.rightGoalColor), geometryParams.goalLineWidth);
    boundaryLinePen = QPen(QtUtils::toQColor(drawingParams.boundaryLineColor), geometryParams.boundaryLineWidth);
}

void FieldWidget::updateDrawRequests(const std::vector<std::shared_ptr<DrawRequest>> &requests) {
    std::lock_guard<std::mutex> guard(mutex);
    this->requests = requests;
    // We sort draw requests taking into account the draw priority.
    std::sort(this->requests.begin(), this->requests.end(),
              [](const std::shared_ptr<DrawRequest> &lhs, const std::shared_ptr<DrawRequest> &rhs) {
                  return lhs->getDrawPriority() < rhs->getDrawPriority();
              });
}

void FieldWidget::draw(QPainter &painter) {
    painter.begin(this);
    painter.fillRect(rect(), QtUtils::toQColor(drawingParams.fieldColor));
    painter.setTransform(QTransform::fromTranslate(rect().width() / 2.0, rect().height() / 2.0), false);
    painter.setTransform(QTransform::fromScale(scale, -scale), true);
    painter.setTransform(QTransform::fromTranslate(screenCenter.x, screenCenter.y), true);
    painter.setRenderHint(QPainter::Antialiasing); // anti-aliasing makes drawings much more smoother
    drawLines(painter);
    drawGoals(painter);
    drawBoundary(painter);
    mutex.lock();
    drawRequests(painter); // we only to protect access to the draw requests
    mutex.unlock();
    drawCursorPosition(painter);
    painter.end();
}

void FieldWidget::drawLines(QPainter &painter) const {
    painter.setPen(fieldLinePen);
    for (auto &line : fieldLines) {
        painter.drawLine(QPointF(line.start.x, line.start.y), QPointF(line.end.x, line.end.y));
    }
    // Drawing center circle
    painter.drawEllipse(QPointF(0.0, 0.0), geometryParams.centerCircleRadius, geometryParams.centerCircleRadius);
}

void FieldWidget::drawGoals(QPainter &painter) const {
    painter.setPen(leftGoalLinePen);
    for (auto &line : leftGoalLines) {
        painter.drawLine(QPointF(line.start.x, line.start.y), QPointF(line.end.x, line.end.y));
    }
    painter.setPen(rightGoalLinePen);
    for (auto &line : rightGoalLines) {
        painter.drawLine(QPointF(line.start.x, line.start.y), QPointF(line.end.x, line.end.y));
    }
}

void FieldWidget::drawBoundary(QPainter &painter) const {
    painter.setPen(boundaryLinePen);
    for (auto &line : boundaryLines) {
        painter.drawLine(QPointF(line.start.x, line.start.y), QPointF(line.end.x, line.end.y));
    }
}

void FieldWidget::drawRequests(QPainter &painter) const {
    for (auto &request : requests) {
        // By employing polymorphism, each request knows how to draw itself
        request->draw(painter);
    }
}

void FieldWidget::drawCursorPosition(QPainter &painter) const {
    QPoint cursorPosition = this->mapFromGlobal(QCursor::pos());
    double x = (cursorPosition.x() - rect().width() / 2.0) / scale - screenCenter.x;
    double y = (cursorPosition.y() - rect().height() / 2.0) / (-scale) - screenCenter.y;
    painter.setPen(textPen);
    painter.setFont(font);
    painter.resetTransform();
    painter.drawText(rect(), Qt::AlignBottom | Qt::AlignRight, QString().sprintf("(%.2f, %.2f)", x, y));
}

void FieldWidget::setDrawingParams(const FieldDrawingParams &drawingParams) {
    this->drawingParams = drawingParams;
    configurePensAndBrushes();
}

void FieldWidget::setGeometryParams(const FieldGeometryParams &geometryParams) {
    this->geometryParams = geometryParams;
    configurePensAndBrushes();
}

void
FieldWidget::setLines(const std::vector<geometry::Line2D> &fieldLines,
                      const std::vector<geometry::Line2D> &leftGoalLines,
                      const std::vector<geometry::Line2D> &rightGoalLines,
                      const std::vector<geometry::Line2D> &boundaryLines) {
    this->fieldLines = fieldLines;
    this->leftGoalLines = leftGoalLines;
    this->rightGoalLines = rightGoalLines;
    this->boundaryLines = boundaryLines;
}

void FieldWidget::fitToScreen() {
    double left = std::numeric_limits<double>::max();
    double top = -std::numeric_limits<double>::max();
    double right = -std::numeric_limits<double>::max();
    double bottom = std::numeric_limits<double>::max();
    for (auto &line : boundaryLines) {
        left = std::min(left, std::min(line.start.x, line.end.x));
        top = std::max(top, std::max(line.start.y, line.end.y));
        right = std::max(right, std::max(line.start.x, line.end.x));
        bottom = std::min(bottom, std::min(line.start.y, line.end.y));
    }
    QSize size = this->size();
    double xScale = size.width() / (right - left);
    double yScale = size.height() / (top - bottom);
    scale = std::min(xScale, yScale);
    screenCenter = math::Vector2d(0.0, 0.0);
}

    std::mutex& FieldWidget::getMutex() {
        return mutex;
    }

}
}