//
// Created by mmaximo on 01/02/2020.
//

#ifndef ITANDROIDS_LIB_FIELDWIDGET_H
#define ITANDROIDS_LIB_FIELDWIDGET_H

#include "utils/Colors.h"

#include <memory>
#include <mutex>

#include <QOpenGLWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>
#include <world_model/Team.h>

#include "math/Pose2D.h"
#include "geometry/Line2D.h"
#include "draw_request/DrawRequest.h"


namespace itandroids_lib {
namespace gui {

/**
 * Represents drawing parameters related to a soccer field.
 */
struct FieldDrawingParams {
    utils::Color fieldColor; /// the color used to draw the field background
    utils::Color fieldLineColor; /// the color used to draw lines
    utils::Color boundaryLineColor; /// the color used to draw a boundary around the field
    utils::Color leftGoalColor; /// the color used to draw the left goal
    utils::Color rightGoalColor; /// the color used to draw the right goal

    /**
     * Default constructor.
     */
    FieldDrawingParams();

    /**
     * Builds and returns an object with drawing parameters for the Small Size League (SSL).
     * These parameters were taken from the SSL 2019 rules and may need to be updated in the future.
     * @return drawing parameters for the SSL field.
     */
    static FieldDrawingParams getSLLParams();
};

/**
 * Represents geometry parameters related to a soccer field.
 */
struct FieldGeometryParams {
    double fieldLineWidth; /// field line width
    double goalLineWidth; /// goal line width
    double boundaryLineWidth; /// boundary line width
    double centerCircleRadius; /// the radius of the center circle

    /**
     * Builds and returns an object with geometry parameters for the Small Size League (SSL).
     * These parameters were taken from the SSL 2019 rules and may need to be updated in the future.
     * @return geometry parameters for the SSL field.
     */
    static FieldGeometryParams getSSLParams();
};

/**
 * Represents a widget which draws a soccer field.
 */
class FieldWidget : public QWidget {
Q_OBJECT

public:

    /**
     * Constructs a field widget. The field
     * @param drawingParams drawing parameters used to draw the soccer field.
     * @param geometryParams geometry parameters used to draw the soccer field.
     * @param fieldLines a vector containing all the field lines, except the lines used for drawing the goals.
     * @param leftGoalLines a vector containing all the left goal lines.
     * @param rightGoalLines a vector containing all the right goal lines.
     * @param boundaryLines a vector containing lines used for drawing a boundary around the field.
     */
    FieldWidget(const FieldDrawingParams &drawingParams, const FieldGeometryParams &geometryParams,
                const std::vector<geometry::Line2D> &fieldLines, const std::vector<geometry::Line2D> &leftGoalLines,
                const std::vector<geometry::Line2D> &rightGoalLines, const std::vector<geometry::Line2D> &boundaryLines);

    /**
     * Starts animation in Gui
     * @param sampleTime animation sample time between timesteps in seconds(s).
     * @param requestsSequence requests to be drawn on gui
     */
    void startAnimation(double sampleTime,
                        const std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>>& requestsSequence);

    /**
     *
     * @param preferredWidth
     * @param preferredHeight
     */
    void setSizeHint(int preferredWidth, int preferredHeight);

    QSize sizeHint() const override;

    /**
     * Updates the draw requests.
     * @param requests requests to be drawn on the field.
     */
    void updateDrawRequests(const std::vector<std::shared_ptr<DrawRequest>> &requests);

    /**
     * Draws the field widget.
     * @param painter painter used to draw.
     */
    void draw(QPainter &painter);

    /**
     * Draws the soccer goals.
     * @param painter painter used to draw.
     */
    void drawGoals(QPainter &painter) const;

    /**
     * Draws the field boundary.
     * @param painter painter used to draw.
     */
    void drawBoundary(QPainter &painter) const;

    /**
     * Draws the field lines.
     * @param painter painter used to draw.
     */
    void drawLines(QPainter &painter) const;

    /**
     * Draws all requests.
     * @param painter painter used to draw.
     */
    void drawRequests(QPainter &painter) const;

    /**
     * Draws the cursor position.
     * @param painter painter used to draw.
     */
    void drawCursorPosition(QPainter &painter) const;

    /**
     * Sets drawing parameters for the soccer field.
     * @param drawingParams new drawing parameters.
     */
    void setDrawingParams(const FieldDrawingParams &drawingParams);

    /**
     * Sets geometry parameters for the soccer field.
     * @param geometryParams new geometry parameters.
     */
    void setGeometryParams(const FieldGeometryParams &geometryParams);

    /**
     * Changes the lines used to draw the soccer field.
     * @param fieldLines field lines.
     * @param leftGoalLines left goal lines.
     * @param rightGoalLines right goal lines.
     * @param boundaryLines boundary lines.
     */
    void setLines(const std::vector<geometry::Line2D> &fieldLines, const std::vector<geometry::Line2D> &leftGoalLines,
                  const std::vector<geometry::Line2D> &rightGoalLines, const std::vector<geometry::Line2D> &boundaryLines);

    /**
     * Fits the widget to the screen, taking screen size into account.
     */
    void fitToScreen();

    // 1280x720 is 720p (HD resolution)
    static const int DEFAULT_PREFERRED_WIDTH = 1280; /// preferred widget width used in Qt's sizeHint()
    static const int DEFAULT_PREFERRED_HEIGHT = 720; /// preferred widget height used in Qt's sizeHint()
    static const char* DEFAULT_FONT; /// default font used for drawing text in this widget
    static constexpr double FONT_SCALE_FACTOR = 0.01; /// scale factor for text

protected:

    QTimer timer;

    /**
     * Update FieldWidget drawings according to received requests within a sample time.
     */
    void updateField();

    void paintEvent(QPaintEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;

    void mouseReleaseEvent(QMouseEvent *event) override;

    /**
     * Configures the Qt pens and brushes.
     */
    void configurePensAndBrushes();

    std::mutex& getMutex();

protected:
    std::mutex mutex; /// mutex used to avoid race conditions, since we except this widget to be used in a multithread context

    QPainter painter; /// Qt painter

    QFont font; /// font used for text

    QSize preferredSize; /// widget's preferred size

    /// Qt pens
    QPen textPen;
    QPen fieldLinePen;
    QPen leftGoalLinePen;
    QPen rightGoalLinePen;
    QPen boundaryLinePen;

    FieldDrawingParams drawingParams;
    FieldGeometryParams geometryParams;
    QPoint previousCursorPosition; /// previous cursor position used to compute screen moves
    math::Vector2d screenCenter;
    double scale; /// current scale (changed by the mouse wheel)
    /// Soccer field's lines
    std::vector<geometry::Line2D> fieldLines;
    std::vector<geometry::Line2D> leftGoalLines;
    std::vector<geometry::Line2D> rightGoalLines;
    std::vector<geometry::Line2D> boundaryLines;
    std::vector<std::shared_ptr<DrawRequest>> requests; /// draw requests
    std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>> requestsSequence; /// animation sequence
    int timestep; /// timestep for simulation
};

}
}

#endif //ITANDROIDS_LIB_FIELDWIDGET_H
