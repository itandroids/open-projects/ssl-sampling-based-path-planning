//
// Created by mmaximo on 02/02/2020.
//

#ifndef ITANDROIDS_LIB_QTUTILS_H
#define ITANDROIDS_LIB_QTUTILS_H

#include <QPainter>
#include <QColor>
#include "../math/Pose2D.h"
#include "../utils/Colors.h"
#include <QtWidgets/QStyle>

namespace itandroids_lib {
namespace gui {

/**
 * Contains many auxiliary methods for dealing with the Qt library.
 */
class QtUtils {
public:
    /**
     * Sets a dark theme to a given Qt application.
     * @param application The Qt application.
     */
    static void setDarkTheme(QApplication &application);

    /**
     * Gets the Fusion style for a Qt application.
     * @return An object for configuring the Fusion style.
     */
    static QStyle* getFusionStyle();

    /**
     * Gets a dark palette for a Qt application.
     * @return A dark palette for a Qt application.
     */
    static QPalette getDarkPalette();

    /**
     * Gets a dark style sheet for a Qt application.
     * @return A dark style sheet for a Qt application.
     */
    static QString getDarkStyleSheet();

    /**
     * Draw an arrow to represent a pose.
     * @param painter which painter will be used to draw.
     * @param pose pose to be drawn.
     * @param size total size of the arrow.
     */
    static void drawPose(QPainter &painter, const itandroids_lib::math::Pose2D &pose, double size);

    /**
     * Normalize angle using Qt conventions.
     * @param angle angle to be normalized.
     * @return normalized angle.
     */
    static double normalizeAngleQt(double angle);

    /**
     * Converts from utils::Color (ITAndroids' color) to QColor
     * @param color
     * @return
     */
    static QColor toQColor(const utils::Color &color);

    static constexpr Qt::GlobalColor DEFAULT_PEN_COLOR = Qt::black; /// default pen color used for drawings

    static constexpr double DEFAULT_PEN_SIZE = 0.0075; /// default pen size used for drawings

private:
    static constexpr double ARROW_HEAD_PROPORTION = 0.3; /// proportion of the head arrow in comparison to its total length
};

}
}

#endif //ITANDROIDS_LIB_QTUTILS_H
