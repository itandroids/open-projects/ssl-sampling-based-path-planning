//
// Created by mmaximo on 5/20/18.
//

#include "DrawPoseRequest.h"

#include "../QtUtils.h"

namespace itandroids_lib {
namespace gui {

DrawPoseRequest::DrawPoseRequest(const itandroids_lib::math::Pose2D &pose, Qt::GlobalColor color) : pose(pose), color(color) {
}

const itandroids_lib::math::Pose2D &DrawPoseRequest::getPose() const {
    return pose;
}

void DrawPoseRequest::draw(QPainter &painter) const {
    painter.setPen(QPen(color, POSE_WIDTH));
    painter.setBrush(QBrush(color));
    QtUtils::drawPose(painter, pose, POSE_SIZE);
}

} // namespace localization_simulator
} // namespace tools