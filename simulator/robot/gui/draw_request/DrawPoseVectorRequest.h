//
// Created by mmaximo on 5/20/18.
//

#ifndef ITANDROIDS_LIB_DRAWPOSEARRAYREQUEST_H
#define ITANDROIDS_LIB_DRAWPOSEARRAYREQUEST_H

#include "../../math/Pose2D.h"
#include <QPainter>
#include "DrawRequest.h"

namespace itandroids_lib {
namespace gui {

class DrawPoseVectorRequest : public DrawRequest {
public:
    DrawPoseVectorRequest(const std::vector<itandroids_lib::math::Pose2D> &poses, Qt::GlobalColor color);

    const std::vector<itandroids_lib::math::Pose2D> &getPoses();

    void draw(QPainter &painter) const override;

private:
    static constexpr double POSE_SIZE = 0.1;
    static constexpr double POSE_WIDTH = 0.02;

    std::vector<itandroids_lib::math::Pose2D> poses;
    Qt::GlobalColor color;
};

}
}

#endif //ITANDROIDS_LIB_DRAWPOSEARRAYREQUEST_H
