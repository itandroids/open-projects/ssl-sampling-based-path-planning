//
// Created by mmaximo on 03/02/2020.
//

#ifndef ITANDROIDS_LIB_DRAWSSLTEAMREQUEST_H
#define ITANDROIDS_LIB_DRAWSSLTEAMREQUEST_H

#include <vector>
#include <QPainter>

#include "../../math/Pose2D.h"
#include "DrawRequest.h"
#include <constants/Constants.h>
#include <world_model/Team.h>

namespace itandroids_lib {

namespace utils {
class Color;
}

namespace gui {

/**
 * Represents a request to draw a SSL team of robots.
 */
class DrawSSLTeamRequest : public DrawRequest {
public:
    /**
     * Constructs a request to draw a SSL team of robots.
     * @param players the players' poses.
     * @param ids the robots' ids.
     * @param teamColor the team color.
     * @param textColor color used for robot id text.
     */
    DrawSSLTeamRequest(const std::vector<math::Pose2D> &players, const std::vector<int> &ids,
            const utils::Color &teamColor, const utils::Color &textColor);

    /**
     * Constructs a request to draw a SSL team of robots given the team
     * @param team of class type Team
     */
    DrawSSLTeamRequest(world_model::Team team);

    void draw(QPainter &painter) const override;

private:
    const double PLAYER_RADIUS = 0.09; /// player radius im meters
    const double FRONT_LINE_DISTANCE = 0.074; /// distance from the player center to the front line (where the dribbler, kicker, and chipper are).
    const double PEN_SIZE = 0.01; /// Qt pen size
    QFont font; /// font used for the robot id
    QPen textPen; /// pen used for the robot id
    QPen playerPen; /// pen used to draw the player
    QBrush brush; /// brush used to draw the player
    std::vector<math::Pose2D> players; /// players' poses
    std::vector<int> ids; /// robots' ids
};

}
}

#endif //ITANDROIDS_LIB_DRAWSSLTEAMREQUEST_H
