//
// Created by mmaximo on 03/02/2020.
//

#include "gui/FieldWidget.h"
#include "DrawSSLTeamRequest.h"

#include "utils/Colors.h"
#include "gui/QtUtils.h"
#include "math/MathUtils.h"

namespace itandroids_lib {
namespace gui {

DrawSSLTeamRequest::DrawSSLTeamRequest(const std::vector<math::Pose2D> &players, const std::vector<int> &ids,
                                       const utils::Color &teamColor, const utils::Color &textColor) : DrawRequest(-1.0),
        players(players), ids(ids), textPen(QPen(QtUtils::toQColor(textColor), PEN_SIZE)),
        playerPen(QPen(QtUtils::DEFAULT_PEN_COLOR, PEN_SIZE)), brush(QBrush(QtUtils::toQColor(teamColor))),
        font(QFont("Times")) {
}

DrawSSLTeamRequest::DrawSSLTeamRequest(world_model::Team team):players(team.teammates), ids(team.getTeammatesIds()), textPen(QPen(QtUtils::toQColor(team.getTextColor()), PEN_SIZE)),
                                                               playerPen(QPen(QtUtils::DEFAULT_PEN_COLOR, PEN_SIZE)), brush(QBrush(QtUtils::toQColor(team.getTeamColor()))),
                                                               font(QFont("Times"))  {
}


void DrawSSLTeamRequest::draw(QPainter &painter) const {
    double angle = std::acos(FRONT_LINE_DISTANCE / PLAYER_RADIUS);
    painter.setFont(font);
    painter.setBrush(brush);
    for (int i = 0; i < players.size(); ++i) {
        auto &player = players[i];
        int id = ids[i];
        double left = player.translation.x - PLAYER_RADIUS;
        double top = player.translation.y - PLAYER_RADIUS;
        double width = 2.0 * PLAYER_RADIUS;
        double height = 2.0 * PLAYER_RADIUS;
        int startAngle = static_cast<int>(std::round(
                math::MathUtils::radiansToDegrees(QtUtils::normalizeAngleQt(-player.rotation - angle)) *
                16));
        int chordLength = -static_cast<int>(std::round(180 + 2.0 * math::MathUtils::radiansToDegrees(M_PI_2 - angle))) * 16;
        painter.setPen(playerPen);
        painter.drawChord(QRectF(left, top, width, height), startAngle, chordLength);
        auto transform = painter.transform();
        painter.scale(FieldWidget::FONT_SCALE_FACTOR, -FieldWidget::FONT_SCALE_FACTOR);
        painter.setPen(textPen);
        painter.drawText(QRectF(left / FieldWidget::FONT_SCALE_FACTOR, -top / FieldWidget::FONT_SCALE_FACTOR,
                width / FieldWidget::FONT_SCALE_FACTOR, -height / FieldWidget::FONT_SCALE_FACTOR),
                        Qt::AlignCenter, QString::number(id));
        painter.setTransform(transform, false);
    }
}

}
}