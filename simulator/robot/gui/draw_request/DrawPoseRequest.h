//
// Created by mmaximo on 5/20/18.
//

#ifndef ITANDROIDS_LIB_POSEDRAWINGREQUEST_H
#define ITANDROIDS_LIB_POSEDRAWINGREQUEST_H

#include "DrawRequest.h"

#include "../../math/Pose2D.h"

namespace itandroids_lib {
namespace gui {

class DrawPoseRequest : public DrawRequest {
public:
    DrawPoseRequest(const itandroids_lib::math::Pose2D &pose, Qt::GlobalColor color);

    const itandroids_lib::math::Pose2D &getPose() const;

    void draw(QPainter &painter) const override;

private:
    static constexpr double POSE_SIZE = 0.1;
    static constexpr double POSE_WIDTH = 0.02;

    itandroids_lib::math::Pose2D pose;
    Qt::GlobalColor color;
};

} // namespace localization_simulator
} // namespace tools

#endif //ITANDROIDS_LIB_POSEDRAWINGREQUEST_H
