//
// Created by mmaximo on 02/02/2020.
//

#ifndef ITANDROIDS_LIB_DRAWREQUEST_H
#define ITANDROIDS_LIB_DRAWREQUEST_H

#include <QPainter>

namespace itandroids_lib {
namespace gui {

/**
 * Represents an abstract draw request.
 */
class DrawRequest {
public:
    /**
     * Constructs an abstract draw request.
     * @param drawPriority priority used to draw this request (higher values mean higher priority).
     */
    explicit DrawRequest(double drawPriority = 0.0);

    /**
     * Draws this request.
     * @param painter painter used to draw.
     */
    virtual void draw(QPainter &painter) const = 0;

    /**
     * Obtaints the draw priority.
     * @return the draw priority.
     */
    double getDrawPriority();

private:
    double drawPriority;
};

}
}

#endif //ITANDROIDS_LIB_DRAWREQUEST_H
