//
// Created by mmaximo on 03/02/2020.
//

#include "DrawRequest.h"

namespace itandroids_lib {
namespace gui {

DrawRequest::DrawRequest(double drawPriority) : drawPriority(drawPriority) {

}

double DrawRequest::getDrawPriority() {
    return drawPriority;
}

}
}