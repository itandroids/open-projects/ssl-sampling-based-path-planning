//
// Created by mmaximo on 03/02/2020.
//

#ifndef ITANDROIDS_LIB_DRAWBALLSREQUEST_H
#define ITANDROIDS_LIB_DRAWBALLSREQUEST_H

#include <vector>

#include <QPainter>

#include "DrawRequest.h"
#include "../../math/Vector2.h"

namespace itandroids_lib {

namespace utils {
class Color;
}

namespace gui {

/**
 * Represents a request to draw balls.
 */
class DrawBallsRequest : public DrawRequest {
public:
    /**
     * Constructs a request to draw balls.
     * @param balls the balls' positions.
     * @param ballRadius the ball radius.
     * @param color the ball color.
     */
    DrawBallsRequest(const std::vector<math::Vector2d> &balls, double ballRadius, const utils::Color &color);

    void draw(QPainter &painter) const override;

private:
    QPen pen;
    QBrush brush;
    std::vector<math::Vector2d> balls;
    double ballRadius;
};

}
}

#endif //ITANDROIDS_LIB_DRAWBALLSREQUEST_H
