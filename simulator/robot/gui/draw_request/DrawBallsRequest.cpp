//
// Created by mmaximo on 03/02/2020.
//

#include "DrawBallsRequest.h"

#include "../QtUtils.h"

namespace itandroids_lib {
namespace gui {

DrawBallsRequest::DrawBallsRequest(const std::vector<math::Vector2d> &balls, double ballRadius,
                                   const utils::Color &color) : DrawRequest(-2.0), balls(balls), ballRadius(ballRadius),
                                                                pen(QtUtils::DEFAULT_PEN_COLOR,
                                                                    QtUtils::DEFAULT_PEN_SIZE),
                                                                brush(QtUtils::toQColor(
                                                                        color)) {
}

void DrawBallsRequest::draw(QPainter &painter) const {
    painter.setPen(pen);
    painter.setBrush(brush);
    for (auto &ball : balls) {
        painter.drawEllipse(QPointF(ball.x, ball.y), ballRadius, ballRadius);
    }
}

}
}