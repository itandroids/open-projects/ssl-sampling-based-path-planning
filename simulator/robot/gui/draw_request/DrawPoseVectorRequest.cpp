//
// Created by mmaximo on 5/20/18.
//

#include "DrawPoseVectorRequest.h"

#include "../QtUtils.h"

namespace itandroids_lib {
namespace gui {

DrawPoseVectorRequest::DrawPoseVectorRequest(const std::vector<itandroids_lib::math::Pose2D> &poses, Qt::GlobalColor color) {
    this->poses = poses;
    this->color = color;
}

const std::vector<itandroids_lib::math::Pose2D> &DrawPoseVectorRequest::getPoses() {
    return poses;
}

void DrawPoseVectorRequest::draw(QPainter &painter) const {
    painter.setPen(QPen(color, POSE_WIDTH));
    painter.setBrush(QBrush(color));
    for (const auto &pose : poses)
        QtUtils::drawPose(painter, pose, POSE_SIZE);
}

}
}