//
// Created by felipe on 14/05/20.
//

#ifndef SIMULATOR_CONSTANTS_H
#define SIMULATOR_CONSTANTS_H


#include <iostream>
#include <vector>
#include <math.h>

namespace robot_constants{
    const std::string OUTPUT_PATH = "/home/felipe/Desktop/ITA/Mestrado/mestrado/scripts/matlab/analysis_results/";
    constexpr double FIELD_SIZE_X = 9.000;
    constexpr double HALF_SIZE_X = FIELD_SIZE_X/2.0;
    constexpr double FIELD_SIZE_Y = 6.000;
    constexpr double HALF_SIZE_Y = FIELD_SIZE_Y/2.0;
    constexpr double STEP_SIZE = 0.1;
    constexpr double ROBOT_RADIUS = 0.09;
    constexpr double MAX_ALLOWED_X = HALF_SIZE_X-ROBOT_RADIUS;
    constexpr double MAX_ALLOWED_Y = HALF_SIZE_Y-ROBOT_RADIUS;
    constexpr double OBSTACLE_RADIUS = 2*ROBOT_RADIUS;
    constexpr double OBSTACLE_RADIUS_CONSERVATIVE = 2.5*ROBOT_RADIUS;
    constexpr double CAMERA_FRAME = 1.0/60.0;
    constexpr double NUM_ROBOTS = 6.0;
    constexpr double PLANNING_TIME = CAMERA_FRAME/NUM_ROBOTS;
    constexpr double COS45 = 0.70710678118;
    constexpr double START_BEGIN_X = -4.0;
    constexpr double START_BEGIN_Y = -2.5;
    constexpr double START_END_X = -3.5;
    constexpr double START_END_Y = 2.5;
    constexpr double GOAL_BEGIN_X = 3.5;
    constexpr double GOAL_BEGIN_Y = -2.5;
    constexpr double GOAL_END_X = 4.0;
    constexpr double GOAL_END_Y = 2.5;
    constexpr double GOAL_BEGIN_YAW = -M_PI;
    constexpr double GOAL_END_YAW = M_PI;
    constexpr double OBSTACLES_LIMIT_X = 1.0;
    constexpr double OBSTACLES_LIMIT_Y = 3.0;
    constexpr double ESCAPE_AREA_Y = 3.3 - ROBOT_RADIUS;
    constexpr double OBSTACLES_LIMIT_REAL_X = OBSTACLES_LIMIT_X - OBSTACLE_RADIUS;
    constexpr double OBSTACLES_LIMIT_REAL_Y = OBSTACLES_LIMIT_Y - OBSTACLE_RADIUS;
    constexpr double ROBOT_MAX_ACCELERATION = 2.7747; // m/s^2
    constexpr double ROBOT_MAX_VELOCITY = 3.8651; // m/s
    inline std::vector<double> OBSTACLE1 = {0.4,0.15};
    inline std::vector<double> OBSTACLE2 = {0.9,2.8};
    inline std::vector<double> OBSTACLE3 = {0.1,-0.5};
    inline std::vector<double> OBSTACLE4 = {1.2,0.8};
    inline std::vector<double> OBSTACLE5 = {0.8,-0.7};
    inline std::vector<double> OBSTACLE6 = {1.1,0.0};
    inline std::vector<double> OBSTACLE7 = {-0.3,2.2};
    inline std::vector<double> OBSTACLE8 = {-0.6,-1.3};
    inline std::vector<double> OBSTACLE9 = {1.3,2.1};
    inline std::vector<double> OBSTACLE10 = {-0.2,0.3};
    inline std::vector<double> OBSTACLE11 = {0.5,-1.8};
    inline std::vector<std::vector<double>> OBSTACLES = {OBSTACLE1,OBSTACLE2,OBSTACLE3,OBSTACLE4,OBSTACLE5,OBSTACLE6
            ,OBSTACLE7,OBSTACLE8,OBSTACLE9,OBSTACLE11};
}

#endif //SIMULATOR_CONSTANTS_H
