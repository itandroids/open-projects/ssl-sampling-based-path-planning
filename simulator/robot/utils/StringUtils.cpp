//
// Created by mmaximo on 04/04/2020.
//

#include "StringUtils.h"

#include <sstream>
#include <iostream>

namespace itandroids_lib {
namespace utils {

std::vector<std::string> StringUtils::split(const std::string &string, char delimiter) {
    static std::vector<std::string> tokens; // to avoid memory reallocation every time the method is called
    tokens.clear();
    std::stringstream ss(string);
    std::string token;
    while (std::getline(ss, token, delimiter)) {
        tokens.emplace_back(token);
    }
    return tokens;
}

void StringUtils::split(const std::string &string, char delimiter, std::vector<std::string> &tokens) {
    tokens.clear();
    std::stringstream ss(string);
    std::string token;
    while (std::getline(ss, token, delimiter)) {
        tokens.emplace_back(token);
    }
}

}
}