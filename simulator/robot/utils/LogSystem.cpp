//
// Created by muzio on 14/03/16.
//

#include "LogSystem.h"

//#include "Logger.h"
#include <boost/filesystem.hpp>

namespace itandroids_lib {
namespace utils {

    const std::string LogSystem::DEFAULT_LOGGING_BASE_DIR = "../logging";
    const std::string LogSystem::DEFAULT_LOGGING_EXTENSION = ".log";

    //private
    LogSystem::LogSystem() :
            baseDirPath(DEFAULT_LOGGING_BASE_DIR),
            extension(DEFAULT_LOGGING_EXTENSION) {
    }

    LogSystem::~LogSystem() {
        loggerMap.clear();
    }

    void LogSystem::configure(std::string basePath, std::string extension) {
        this->baseDirPath = basePath;
        this->extension = extension;
    }

    LogSystem &LogSystem::getInstance() {
        static LogSystem instance;

        return instance;
    }

    Logger &LogSystem::getLogger(std::string logName) {
        if (!baseDirCreated)
            createBaseDir();

        if (loggerMap.find(logName) == loggerMap.end()) {
            loggerMap[logName] = std::unique_ptr<Logger>(
                    new Logger(baseDirPath + "/" + logName + extension));
        }

        return *loggerMap[logName];
    }

    Logger &LogSystem::getLogger() {
        return getLogger("main");
    }

    void LogSystem::createBaseDir() {
        boost::filesystem::path dir(baseDirPath);
        if(boost::filesystem::create_directory(dir)) {
            std::cerr<< "Directory Created: "<<baseDirPath<<std::endl;
        }
    }


}
}