//
// Created by muzio on 14/03/16.
//

#ifndef ITANDROIDS_LIB_UTILS_LOGSYSTEM_H
#define ITANDROIDS_LIB_UTILS_LOGSYSTEM_H

#include <string>
#include <map>
#include <memory>
#include "Logger.h"

namespace itandroids_lib {
namespace utils {

class LogSystem {
public:
    static const std::string DEFAULT_LOGGING_BASE_DIR;
    static const std::string DEFAULT_LOGGING_EXTENSION;

    virtual ~LogSystem();

    /**
     * Function that configures base path and extension
     * for logging.
     *
     * @param path and extension
     */
    void configure(std::string basePath, std::string extension);
    Logger &getLogger(std::string logger);

    Logger &getLogger();

    //singleton
    static LogSystem &getInstance();

    LogSystem(const LogSystem&) = delete;


private:
    LogSystem();

    void createBaseDir();

    std::string extension;
    std::string baseDirPath;
    bool baseDirCreated = false;
    std::map<std::string, std::unique_ptr<Logger> > loggerMap;
};

}
}

#endif //ITANDROIDS_LIB_UTILS_LOGSYSTEM_H
