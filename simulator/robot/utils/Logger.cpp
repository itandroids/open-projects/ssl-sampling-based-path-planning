//
// Created by muzio on 12/03/16.
//

#include "Logger.h"

namespace itandroids_lib {
namespace utils {

    //private
    Logger::Logger(std::string filePath) :
        filePath(filePath){
        stream = new std::ofstream(filePath);
    }

    Logger::~Logger() {
        closeStream();
    }

    Logger &Logger::log(Level level) {
        this->debugLevel = level;
        std::string levelMessage("[");
        levelMessage.append(toString(debugLevel));
        levelMessage.append("] ");

        writeToStream(levelMessage);
        return *this;
    }

    Logger &Logger::log(std::string level) {
        std::string levelMessage = "[" + level + "] ";

        writeToStream(levelMessage);
        return *this;
    }

    void Logger::closeStream() {
        *stream << std::endl;
        stream->close();
    }

    void Logger::writeToStream(std::string message) {
        if (initialized)
            *stream << std::endl;
        initialized = true;

        *stream << message;
    }
}
}
