//
// Created by mmaximo on 04/04/2020.
//

#ifndef ITANDROIDS_LIB_STRINGUTILS_H
#define ITANDROIDS_LIB_STRINGUTILS_H

#include <vector>
#include <string>

namespace itandroids_lib {
namespace utils {

/**
 * Provides utilitary methods to deal with strings.
 */
class StringUtils {
public:
    /**
     * Splits a string into tokens.
     * For example, the string "home/mmaximo/itandroids" will be splitted into {"home", "mmaximo", "itandroids"}
     * if '/' is used as delimiter.
     * This method is more convenient to use, but a more efficient version is also provided where a reference to the
     * vector of tokens is passed as an argument.
     * @param string String to be splitted.
     * @param delimiter Delimiter used to split the string.
     * @return A vector containing the tokens.
     */
    static std::vector<std::string> split(const std::string &string, char delimiter = ' ');


    /**
     * Splits a string into tokens.
     * For example, the string "home/mmaximo/itandroids" will be splitted into {"home", "mmaximo", "itandroids"}
     * if '/' is used as delimiter.
     * This method is more efficient since it avoids memory reallocation, but a more convenient version is provided
     * where the vector is returned, so there is no need to pass it as an argument.
     * @param string String to be splitted (input).
     * @param delimiter Delimiter used to split the string (input).
     * @param tokens Vector containing the tokens (output).
     */
    static void split(const std::string &string, char delimiter, std::vector<std::string> &tokens);
};

}
}

#endif //ITANDROIDS_LIB_STRINGUTILS_H
