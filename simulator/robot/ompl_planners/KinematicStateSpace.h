//
// Created by felipe on 21/09/20.
//

#ifndef SIMULATOR_KINEMATICSTATESPACE_H
#define SIMULATOR_KINEMATICSTATESPACE_H

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <boost/math/constants/constants.hpp>
#include <controllers/tracking/MinimumTime2DController.h>
#include <ompl/base/StateValidityChecker.h>
#include <ompl/base/MotionValidator.h>
#include <ompl/datastructures/NearestNeighborsGNATNoThreadSafety.h>
#include <ompl/base/ValidStateSampler.h>
#include <geometry/KDTree.h>
#include <constants/Constants.h>
#include <bits/stdc++.h>
#include <iostream>
#include <fstream>

namespace dss
{

    class KinematicStateSampler : public ompl::base::ValidStateSampler
    {
    public:
        KinematicStateSampler(const ompl::base::SpaceInformation *si);

        // Generate a sample in the valid part of the R^3 state space
        // Valid states satisfy the following constraints:
        // -1<= x,y,z <=1
        // if .25 <= z <= .5, then |x|>.8 and |y|>.8
        bool sample(ompl::base::State *state);
        // We don't need this in the example below.
        bool sampleNear(ompl::base::State* /*state*/, const ompl::base::State* /*near*/, const double /*distance*/);
    protected:
        ompl::RNG rng_;

    private:
        double boundsX;
        double boundsY;
        double maxVelocity;
        double maxAcceleration;
    };

    class DynamicMotionValidityChecker : public ompl::base::MotionValidator {
    public:

        /**
         * Constructor for DynamicMotionValidityChecker class
         * @param si space information ptr
         * @param solver Boundary Value Problem solver
         */
        DynamicMotionValidityChecker(ompl::base::SpaceInformationPtr si, std::shared_ptr<controller::MinimumTime2DController> solver);

        /**
         * Constructor for DynamicMotionValidityChecker changing output file name.
         * @param si space information ptr
         * @param solver Boundary Value Problem solver
         * @param fileName output file name
         */
        DynamicMotionValidityChecker(ompl::base::SpaceInformationPtr si, std::shared_ptr<controller::MinimumTime2DController> solver, std::string fileName);

        /**
         * Destructor for DynamicMotionValidityChecker class
         */
        ~DynamicMotionValidityChecker();

        /**
         * Function to check motion between states 1 and 2
         * @param state1
         * @param state2
         * @return motion checker boolean
         */
        bool checkMotion(const ompl::base::State *state1,const ompl::base::State *state2) const;

        /**
         * @param s1 initial motion state
         * @param s2 final motion state
         * @param lastValid pair <last valid motion state, time parameterized in [0,1]>
         * @return motion checker boolean
         */
        bool checkMotion(const ompl::base::State *s1, const ompl::base::State *s2, std::pair<ompl::base::State *, double> &lastValid) const;

        /**
         * Function to evaluate narrow phase.
         * @param state1
         * @param state2
         * @return narrow phase result
         */
        bool evaluateNarrowPhase(const ompl::base::State *state1,const ompl::base::State *state2) const;

        /**
         * Broad phase motion validator
         * @param state1 initial motion state
         * @param state2 final motion state
         * @param beta rotation angle
         * @param theta angle to define velocity limits
         * @param maxAccelerationX max acceleration over axis x
         * @param maxAccelerationY max acceleration over axis y
         * @return motion validity boolean
         */
        bool broadPhaseMotionValidator(const ompl::base::State *state1, const ompl::base::State *state2,
                                       const double& beta, const double& theta, const double& maxAccelerationX,
                                       const double& maxAccelerationY) const;

        /**
         * Checks if rectangle is fully inside the field
         * @param minx minimum x coordinate
         * @param miny minimum y coordinate
         * @param maxx maximum x coordinate
         * @param maxy maximum y coordinate
         * @param beta frame rotation angle
         * @return rectangle validity boolean
         */
        bool checkBroadPhaseRectangle(const double& minx,const double& miny,
                                      const double& maxx,const double& maxy, const double& beta) const;

        /**
         * Narrow phase motion validador
         * @param state1 initial motion state
         * @param state2 final motion state
         * @param beta rotation angle
         * @param theta angle to define velocity limits
         * @param maxAccelerationX max acceleration over axis x
         * @param maxAccelerationY max acceleration over axis y
         * @param time total time to complete the BVP, i.e. BVP cost
         * @return motion validity boolean
         */
        bool narrowPhaseMotionValidator(const ompl::base::State *state1, const ompl::base::State *state2,
                                        const double& beta, const double& theta, const double& maxAccelerationX,
                                        const double& maxAccelerationY, const double& time) const;
        /**
         * API that emulates robot trajectory between initial and final states. This api calls the function
         * motionEmulation of this class, printing the resulting trajectory in a file.
         * @param state1 initial state to be emulated
         * @param state2 final state to be emulated
         * @param sampleTime emulation sample time
         * @return square error of final emulated arrival state and expected final state
         */
        double emulateTrajectory(const ompl::base::State *state1,const ompl::base::State *state2, const double& sampleTime);

        /**
         * API that evaluates time to complete trajectory from state1 to state2.
         * @param state1 initial state to be emulated
         * @param state2 final state to be emulated
         * @return trajectory time
         */
        double getTimeForTrajectory(const ompl::base::State *state1,const ompl::base::State *state2);

        /**
         * Emulates robot moving from initial state to goal state, printing it to a file
         * @param state1 initial state
         * @param state2 final state
         * @param beta
         * @param theta
         * @param maxAccelerationX max acceleration over axis X
         * @param maxAccelerationY max acceleration over axis Y
         * @param time time of arrival at final state
         * @param sampleTime emulation sample time
         * @return emulation completed
         */
        double motionEmulation(const ompl::base::State *state1, const ompl::base::State *state2,
                             const double& beta, const double& theta, const double& maxAccelerationX,
                             const double& maxAccelerationY, const double& time, const double& sampleTime);

        /**
         * Narrow phase motion validador
         * @param state1 initial motion state
         * @param state2 final motion state
         * @param beta rotation angle
         * @param theta angle to define velocity limits
         * @param maxAccelerationX max acceleration over axis x
         * @param maxAccelerationY max acceleration over axis y
         * @param sampleTime total time to complete the BVP, i.e. BVP cost
         * @param stater state result
         */
        void motionSteering (const ompl::base::State *state1, const ompl::base::State *state2,
                                                           const double& beta, const double& theta, const double& maxAccelerationX,
                                                           const double& maxAccelerationY, const double& sampleTime,const ompl::base::State *stater) const;

        /**
         * Steering function
         * @param state1
         * @param state2
         * @param emulationTime time to be emulated
         * @param stater state result
         */
        void steeringFunction(const ompl::base::State *state1, const ompl::base::State *state2, double emulationTime, const ompl::base::State *stater);

        /**
         * Sets maximum acceleration
         * @param maxAcceleration maximum acceleration allowed
         */
        void setMaxAcceleration(double maxAcceleration);

        /**
         * Sets maximum velocity
         * @param maxVelocity maximum velocity allowed
         */
        void setMaxVelocity(double maxVelocity);

        /**
         * Set obstacles. The vector obstacles contains a list o positions (x,y) of each obstacle.
         * E.g. {(0.0,1.0),(1.0,1.0),(1.4,2.0)}. Thus, we have obstacles on each (x,y) listed.
         * Obstacles must be in 4x1 representation. This function clears internal datastructures whenever
         * adds an obstacle.
         * @param obstacles.
         */
        void setObstacles(std::vector<std::vector<double>> &obstacles);

        /**
         * Adds obstacle to nearest neighbor datastructure
         * @param obstacle
         */
        void setObstacle(double* obstacle);

        /**
         * Checks whether line segment pq intersects with circle (center o and radius r).
         * @param px line segment origin x coordinate
         * @param py line segment origin y coordinate
         * @param qx line segment end x coordinate
         * @param qy line segment end y coordinate
         * @param ox circle center x coordinate
         * @param oy circle center y coordinate
         * @param r circle's radius
         * @return true if line is observable
         */
        static bool checkObsLine(const double& px,const double& py,
                                 const double& qx, const double& qy,
                                 const double& ox, const double& oy,
                                 const double& r);

        /**
         * Check if rectangle of sizes given by x0,y0,x1,y1 coordinates
         * @param x0
         * @param y0
         * @param x1
         * @param y1
         * @param ox circle's center x coordinate
         * @param oy circle's center y coordinate
         * @param r circle's radius
         * @return true if rectangle does not intersect the circle
         */
        static bool checkRectangle(const double& x0,
                                   const double& y0,
                                   const double& x1,
                                   const double& y1,
                                   const double& ox,
                                   const double& oy,
                                   const double& r);

        /**
         * Gets Bhaskara roots for ax^2+bx+c=0 equation
         * @param a
         * @param b
         * @param c
         * @return {root1,root2}
         */
        static std::vector<double> getBhaskaraRoots(const double& a, const double& b, const double& c);

        /**
         * Sets obstacle radius value in meters.
         * @param obstacleRadius obstacles' radius
         */
        void setObstacleRadius(double obstacleRadius);


        /**
         * Print obstacles to file
         */
        void printObstaclesToFile();

        /**
         * Print message to file
         * @param message to be printed
         */
        void printToFile(std::string message);

        /**
         * Set emulation sample time
         * @param emulationSampleTime
         */
        void setEmulationSampleTime(const double& emulationSampleTime);

        /**
         * @param gnatCollisionChecking parameter to set collision checking with gnat.
         */
        void setGnatCollisionChecking(bool gnatCollisionChecking);

        /**
         * Function to set field limits 2x1 vector [x_max ; y_max]^T
         * @param fieldLimits field limits
         */
        void setFieldLimits(const std::vector<double> &fieldLimits);


    private:

        /**
         * Configures internal datastructures.
         */
        void configure();

        inline bool checkLineAgainstObstacles(double* state1, double* state2) const;

        /**
         * Line circle collision checker
         */

        std::weak_ptr<ompl::base::SpaceInformation> spaceInfo;

        bool gnatCollisionChecking;

        bool kdTreeCollisionChecking;

        double maxAcceleration = 1000;

        double maxVelocity = 1000;

        std::vector<double> fieldLimits;

        /// Vector with [x,y] position of obstacles
        std::vector<std::vector<double>> obstacles;

        /// Nearest neighbor
        std::shared_ptr<ompl::NearestNeighborsGNATNoThreadSafety<double*>> nn;

        std::shared_ptr<geometry::KDTree> kd;

        double obstacleRadius = 0.0;


        /// Emulation sample time
        double emulationSampleTime = 0.001;

        std::shared_ptr<controller::MinimumTime2DController> bvpSolver;

        /// Output file
        std::ofstream outputFile;

        /**
         * Rotate cartesian coordinates to the coordinates in the specified frame.
         * @param beta rotation angle
         * @param state state to be rotated
         * @param rotatedState state rotated (is assigned during execution)
         */
        inline void rotateCoordinates(double beta, double* state,double* rotatedState) const;

        /**
         * Rotate cartesian coordinates to the coordinates in the specified frame with more accuracy.
         * @param beta rotation angle
         * @param state state to be rotated
         * @param rotatedState state rotated (is assigned during execution)
         */
        inline void rotateCoordinatesAccuracy(double beta, double* state,double* rotatedState) const;


        /**
         * Rotate cartesian coordinates to the coordinates in the specified frame.
         * @param beta rotation angle
         * @param state state to be rotated
         * @param rotatedState state rotated (is assigned during execution)
         */
        inline void rotateCoordinates(double beta,const std::vector<double>& state,std::vector<double>& rotatedState) const;

        /**
         * Rotate obstacle coordinates
         * @param beta rotation angle
         * @param obstacle vector with obstacle positions [x 0 y 0] ^T
         * @param rotated obstacle vector with rotated static obstacle positions [x 0 y 0]^T
         */
        inline void rotateObstacleCoordinates(double beta, const std::vector<double>& obstaclePosition,
                                       std::vector<double>& rotatedObstacle) const;

        /**
         * Distance metric to be used with nearest neighbor queries
         * @param s1 state 1
         * @param s2 state 2
         * @return distance between s1 and s2
         */
        static double distance(const double* s1,const double* s2);

        /**
         * Function to check the position limits of a given state over the field.
         * @param state to be checked
         * @return true if valid
         */
        inline bool checkLimits(double* state) const;


    };
    class DynamicStateValidityChecker : public ompl::base::StateValidityChecker
    {
    public:
        /**
         * Constructor for DynamicStateValidityChecker class
         * @param spaceInfo
         * @param realSpaceDimensions number of real space dimensions
         */
        DynamicStateValidityChecker(const ompl::base::SpaceInformationPtr &spaceInfo, int realSpaceDimensions);

        ~DynamicStateValidityChecker() = default;

        bool isValid(const ompl::base::State *state) const;

        /**
         * Checks if state belongs to free space
         * @param obstacleRadius
         * @return true or false
         */
        bool belongsToFree(double* state, double obstacleRadius) const;

        /**
         * Checks if state belongs to free space using a default radius
         * @param state
         * @return
         */
        bool belongsToFree(std::vector<double>& state) const;

        /**
         * Function to set circular obstacles' positions.
         * Obstacles must be set as a static state [x; 0; y; 0].
         * @param obstacle array
         */
        void setObstacle(double *obstacle);

        /**
         * Function to set circular obstacles' positions in the datastructure
         * Obstacles must be set as static state [x; 0; y; 0]^T.
         * @param obstacles matrix
         */
        void setObstacles(std::vector<std::vector<double>>& obstacles);

        /**
         * @return obstacles vector
         */
        const std::vector<std::vector<double>> &getObstacles() const;

        /**
         * Set obstacles to class
         * @param obstacleRadius
         */
        void setObstacleRadius(const double& obstacleRadius);

        /**
         * Set GNAT nearest neighbor collision checking
         * @param useGnatCollisionChecking
         */
        void setGnatCollisionChecking(bool useGnatCollisionChecking);


        /**
         * Set KD-Tree nearest neighbor collision checking
         * @param useKDTreeCollisionChecking
         */
        void setUseKDTreeCollisionChecking(bool useKDTreeCollisionChecking);

    private:
        /**
         * Configures internal datastructures.
         */
        void configure();

        /// Distance to obstacle
        double distance(const double* s1,const double* s2) const;

        /// Obstacle radius
        double obstacleRadius = 0.0;

        /// State space dimension
        int realSpaceDimensions;

        /// Space information wrapper
        std::weak_ptr<ompl::base::SpaceInformation> spaceInfo;

        /// Use nearest neighbor collision checking with GNAT
        bool useGnatCollisionChecking = false;

        /// Use nearest neighbor collision checking with KD Tree
        bool useKDTreeCollisionChecking = false;

        double maxAcceleration = robot_constants::ROBOT_MAX_ACCELERATION;

        /// Obstacle list
        std::vector<std::vector<double>> obstacles;

        /// Nearest neighbor datastructure GNAT
        std::shared_ptr<ompl::NearestNeighborsGNATNoThreadSafety<double*>> nn;

        /// Nearest neighbor datastructure KD-Tree
        std::shared_ptr<geometry::KDTree> kdtree;
    };

    class KinematicStateSpace : public ompl::base::RealVectorStateSpace
    {
    public:

        KinematicStateSpace(std::shared_ptr<controller::MinimumTime2DController> bvpSolver, int dim);

        bool hasSymmetricDistance() const;

        bool isMetricSpace() const;

        bool hasSymmetricInterpolate() const;

        double distance(const ompl::base::State *state1, const ompl::base::State *state2) const override;

        void enforceBounds(ompl::base::State *state) const override;

        /**
         * Set max acceleration to bvp solver.
         * @param maxAcceleration
         */
        void setMaxAcceleration(double maxAcceleration);

        /**
         * Set max velocity to bvp solver.
         * @param maxVelocity
         */
        void setMaxVelocity(double maxVelocity);

        /**
         * State interpolation. Can be used as a state propagation
         * @param from state from
         * @param to state to
         * @param t propagation time
         * @param state state to be propagated
         */
        //void interpolate (const ompl::base::State *from, const ompl::base::State *to,
         //                         double t, ompl::base::State *state) const;


    private:

        /**
         * Bounds position
         * @param i axis number
         * @param rstate state to be bounded
         */
        void boundPositions(int i, ompl::base::RealVectorStateSpace::StateType *rstate) const;

        /**
         * Bounds velocity
         * @param rstate state to be bounded
         */
        void boundVelocity(ompl::base::RealVectorStateSpace::StateType *rstate) const;


        double dimensions = 4;
        std::shared_ptr<controller::MinimumTime2DController> bvpSolver;
        double maxAcceleration = 1000;
        double maxVelocity = 1000;



    };
}



#endif //SIMULATOR_KINEMATICSTATESPACE_H
