//
// Created by felipe on 21/09/20.
//

#include "KinematicStateSpace.h"
#include <math.h>
#include <constants/Constants.h>
#include "ompl/base/SpaceInformation.h"


namespace dss {

    KinematicStateSampler::KinematicStateSampler(const ompl::base::SpaceInformation *si) : ompl::base::ValidStateSampler(si)
    {
        name_ = "kinematic state sampler";
        ompl::base::RealVectorBounds bounds = si->getStateSpace()->as<ompl::base::RealVectorStateSpace>()->getBounds();
        boundsX = bounds.high[0];
        boundsY = bounds.high[2];
        maxVelocity = robot_constants::ROBOT_MAX_VELOCITY;
        maxAcceleration = robot_constants::ROBOT_MAX_ACCELERATION;
    }
    // Generate a sample in the valid part of the R^3 state space
    // Valid states satisfy the following constraints:
    // -1<= x,y,z <=1
    // if .25 <= z <= .5, then |x|>.8 and |y|>.8
    bool KinematicStateSampler::sample(ompl::base::State *state)
    {
        double* val = static_cast<ompl::base::RealVectorStateSpace::StateType*>(state)->values;
        double angle = 0.0;
        double radius = 0.0;
        double runOffDistanceX = 0.0;
        double runOffDistanceY = 0.0;

        while(true) {
            angle = rng_.uniformReal(0,2*M_PI);
            radius = maxVelocity*sqrt(rng_.uniformReal(0,1));
            val[1] = radius * cos(angle);
            val[3] = radius * sin(angle);

            //if(val[1] > 0)
            //    constraintXUpper = 1;
            //else constraintXBelow = 1;

            //if(val[3] > 0)
            //    constraintYUpper = 1;
            //else constraintYBelow = 1;

            runOffDistanceX = val[1] * val[1] / (2 * maxAcceleration * robot_constants::COS45);
            runOffDistanceY = val[3] * val[3] / (2 * maxAcceleration * robot_constants::COS45);

            if (boundsX >= runOffDistanceX && boundsY >= runOffDistanceY) {
                val[0] = rng_.uniformReal(-boundsX + runOffDistanceX, boundsX - runOffDistanceX);
                val[2] = rng_.uniformReal(-boundsY + runOffDistanceY, boundsY - runOffDistanceY);
                break;
            }
        }


        //val[0] = rng_.uniformReal(-boundsX,boundsX);
        //val[2] = rng_.uniformReal(-boundsY,boundsY);

        //assert(si_->isValid(state));
        return true;
    }
    // We don't need this in the example below.
    bool KinematicStateSampler::sampleNear(ompl::base::State* /*state*/, const ompl::base::State* /*near*/, const double /*distance*/)
    {
        throw ompl::Exception("MyValidStateSampler::sampleNear", "not implemented");
        return false;
    }

    DynamicMotionValidityChecker::DynamicMotionValidityChecker(ompl::base::SpaceInformationPtr si, std::shared_ptr<controller::MinimumTime2DController> solver)
            : ompl::base::MotionValidator(si), spaceInfo(si), bvpSolver(solver) , maxAcceleration(maxAcceleration), maxVelocity(maxVelocity), obstacles(), gnatCollisionChecking(false),
              fieldLimits({robot_constants::MAX_ALLOWED_X,0,robot_constants::MAX_ALLOWED_Y,0}), kdTreeCollisionChecking(false)
    {
        outputFile.open("narrowPhaseAnalysis.txt");
        configure();
    }

    DynamicMotionValidityChecker::DynamicMotionValidityChecker(ompl::base::SpaceInformationPtr si,
                                                               std::shared_ptr<controller::MinimumTime2DController> solver,
                                                               std::string fileName):
            ompl::base::MotionValidator(si), spaceInfo(si), bvpSolver(solver) , maxAcceleration(maxAcceleration), maxVelocity(maxVelocity), obstacles(), gnatCollisionChecking(false),
            fieldLimits({robot_constants::MAX_ALLOWED_X,0,robot_constants::ESCAPE_AREA_Y,0}), kdTreeCollisionChecking(false){
        outputFile.open(fileName);
        configure();
    }

    void DynamicMotionValidityChecker::configure(){
        nn = std::make_shared<ompl::NearestNeighborsGNATNoThreadSafety<double*>>();
        nn->setDistanceFunction([this](const double *a, const double *b) { return distance(a, b); });

        /// Setting number of dimensions to 4.

        kd = std::make_shared<geometry::KDTree>(4);

        /// Setting KDTree offset to 2.

        kd->setOffset(2);
    }

    DynamicMotionValidityChecker::~DynamicMotionValidityChecker()
    {
        outputFile.close();
    }

    bool DynamicMotionValidityChecker::checkLimits(double* state) const {
        return !(fabs(state[0]) > fieldLimits[0] || fabs(state[2]) > fieldLimits[2]);
    }

    bool DynamicMotionValidityChecker::checkMotion(const ompl::base::State *state1,
                                                   const ompl::base::State *state2) const {

        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxVelocity(maxVelocity);
        bvpSolver->setMaxAcceleration(maxAcceleration);

        /// We must evaluate the angle reference to get the right collision rectangle
        std::vector<double> angles = bvpSolver->getBestFrame();
        double maxAccelerationX = 0.0;
        double maxAccelerationY = 0.0;
        double beta = bvpSolver->getBetaFromFrame(&angles[0]);
        double theta = bvpSolver->getThetaFromFrame(&angles[0]);

        double time = bvpSolver->getTimeForFrame(angles,maxAccelerationX,maxAccelerationY);

        if(time > 10)
            return false;

        return (broadPhaseMotionValidator(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY)
                || narrowPhaseMotionValidator(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY,time));
        //return narrowPhaseMotionValidator(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY,time);
    }

    bool DynamicMotionValidityChecker::evaluateNarrowPhase(const ompl::base::State *state1,
                                                           const ompl::base::State *state2) const {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxVelocity(maxVelocity);
        bvpSolver->setMaxAcceleration(maxAcceleration);

        /// We must evaluate the angle reference to get the right collision rectangle
        std::vector<double> angles = bvpSolver->getBestFrame();
        double maxAccelerationX = 0.0;
        double maxAccelerationY = 0.0;
        double beta = bvpSolver->getBetaFromFrame(&angles[0]);
        double theta = bvpSolver->getThetaFromFrame(&angles[0]);

        double time = bvpSolver->getTimeForFrame(angles,maxAccelerationX,maxAccelerationY);

        return (narrowPhaseMotionValidator(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY,time));
    }

    void DynamicMotionValidityChecker::setGnatCollisionChecking(bool gnatCollisionChecking) {
        this->gnatCollisionChecking = gnatCollisionChecking;
    }

    bool DynamicMotionValidityChecker::checkLineAgainstObstacles(double *state1, double* state2) const {
        for(int i=obstacles.size();i--;)
        {
            if(!checkObsLine(state1[0],state1[2],state2[0],state2[2],obstacles[i][0],obstacles[i][2],obstacleRadius))
                return false;
        }

        return true;
    }

    bool DynamicMotionValidityChecker::checkMotion(const ompl::base::State *s1, const ompl::base::State *s2,
                                                   std::pair<ompl::base::State *, double> &lastValid) const {
        /// to do: add last valid state
        lastValid.first = nullptr;
        lastValid.second = 0;
        return false;
    }

    bool DynamicMotionValidityChecker::checkObsLine(const double& px, const double& py,
                                                    const double& qx, const double& qy,
                                                    const double& ox, const double& oy,
                                                    const double& r)
    {
        /// Evaluating second degree equation parameters a,b and c

        double a = (qx-px)*(qx-px) + (qy-py)*(qy-py);
        double b = 2.0*((px-ox)*(qx-px)+(py-oy)*(qy-py));
        double c = (px-ox)*(px-ox)+(py-oy)*(py-oy)-r*r;

        /// Getting analytical solution

        std::vector<double> roots = getBhaskaraRoots(a,b,c);


        /// Roots must be both lying outside of the interval [0,1]

        return (roots.empty()||((roots[0]<0 && roots[1]<0)||(roots[0]>1 && roots[1]>1)));

    }

    std::vector<double> DynamicMotionValidityChecker::getBhaskaraRoots(const double& a,const double& b,const double& c) {
        std::vector<double> roots;

        /// Evaluating delta

        double delta = b*b-4*a*c;

        if(delta >= 0)
        {
            roots = {0.0,0.0};

            /// Evaluating roots

            roots[0] = (b*b+sqrt(delta))/(2*a);
            roots[1] = -b/a-roots[0];
        }

        return roots;
    }

    bool DynamicMotionValidityChecker::checkRectangle(const double &x0, const double &y0, const double &x1,
                                                      const double &y1, const double &ox, const double &oy,
                                                      const double &r) {
        if(x0 <= ox && x1 >= ox && y0 <= oy && y1 >= oy)
            return false;
        int edges = 0;

        return (checkObsLine(x0,y0,x1,y0,ox,oy,r) && checkObsLine(x0,y0,x0,y1,ox,oy,r) &&
                checkObsLine(x1,y0,x1,y1,ox,oy,r) && checkObsLine(x0,y1,x1,y1,ox,oy,r));

    }

    bool DynamicMotionValidityChecker::checkBroadPhaseRectangle(const double &minx, const double &miny,
                                                                const double &maxx, const double &maxy, const double& beta) const {
        double s = bvpSolver->fastSin(-beta);
        double c = bvpSolver->fastCos(-beta);

        std::vector<double> xLimits = {minx,maxx};
        std::vector<double> yLimits = {miny,maxy};
        double px =0.0, py=0.0;

        for(int i=0;i<xLimits.size();i++)
            for(int j=0; j<yLimits.size();j++)
            {
                px = xLimits[i]*c+yLimits[j]*s;
                py = -xLimits[i]*s+yLimits[j]*c;
                if(fabs(px) > fieldLimits[0] || fabs(py) > fieldLimits[2])
                {
                    return false;
                }

            }

        return true;
    }

    bool DynamicMotionValidityChecker::broadPhaseMotionValidator(const ompl::base::State *state1,
                                                                 const ompl::base::State *state2,
                                                                 const double& beta,
                                                                 const double& theta,
                                                                 const double &maxAccelerationX,
                                                                 const double &maxAccelerationY) const {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        double rotatedState1[4];
        double rotatedState2[4];

        /// Check if rectangle is fully inside field

        rotateCoordinates(beta,s1,&rotatedState1[0]);
        rotateCoordinates(beta,s2,&rotatedState2[0]);


        double x0 = rotatedState1[0];
        double vx0 = rotatedState1[1];
        double y0 = rotatedState1[2];
        double vy0 = rotatedState1[3];

        double xf = rotatedState2[0];
        double vxf = rotatedState2[1];
        double yf = rotatedState2[2];
        double vyf = rotatedState2[3];

        /// Motion feasibility checking of previous frame method.

        /*
        double beta1 = acos(fabs(vx0)/maxVelocity);
        double beta2 = asin(fabs(vy0)/maxVelocity);

        double beta = atan2(fabs(vyf),fabs(vxf));

        if(beta < std::min(beta1,beta2) || beta > std::max(beta1,beta2))
            return false;
        */




        double minxr = std::min(x0-vx0*vx0/(2*maxAccelerationX),xf-vxf*vxf/(2*maxAccelerationX));
        double maxxr = std::max(x0+vx0*vx0/(2*maxAccelerationX),xf+vxf*vxf/(2*maxAccelerationX));
        double minyr = std::min(y0-vy0*vy0/(2*maxAccelerationY),yf-vyf*vyf/(2*maxAccelerationY));
        double maxyr = std::max(y0+vy0*vy0/(2*maxAccelerationY),yf+vyf*vyf/(2*maxAccelerationY));

        //std::cout << minxr << std::endl;
        //std::cout << maxxr << std::endl;
        //std::cout << minyr << std::endl;
        //std::cout << maxyr << std::endl;
        if(!checkBroadPhaseRectangle(minxr,maxxr,minyr,maxyr,beta))
            return false;

        std::vector<double> obstaclePosition(2,0);

        /// Now, we should check the rectangle formed against the obstacles
        for(int i=obstacles.size();i--;)
        {
            rotateObstacleCoordinates(beta,obstacles[i],obstaclePosition);
            if(!checkRectangle(minxr,minyr,maxxr,maxyr,obstaclePosition[0],obstaclePosition[1],obstacleRadius))
                return false;
        }

        return true;
    }

    double DynamicMotionValidityChecker::getTimeForTrajectory(const ompl::base::State *state1,
                                                              const ompl::base::State *state2) {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        /// Setting initial and final states plus dynamic constraints
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxVelocity(maxVelocity);
        bvpSolver->setMaxAcceleration(maxAcceleration);

        /// We must evaluate the angle reference to get the right collision rectangle
        std::vector<double> angles = bvpSolver->getBestFrame();
        double maxAccelerationX = 0.0;
        double maxAccelerationY = 0.0;
        double beta = bvpSolver->getBetaFromFrame(&angles[0]);
        double theta = bvpSolver->getThetaFromFrame(&angles[0]);

        return bvpSolver->getTimeForFrame(angles,maxAccelerationX,maxAccelerationY);

    }

    double DynamicMotionValidityChecker::emulateTrajectory(const ompl::base::State *state1,
                                                         const ompl::base::State *state2,
                                                        const double& sampleTime) {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        /// Setting initial and final states plus dynamic constraints
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxVelocity(maxVelocity);
        bvpSolver->setMaxAcceleration(maxAcceleration);

        /// We must evaluate the angle reference to get the right collision rectangle
        std::vector<double> angles = bvpSolver->getBestFrame();
        double maxAccelerationX = 0.0;
        double maxAccelerationY = 0.0;
        double beta = bvpSolver->getBetaFromFrame(&angles[0]);
        double theta = bvpSolver->getThetaFromFrame(&angles[0]);

        double time = bvpSolver->getTimeForFrame(angles,maxAccelerationX,maxAccelerationY);

        return motionEmulation(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY,time, sampleTime);

    }

    double DynamicMotionValidityChecker::motionEmulation(const ompl::base::State *state1, const ompl::base::State *state2,
                                                       const double &beta, const double &theta,
                                                       const double &maxAccelerationX, const double &maxAccelerationY,
                                                       const double &time, const double& sampleTime) {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        double rotatedState1[4];
        double rotatedState2[4];
        double originalState[4] = {s1[0],s1[1],s1[2],s1[3]};

        rotateCoordinatesAccuracy(beta,s1,&rotatedState1[0]);
        rotateCoordinatesAccuracy(beta,s2,&rotatedState2[0]);

        double alpha = 0.0;
        int timesteps =  time/sampleTime;
        //std::cout << time << std::endl;

        double maxVelocityX = maxVelocity*cos(theta);
        double maxVelocityY = maxVelocity*sin(theta);


        double x,y;
        double tx = 0.0,ty = 0.0;

        //std::cout << rotatedState1[0] << " " <<   rotatedState1[1] << " " <<  rotatedState1[2] << " " << rotatedState1[3] << std::endl;

        for(int i=0;i<timesteps ;i++) {
            x = originalState[0];
            y = originalState[2];
            //std::cout << x << " " << y << " " << std::endl;
            tx = bvpSolver->BVPsolution(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0]);
            ty = bvpSolver->BVPsolution(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2]);
            bvpSolver->BVPemulation(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0],sampleTime);
            bvpSolver->BVPemulation(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2],sampleTime);

            rotateCoordinates(-beta,rotatedState1,&originalState[0]);

            outputFile << originalState[0] << " "
                       << originalState[1] << " "
                       << originalState[2] << " "
                       << originalState[3] << std::endl;

        }

        //std::cout << rotatedState1[0] << " " <<   rotatedState1[1] << " " <<  rotatedState1[2] << " " << rotatedState1[3] << std::endl;
        //std::cout << rotatedState2[0] << " " <<  rotatedState2[1] << " "  <<  rotatedState2[2] << " " << rotatedState2[3] << std::endl;
        double error = 0.0;

        for(int i=0;i<4;i++)
        {
            error += (rotatedState1[i]-rotatedState2[i])*(rotatedState1[i]-rotatedState2[i]);
        }

        error = sqrt(error);

        return error;

    }

    void DynamicMotionValidityChecker::steeringFunction(const ompl::base::State *state1,
                                                        const ompl::base::State *state2, double emulationTime,
                                                        const ompl::base::State *stater) {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxVelocity(maxVelocity);
        bvpSolver->setMaxAcceleration(maxAcceleration);

        /// We must evaluate the angle reference to get the right collision rectangle
        std::vector<double> angles = bvpSolver->getBestFrame();
        double maxAccelerationX = 0.0;
        double maxAccelerationY = 0.0;
        double beta = bvpSolver->getBetaFromFrame(&angles[0]);
        double theta = bvpSolver->getThetaFromFrame(&angles[0]);

        double time = bvpSolver->getTimeForFrame(angles,maxAccelerationX,maxAccelerationY);

        double finalTime = (time < emulationTime) ? time:emulationTime;

        motionSteering(state1,state2,beta,theta,maxAccelerationX,maxAccelerationY,finalTime,stater);
    }

    void DynamicMotionValidityChecker::motionSteering(const ompl::base::State *state1, const ompl::base::State *state2,
                                                      const double &beta, const double &theta,
                                                      const double &maxAccelerationX, const double &maxAccelerationY,
                                                      const double &sampleTime,
                                                      const ompl::base::State *stater) const {
        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        double rotatedState1[4];
        double rotatedState2[4];
        double originalState[4] = {s1[0],s1[1],s1[2],s1[3]};

        rotateCoordinatesAccuracy(beta,s1,&rotatedState1[0]);
        rotateCoordinatesAccuracy(beta,s2,&rotatedState2[0]);

        double alpha = 0.0;
        int timesteps =  1;
        //std::cout << time << std::endl;

        double maxVelocityX = maxVelocity*cos(theta);
        double maxVelocityY = maxVelocity*sin(theta);


        double x,y;
        double tx = 0.0,ty = 0.0;

        //std::cout << rotatedState1[0] << " " <<   rotatedState1[1] << " " <<  rotatedState1[2] << " " << rotatedState1[3] << std::endl;

        for(int i=0;i<timesteps ;i++) {
            x = originalState[0];
            y = originalState[2];
            tx = bvpSolver->BVPsolution(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0]);
            ty = bvpSolver->BVPsolution(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2]);
            bvpSolver->BVPemulation(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0],sampleTime);
            bvpSolver->BVPemulation(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2],sampleTime);

            rotateCoordinates(-beta,rotatedState1,&originalState[0]);

        }

        //std::cout << rotatedState1[0] << " " <<   rotatedState1[1] << " " <<  rotatedState1[2] << " " << rotatedState1[3] << std::endl;
        //std::cout << rotatedState2[0] << " " <<  rotatedState2[1] << " "  <<  rotatedState2[2] << " " << rotatedState2[3] << std::endl;
        double error = 0.0;

        for(int i=0;i<4;i++)
        {
            error += (rotatedState1[i]-rotatedState2[i])*(rotatedState1[i]-rotatedState2[i]);
        }

        stater->as<ompl::base::RealVectorStateSpace::StateType>()->values[0] = originalState[0];
        stater->as<ompl::base::RealVectorStateSpace::StateType>()->values[1] = originalState[1];
        stater->as<ompl::base::RealVectorStateSpace::StateType>()->values[2] = originalState[2];
        stater->as<ompl::base::RealVectorStateSpace::StateType>()->values[3] = originalState[3];

    }


    bool DynamicMotionValidityChecker::narrowPhaseMotionValidator(const ompl::base::State *state1,
                                                                  const ompl::base::State *state2,
                                                                  const double& beta,
                                                                  const double& theta,
                                                                  const double &maxAccelerationX,
                                                                  const double &maxAccelerationY,
                                                                  const double &time) const {

        int numberOfIterations = time/emulationSampleTime;



        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        double rotatedState1[4];
        double rotatedState2[4];
        double originalState[4] = {s1[0],s1[1],s1[2],s1[3]};
        double previousState[4] = {0.0,0.0,0.0,0.0};

        rotateCoordinates(beta,s1,&rotatedState1[0]);
        rotateCoordinates(beta,s2,&rotatedState2[0]);

        double alpha = 0.0;



        /// We are not checking the whole path, only parts of it

        int checkingFrequency = 5;

        double maxVelocityX = maxVelocity*cos(theta);
        double maxVelocityY = maxVelocity*sin(theta);

        /// Radius to perform nearest neighbor query

        double checkingRadius = sqrt(maxVelocityX*maxVelocityX+maxVelocityY*maxVelocityY)*checkingFrequency*emulationSampleTime
                                + obstacleRadius;


        std::vector<double*> nearestObstacles;
        double* nearestObs;


        for(int j=numberOfIterations;j--;) {

            //double tx = bvpSolver->BVPsolution(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0]);
            //double ty = bvpSolver->BVPsolution(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2]);
            bvpSolver->BVPemulation(rotatedState1[1],maxVelocityX,rotatedState2[1],maxAccelerationX,rotatedState1[0],rotatedState2[0],emulationSampleTime);
            bvpSolver->BVPemulation(rotatedState1[3],maxVelocityY,rotatedState2[3],maxAccelerationY,rotatedState1[2],rotatedState2[2],emulationSampleTime);

            if(!(j%checkingFrequency))
            {
                if(!checkLimits(originalState))
                    return false;

                previousState[0] = originalState[0];
                previousState[2] = originalState[2];

                /// Rotate state again for collision checking
                rotateCoordinates(-beta,rotatedState1,&originalState[0]);



                /// Get nearest neighbor using GNAT datastructure. Important for scalability.
                //std::cout << nearestObs[0] << " " << nearestObs[1] << " " << nearestObs[2] << " " << nearestObs[3] << std::endl;
                //checkLineAgainstObstacles(&previousState[0],&originalState[0]);

                if(!checkLineAgainstObstacles(&previousState[0],&originalState[0]))
                    return false;

                /*if(gnatCollisionChecking) {
                    nn->nearestR(&originalState[0], checkingRadius, nearestObstacles);

                    for (int i = nearestObstacles.size(); i--;) {

                        if (!checkObsLine(previousState[0], previousState[2], originalState[0], originalState[2],
                                          nearestObstacles[i][0], nearestObstacles[i][2], obstacleRadius))
                            return false;
                    }
                }
                else{
                    if(!checkLineAgainstObstacles(&previousState[0],&originalState[0]))
                        return false;
                }*/

            }
        }

        return true;
    }

    void DynamicMotionValidityChecker::rotateCoordinates(double beta, double* state ,
                                                         double* rotatedState) const {

        double c = bvpSolver->fastCos(beta);
        double s = bvpSolver->fastSin(beta);
        rotatedState[0] = state[0]*c+state[2]*s;
        rotatedState[2] = -state[0]*s+state[2]*c;
        rotatedState[1] = state[1]*c+state[3]*s;
        rotatedState[3] = -state[1]*s+state[3]*c;
    }

    void DynamicMotionValidityChecker::rotateCoordinatesAccuracy(double beta, double *state,
                                                                 double *rotatedState) const {
        double c = cos(beta);
        double s = sin(beta);
        rotatedState[0] = state[0]*c+state[2]*s;
        rotatedState[2] = -state[0]*s+state[2]*c;
        rotatedState[1] = state[1]*c+state[3]*s;
        rotatedState[3] = -state[1]*s+state[3]*c;

    }

    void DynamicMotionValidityChecker::rotateCoordinates(double beta, const std::vector<double>& state ,
                                                         std::vector<double>& rotatedState) const {

        double c = bvpSolver->fastCos(beta);
        double s = bvpSolver->fastSin(beta);
        rotatedState[0] = state[0]*c+state[2]*s;
        rotatedState[2] = -state[0]*s+state[2]*c;
        rotatedState[1] = state[1]*c+state[3]*s;
        rotatedState[3] = -state[1]*s+state[3]*c;
    }

    void DynamicMotionValidityChecker::rotateObstacleCoordinates(double beta,
                                                                 const std::vector<double> &obstaclePosition,
                                                                 std::vector<double> &rotatedObstacle) const {
        double c = bvpSolver->fastCos(beta);
        double s = bvpSolver->fastSin(beta);
        rotatedObstacle[0] = obstaclePosition[0]*c+obstaclePosition[2]*s;
        rotatedObstacle[1] = -obstaclePosition[0]*s+obstaclePosition[2]*c;
    }

    void DynamicMotionValidityChecker::setMaxAcceleration(double maxAcceleration) {
        this->maxAcceleration = maxAcceleration;
    }

    void DynamicMotionValidityChecker::setMaxVelocity(double maxVelocity) {
        this->maxVelocity = maxVelocity;
    }

    double DynamicMotionValidityChecker::distance(const double *s1, const double *s2) {
        return sqrt((s1[0]-s2[0])*(s1[0]-s2[0])+(s1[2]-s2[2])*(s1[2]-s2[2]));
    }

    void DynamicMotionValidityChecker::setObstacle(double *obstacle) {
        nn->add(obstacle);
    }

    void DynamicMotionValidityChecker::setObstacles(std::vector<std::vector<double>> &obstacles) {
        nn->clear();
        kd->clear();
        this->obstacles = obstacles;
        for(int i = 0;i<obstacles.size();i++)
        {
            nn->add(&obstacles[i][0]);
            kd->insert(obstacles[i]);
        }
    }

    void DynamicMotionValidityChecker::setObstacleRadius(double obstacleRadius) {
        this->obstacleRadius = obstacleRadius;
    }

    void DynamicMotionValidityChecker::printObstaclesToFile() {
        for(int i=obstacles.size();i--;)
        {
            outputFile << obstacles[i][0] << " " << obstacles[i][1] << " "  <<
                       obstacles[i][2] << " " << obstacles[i][3] << std::endl;
        }
    }

    void DynamicMotionValidityChecker::printToFile(std::string message) {
        outputFile << message << std::endl;
    }

    void DynamicMotionValidityChecker::setEmulationSampleTime(const double &emulationSampleTime) {
        this->emulationSampleTime = emulationSampleTime;
    }

    void DynamicMotionValidityChecker::setFieldLimits(const std::vector<double> &fieldLimits) {
        this->fieldLimits = fieldLimits;
    }

    DynamicStateValidityChecker::DynamicStateValidityChecker(const ompl::base::SpaceInformationPtr &spaceInfo, int realSpaceDimensions) :
            ompl::base::StateValidityChecker(spaceInfo), spaceInfo(spaceInfo), realSpaceDimensions(realSpaceDimensions), useGnatCollisionChecking(true)
    {
        configure();
    }

    void DynamicStateValidityChecker::configure()
    {
        nn = std::make_shared<ompl::NearestNeighborsGNATNoThreadSafety<double*>>();
        nn->setDistanceFunction([this](const double *a, const double *b) { return distance(a, b); });

        kdtree = std::make_shared<geometry::KDTree>(4);
        kdtree->setOffset(2);
    }

    bool DynamicStateValidityChecker::belongsToFree(double* state ,double obstacleRadius) const {
        if(useGnatCollisionChecking && nn->size() == 0)
            return true;

        if(useGnatCollisionChecking)
        {
            double* nearestObstacle =  nn->nearest(state);

            double distanceToNearestObstacle = distance(state,nearestObstacle);

            /// checks if distance is larger than obstacle radius.
            return (distance(state,nearestObstacle) > obstacleRadius);
        }
        else if(useKDTreeCollisionChecking)
        {
            double* nearestObstacle
                    =  &(kdtree->nearestNeighbor(std::vector<double>{state[0],state[1],state[2],state[3]}))[0];

            double distanceToNearestObstacle = distance(state,nearestObstacle);

            /// checks if distance is larger than obstacle radius.
            return (distance(state,nearestObstacle) > obstacleRadius);
        }
        else{
            for(int i=obstacles.size();i--;)
            {
                if(distance(state,&obstacles[i][0]) < obstacleRadius)
                    return false;
            }
        }

        return true;

    }

    bool DynamicStateValidityChecker::isValid(const ompl::base::State *state) const {
        double* realState = state->as<ompl::base::RealVectorStateSpace::StateType>()->values;

        return belongsToFree(realState, obstacleRadius);
    }

    double DynamicStateValidityChecker::distance(const double *s1, const double *s2) const {
        /// The distance uses only the components [0] and [2] (position). The distance returned will be squared, to reduce computational cost.
        /// The distance must be metric
        double dy = s2[2]-s1[2];
        double dx = s2[0]-s1[0];
        double distToObst = sqrt(dx*dx + dy*dy);
        return distToObst;
        //return distToObst;
        //double projVelocity = (dx*s1[1]+dy*s1[3])/(distToObst);
        //double projVelocityX = projVelocity/(distToObst)*dx;
        //double projVelocityY = projVelocity/(distToObst)*dy;
        //if(projVelocityX*dx+projVelocityY*dy < 0)
        //    return distToObst;
        //return distToObst - (projVelocity*projVelocity)/(2*maxAcceleration);

    }

    void DynamicStateValidityChecker::setObstacle(double* obstacle) {
            nn->add(obstacle);
            kdtree->insert(std::vector<double>{obstacle[0],obstacle[1],obstacle[2],obstacle[3]});
    }

    void DynamicStateValidityChecker::setObstacles(std::vector<std::vector<double>> &obstacles) {
        nn->clear();
        kdtree->clear();
        this->obstacles = obstacles;
        for (int i=0;i<obstacles.size();i++)
        {
            setObstacle(&obstacles[i][0]);
        }
    }

    void DynamicStateValidityChecker::setObstacleRadius(const double& obstacleRadius) {
        this->obstacleRadius = obstacleRadius;
    }

    void DynamicStateValidityChecker::setGnatCollisionChecking(bool useGnatCollisionChecking) {
        this->useGnatCollisionChecking = useGnatCollisionChecking;
    }

    void DynamicStateValidityChecker::setUseKDTreeCollisionChecking(bool useKDTreeCollisionChecking) {
        this->useKDTreeCollisionChecking = useKDTreeCollisionChecking;
    }

    KinematicStateSpace::KinematicStateSpace(std::shared_ptr<controller::MinimumTime2DController> bvpSolver, int dim):
            dimensions(dim), ompl::base::RealVectorStateSpace(dim), bvpSolver(bvpSolver)
    {
    }

    double KinematicStateSpace::distance(const ompl::base::State *state1, const ompl::base::State *state2) const {

        /*
        double x0 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vx0 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double y0 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vy0 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];

        double xf = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values[0];
        double vxf = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values[1];
        double yf = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values[2];
        double vyf = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values[3];*/

        double* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>()->values;
        double* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>()->values;


        double beta = bvpSolver->getBeta(s1[0], s2[0],
                                s1[1], s2[1], s1[2], s2[2], s1[3], s2[3], maxAcceleration, maxVelocity);

        double alpha = 0.0;
        /*return bvpSolver->getArrivalTimeDecoupled(s1[0], s2[0],
                                                  s1[1], s2[1], s1[2], s2[2], s1[3], s2[3], maxAcceleration, maxVelocity, beta, alpha);*/
        bvpSolver->setInitialState(s1);
        bvpSolver->setFinalState(s2);
        bvpSolver->setMaxAcceleration(maxAcceleration);
        bvpSolver->setMaxVelocity(maxVelocity);
        std::vector<double> angles = bvpSolver->getBestFrame();

        double ans = bvpSolver->getTimeForFrame(&angles[0]);

        return ans;
    }


    void KinematicStateSpace::enforceBounds(ompl::base::State *state) const {
        auto *rstate = static_cast<ompl::base::RealVectorStateSpace::StateType*>(state);
        boundPositions(0,rstate); /// bounding x
        boundPositions(2,rstate); /// bounding y
        boundVelocity(rstate);
    }

    void KinematicStateSpace::boundPositions(int i, ompl::base::RealVectorStateSpace::StateType* rstate) const {
        if (rstate->values[i] > bounds_.high[i])
            rstate->values[i] = bounds_.high[i];
        else if (rstate->values[i] < bounds_.low[i])
            rstate->values[i] = bounds_.low[i];
    }

    void KinematicStateSpace::boundVelocity(ompl::base::RealVectorStateSpace::StateType* rstate) const {
        double angle = atan2(rstate->values[3], rstate->values[1]);
        double rho2 = rstate->values[1] * rstate->values[1] + rstate->values[3] * rstate->values[3];
        if (rho2 < maxVelocity * maxVelocity) {
            rstate->values[1] = maxVelocity * cos(angle);
            rstate->values[3] = maxVelocity * sin(angle);
        }
    }

    bool KinematicStateSpace::isMetricSpace() const {
        /// The space is not metric and symmetric
        return false;
    }

    bool KinematicStateSpace::hasSymmetricDistance() const {
        return false;
    }

    bool KinematicStateSpace::hasSymmetricInterpolate() const {
        return false;
    }

    void KinematicStateSpace::setMaxAcceleration(double maxAcceleration) {
        KinematicStateSpace::maxAcceleration = maxAcceleration;
    }

    void KinematicStateSpace::setMaxVelocity(double maxVelocity) {
        KinematicStateSpace::maxVelocity = maxVelocity;
    }

};