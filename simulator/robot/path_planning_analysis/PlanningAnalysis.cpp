//
// Created by felipe on 14/05/20.
//

#include "PlanningAnalysis.h"
#include <ompl/base/PlannerStatus.h>
#include <math.h>


namespace planning_analysis {

    AnalysisResult::AnalysisResult(int size):timeResult(size,0.0), planningSucceeded(size,false)
    {
    }

    void AnalysisResult::print(std::ostream &out) {
        out << "Printing average results. \n";
        out << "Average time of " << averageTime << " seconds. \n";
    }

    PlanningAnalysis::PlanningAnalysis(const std::vector<double>& boundsStart, const std::vector<double>& boundsGoal):clock(),
                                                                                                                      randomStateGen(),
                                                                                                                    boundsStart(boundsStart),
                                                                                                                    boundsGoal(boundsGoal)
    {
    }

    PlanningAnalysis::PlanningAnalysis(const std::vector<double> &boundsStart, const std::vector<double> &boundsGoal,
                                       int seed) : clock(),
                                                   randomStateGen(seed),
                                                   boundsStart(boundsStart),
                                                   boundsGoal(boundsGoal)
    {

    }

    std::vector<double> PlanningAnalysis::linearSpacedVector(double min, double max, int size) {
        std::vector<double> ans(size,0);
        double space = (max-min)/(size-1);

        for(int i=0;i<size;i++)
            ans[i] = (min+i*space);

        return ans;
    }


    AnalysisResult PlanningAnalysis::MonteCarloTimingSimulation(path_planning::PathPlannerBase &planner,
                                                        int monteCarloIterations,
                                                                int numberOfObstacles) {
        // Initializing variables
        double toc = 0.0;
        double tic = 0.0;
        double time = 0.0;
        bool solved = true;

        std::vector<double> computationTime = linearSpacedVector(0.0005,0.0035,80);

        std::vector<double> start;
        std::vector<double> goal;

        // Initializing obstacles vector and obstacle bounds
        std::vector<double> obstacleBounds(4,0.0);
        obstacleBounds[0] = -robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[1] = robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[2] = -robot_constants::OBSTACLES_LIMIT_REAL_Y;
        obstacleBounds[3] = robot_constants::OBSTACLES_LIMIT_REAL_Y;

        std::vector<std::vector<double>> obstacles;


        AnalysisResult result(monteCarloIterations);

        result.computationTimeSequence = computationTime;
        result.stdDev = std::vector<double>(computationTime.size(),0);
        result.avgCost = std::vector<double>(computationTime.size(),-1.0);
        result.successProbability = std::vector<double>(computationTime.size(),0);
        result.bestCost = std::vector<double>(computationTime.size(),100000);

        start = randomStateGen.returnRandomState(boundsStart);
        goal = randomStateGen.returnRandomState(boundsGoal);
        std::cout << start[0] << " " << start[1] << std::endl;
        std::cout << goal[0] << " " << goal[1] << std::endl;

        std::vector<std::vector<std::vector<double>>> obstaclesSequence(monteCarloIterations);

        for(int i=0;i<monteCarloIterations;i++)
            obstaclesSequence[i] = randomStateGen.generateRandomObstacles(robot_constants::OBSTACLE_RADIUS,
                                                                             numberOfObstacles,
                                                                             obstacleBounds);

        for(int i=0;i < monteCarloIterations; i++)
        {
            /*
             * Setting start and goal
             */

            planner.setStartAndGoal(start, goal);
            planner.setObstaclePositions(obstaclesSequence[i]);

            /*
             * Solving problem
             */
            solved = planner.getSolvingTime(robot_constants::PLANNING_TIME, clock, time);

            /*
             * Evaluating time
             */
            result.timeResult[i] = time;
            result.planningSucceeded[i] = solved;
            result.averageTime += time;
        }
        result.averageTime /= monteCarloIterations;

        int successProbabilityIterations = monteCarloIterations;
        std::vector<std::vector<double>> allCosts(successProbabilityIterations,std::vector<double>(successProbabilityIterations,-1));

        /*
         * Setting start and goal
         */


        std::vector<std::vector<double>> fixedObstacles = randomStateGen.generateRandomObstacles(robot_constants::OBSTACLE_RADIUS,
                                                           numberOfObstacles,
                                                           obstacleBounds);
        for(int i=0;i<computationTime.size();i++)
        {
            int successes = 0;

            for(int j=0;j < successProbabilityIterations; j++)
            {
                /*
                * Setting start and goal
                */

                planner.setStartAndGoal(start, goal);
                planner.setObstaclePositions(fixedObstacles);
                /*
                 * Solving problem
                 */
                solved = planner.getSolvingTime(computationTime[i], clock, time);


                //std::cout << result.successProbability[i] << std::endl;
                if(solved)
                {
                    successes++;
                    result.bestCost[i] = std::min(result.bestCost[i],planner.getPathLength());
                    result.avgCost[i] += planner.getPathLength();
                    allCosts[i][j] = planner.getPathLength();
                }
            }
            result.avgCost[i] /= successes;

            for(int j=0;j < successProbabilityIterations; j++)
            {
                /*
                * Setting start and goal
                */


                obstacles = randomStateGen.generateRandomObstacles(robot_constants::OBSTACLE_RADIUS,
                                                                   numberOfObstacles,
                                                                   obstacleBounds);

                planner.setStartAndGoal(start, goal);
                planner.setObstaclePositions(obstaclesSequence[j]);
                /*
                 * Solving problem
                 */
                solved = planner.getSolvingTime(computationTime[i], clock, time);

                /*
                 * Evaluating time
                 */
                result.successProbability[i] += solved ? 1:0;
            }
            result.successProbability[i] /= successProbabilityIterations;
            successes = 0;
        }

        for(int i=0; i<computationTime.size();i++)
        {
            int successes = 0;
            for(int j=0; j< successProbabilityIterations; j++)
            {
                if(allCosts[i][j] > 0)
                {
                    successes++;
                    result.stdDev[i] += (allCosts[i][j] - result.avgCost[i])*(allCosts[i][j] - result.avgCost[i]);
                }

            }
            if(successes > 1)
                result.stdDev[i] = sqrt(result.stdDev[i]/(successes-1));
            else result.stdDev[i]=0;
        }






        return result;
    }

    std::vector<double> PlanningAnalysis::MonteCarloAverageLength(path_planning::PathPlannerBase &planner, int MonteCarloIterations,
                                                     int numberOfObstacles) {
        // Initializing variables
        double toc = 0.0;
        double tic = 0.0;
        double time = 0.0;
        bool solved = true;

        std::vector<double> start;
        std::vector<double> goal;

        // Initializing average path length answer
        double ans = 0;
        int success = 0;
        double successProbability = 0;

        // Initializing obstacles vector and obstacle bounds
        std::vector<double> obstacleBounds(4,0.0);
        obstacleBounds[0] = -robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[1] = robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[2] = -robot_constants::OBSTACLES_LIMIT_REAL_Y;
        obstacleBounds[3] = robot_constants::OBSTACLES_LIMIT_REAL_Y;

        std::vector<std::vector<double>> obstacles;


        AnalysisResult result(MonteCarloIterations);

        start = randomStateGen.returnRandomState(boundsStart);
        goal = randomStateGen.returnRandomState(boundsGoal);

        for(int i=0;i < MonteCarloIterations; i++)
        {
            /*
             * Setting start and goal
             */


            obstacles = randomStateGen.generateRandomObstacles(robot_constants::OBSTACLE_RADIUS,
                                                               numberOfObstacles,
                                                               obstacleBounds);

            planner.setStartAndGoal(start, goal);
            planner.setObstaclePositions(obstacles);

            /*
             * Solving problem
             */
            solved = planner.getSolvingTime(robot_constants::PLANNING_TIME, clock, time);

            /*
             * Evaluating time
             */
            if(solved)
            {
                ans += planner.getPathLength();
                success++;
            }
        }
        ans /= success;
        successProbability = (double)success/(double)MonteCarloIterations;

        return {ans,successProbability};
    }

    std::vector<double> PlanningAnalysis::idealTrackingAnalysis(path_planning::PathPlannerBase &planner,
                                                                int trackingIterations,
                                                                bool clearBetweenIterations) {
        /// Initializing variables
        double toc = 0.0;
        double tic = 0.0;
        double time = 0.0;
        bool solved = true;

        double mu = 0.01;

        std::vector<double> start;
        std::vector<double> currentPosition;
        std::vector<double> goal;

        /// Initializing average path length answer
        double ans = 0;
        int success = 0;
        double successProbability = 0;

        std::vector<double> pathLength(trackingIterations,0);

        /// Initializing obstacles vector and obstacle bounds
        std::vector<double> obstacleBounds(4,0.0);
        obstacleBounds[0] = -robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[1] = robot_constants::OBSTACLES_LIMIT_REAL_X;
        obstacleBounds[2] = -robot_constants::OBSTACLES_LIMIT_REAL_Y;
        obstacleBounds[3] = robot_constants::OBSTACLES_LIMIT_REAL_Y;

        std::vector<std::vector<double>> obstacles;


        start = randomStateGen.returnRandomState(boundsStart);
        goal = randomStateGen.returnRandomState(boundsGoal);
        
        std::cout << start[0] << " " << start[1] << std::endl;
        std::cout << goal[0] << " " << goal[1] << std::endl;

        int numberOfObstacles = 9;
        int numberOfPlannings = 50;

        /**
         * Setting start, goal and obstacles
         */
        obstacles = randomStateGen.generateRandomObstacles(robot_constants::OBSTACLE_RADIUS_CONSERVATIVE,
                                                           numberOfObstacles,
                                                           obstacleBounds);
        int numberOfCollisions = 0;
        planner.setObstaclePositions(obstacles);

        for(int j=0; j<numberOfPlannings;j++)
        {
            std::cout << j << std::endl;
            currentPosition = start;
            for(int i=0;i < trackingIterations; i++)
            {

                planner.setStartAndGoal(currentPosition, goal);

                planner.setObstaclePositions(obstacles);
                /**
                 * Solving problem
                 */
                solved = planner.getSolvingTimeApproximate(robot_constants::PLANNING_TIME, clock, time, clearBetweenIterations);

                /**
                 * Evaluating time
                 */
                if(solved)
                {
                    std::vector<double> goToState = planner.getWaypoint(1)[0];
                    std::vector<double> direction = {goToState[0]-currentPosition[0],goToState[1]-currentPosition[1]};
                    double norm = sqrt(direction[0]*direction[0]+direction[1]*direction[1]);
                    if(norm == 0)
                        norm = 1.0;
                    direction[0] /= norm;
                    direction[1] /= norm;
                    currentPosition[0] += mu*direction[0];
                    currentPosition[1] += mu*direction[1];
                    for(int k=0;k<obstacles.size();k++)
                    {
                        double distance2 = (obstacles[k][0]-currentPosition[0])*(obstacles[k][0]-currentPosition[0]) +
                                (obstacles[k][1]-currentPosition[1])*(obstacles[k][1]-currentPosition[1]);
                        if(distance2<robot_constants::OBSTACLE_RADIUS*robot_constants::OBSTACLE_RADIUS)
                        {
                            std::cout << obstacles[k][0] << " " << obstacles[k][1] << std::endl;
                            std::cout << currentPosition[0] << " " << currentPosition[1] << std::endl;
                            numberOfCollisions++;
                            break;
                        }
                    }
                    if(std::isnan(direction[0]))
                    {
                        std::cout << "NAN" << std::endl;
                    }

                }
                double distance = sqrt((goal[0]-currentPosition[0])*(goal[0]-currentPosition[0])+(goal[1]-currentPosition[1])*(goal[1]-currentPosition[1]));
                pathLength[i]+= distance/numberOfPlannings;
            }
            if(!clearBetweenIterations)
                planner.clearPathPlanner();

        }

        std::cout << "Collision count: " << numberOfCollisions << std::endl;



        return pathLength;
    }
}