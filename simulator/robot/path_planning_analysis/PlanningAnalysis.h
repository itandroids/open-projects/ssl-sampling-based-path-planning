//
// Created by felipe on 14/05/20.
//

#ifndef SIMULATOR_PLANNINGANALYSIS_H
#define SIMULATOR_PLANNINGANALYSIS_H

#include <random>
#include <path_planning/PathPlanning.h>
#include <constants/Constants.h>
#include <clock/Clock.h>
#include <world_model/RandomStateGenerator.h>

namespace planning_analysis {

    /**
     * Returns result of analysis
     */
    struct AnalysisResult {
        AnalysisResult(int size);

        // Stores amount of time spent to evaluate path
        std::vector<double> timeResult;

        // Vector to store whether the planning suceeded or not
        std::vector<bool> planningSucceeded;

        // Vector to store computation times used on success probability
        std::vector<double> computationTimeSequence;

        // Vector to store success probability for each planner
        std::vector<double> successProbability;

        // Vector to store standard deviation of average cost
        std::vector<double> stdDev;

        // Vector to store average path cost obtained
        std::vector<double> avgCost;

        // Best cost
        std::vector<double> bestCost;

        // Average time over path planning done
        double averageTime;

        /**
         * print result using a stream
         * @param out
         */
        void print(std::ostream &out);

    };

    typedef struct AnalysisResult AnalysisResult;




    /**
     * Class to analyze path planning
     */
    class PlanningAnalysis {
    private:
        tools::Clock clock;
        random_state::RandomStateGenerator randomStateGen;
        std::vector<double> boundsStart;
        std::vector<double> boundsGoal;

    public:
        /**
         * Constructor for PlanningAnalysis class
         * @param boundsStart bounds to define start state
         * @param boundsGoal bounds to define goal state
         */
        PlanningAnalysis(const std::vector<double>& boundsStart, const std::vector<double>& boundsGoal);

        /**
         * Constructor for PlanningAnalysis class
         * @param boundsStart bounds to define start state
         * @param boundsGoal bounds to define goal state
         * @param seed random generator seed
         */
        PlanningAnalysis(const std::vector<double>& boundsStart, const std::vector<double>& boundsGoal, int seed);

        ~PlanningAnalysis() = default;

        /**
         * Function to simulate iterations of the planner
         * @param planner
         * @param monteCarloIterations
         * @param numberOfObstacles
         * @return AnalysisResult
         */
        AnalysisResult MonteCarloTimingSimulation(path_planning::PathPlannerBase& planner,
                                                  int monteCarloIterations,
                                                  int numberOfObstacles = 9);

        /**
         * Returns average path length across all runs with default computing cost.
         * @param planner
         * @param MonteCarloIterations
         * @param numberOfObstacles
         * @return vector with average path length [0] and success probability [1]
         */
        std::vector<double> MonteCarloAverageLength(path_planning::PathPlannerBase& planner,
                                                  int MonteCarloIterations,
                                                  int numberOfObstacles = 9);

        /**
         * Returns a vector with path length found in the corresponding replanning. {replanning_1, replanning_2,...}
         * @param planner
         * @param trackingIterations
         * @param clearBetweenIterations clear the planner datastructures between iterations
         * @return vector with path length vs replanning number
         */
        std::vector<double> idealTrackingAnalysis(path_planning::PathPlannerBase& planner,
                                                  int trackingIterations,
                                                  bool clearBetweenIterations = true);

        /**
         * Creates linear spaced vector
         * @param min
         * @param max
         * @param spaces
         * @return linear spaced vector
         */
        std::vector<double> linearSpacedVector(double min, double max, int size);



    };
}





#endif //SIMULATOR_PLANNINGANALYSIS_H
