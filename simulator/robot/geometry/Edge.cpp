//
// Created by francisco on 02/02/17.
//

#include "Edge.h"
#include "Triangle.h"

namespace itandroids_lib{
namespace geometry{


Edge::Edge(int id, Vertex* v0, Vertex* v1) {
    E_id = id;
    E_vertices[0] = v0;
    E_vertices[1] = v1;
    std::fill_n(E_triangles, 2, static_cast<Triangle*>(0));
}

Edge::~Edge() {
}

Vertex* Edge::vertex(std::size_t i) {
    return E_vertices[i];
}

void Edge::removeTriangle(TrianglePtr tri) {
    if (E_triangles[0] == tri) {
        E_triangles[0] = static_cast<Triangle*>(0);
    }
    if (E_triangles[1] == tri) {
        E_triangles[1] = static_cast<Triangle*>(0);
    }
}

void Edge::setTriangle(TrianglePtr tri) {
    if (E_triangles[0] == tri)
        return;
    if (E_triangles[1] == tri)
        return;
    if (!E_triangles[0])
        E_triangles[0] = tri;
    else if (!E_triangles[1])
        E_triangles[1] = tri;
}

int Edge::getId() {
    return E_id;
}

Vertex* Edge::getVertex(std::size_t index) {
    return E_vertices[index];
}

Triangle* Edge::getTriangle(const std::size_t index) {
    return E_triangles[index];
}

bool Edge::hasVertex(const Vertex* v) {
    return (E_vertices[0] == v || E_vertices[1] == v);
}
}
}