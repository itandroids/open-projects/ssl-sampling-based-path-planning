//
// Created by muzio on 10/04/16.
//

#ifndef ITANDROIDS_LIB_GEOMETRY_LINE2D_H
#define ITANDROIDS_LIB_GEOMETRY_LINE2D_H

#include <cmath>
#include "../math/Vector2.h"
#include <iostream>

namespace itandroids_lib{
namespace geometry {

typedef math::Vector2<double> Point2D;
typedef double AngRad;

#ifndef EPSILON
const double EPSILON = 0.0001;
#endif
#ifndef TWOPI
#define TWOPI (M_PI*2)
#endif

/* This method normalizes an angle. This means that the resulting angle lies
        between -PI and PI degrees.
\param ang the angle which must be normalized
\return the result of normalizing the given angle */
static AngRad normalizeAngle(AngRad ang) {
    while (ang > M_PI) ang -= TWOPI;
    while (ang < -M_PI) ang += TWOPI;

    return (ang);
}

/****************/
/* Class Line2D */
/****************/

/*!This class contains the representation of a line. A line is defined
  by the formula ay + bx + c = 0. */
class Line2D {
public:
    // a line is defined by the formula: ay + bx + c = 0
    double m_a;
    /*!< This is the a coefficient in the line ay + bx + c = 0 */
    double m_b;
    /*!< This is the b coefficient in the line ay + bx + c = 0 */
    double m_c;
    /*!< This is the c coefficient in the line ay + bx + c = 0 */

    Point2D start;
    Point2D end;
    bool isLineSegment;

    Point2D center;

    Line2D ();

    Line2D(double a, double b, double c);

    Line2D(const Point2D &p1, const Point2D &p2);

    Line2D(const Point2D &p, const AngRad &ang);

    Line2D operator-() const;


    // get intersection points with this line
    Point2D getIntersection(Line2D line) const;

    AngRad getMyAngleWith(Line2D line) const;

    AngRad getAngleToLine(const Line2D &line) const;

    Point2D getMyIntersection(Line2D line) const;

    //int     getCircleIntersectionPoints( Circle  circle,
    //				       Point2D *pSolution1,
    //			       Point2D *pSolution2 ) const;
    Line2D getOrthogonalLine(const Point2D &p) const;

    Point2D getPointOnLineClosestTo(const Point2D &p) const;

    Point2D getPointOnLineSegClosestTo(Point2D p) const;

    //Point2D getPointOnLineSegClosestTo2( Point2D p ) const;
    bool intersectWithLineSeg(Line2D l, Point2D &pt) const;


    double getDistanceToPoint(Point2D p) const;

    bool isInBetween(Point2D p,
                     Point2D p1,
                     Point2D p2) const;

    // calculate associated variables in the line
    double getYGivenX(double x) const;

    double getXGivenY(double y) const;

    double getACoefficient() const;

    double getBCoefficient() const;

    double getCCoefficient() const;

    double getSlope() const;

    double getYIntercept() const;

    double getXIntercept() const;

    const Point2D& getStart() const;

    const Point2D& getEnd() const;

    const Point2D& getCenter() const;

    // static methods to make a line using an easier representation.
    static Line2D makeLineFromTwoPoints(Point2D p1,
                                                Point2D p2);

    static Line2D makeLineFromPositionAndAngle(Point2D p,
                                                       AngRad ang);
};

} // namespace geometry
} // namespace itandroids_lib


#endif //ITANDROIDS_LIB_GEOMETRY_LINE2D_H
