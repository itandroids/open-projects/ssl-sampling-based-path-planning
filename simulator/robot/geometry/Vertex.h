//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_VERTEX_H
#define ITANDROIDS_LIB_VERTEX_H

#include "Vector2D.h"

namespace itandroids_lib {
namespace geometry {
class Vertex {
private:
    int V_id;
    Vector2D V_pos;

public:
    Vertex() :
            V_id(0) {
    }
    Vertex(int id, Vector2D & p) {
        V_id = id;
        V_pos = p;
    }

    Vertex(int id, double & xx, double & yy) {
        V_id = id;
        V_pos.x = xx;
        V_pos.y = yy;
    }

    ~Vertex() {
    }

    Vertex & assign(int id, Vector2D & pos) {
        V_id = id;
        V_pos = pos;
        return *this;
    }

    Vertex & assign(int id, double & xx, double & yy) {
        V_id = id;
        V_pos.x = xx;
        V_pos.y = yy;
        return *this;
    }

    int getId() {
        return V_id;
    }

    Vector2D & pos() {
        return V_pos;
    }

};

}
}

#endif //ITANDROIDS_LIB_VERTEX_H
