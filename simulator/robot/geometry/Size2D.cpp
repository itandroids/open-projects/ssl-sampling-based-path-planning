//
// Created by francisco on 02/02/17.
//

#include "Size2D.h"

namespace itandroids_lib{
namespace geometry{

Size2D::Size2D() {
    S_length = 0;
    S_width = 0;
}
Size2D::Size2D(double & length, double & width) {
    S_length = length;
    S_width = width;
}
Size2D::Size2D(double length, double width) {
    S_length = length;
    S_width = width;
}

double & Size2D::length() {
    return S_length;
}

double & Size2D::width() {
    return S_width;
}
}
}
