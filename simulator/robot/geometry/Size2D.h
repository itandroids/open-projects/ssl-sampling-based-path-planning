//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_SIZE2D_H
#define ITANDROIDS_LIB_SIZE2D_H

namespace itandroids_lib {
namespace geometry {

class Size2D {
private:
    double S_length;
    double S_width;
public:
    Size2D();

    Size2D(double &length, double &width);

    Size2D(double length, double width);

    double &length();

    double &width();

};

}
}


#endif //ITANDROIDS_LIB_SIZE2D_H
