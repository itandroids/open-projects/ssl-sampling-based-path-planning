//delaunay_triangulation.h includes


#ifndef SOURCE_GEOMETRY_DELAUNAY_TRIANGULATION_H_
#define SOURCE_GEOMETRY_DELAUNAY_TRIANGULATION_H_
//triangle_2d.cpp includes = line_2d.h includes
#include <cmath>

#include <boost/array.hpp>
#include <algorithm>
#include <map>
#include <vector>
#include "Vertex.h"
#include "Edge.h"
#include "Triangle.h"
#include "Rect2D.h"
#include "Vector2D.h"
//line_2d.cpp includes
//#include <iostream>
//#include <limits>

// vector_2d includes
// #include <limits>
// #include <cmath>
// #include <functional> desnecessario
#include <iostream>

namespace itandroids_lib {
namespace geometry {

typedef std::vector<Vertex> VertexCont;
typedef std::map<int, EdgePtr> EdgeCont;
typedef std::map<int, TrianglePtr> TriangleCont;

class DelaunayTriangulation {
private:
	int edgeCounter;
	int triCounter;
	Vertex initialVertex[3];
	EdgePtr initialEdge[3];
	VertexCont M_vertices;
	EdgeCont M_edges;

	TriangleCont M_triangles;

public:
	static const double EPSILON;

	enum ContainedType {
		NOT_CONTAINED, CONTAINED, ONLINE, SAME_VERTEX,
	};

	DelaunayTriangulation();
	DelaunayTriangulation(Rect2D & region);
	~DelaunayTriangulation();
	void clear();
	void clearResults();

	Triangle * findTriangleContains(Vector2D & pos);

	Vertex * findNearestVertex(Vector2D & pos);

	const VertexCont & getVertices() {
		return M_vertices;
	}

	const EdgeCont & getEdges() {
		return M_edges;
	}

	const TriangleCont & getTriangles() {
		return M_triangles;
	}

	int addVertex(double x, double y);

	void addVertices(std::vector<Vector2D> & v);

	const Vertex* getVertex(const int id);

	void compute();

	const Triangle* findTriangleContains(const double x, const double y);
	const Vertex* findNearestVertex(const double x, const double y);

private:

	void createInitialTriangle();
	void createInitialTriangle(Rect2D & region);
	void removeInitialVertices();

	bool updateContainedVertex(Vertex* vertex, TrianglePtr tri);
	bool updateOnlineVertex(Vertex* vertex, TrianglePtr tri);
	bool legalizeEdge(TrianglePtr new_tri, Vertex* new_vertex,
			EdgePtr old_edge);

	ContainedType findTriangleContains(double x, double y, TrianglePtr* sol);
	ContainedType findTriangleContains(Vector2D & pos, TrianglePtr * sol);

	void removeEdge(int id) {
		EdgeCont::iterator it = M_edges.find(id);
		if (it != M_edges.end()) {
			delete it->second;
			M_edges.erase(it);
		}
	}

	void removeEdge(Edge* edge) {
		if (edge) {
			removeEdge(edge->getId());
		}
	}

	void removeTriangle(int id) {
		TriangleCont::iterator it = M_triangles.find(id);
		if (it != M_triangles.end()) {
			delete it->second;
			M_triangles.erase(it);
		}
	}

	void removeTriangle(TrianglePtr tri) {
		if (tri) {
			removeTriangle(tri->getId());
		}
	}

	EdgePtr createEdge(Vertex* v0, Vertex* v1) {
		EdgePtr ptr = new Edge(edgeCounter++, v0, v1);
		M_edges.insert(EdgeCont::value_type(ptr->getId(), ptr));
		return ptr;
	}

	TrianglePtr createTriangle(Edge* e0, Edge* e1, Edge* e2) {
		TrianglePtr ptr = new Triangle(triCounter++, e0, e1, e2);
		M_triangles.insert(TriangleCont::value_type(ptr->getId(), ptr));

		return ptr;
	}

};
}
}
#endif /* SOURCE_GEOMETRY_DELAUNAY_TRIANGULATION */
