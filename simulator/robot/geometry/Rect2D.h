//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_RECT2D_H
#define ITANDROIDS_LIB_RECT2D_H

#include "Size2D.h"
#include "Vector2D.h"

namespace itandroids_lib{
namespace geometry{

class Rect2D {
private:
    Vector2D M_top_left;
    Size2D M_size;
public:

    Rect2D(Vector2D top_left, Vector2D bottom_right);

    double & left();
    double right();

    double & top();

    double bottom();

    Size2D & size();

    Vector2D center();

};
}
}



#endif //ITANDROIDS_LIB_RECT2D_H
