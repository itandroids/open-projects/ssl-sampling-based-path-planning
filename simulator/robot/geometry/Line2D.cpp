//
// Created by luis on 13/03/16.
//

#include "Line2D.h"
#include "GeometryUtils.h"

namespace itandroids_lib{
namespace geometry{

    /****************/
    /* Class Line2D */
    /****************/
    Line2D::Line2D() {
        m_a = 0;
        m_b = 0;
        m_c = 0;
        isLineSegment = false;
    }

    /*! This constructor creates a line by given the three coefficents of the line.
    A line is specified by the formula ay + bx + c = 0.
    \param a a coefficients of the line
    \param b b coefficients of the line
    \param c c coefficients of the line */
    Line2D::Line2D(double a, double b, double c) {
        m_a = a;
        m_b = b;
        m_c = c;
        isLineSegment = false;
    }

    /*! This constructor creates a line given two points.
    \param p1 first point
    \param p2 second point */
    Line2D::Line2D(const Point2D &p1, const Point2D &p2) {
        /*// Changing to a different (and I think better formula) MQ 16/10/2009
        m_a = p1.getY() - p2.getY();
        m_b = p2.getX() - p1.getX();
        m_c = p2.getY()*p1.getX() - p2.getX()*p1.getY();

        //normalize a,b,c
        double denom=sqrt(pow(m_a,2)+pow(m_b,2));
        m_a=m_a/denom;
        m_b=m_b/denom;
        m_c=m_c/denom;

        center = (p1 + p2) / 2;
        */

        // 1*y + bx + c = 0 => y = -bx - c
        // with -b the direction coefficient (or slope)
        // and c = - y - bx
        m_a = 1.0;
        double dTemp = p2.x - p1.x; // determine the slope
        if (fabs(dTemp) < EPSILON) {
            // ay + bx + c = 0 with vertical slope=> a = 0, b = 1
            m_a = 0.0;
            m_b = 1.0;
        }
        else {
            // y = (-b)x -c with -b the slope of the line
            m_a = 1.0;
            m_b = -(p2.y - p1.y) / dTemp;
        }
        // ay + bx + c = 0 ==> c = -a*y - b*x
        m_c = -m_a * p2.y - m_b * p2.x;

        center = (p1 + p2) / 2;

        //normalise a,b,c
        double denom = sqrt(pow(m_a, 2) + pow(m_b, 2));
        m_a = m_a / denom;
        m_b = m_b / denom;
        m_c = m_c / denom;

        isLineSegment = true;
        //start = Point2D(p1.x, p1.y);
        //end = Point2D(p2.x, p2.y);
        start = p1;
        end = p2;
        center = (p1 + p2) / 2;
    }

    /*! This constructor creates a line given a position and an angle.
    \param p position through which the line passes
    \param ang direction of the line */
    Line2D::Line2D(const Point2D &p, const AngRad &ang) {
        // calculate point somewhat further in direction 'angle' and make
        // line from these two points.
        Point2D other(p.x + 100 * cos(ang), p.y + 100 * sin(ang)); //I'VE CHANGED HERE
        *this = Line2D(p, other);
        isLineSegment = false;
    }

    Line2D Line2D::operator-() const {

        return Line2D(m_a, m_b, -m_c);

    }

    /*! This method returns the intersection point between the current Line and
    the specified line.
    \param line line with which the intersection should be calculated.
    \return Point2D position that is the intersection point. */
    Point2D Line2D::getIntersection(Line2D line) const {
        Point2D p;
        double x, y;

        if (getSlope() == line.getSlope()) // lines are parallel, no intersection
        {
            return p;
        }
        if (m_a == 0)               // bx + c = 0 and a2*y + b2*x + c2 = 0 ==> x = -c/b
        {   // calculate x using the current line
            x = -m_c / m_b;                // and calculate the y using the second line
            y = line.getYGivenX(x);
        }
        else if (line.getACoefficient() == 0) {   // ay + bx + c = 0 and b2*x + c2 = 0 ==> x = -c2/b2
            x = -line.getCCoefficient() / line.getBCoefficient(); // calculate x using
            y = getYGivenX(x);       // 2nd line and calculate y using current line
        }
            // ay + bx + c = 0 and a2y + b2*x + c2 = 0
            // y = (-b2/a2)x - c2/a2
            // bx = -a*y - c =>  bx = -a*(-b2/a2)x -a*(-c2/a2) - c ==>
            // ==> a2*bx = a*b2*x + a*c2 - a2*c ==> x = (a*c2 - a2*c)/(a2*b - a*b2)
            // calculate x using the above formula and the y using the current line
        else {
            x = (m_a * line.getCCoefficient() - line.getACoefficient() * m_c) /
                (line.getACoefficient() * m_b - m_a * line.getBCoefficient());
            y = getYGivenX(x);
        }

        return Point2D(x, y);
    }

    /*! This method returns the orthogonal line to a Point2D. This is the
    line between the specified position and the closest point on the
    line to this position.  \param p Point2D point with which tangent
    line is calculated.  \return Line line tangent to this position */
    Line2D Line2D::getOrthogonalLine(const Point2D &p) const {
        // ay + bx + c = 0 -> y = (-b/a)x + (-c/a)
        // tangent: y = (a/b)*x + C1 -> by - ax + C2 = 0 => C2 = ax - by
        // with p.y = y, p.x = x
        return Line2D(m_b, -m_a, m_a * p.x - m_b * p.y);
    }


    /**
    * Mohan::This method returns the absolute value of the acute angle
    * between the current line and the line sent in as input. \param
    * line: line with which the angle is to be determined. \return
    * Angrad: angle between the current line and input line...
    */
    AngRad Line2D::getMyAngleWith(Line2D line) const {
        double ang = 0;

        double m1, m2;

        // In our case we shall never have a case with a line that is
        // parallel to the y-axis but I am checking for that as a
        // matter-of-fact...
        // Both lines parallel to y-axis...
        if (m_a <= EPSILON && line.getACoefficient() <= EPSILON) {
            return ang;
        }
            // Current line parallel to y-axis...
        else if (fabs(m_a) <= EPSILON) {
            ang = atan(fabs(line.getACoefficient() / line.getBCoefficient()));
            return ang;
        }
            // Input line parallel to y-axis...
        else if (fabs(line.getACoefficient()) <= EPSILON) {
            ang = atan(fabs(m_a / m_b));
            return ang;
        }
            // Neither line parallel to the y-axis...
        else {
            m1 = -m_b / m_a;
            m2 = -line.getBCoefficient() / line.getACoefficient();
            ang = atan(fabs(m2 - m1) / (1 + m1 * m2));
            return fabs(ang);
        }
    }


    /**
    * Mohan::This method returns the value of the normalized angle
    * between the current line and the line sent in as input. \param
    * line: line with which the angle is to be determined. \return
    * Angrad: angle between the current line and input line...
    */
    AngRad Line2D::getAngleToLine(const Line2D &line) const {
        double ang = 0;

        double m1, m2;

        // In our case we shall never have a case with a line that is
        // parallel to the y-axis but I am checking for that as a
        // matter-of-fact...
        // Both lines parallel to y-axis...
        if (fabs(m_a) <= EPSILON && fabs(line.getACoefficient()) <= EPSILON) {
            return ang;
        }
            // Current line parallel to y-axis...
        else if (fabs(m_a) <= EPSILON) {
            ang = atan(fabs(line.getACoefficient() / line.getBCoefficient()));
            return normalizeAngle(ang);
        }
            // Input line parallel to y-axis...
        else if (fabs(line.getACoefficient()) <= EPSILON) {
            ang = atan(fabs(m_a / m_b));
            return normalizeAngle(ang);
        }
            // Neither line parallel to the y-axis...
        else {
            m1 = -m_b / m_a;
            m2 = -line.getBCoefficient() / line.getACoefficient();
            ang = atan(fabs(m2 - m1) / (1 + m1 * m2));
            return normalizeAngle(ang);
        }
    }

    /**
    * Mohan::This method returns the intersection point between the
    * current Line and the specified line.  \param line line with which
    * the intersection should be calculated.  \return Point2D position
    * that is the intersection point. */
    Point2D Line2D::getMyIntersection(Line2D line) const {
        Point2D p(0, 0);
        double x, y;

        double SLOPE_DIFF = EPSILON * 10;

        double y_num = m_b * line.getCCoefficient() - line.getBCoefficient() * m_c;
        double denom = m_a * line.getBCoefficient() - line.getACoefficient() * m_b;

        // If the two lines are parallel, return the zero vector as the
        // intersection point...
        if (fabs(denom) <= SLOPE_DIFF) {
            return p;
        }

        double x_num = line.getACoefficient() * m_c - m_a * line.getCCoefficient();

        x = x_num / denom;
        y = y_num / denom;
        return Point2D(x, y);
    }

    /*! This method returns the closest point on a line to a given position.
    \param p point to which closest point should be determined
    \return Point2D closest point on line to 'p'. */
    Point2D Line2D::getPointOnLineClosestTo(const Point2D &p) const {
        Line2D l2 = getOrthogonalLine(p);  // get tangent line
        return getMyIntersection(l2);     // and intersection between the two lines
    }

    ///*! This method returns the closest point on a line segment to a given position.
    //  \param p point to which closest point should be determined
    //  \return Point2D closest point on line segment to 'p'. */
    //Point2D Line2D::getPointOnLineSegClosestTo2( Point2D p ) const
    //{
    //  double ab = start.getDistanceTo(p);
    //  double bc = p.getDistanceTo(end);
    //  double ac = start.getDistanceTo(end);
    //  if (abs(ab + bc - ac) < 0.001) {
    //  	return p;
    //  } else {
    //  	if (bc < ab) {
    //  		return end;
    //  	} else {
    //  		return start;
    //  	}
    //  }
    //}

    Point2D Line2D::getPointOnLineSegClosestTo(Point2D p) const {
        double px = end.x - start.x;
        double py = end.y - start.y;
        double norm_squared = std::max(px * px + py * py, 1e-10);
        double u = ((p.x - start.x) * px + (p.y - start.y) * py) / norm_squared;
        if (u > 1)
            u = 1;
        else if (u < 0)
            u = 0;
        Point2D myPoint = Point2D(start.x + u * px, start.y + u * py);
        return myPoint;
    }

    bool Line2D::intersectWithLineSeg(Line2D l, Point2D &pt) const {
        double x1 = start.x;
        double x2 = end.x;
        double x3 = l.start.x;
        double x4 = l.end.x;

        double y1 = start.y;
        double y2 = end.y;
        double y3 = l.start.y;
        double y4 = l.end.y;

        double denom = (y4 - y3) * (x2 - x1) + (x4 - x3) * (y2 - y1);
        if (fabs(denom) < 1e-10) {
            return false;
        }
        double ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denom;
        double ub = ((x2 - x1) * (y1 - y3) + (y2 - y1) * (x1 - x3)) / denom;

        if (0 < ua and ua < 1 and 0 < ub and ub < 1) {
            pt.x = (x1 + ua * (x2 - x1));
            pt.y = (y1 + ua * (y2 - y1));
            return true;
        } else {
            return false;
        }
    }

    const Point2D&Line2D::getStart() const {
        return start;
    }

    const Point2D&Line2D::getEnd() const {
        return end;
    }

    /*! This method returns the distance between a specified position and the
    closest point on the given line.
    \param p position to which distance should be calculated
    \return double indicating the distance to the line. */
    double Line2D::getDistanceToPoint(Point2D p) const {
        return GeometryUtils::getDistanceTo(p, getPointOnLineClosestTo(p));
    }

    /*! This method determines whether the projection of a point on the current line
    lies between two other points ('point1' and 'point2') that lie on the same
    line.
    \param p point of which projection is checked.
    \param p1 first point on line
    \param p2 second point on line
    \return true when projection of 'p' lies between 'point1' and 'point2'.*/
    bool Line2D::isInBetween(Point2D p, Point2D p1, Point2D p2) const {
        p = getPointOnLineClosestTo(p); // get closest point
        double dDist = GeometryUtils::getDistanceTo(p1, p2); // get distance between 2 p

        // if the distance from both points to the projection is smaller than this
        // dist, the p lies in between.
        return GeometryUtils::getDistanceTo(p, p1) <= dDist &&
               GeometryUtils::getDistanceTo(p, p2) <= dDist;
    }

    /*! This method calculates the y coordinate given the x coordinate
    \param x coordinate
    \return y coordinate on this line */
    double Line2D::getYGivenX(double x) const {
        if (m_a == 0) {
            std::cout << "(Line2D::getYGivenX) Cannot calculate Y coordinate: " << std::endl;
            return 0;
        }
        // ay + bx + c = 0 ==> ay = -(b*x + c)/a
        return -(m_b * x + m_c) / m_a;
    }

    /*! This method calculates the x coordinate given the y coordinate
    \param y coordinate
    \return x coordinate on this line */
    double Line2D::getXGivenY(double y) const {
        if (m_b == 0) {
            std::cout << "(Line2D::getXGivenY) Cannot calculate X coordinate" << std::endl;
            return 0;
        }
        // ay + bx + c = 0 ==> bx = -(a*y + c)/a
        return -(m_a * y + m_c) / m_b;
    }

    /*! This method creates a line given two points.
    \param p1 first point
    \param p2 second point
    \return line that passes through the two specified points. */
    Line2D Line2D::makeLineFromTwoPoints(Point2D p1, Point2D p2) {
        return Line2D(p1, p2);
    }

    /*! This method creates a line given a position and an angle.
    \param p position through which the line passes
    \param ang direction
    of the line.
    \return line that goes through position 'p' with angle 'ang'. */
    Line2D Line2D::makeLineFromPositionAndAngle(Point2D p, AngRad ang) {
        return Line2D(p, ang);
    }

    /*! This method returns the a coefficient from the line ay + bx + c = 0.
    \return a coefficient of the line. */
    double Line2D::getACoefficient() const {
        return m_a;
    }

    /*! This method returns the b coefficient from the line ay + bx + c = 0.
    \return b coefficient of the line. */
    double Line2D::getBCoefficient() const {
        return m_b;
    }

    /*! This method returns the c coefficient from the line ay + bx + c = 0.
    \return c coefficient of the line. */
    double Line2D::getCCoefficient() const {
        return m_c;
    }

    /*! This method returns the slope of the line.  \return m the slope of
    the line. */
    double Line2D::getSlope() const {
        if (m_a == 0) {
            std::cout << "(Line2D:getSlope): divide by zero" << std::endl;
            return 1000;
        }
        return -m_b / m_a;
    }

    /*! This method returns the y-intercept of the line.
    \return b the y-intercept of the line. */
    double Line2D::getYIntercept() const {
        if (m_a == 0) {
            return -m_c;
        }
        return -m_c / m_a;
    }

    /*! This method returns the x-intercept of the line.
    \return the x-intercept of the line. */
    double Line2D::getXIntercept() const {
        if (m_b == 0) {
            return -m_c;
        }
        return -m_c / m_b;
    }

    const Point2D& Line2D::getCenter() const {
        return center;
    }

} // namespace geometry
} // namespace itandroids_lib

