//
// Created by muzio on 10/04/16.
//

#ifndef ITANDROIDS_LIB_GEOMETRYUTILS_H
#define ITANDROIDS_LIB_GEOMETRYUTILS_H

#include "Line2D.h"

namespace itandroids_lib {
namespace geometry {

class GeometryUtils {
public:
    static double getDistanceTo(const Point2D &p1, const Point2D &p2);
    static bool pointIsBetweenLines(const Point2D& p, const Line2D& line1, const Line2D& line2);
private:
};

} // namespace itandroids_lib
} // namespace geometry


#endif //ITANDROIDS_LIB_GEOMETRYUTILS_H
