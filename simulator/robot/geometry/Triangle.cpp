//
// Created by francisco on 02/02/17.
//

#include "Triangle.h"
#include "Edge.h"

namespace itandroids_lib{
namespace geometry{

Triangle::Triangle(int id, EdgePtr e0, EdgePtr e1, EdgePtr e2) {
    T_edges[0] = e0;
    T_edges[1] = e1;
    T_edges[2] = e2;
    T_id = id;

    for (std::size_t i = 0; i < 3; ++i) {
        T_edges[i]->setTriangle(this);
    }

    T_vertices[0] = T_edges[0]->vertex(0);
    T_vertices[1] = T_edges[0]->vertex(1);

    T_vertices[2] = (
            (T_vertices[0] != T_edges[1]->vertex(0)
             && T_vertices[1] != T_edges[1]->vertex(0)) ?
            T_edges[1]->vertex(0) : T_edges[1]->vertex(1));

    T_circumcenter = Triangle2D::circumcenter(T_vertices[0]->pos(),
                                              T_vertices[1]->pos(), T_vertices[2]->pos());

    T_circumradius = T_circumcenter.dist(T_vertices[0]->pos());
}

Triangle::~Triangle() {
    T_edges[0]->removeTriangle(this);
    T_edges[1]->removeTriangle(this);
    T_edges[2]->removeTriangle(this);
}

int Triangle::getId() {
    return T_id;
}

Vertex* Triangle::getVertex(std::size_t index) {
    return T_vertices[index];
}

Edge* Triangle::getEdge(std::size_t index) {
    return T_edges[index];
}

Vector2D & Triangle::getCircumcenter() {
    return T_circumcenter;
}

double & Triangle::getCircumradius() {
    return T_circumradius;
}

bool Triangle::contains(Vector2D & pos) {
    return pos.dist2(T_circumcenter) < T_circumradius * T_circumradius;
}

bool Triangle::hasVertex(Vertex * v) {
    return (v == T_vertices[0] || v == T_vertices[1] || v == T_vertices[2]);
}

bool Triangle::hasEdge(EdgePtr e) {
    return (T_edges[0] == e || T_edges[1] == e || T_edges[2] == e);
}

Vertex* Triangle::getVertexExclude(Vertex* v1, Vertex* v2) {
    for (std::size_t index = 0; index < 3; ++index) {
        if (T_vertices[index] != v1 && T_vertices[index] != v2)
            return T_vertices[index];
    }
    return static_cast<Vertex *>(0);
}

Vertex* Triangle::getVertexExclude(Edge* edge) {
    return getVertexExclude(edge->vertex(0), edge->vertex(1));
}

Edge* Triangle::getEdgeInclude(Vertex* v1, Vertex* v2) {
    for (std::size_t index = 0; index < 3; ++index) {
        if (T_edges[index]->hasVertex(v1)
            && T_edges[index]->hasVertex(v2)) {
            return T_edges[index];
        }
    }
    return static_cast<Edge*>(0);
}

Edge* Triangle::getEdgeExclude(const Vertex* v) {
    for (std::size_t index = 0; index < 3; ++index) {
        if (!T_edges[index]->hasVertex(v))
            return T_edges[index];
    }
    return static_cast<Edge *>(0);
}
}
}