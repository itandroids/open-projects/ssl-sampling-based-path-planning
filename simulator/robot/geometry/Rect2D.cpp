//
// Created by francisco on 02/02/17.
//

#include "Rect2D.h"

namespace itandroids_lib{
namespace geometry{

Rect2D::Rect2D(Vector2D top_left, Vector2D bottom_right) {
M_top_left = top_left;
Size2D R_size(std::fabs(bottom_right.x - top_left.x),
              std::fabs(bottom_right.y - top_left.y));
M_size = R_size;
if (bottom_right.x - top_left.x < 0.0)
M_top_left.x = bottom_right.x;
if (bottom_right.y - top_left.y < 0.0) {
M_top_left.y = bottom_right.y;
}
}

double & Rect2D::left() {
    return M_top_left.x;
}
double Rect2D::right() {
    return left() + size().length();
}

double & Rect2D::top() {
    return M_top_left.y;
}

double Rect2D::bottom() {
    return top() + size().width();
}

Size2D & Rect2D::size() {
    return M_size;
}

Vector2D Rect2D::center() {
    return Vector2D((left() + right()) * 0.5, (top() + bottom()) * 0.5);
}
}
}
