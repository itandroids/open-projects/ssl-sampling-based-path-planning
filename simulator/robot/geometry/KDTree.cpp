//
// Created by felipe on 28/12/20.
//

#include "KDTree.h"

namespace geometry{
    KDTree::KDTree(int dimensions):dimensions(dimensions) {
        root = nullptr;
    }

    KDTree::KDTree(std::vector<double> values,int dimensions)
    {
        root = new KDnode(values);
    }

    void KDTree::insert(std::vector<double> values) {
        insert(values,root,0);
    }

    KDTree::~KDTree(){
        deleteTree(root);
    }

    void KDTree::clear(){
        deleteTree(root);
    }

    void KDTree::deleteTree(KDnode*& node){
        if(node == nullptr)
            return;
        deleteTree(node->left);
        deleteTree(node->right);
        delete node;
        node = nullptr;
    }

    void KDTree::insert(const std::vector<double>& values, KDnode*& node, int cd) {
        if(node == nullptr)
        {
            node = new KDnode(values);
            return;
        }
        if(node->values[cd] >= values[cd])
            insert(values,node->right,(cd+offset)%dimensions);
        else{
            insert(values,node->left,(cd+offset)%dimensions);
        }
    }

    std::vector<double> KDTree::nearestNeighbor(const std::vector<double>& values) {
        return nearestNeighbor(values,root,0)->values;
    }

    std::vector<double> KDTree::nearestNeighbor(double *values) {
        return nearestNeighbor(values,root,0)->values;
    }


    KDnode* KDTree::nearestNeighbor(double* values, KDnode *root, int depth) {
        if(root == nullptr)
            return nullptr;

        KDnode* nextBranch;
        KDnode* otherBranch;

        if(root->values[depth%dimensions] < values[depth%dimensions])
        {
            nextBranch = root->right;
            otherBranch = root->left;
        }
        else
        {
            nextBranch = root->left;
            otherBranch = root->right;
        }

        KDnode* temp = nearestNeighbor(values,nextBranch,depth+offset);
        KDnode* best = closest(values,root,temp);

        double radiusSquared = (values[0]-best->values[0])*(values[0]-best->values[0]) +
                               (values[offset]-best->values[offset])*(values[offset]-best->values[offset]);

        double dist = values[depth%dimensions]-root->values[depth%dimensions];

        if(radiusSquared > dist*dist){
            temp = nearestNeighbor(values,otherBranch,depth+offset);
            best = closest(values,best,temp);
        }
        return best;
    }

    KDnode* KDTree::closest(const std::vector<double> &target, KDnode *n1, KDnode *n2) {

        /// n1 is never null pointer since n1 is a root of another child
        if(n2 == nullptr)
            return n1;

        double distanceSquared1 = (target[0]-n1->values[0])*(target[0]-n1->values[0]) +
                (target[offset]-n1->values[offset])*(target[offset]-n1->values[offset]);
        double distanceSquared2 = (target[0]-n2->values[0])*(target[0]-n2->values[0]) +
                                  (target[offset]-n2->values[offset])*(target[offset]-n2->values[offset]);

        if(distanceSquared1<distanceSquared2)
            return n1;
        return n2;
    }

    KDnode* KDTree::closest(double* target, KDnode *n1, KDnode *n2) {

        /// n1 is never null pointer since n1 is a root of another child
        if(n2 == nullptr)
            return n1;

        double distanceSquared1 = (target[0]-n1->values[0])*(target[0]-n1->values[0]) +
                                  (target[offset]-n1->values[offset])*(target[offset]-n1->values[offset]);
        double distanceSquared2 = (target[0]-n2->values[0])*(target[0]-n2->values[0]) +
                                  (target[offset]-n2->values[offset])*(target[offset]-n2->values[offset]);

        if(distanceSquared1<distanceSquared2)
            return n1;
        return n2;


    }


    KDnode* KDTree::nearestNeighbor(const std::vector<double>& values, KDnode *root, int depth) {
        if(root == nullptr)
            return nullptr;

        KDnode* nextBranch;
        KDnode* otherBranch;

        if(root->values[depth%dimensions] < values[depth%dimensions])
        {
            nextBranch = root->right;
            otherBranch = root->left;
        }
        else
        {
            nextBranch = root->left;
            otherBranch = root->right;
        }

        KDnode* temp = nearestNeighbor(values,nextBranch,depth+offset);
        KDnode* best = closest(values,root,temp);

        double radiusSquared = (values[0]-best->values[0])*(values[0]-best->values[0]) +
                               (values[offset]-best->values[offset])*(values[offset]-best->values[offset]);

        double dist = values[depth%dimensions]-root->values[depth%dimensions];

        if(radiusSquared > dist*dist){
            temp = nearestNeighbor(values,otherBranch,depth+offset);
            best = closest(values,best,temp);
        }
        return best;
    }

    void KDTree::setOffset(int offset) {
        this->offset = offset;
    }

    KDnode::KDnode(std::vector<double> values): values(values)
    {
    }


}