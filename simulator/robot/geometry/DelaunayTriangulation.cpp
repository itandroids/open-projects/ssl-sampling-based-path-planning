#include "DelaunayTriangulation.h"

//double DelaunayTriangulation::EPSILON = 1.0e-10;

namespace itandroids_lib {
namespace geometry {

const double DelaunayTriangulation::EPSILON = 1.0e-10;

std::pair<std::size_t, std::size_t> edge_pairs[3] = { std::pair<std::size_t,
		std::size_t>(0, 1), std::pair<std::size_t, std::size_t>(1, 2),
		std::pair<std::size_t, std::size_t>(2, 0) };

DelaunayTriangulation::~DelaunayTriangulation() {
	clear();
}

void DelaunayTriangulation::clear() {
	clearResults();
	M_vertices.clear();
}

void DelaunayTriangulation::clearResults() {
	edgeCounter = triCounter = 0;

	for (TriangleCont::iterator it = M_triangles.begin();
			it != M_triangles.end(); ++it) {
		delete it->second;
	}

	for (EdgeCont::iterator it = M_edges.begin(); it != M_edges.end(); ++it) {
		delete it->second;
	}

	M_triangles.clear();
	M_edges.clear();
}

int DelaunayTriangulation::addVertex(double x, double y) {
	for (VertexCont::iterator it = M_vertices.begin(), end = M_vertices.end();
			it != end; ++it) {
		if (std::pow(it->pos().x - x, 2) + std::pow(it->pos().y - y, 2)
				< 1.0e-6) {
			return -1;
		}
	}

	int id = M_vertices.size();
	M_vertices.push_back(Vertex(id, x, y));
	return id;
}

void DelaunayTriangulation::addVertices(std::vector<Vector2D> & v) {
	M_vertices.reserve(M_vertices.size() + v.size());

	int id = M_vertices.size();

	std::vector<Vector2D>::iterator end = v.end();
	for (std::vector<Vector2D>::iterator it = v.begin(); it != end;
			++it, ++id) {
		M_vertices.push_back(Vertex(id, it->x, it->y));

	}
}

void DelaunayTriangulation::createInitialTriangle(Rect2D & region) {
	clearResults();
	double max_size = std::max(region.size().length() + 1.0,
			region.size().width() + 1.0);
	Vector2D center = region.center();
	center.x += std::max(1000.0 * max_size, 1000.0);

	initialVertex[0].assign(-1, center.x, center.y);

	center.x -= std::max(1000.0 * max_size, 1000.0);
	center.y += std::max(1000.0 * max_size, 1000.0);

	initialVertex[1].assign(-2, center.x, center.y);

	center.x -= std::max(1000.0 * max_size, 1000.0);
	center.y -= std::max(1000.0 * max_size, 1000.0);
	center.y -= std::max(1000.0 * max_size, 1000.0);

	initialVertex[2].assign(-3, center.x, center.y);

	EdgePtr edge0 = createEdge(&initialVertex[0], &initialVertex[1]);
	EdgePtr edge1 = createEdge(&initialVertex[1], &initialVertex[2]);
	EdgePtr edge2 = createEdge(&initialVertex[2], &initialVertex[0]);

	createTriangle(edge0, edge1, edge2);
}

void DelaunayTriangulation::createInitialTriangle() {
	if (M_vertices.empty()) {
		return;
	}

	VertexCont::iterator vit = M_vertices.begin();

	double min_x = vit->pos().x;
	double max_x = vit->pos().x;
	double min_y = vit->pos().y;
	double max_y = vit->pos().y;

	++vit;
	VertexCont::iterator vend = M_vertices.end();
	for (; vit != vend; ++vit) {
		if (vit->pos().x < min_x)
			min_x = vit->pos().x;
		else if (max_x < vit->pos().x) {
			max_x = vit->pos().x;
		}

		if (vit->pos().y < min_y)
			min_y = vit->pos().y;
		else if (max_y < vit->pos().y)
			max_y = vit->pos().y;
	}
	Rect2D auxRect = Rect2D(Vector2D(min_x - 1.0, min_y - 1.0),
			Vector2D(max_x + 1.0, max_y + 1.0));
	createInitialTriangle(auxRect);
}

void DelaunayTriangulation::removeInitialVertices() {
	std::vector<EdgePtr> removed_edges;

	EdgeCont::iterator map_end = M_edges.end();
	for (EdgeCont::iterator it = M_edges.begin(); it != map_end; ++it) {
		for (std::size_t i = 0; i < 3; ++i) {
			if (it->second->getVertex(0) == &initialVertex[i]
					|| it->second->getVertex(1) == &initialVertex[i]) {
				removed_edges.push_back(it->second);
				break;
			}
		}
	}

	std::vector<EdgePtr>::iterator edge_end = removed_edges.end();
	for (std::vector<EdgePtr>::iterator it = removed_edges.begin();
			it != edge_end; ++it) {
		removeTriangle((*it)->getTriangle(0));
		removeTriangle((*it)->getTriangle(1));

		removeEdge((*it)->getId());
	}
}

const Vertex * DelaunayTriangulation::getVertex(int id) {
	if (M_vertices.empty() || id < 0
			|| static_cast<int>(M_vertices.size()) < id) {
		return static_cast<Vertex *>(0);
	}
	return &M_vertices[id];
}

Triangle * DelaunayTriangulation::findTriangleContains(Vector2D & pos) {
	Triangle * tri = static_cast<TrianglePtr>(0);
	findTriangleContains(pos, &tri);
	return tri;
}

Vertex * DelaunayTriangulation::findNearestVertex(Vector2D & pos) {
	Vertex * candidate = static_cast<Vertex *>(0);

	double min_dist2 = 10000000.0;
	VertexCont::iterator end = M_vertices.end();
	for (VertexCont::iterator it = M_vertices.begin(); it != end; ++it) {
		double d2 = it->pos().dist2(pos);
		if (d2 < min_dist2) {
			candidate = &(*it);
			min_dist2 = d2;
		}
	}

	return candidate;
}

void DelaunayTriangulation::compute() {

	if (M_vertices.size() < 3) {
		removeInitialVertices();
		return;
	}

	if (M_triangles.empty() || M_triangles.size() > 3) {
		createInitialTriangle();
	}

	int loop = 0;
	for (VertexCont::iterator vit = M_vertices.begin(), end = M_vertices.end();
			vit != end; ++vit) {
		++loop;
		TrianglePtr tri = static_cast<TrianglePtr>(0);
		ContainedType type = findTriangleContains(vit->pos(), &tri);

		if (!tri || type == NOT_CONTAINED) {
			clearResults();
			return;
		}

		if (type == CONTAINED) {
			if (!updateContainedVertex(&(*vit), tri)) {
				clearResults();
				return;
			}
		} else {
			if (!updateOnlineVertex(&(*vit), tri)) {
				clearResults();
				return;
			}
		}
	}
	removeInitialVertices();

// SAÍDA DA TELA DO RESULTADO

}

bool DelaunayTriangulation::updateContainedVertex(Vertex * new_vertex,
		TrianglePtr tri) {
	EdgePtr new_edges[3];
	for (std::size_t i = 0; i < 3; ++i) {
		new_edges[i] = createEdge(new_vertex, tri->getVertex(i));
	}

	EdgePtr old_edges[3]; // edges of 'tri'
	TrianglePtr new_tri[3];

	for (std::size_t i = 0; i < 3; ++i) {
		old_edges[i] = tri->getEdgeInclude(
				new_edges[edge_pairs[i].first]->getVertex(1),
				new_edges[edge_pairs[i].second]->getVertex(1));

		old_edges[i]->removeTriangle(tri);
		new_tri[i] = createTriangle(old_edges[i],
				new_edges[edge_pairs[i].first],
				new_edges[edge_pairs[i].second]);
		if (!new_tri[i]->getCircumcenter().isValid()) {
			return false;
		}
	}
	removeTriangle(tri);

	for (std::size_t i = 0; i < 3; ++i) {
		if (!legalizeEdge(new_tri[i], new_vertex, old_edges[i])) {
			return false;
		}
	}
	return true;
}

bool DelaunayTriangulation::updateOnlineVertex(Vertex * new_vertex,
		TrianglePtr tri) {
	int online_count = 0;
	EdgePtr online_edge = static_cast<EdgePtr>(0);
	for (std::size_t i = 0; i < 3; ++i) {
		Vector2D rel0(tri->getEdge(i)->getVertex(0)->pos() - new_vertex->pos());
		Vector2D rel1(tri->getEdge(i)->getVertex(1)->pos() - new_vertex->pos());

		if (std::fabs(rel0.outerProduct(rel1)) <= EPSILON) {
			online_edge = tri->getEdge(i);
			++online_count;
		}
	}

	if (online_count >= 2) {
		return false;
	}

	if (!online_edge) {
		return false;
	}

	EdgePtr new_edge[2];
	for (std::size_t i = 0; i < 2; ++i) {
		new_edge[i] = createEdge(new_vertex, online_edge->getVertex(i));
	}

	TrianglePtr new_tri_in_tri[2];
	EdgePtr old_edge_in_tri[2];

	{
		Vertex * tri_vertex = tri->getVertexExclude(online_edge);

		if (!tri_vertex) {
			return false;
		}

		EdgePtr new_edge_in_tri = createEdge(new_vertex, tri_vertex);

		for (std::size_t i = 0; i < 2; ++i) {
			old_edge_in_tri[i] = tri->getEdgeInclude(new_edge[i]->getVertex(1),
					tri_vertex);

			old_edge_in_tri[i]->removeTriangle(tri);

			new_tri_in_tri[i] = createTriangle(old_edge_in_tri[i], new_edge[i],
					new_edge_in_tri);
			if (!new_tri_in_tri[i]->getCircumcenter().isValid()) {
				return false;
			}
		}
	}

	Triangle * adjacent = (
			online_edge->getTriangle(0) == tri ?
					online_edge->getTriangle(1) : online_edge->getTriangle(0));
	bool exist_adjacent = (adjacent ? true : false);

	TrianglePtr new_tri_in_adjacent[2];
	EdgePtr old_edge_in_adjacent[2];

	if (exist_adjacent) {
		Vertex * adjacent_vertex = adjacent->getVertexExclude(online_edge);

		if (!adjacent_vertex) {
			return false;
		}

		EdgePtr new_edge_in_adjacent = createEdge(new_vertex, adjacent_vertex);

		for (std::size_t i = 0; i < 2; ++i) {
			old_edge_in_adjacent[i] = adjacent->getEdgeInclude(
					new_edge[i]->getVertex(1), adjacent_vertex);
			old_edge_in_adjacent[i]->removeTriangle(adjacent);
			new_tri_in_adjacent[i] = createTriangle(old_edge_in_adjacent[i],
					new_edge[i], new_edge_in_adjacent);
			if (!new_tri_in_adjacent[i]->getCircumcenter().isValid()) {
				return false;
			}
		}
	}

	removeTriangle(tri);
	if (exist_adjacent) {
		removeTriangle(adjacent);
	}
	removeEdge(online_edge);

	for (std::size_t i = 0; i < 2; ++i) {
		if (!legalizeEdge(new_tri_in_tri[i], new_vertex, old_edge_in_tri[i])) {
			return false;
		}
	}

	if (exist_adjacent) {
		for (std::size_t i = 0; i < 2; ++i) {
			if (!legalizeEdge(new_tri_in_adjacent[i], new_vertex,
					old_edge_in_adjacent[i])) {
				return false;
			}
		}
	}

	return true;
}

bool DelaunayTriangulation::legalizeEdge(TrianglePtr new_tri,
		Vertex * new_vertex, EdgePtr shared_edge) {
	if (!new_tri) {
		return false;
	}

	TrianglePtr adjacent = (
			shared_edge->getTriangle(0) == new_tri ?
					shared_edge->getTriangle(1) : shared_edge->getTriangle(0));
	if (!adjacent) {
		return true;
	}

	if (!adjacent->contains(new_vertex->pos())) {
		return true;
	}

	Vertex * adjacent_vertex = adjacent->getVertexExclude(shared_edge);

	EdgePtr edge_in_new_tri[2];
	EdgePtr edge_in_adjacent[2];
	{
		std::size_t idx = 0;
		for (std::size_t i = 0; i < 3; ++i) {
			if (new_tri->getEdge(i) != shared_edge) {
				edge_in_new_tri[idx] = new_tri->getEdge(i);
				edge_in_new_tri[idx]->removeTriangle(new_tri);
				++idx;
			}
		}

		idx = 0;
		for (std::size_t i = 0; i < 3; ++i) {
			if (adjacent->getEdge(i) != shared_edge) {
				edge_in_adjacent[idx] = adjacent->getEdge(i);
				edge_in_adjacent[idx]->removeTriangle(adjacent);
				++idx;
			}
		}

		if (edge_in_adjacent[1]->hasVertex(edge_in_new_tri[0]->getVertex(0))
				|| edge_in_adjacent[1]->hasVertex(
						edge_in_new_tri[0]->getVertex(1))) {
			std::swap(edge_in_adjacent[0], edge_in_adjacent[1]);
		}
	}

	EdgePtr new_edge = createEdge(new_vertex, adjacent_vertex);

	TrianglePtr flipped_tri[2];
	for (std::size_t i = 0; i < 2; ++i) {
		flipped_tri[i] = createTriangle(new_edge, edge_in_new_tri[i],
				edge_in_adjacent[i]);
		if (!flipped_tri[i]->getCircumcenter().isValid()) {
			return false;
		}
	}

	removeTriangle(new_tri);
	removeTriangle(adjacent);
	removeEdge(shared_edge);

	for (std::size_t i = 0; i < 2; ++i) {
		if (!legalizeEdge(flipped_tri[i], new_vertex, edge_in_adjacent[i])) {
			return false;
		}
	}

	return true;
}
DelaunayTriangulation::ContainedType DelaunayTriangulation::findTriangleContains(
		Vector2D & pos, TrianglePtr * sol) {
	TriangleCont::iterator end = M_triangles.end();
	for (TriangleCont::iterator it = M_triangles.begin(); it != end; ++it) {
		TrianglePtr tri = it->second;

		if (std::fabs(tri->getCircumcenter().x - pos.x) > tri->getCircumradius()
				|| std::fabs(tri->getCircumcenter().y - pos.y)
						> tri->getCircumradius()) {
			continue;
		}

		Vector2D rel0(tri->getVertex(0)->pos() - pos);
		Vector2D rel1(tri->getVertex(1)->pos() - pos);
		Vector2D rel2(tri->getVertex(2)->pos() - pos);

		double outer0 = rel0.outerProduct(rel1);
		double outer1 = rel1.outerProduct(rel2);
		double outer2 = rel2.outerProduct(rel0);

		if (std::fabs(outer0) <= EPSILON) {
			if (rel0.x * rel1.x > EPSILON || rel0.y * rel1.y > EPSILON) {
				continue;
			}
			*sol = tri;
			return ONLINE;
		}

		if (std::fabs(outer1) <= EPSILON) {
			if (rel1.x * rel2.x > EPSILON || rel1.y * rel2.y > EPSILON) {
				continue;
			}
			*sol = tri;
			return ONLINE;
		}

		if (std::fabs(outer2) <= EPSILON) {
			if (rel2.x * rel0.x > EPSILON || rel2.y * rel0.y > EPSILON) {
				continue;
			}
			*sol = tri;
			return ONLINE;
		}

		if ((outer0 >= 0.0 && outer1 >= 0.0 && outer2 >= 0.0)
				|| (outer0 <= 0.0 && outer1 <= 0.0 && outer2 <= 0.0)) {
			*sol = tri;
			return CONTAINED;
		}
	}

	return NOT_CONTAINED;
}

DelaunayTriangulation::DelaunayTriangulation(Rect2D & region) {
	createInitialTriangle(region);
}

DelaunayTriangulation::DelaunayTriangulation() {
	edgeCounter = 0;
	triCounter = 0;
}
}
}
