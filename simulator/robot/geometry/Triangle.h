//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_TRIANGLE_H
#define ITANDROIDS_LIB_TRIANGLE_H

#include "Vector2D.h"
#include "Triangle2D.h"
#include "Vertex.h"
#include <boost/array.hpp>

namespace itandroids_lib{
namespace geometry{


class Edge;
class Triangle;

typedef Edge* EdgePtr;
typedef Triangle* TrianglePtr;

class Triangle {
private:
    int T_id;
    boost::array<Vertex*, 3> T_vertices;
    boost::array<EdgePtr, 3> T_edges;
    Vector2D T_circumcenter;
    double T_circumradius;

public:

    Triangle(int id, EdgePtr e0, EdgePtr e1, EdgePtr e2);

    ~Triangle();

    int getId();

    Vertex* getVertex(std::size_t index);

    Edge* getEdge(std::size_t index);

    Vector2D & getCircumcenter();

    double & getCircumradius();

    bool contains(Vector2D & pos);

    bool hasVertex(Vertex * v);

    bool hasEdge(EdgePtr e);

    Vertex* getVertexExclude(Vertex* v1, Vertex* v2);
    Vertex* getVertexExclude(Edge* edge);

    Edge* getEdgeInclude(Vertex* v1, Vertex* v2);

    Edge* getEdgeExclude(const Vertex* v);
};

}
}


#endif //ITANDROIDS_LIB_TRIANGLE_H
