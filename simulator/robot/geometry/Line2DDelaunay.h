//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_LINE2DDELAUNAY_H
#define ITANDROIDS_LIB_LINE2DDELAUNAY_H

#include "Vector2D.h"

namespace itandroids_lib{
namespace geometry{

class Line2DDelaunay   // Line Formula: aX + bY + c = 0
{
private:
    double L_a;
    double L_b;
    double L_c;
    static const double EPSILON;

public:

    Line2DDelaunay() {
    }

    Line2DDelaunay(const double & a, const double & b, const double & c) {
        L_a = a;
        L_b = b;
        L_c = c;
    }

    Line2DDelaunay(const Vector2D & p1, const Vector2D & p2) {
        assign(p1, p2);
    }

    const Line2DDelaunay & assign(const Vector2D & p1, const Vector2D & p2) {
        L_a = -(p2.y - p1.y);
        L_b = p2.x - p1.x;
        L_c = -L_a * p1.x - L_b * p1.y;
        return *this;
    }

    const double & a() const {
        return L_a;
    }
    const double & b() const {
        return L_b;
    }
    const double & c() const {
        return L_c;
    }
    Vector2D intersection(const Line2DDelaunay & line) {
        return intersection(*this, line);
    }
    static Vector2D intersection(const Line2DDelaunay & line1, const Line2DDelaunay & line2) {
        double tmp = line1.a() * line2.b() - line1.b() * line2.a();

        if (std::fabs(tmp) < EPSILON) {
            Vector2D invalidated(1.79769e+308, 1.79769e+308);
            return invalidated;
        }

        return Vector2D((line1.b() * line2.c() - line2.b() * line1.c()) / tmp,
                        (line2.a() * line1.c() - line1.a() * line2.c()) / tmp);
    }

    static Line2DDelaunay perpendicular_bisector(const Vector2D & p1,
                                                 const Vector2D & p2) {
        if (std::fabs(p2.x - p1.x) < EPSILON
            && std::fabs(p2.y - p1.y) < EPSILON) {
            return Line2DDelaunay(p1, Vector2D(p1.x + 1.0, p1.y));
        }

        double tmp = (p2.x * p2.x - p1.x * p1.x + p2.y * p2.y - p1.y * p1.y)
                     * (-0.5);

        return Line2DDelaunay(p2.x - p1.x, p2.y - p1.y, tmp);
    }
};

}
}




#endif //ITANDROIDS_LIB_LINE2DDELAUNAY_H
