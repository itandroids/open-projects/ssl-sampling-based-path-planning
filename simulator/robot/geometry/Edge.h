//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_EDGE_H
#define ITANDROIDS_LIB_EDGE_H
#include "Vertex.h"
#include <algorithm>

namespace itandroids_lib{
namespace geometry{
class Triangle;
using TrianglePtr = Triangle*;
class Edge {
private:
    int E_id;
    Vertex * E_vertices[2];
    TrianglePtr E_triangles[2];

public:

    Edge(int id, Vertex* v0, Vertex* v1);

    ~Edge();

    Vertex* vertex(std::size_t i);

    void removeTriangle(TrianglePtr tri);

    void setTriangle(TrianglePtr tri);

    int getId();

    Vertex* getVertex(std::size_t index);

    Triangle* getTriangle(const std::size_t index);

    bool hasVertex(const Vertex* v);
};


} }
#endif //ITANDROIDS_LIB_EDGE_H
