//
// Created by felipe on 28/12/20.
//

#ifndef KINODYNAMIC_PLANNING_KDTREE_H
#define KINODYNAMIC_PLANNING_KDTREE_H

#include <iostream>
#include <vector>


namespace geometry {

    /**
     * Defines KD Tree node
     */
    class KDnode{
    public:
        /**
         * KDNode constructor
         * @param values
         */
        KDnode(std::vector<double> values);

        KDnode* left = nullptr;

        KDnode* right = nullptr;

        std::vector<double> values;

    };


    /**
     * Defines KD Tree
     */
    class KDTree {
    public:
        /**
         * Initalize KDTree with its dimensions
         * @param dimensions number of dimensions
         */
        KDTree(int dimensions);

        /**
         * Initialize KDTree with first node value and its dimensions
         * @param values
         * @param dimensions
         */
        KDTree(std::vector<double> values, int dimensions);

        /**
         * KDTree destructor. Recursively deletes all nodes.
         */
        ~KDTree();

        /**
         * Recursively deletes all nodes.
         */
        void clear();

        /**
         * Insert node to KD Tree
         * @param values
         */
        void insert(std::vector<double> values);

        /**
         * API to call nearest neighbor operation
         * @param values target value
         * @return nearest value
         */
        std::vector<double> nearestNeighbor(const std::vector<double>& values);

        /**
         * API to call nearest neighbor operation.
         * @param values target value
         * @return nearest value
         */
        std::vector<double> nearestNeighbor(double* values);

        /**
         * Changes KD-Tree offset (defaulted to 1).
         * @param offset offset value.
         */
        void setOffset(int offset);


    private:
        /// Tree dimension
        int dimensions;

        /// Tree dimension offset
        int offset=1;

        /**
         * Recursively inserts node to tree.
         * @param values value of the node
         * @param node (sub-)tree root to be inserted
         * @param cd coordinate to be considered for comparison
         */
        void insert(const std::vector<double>& values, KDnode*& node, int cd);

        /**
         * Recursively deletes nodes
         * @param node to be deleted.
         */
        void deleteTree(KDnode*& node);

        /**
         * Function to find nearest node from a given target.
         * @param values target.
         * @param root (sub-)tree root.
         * @param depth tree depth.
         * @return nearest node
         */
        KDnode* nearestNeighbor(const std::vector<double>& values, KDnode* root, int depth);

        /**
         * Function to find nearest node from a given target.
         * @param values target.
         * @param root (sub-)tree root.
         * @param depth tree depth.
         * @return nearest node
         */
        KDnode* nearestNeighbor(double* values, KDnode* root, int depth);

        /// Tree root
        KDnode* root;

        /**
         * Finds closest node from a given target
         * @param target
         * @param n1 (sub-)tree root
         * @param n2 node to be compared against
         * @return closest node to target
         */
        KDnode* closest(const std::vector<double>& target, KDnode* n1, KDnode* n2);

        /**
         * Finds closest node from a given target
         * @param target
         * @param n1 (sub-)tree root
         * @param n2 node to be compared against
         * @return closest node to target
         */
        KDnode* closest(double* target, KDnode* n1, KDnode* n2);

    };
}





#endif //KINODYNAMIC_PLANNING_KDTREE_H
