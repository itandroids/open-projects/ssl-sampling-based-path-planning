//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_VECTOR2D_H
#define ITANDROIDS_LIB_VECTOR2D_H

#include <cmath>
#include "../math/Vector2.h"

namespace itandroids_lib{
namespace geometry{

using math::Vector2d;

class Vector2D : public math::Vector2d{
public:
    static const double EPSILON;
    static const double ERROR_VALUE;
    Vector2D(){

    }

    using Vector2d::Vector2;

    Vector2D(Vector2d vector2) : Vector2d(vector2){

    }


    bool isValid() {
        return ((x != ERROR_VALUE) && (y != ERROR_VALUE));
    }

    double r2() {
        return squareAbs();
    }

    double dist(const Vector2D & p) {
        return distance(p
        );
    }

    double dist2(const Vector2D & p) {
        return std::pow(this->x - p.x, 2) + std::pow(this->y - p.y, 2);
    }

    double outerProduct(Vector2D & v) {
        return (this->x * v.y - this->y * v.x);
    }

    Vector2D & scale(const double & scalar) {
        this->x *= scalar;
        this->y *= scalar;
        return *this;
    }



};

}
}

#endif //ITANDROIDS_LIB_VECTOR2D_H
