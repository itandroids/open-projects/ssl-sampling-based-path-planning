//
// Created by francisco on 02/02/17.
//

#ifndef ITANDROIDS_LIB_TRIANGLE2D_H
#define ITANDROIDS_LIB_TRIANGLE2D_H

#include <cmath>


#include "Vector2D.h"
#include "Line2DDelaunay.h"

namespace itandroids_lib{
namespace geometry{


class Triangle2D {
public:
    static Vector2D circumcenter(Vector2D & a, Vector2D & b, Vector2D & c) {

        Line2DDelaunay perpendicular_ab = Line2DDelaunay(a, b);
        Line2DDelaunay perpendicular_bc = Line2DDelaunay(b, c);
        Line2DDelaunay perpendicular_ca = Line2DDelaunay(c, a);

        Vector2D sol = perpendicular_ab.intersection(perpendicular_bc);

        if (!sol.isValid()) {

            ///Line2D perpendicular_ca = Line2D::perpendicular_bisector(c, a);
            sol = perpendicular_ab.intersection(perpendicular_ca);

            if (sol.isValid()) {
                return sol;
            }

            sol = perpendicular_bc.intersection(perpendicular_ca);

            if (sol.isValid()) {
                return sol;
            }
        }

        Vector2D ab = b - a;
        Vector2D ca = c - a;

        double tmp = ab.outerProduct(ca);

        if (std::fabs(tmp) < 1.0e-10) {
            Vector2D invalidated(1.79769e+308, 1.79769e+308);
            return invalidated;
        }

        double inv = 0.5 / tmp;
        double ab_len2 = ab.r2();
        double ca_len2 = ca.r2();
        double xcc = inv * (ab_len2 * ca.y - ca_len2 * ab.y);
        double ycc = inv * (ab.x * ca_len2 - ca.x * ab_len2);

        return Vector2D(a.x + xcc, a.y + ycc);
    }
};
}
}

#endif //ITANDROIDS_LIB_TRIANGLE2D_H
