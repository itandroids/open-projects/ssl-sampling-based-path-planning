//
// Created by muzio on 10/04/16.
//

#include "GeometryUtils.h"

namespace itandroids_lib {
namespace geometry {

double GeometryUtils::getDistanceTo(const Point2D &p1, const Point2D &p2) {
    return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

bool GeometryUtils::pointIsBetweenLines(const Point2D &p,
                                        const Line2D &line1,
                                        const Line2D &line2) {
    Point2D a, b;
    a = line1.getStart();
    b = line1.getEnd();
    bool isLeftOfLine1 = ((b.x - a.x) * (p.y - a.y) - (b.y - a.y) * (p.x - a.x)) < 0;

    a = line2.getStart();
    b = line2.getEnd();
    bool isRightOfLine2 = ((b.x - a.x) * (p.y - a.y) - (b.y - a.y) * (p.x - a.x)) > 0;

    return isLeftOfLine1 && isRightOfLine2;
}

} // namespace itandroids_lib
} // namespace geometry