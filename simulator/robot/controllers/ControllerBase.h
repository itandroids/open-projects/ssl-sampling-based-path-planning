//
// Created by felipe on 01/04/20.
//

#ifndef SIMULATOR_CONTROLLER_H
#define SIMULATOR_CONTROLLER_H



#include <Eigen/Dense>
#include "world_model/WorldModel.h"

namespace controller
{

    class ControllerBase
    {
        public:
        /**
         * Constructor for base class ControllerBase
         * @param worldModel
         */
        ControllerBase() = default;

        virtual ~ControllerBase() = default;


        /**
         * Gets control input "u"
         * @return
         */
        virtual Eigen::VectorXd getControlInput(Eigen::VectorXd& currentState) = 0;

        /**
         * Sets control reference to controller
         * @param newReference
         */
        virtual void setReference(const Eigen::VectorXd& newReference) = 0;

        private:

        /**
         * Field to store control inputs
         */
        Eigen::VectorXd controlInput;

    };
}

#endif //SIMULATOR_SPEEDCONTROLLER_H