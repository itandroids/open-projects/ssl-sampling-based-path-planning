//
// Created by felipe on 29/06/20.
//

#include "controllers/tracking/Tracking.h"
#include <math.h>
#include <math/MathUtils.h>

namespace controller {
    Tracker::Tracker(const Eigen::MatrixXd& gainMatrix,
                     const Eigen::MatrixXd& sensorMatrix,
                     const Eigen::VectorXd& reference):
            ControllerBase(), reference(3), gainMatrix(gainMatrix), sensorMatrix(sensorMatrix)
    {
        this->reference << 0.0, 0.0, 0.0;
    }

    Eigen::VectorXd Tracker::getControlInput(Eigen::VectorXd& currentState) {
        /// Wrapping current state angle to pi
        currentState(2) = itandroids_lib::math::MathUtils::wrapToPi(currentState(2));

        /// Evaluating error

        Eigen::MatrixXd error = reference-sensorMatrix*currentState;

        /// Wrapping angle error to pi

        error(2) = itandroids_lib::math::MathUtils::wrapToPi(error(2));

        /// PD tracking

        Eigen::VectorXd controlInput = gainMatrix*(error);

        /// Returns saturated control input

        controlInput(2) = itandroids_lib::math::MathUtils::wrapToPi(controlInput(2));

        return controlInput;
    }

    void Tracker::setReference(const Eigen::VectorXd& newReference) {
        reference = newReference;
    }

}