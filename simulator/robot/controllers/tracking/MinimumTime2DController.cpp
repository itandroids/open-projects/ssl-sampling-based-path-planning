//
// Created by felipe on 12/09/20.
//

#include "MinimumTime2DController.h"
#include "math.h"

# include <cstdlib>
# include <iostream>
# include <iomanip>
#include <functional>
#include <math/MathUtils.h>
#include <fstream>


# include "optimizers/asa047.h"



namespace controller {

    MinimumTime2DController::MinimumTime2DController(double epsilon)
    {
        this->epsilon = epsilon;
        cacheTrigonometricFunctions();
    }


    double MinimumTime2DController::synchronizeSpeed(const std::vector<double> &start, const std::vector<double> &goal,
                                                     const std::vector<double> &currentVelocity,
                                                     const std::vector<double> &finalVelocity, double maxSpeed,
                                                     double maxAcceleration, int maxIterations) {
        double tx =0.0,ty=0.0;
        double alpha = M_PI_4, lowerBound = 0.0, upperBound =M_PI_2;
        int iteration = 0;

        while(iteration++<maxIterations){
            tx = computeTimeDOF(start[0],goal[0],currentVelocity[0],finalVelocity[0],maxSpeed*cos(alpha),maxAcceleration*cos(alpha));
            ty = computeTimeDOF(start[1],goal[1],currentVelocity[1],finalVelocity[1],maxSpeed*sin(alpha),maxAcceleration*sin(alpha));

            if(fabs(tx-ty) < epsilon)
                return alpha;
            else if (tx > ty) lowerBound = alpha;
            else upperBound = alpha;
            alpha = (upperBound+lowerBound)*0.5;
        }

        return alpha;
    }

    double MinimumTime2DController::getArrivalTime(const std::vector<double> &start, const std::vector<double> &goal,
                                                   const std::vector<double> &currentVelocity,
                                                   const std::vector<double> &finalVelocity, double maxSpeed,
                                                   double maxAcceleration, double alpha) {

        double tx = computeTimeDOF(start[0],goal[0],currentVelocity[0],finalVelocity[0],maxSpeed*cos(alpha),maxAcceleration*cos(alpha));
        double ty = computeTimeDOF(start[1],goal[1],currentVelocity[1],finalVelocity[1],maxSpeed*sin(alpha),maxAcceleration*sin(alpha));

        return std::max(tx,ty);

    }

    double MinimumTime2DController::trivialBVPFF(double v0, double vc, double vf, double maxAcceleration, double x0,
                                               double xf) {
        double d1 = (vc*vc-v0*v0)/(2*maxAcceleration);
        double d2 = (vc*vc-vf*vf)/(2*maxAcceleration);
        double d = fabs(xf-x0);
        if(d1+d2 <= d)
        {
            return (2*vc-v0-vf)/maxAcceleration + (d-d1-d2)/vc;
        }
        else {
            double v = sqrt((v0*v0+vf*vf+2*maxAcceleration*d)/2.0);
            return (2*v-v0-vf)/maxAcceleration;
        }
    }

    double MinimumTime2DController::BVPsolution(const double& v0, const double& vc, const double& vf, const double& maxAcceleration, const double& x0,
                                                const double& xf) {


        double invMaxAcceleration = 1/maxAcceleration;
        double invVc = 1/vc;
        double v02 = v0*v0;
        double vf2 = vf*vf;

        if((xf-x0)*(xf-x0)+(vf-v0)*(vf-v0) < epsilonState*epsilonState)
            return 0;


        if(x0 != xf && vc == 0)
            return 100000;

        int sgn = (vf>v0) ? 1:-1;

        double distanceFactor = invMaxAcceleration/(2)*(v02+vf2);
        double distanceFactor2 = invMaxAcceleration*vc*vc;
        double distanceFactor3 = invMaxAcceleration/2*(v02-vf2);

        double B = distanceFactor3*sgn+xf;
        double C = distanceFactor-distanceFactor2+xf;
        double D = -distanceFactor+distanceFactor2+xf;
        bool A = (fabs(vf) <= vc && fabs(v0) <= vc );

        if(A)
        {
            if(x0 <= B)
            {
                if(x0 >= C)
                    return (-(v0+vf)+2*sqrt(v02-maxAcceleration*(distanceFactor3+x0-xf)))*invMaxAcceleration;
                else return (vc-v0-vf)*invMaxAcceleration+(distanceFactor+xf-x0)*invVc;
            }
            else {
                if (x0 <= D)
                    return ((v0 + vf) + 2 * sqrt(v02 + maxAcceleration * (-distanceFactor3 + x0 - xf)))*invMaxAcceleration;
                else return (vc + v0 + vf)*invMaxAcceleration + (distanceFactor - (xf - x0))*invVc;
            }
        }

        //unfeasible condition
        return 100000;

    }

    double MinimumTime2DController::BVPcontrolInput(const double &v0, const double &vc, const double &vf,
                                                    const double &maxAcceleration, const double &x0,
                                                    const double &xf) {
        int sgn = 0;
        if(vf>v0) sgn = 1;
        else if(v0>vf) sgn = -1;
        double eps = 0.0000000000;
        double curveSwitch = 1.0/(2*maxAcceleration)*sgn*(v0*v0-vf*vf);

        if ((v0 > -vc) && (v0 <= vc) && (v0 > vf) && abs(x0 +1.0/(2*maxAcceleration)*(v0*v0-vf*vf) -xf)<eps)
            return -maxAcceleration;
        else if (((v0 >= -vc) && (v0 < vc )&& (x0 < curveSwitch +xf)) || ((v0 >= -vc) && (v0 < vc) && (v0 < vf) && abs(x0 - 1.0/(2*maxAcceleration)*(v0*v0-vf*vf) - xf) < eps))
            return maxAcceleration;
        else if(((v0 > -vc) && (v0 <= vc )&& (x0 > curveSwitch +xf)))
            return -maxAcceleration;
        else
            return 0.0;
    }

    double MinimumTime2DController::computeTimeDOF(double initialPosition,
                                                   double finalPosition,
                                                   double currentVelocity,
                                                   double finalVelocity,
                                                   double cruiseVelocity,
                                                   double maxAcceleration) {
        double d = fabs(finalPosition-initialPosition);
        double sgn = 1;
        if(finalVelocity < 0)
            sgn = -1;


        /// Speed normalization
        currentVelocity *= sgn;
        finalVelocity *= sgn;

        double d1 = (cruiseVelocity*cruiseVelocity-currentVelocity*currentVelocity)/(2*maxAcceleration);
        double d2 = (cruiseVelocity*cruiseVelocity-finalVelocity*finalVelocity)/(2*maxAcceleration);

        if(currentVelocity > 0 && finalVelocity > 0)
        {
            double d1 = (cruiseVelocity*cruiseVelocity-currentVelocity*currentVelocity)/(2*maxAcceleration);
            double d2 = (cruiseVelocity*cruiseVelocity-finalVelocity*finalVelocity)/(2*maxAcceleration);
            if(d1>d)
            {
                double v = sqrt(currentVelocity*currentVelocity+2*maxAcceleration*d);
                return (v-currentVelocity)/maxAcceleration;
            }
            else if(d1+d2<d)
            {
                return (2*cruiseVelocity-currentVelocity-finalVelocity)/maxAcceleration + (d-d1-d2)/cruiseVelocity;
            }
            else {
                double v = sqrt((finalVelocity*finalVelocity+ currentVelocity*currentVelocity+2*maxAcceleration*d)/2.0);
                return (2*v-finalVelocity-currentVelocity)/maxAcceleration;
            }

        }
        else if(currentVelocity < 0 && finalVelocity > 0)
        {
            double d1 = (currentVelocity*currentVelocity)/(2*maxAcceleration);
            double d2 = (cruiseVelocity*cruiseVelocity)/(2*maxAcceleration);
            double d3 = (cruiseVelocity*cruiseVelocity-finalVelocity*finalVelocity)/(2*maxAcceleration);

            if(d1>d)
            {
                double v = sqrt(currentVelocity*currentVelocity+2*maxAcceleration*d);
                return (v-currentVelocity)/maxAcceleration;
            }
            else if(d3+d2<d+d1)
            {
                return (currentVelocity+2*cruiseVelocity-finalVelocity)/maxAcceleration+(d1+d-d2-d3)/cruiseVelocity;
            }
            else {
                double v = sqrt((finalVelocity*finalVelocity+ currentVelocity*currentVelocity+2*maxAcceleration*d)/2.0);
                return (2*v-finalVelocity-currentVelocity)/maxAcceleration;
            }

        }


        /// Checking case
        if(currentVelocity > 0)
        {
            if(d1>d)
            {
                double v = sqrt(currentVelocity*currentVelocity+2*maxAcceleration*d);
                return (v-currentVelocity)/maxAcceleration;
            }
            else if(d1+d2<d)
            {
                return (2*cruiseVelocity-currentVelocity-finalVelocity)/maxAcceleration + (d-d1-d2)/cruiseVelocity;
            }
            else {
                double v = sqrt((finalVelocity*finalVelocity+ currentVelocity*currentVelocity+2*maxAcceleration*d)/2.0);
                return (2*v-finalVelocity-currentVelocity)/maxAcceleration;
            }

        }
        else {

            if(d2>d+d1)
            {
                double v = sqrt(2*maxAcceleration*(d1+d));
                return (currentVelocity+v)/maxAcceleration;
            }
            else if(d2-d1<d)
            {
                return (2*cruiseVelocity+currentVelocity-finalVelocity)/maxAcceleration + (d-d1-d2)/cruiseVelocity;
            }
            else{
                double v = sqrt((finalVelocity*finalVelocity + currentVelocity*currentVelocity+2*maxAcceleration*d)/2.0);
                return (2*v-finalVelocity+currentVelocity)/maxAcceleration;
            }
        }

    }

    double MinimumTime2DController::getArrivalTime(double x0, double xf, double vx0, double vxf, double y0, double yf,
                                                   double vy0, double vyf, double maxAccel, double maxVelocity) {
        double tx =0.0,ty=0.0;
        double alpha = M_PI_4, lowerBound = 0.0, upperBound =M_PI_2;
        int iteration = 0;
        int maxIterations = 10;

        while(iteration++<maxIterations){
            tx = BVPsolution(vx0,maxVelocity*cos(alpha),vxf,maxAccel*cos(alpha),x0,xf);
            ty = BVPsolution(vy0,maxVelocity*sin(alpha),vyf,maxAccel*sin(alpha),y0,yf);
            if(fabs(tx-ty) < epsilon)
                return std::max(tx,ty);
            else if (tx > ty) lowerBound = alpha;
            else upperBound = alpha;
            alpha = (upperBound+lowerBound)*0.5;
        }

        return std::max(tx,ty);
    }

    double MinimumTime2DController::getArrivalTimeDecoupled(const double &x0, const double &xf, const double &vx0,
                                                            const double &vxf, const double &y0, const double &yf,
                                                            const double &vy0, const double &vyf,
                                                            const double &maxAccel, const double &maxVelocity,
                                                            const double &beta, double& alpha) {
        double tx =0.0,ty=0.0;
        alpha = M_PI_4;
        double lowerBound = 0.0, upperBound =M_PI_2;
        int iteration = 0;
        int maxIterations = 20;
        double minmaxTime = 100000;
        double minmaxAlpha = M_PI_4;

        double maxVelocityX = maxVelocity*cos(beta);
        double maxVelocityY = maxVelocity*sin(beta);

        while(iteration++<maxIterations){
            tx = BVPsolution(vx0,maxVelocityX,vxf,maxAccel*cos(alpha),x0,xf);
            ty = BVPsolution(vy0,maxVelocityY,vyf,maxAccel*sin(alpha),y0,yf);
            //std::cout << tx << " " << ty << " " << alpha <<  std::endl;
            if(minmaxTime > std::max(tx,ty)){
                minmaxTime = std::max(tx,ty);
                minmaxAlpha = alpha;
            }
            if(fabs(tx-ty) < epsilon)
                return std::max(tx,ty);
            else if (tx > ty) upperBound = alpha;
            else lowerBound = alpha;
            alpha = (upperBound+lowerBound)*0.5;
        }
        alpha = minmaxAlpha;

        return minmaxTime;
    }

    void MinimumTime2DController::BVPemulation(double &v0, const double &vc, const double &vf, const double &maxAcceleration, double &x0,
                                               const double &xf, const double& sampleTime) {

        double u = BVPcontrolInput(v0, vc, vf,maxAcceleration,x0,xf);
        x0+= v0*sampleTime*0.5;
        //x0+= v0*sampleTime;
        v0+= u*sampleTime;

        //std::cout << BVPcontrolInput(v0, vc, vf,maxAcceleration,x0,xf) << std::endl;

        /// Sometimes the speed may surpass the maximum allowed. This if ensures it keeps in the allowed interval.
        if(v0 > vc) v0 = vc;
        else if(v0 < -vc) v0 = -vc;


        x0+= v0*sampleTime*0.5;
    }

    void MinimumTime2DController::BVPemulationVerbose(double &v0, const double &vc, const double &vf,
                                                      const double &maxAcceleration, double &x0, const double &xf,
                                                      const double &sampleTime, std::ofstream &output) {

        double u = BVPcontrolInput(v0, vc, vf,maxAcceleration,x0,xf);
        int sgn = 0;
        if(vf>v0) sgn = 1;
        else if(v0>vf) sgn = -1;
        double eps = 0.0000000000;
        double curveSwitch = 1.0/(2*maxAcceleration)*sgn*(v0*v0-vf*vf)+xf-x0;

        x0+= v0*sampleTime*0.5;


        v0+= u*sampleTime;


        /// Sometimes the speed may surpass the maximum allowed. This if ensures it keeps in the allowed interval.
        if(v0 > vc) v0 = vc;
        else if(v0 < -vc) v0 = -vc;


        x0+= v0*sampleTime*0.5;



        /// Printing to file
        output << x0 << " " << v0 << " " << u << " " << curveSwitch << std::endl;
    }

    double MinimumTime2DController::getBeta(const double &x0, const double &xf, const double &vx0, const double &vxf,
                                            const double &y0, const double &yf, const double &vy0, const double &vyf,
                                            const double &maxAccel, const double &maxVelocity) {
        /// todo: beta optimization
        double betaMin0 = std::min(acos(fabs(vx0)/maxVelocity),asin(fabs(vy0)/maxVelocity));
        double betaMax0 = std::max(acos(fabs(vx0)/maxVelocity),asin(fabs(vy0)/maxVelocity));
        double betaMinf = std::min(acos(fabs(vxf)/maxVelocity),asin(fabs(vyf)/maxVelocity));
        double betaMaxf = std::max(acos(fabs(vxf)/maxVelocity),asin(fabs(vyf)/maxVelocity));

        betaMinf = std::min(betaMin0,betaMinf);
        betaMaxf = std::max(betaMax0,betaMaxf);

        return (betaMinf+betaMaxf)*0.5;
    }

    double MinimumTime2DController::getBetaFromFrame(double *angles) {
        double position[2];
        position[0] = maxVelocity*(cos(angles[0])+cos(angles[1]))*0.5;
        position[1] = maxVelocity*(sin(angles[0])+sin(angles[1]))*0.5;
        return atan2(position[1],position[0]);
    }

    double MinimumTime2DController::getThetaFromFrame(double *angles) {
        double position[2];
        position[0] = maxVelocity*(cos(angles[0])+cos(angles[1]))*0.5;
        position[1] = maxVelocity*(sin(angles[0])+sin(angles[1]))*0.5;
        return acos(sqrt(position[0]*position[0]+position[1]*position[1])/maxVelocity);
    }


    double MinimumTime2DController::getTimeForFrame(double* angles) {
        if(angles[0] < -M_PI || angles[0] > M_PI || angles[1] < -M_PI || angles[1] > M_PI )
            return 100000;

        double position[2];
        position[0] = maxVelocity*(fastCos(angles[0])+fastCos(angles[1]))*0.5;
        position[1] = maxVelocity*(fastSin(angles[0])+fastSin(angles[1]))*0.5;

        double beta = atan2(position[1],position[0]);
        double theta = acos(sqrt(position[0]*position[0]+position[1]*position[1])/maxVelocity);

        return decoupledSynchronization(initialState,finalState, maxAcceleration,maxVelocity*cos(theta),maxVelocity*sin(theta),beta);

    }

    double MinimumTime2DController::getTimeForFrame(const std::vector<double>& angles, double &maxAccelerationX,
                                                    double &maxAccelerationY) {
        double position[2];

        position[0] = maxVelocity*(cos(angles[0])+cos(angles[1]))*0.5;
        position[1] = maxVelocity*(sin(angles[0])+sin(angles[1]))*0.5;

        double beta = atan2(position[1],position[0]);
        double theta = acos(sqrt(position[0]*position[0]+position[1]*position[1])/maxVelocity);

        return decoupledSynchronization(initialState,finalState, maxAcceleration,maxVelocity*cos(theta),maxVelocity*sin(theta), maxAccelerationX, maxAccelerationY ,beta);
    }

    void MinimumTime2DController::getRotatedVector(const double (&vector)[4], double (&rotatedVector)[4],
                                                     double angle) {
        double c = cos(angle);
        double s = sin(angle);
        rotatedVector[0] = vector[0]*c+vector[2]*s;
        rotatedVector[1] = vector[1]*c+vector[3]*s;
        rotatedVector[2] = -vector[0]*s+vector[2]*c;
        rotatedVector[3] = -vector[1]*s+vector[3]*c;
    }

    void MinimumTime2DController::getRotatedVector(const double (&vector1)[4], const double (&vector2)[4], double (&rotatedVector1)[4],
                                                   double (&rotatedVector2)[4], double angle) {

        double c = cos(angle);
        double s = sin(angle);
        rotatedVector1[0] = vector1[0]*c+vector1[2]*s;
        rotatedVector1[1] = vector1[1]*c+vector1[3]*s;
        rotatedVector1[2] = -vector1[0]*s+vector1[2]*c;
        rotatedVector1[3] = -vector1[1]*s+vector1[3]*c;
        rotatedVector2[0] = vector2[0]*c+vector2[2]*s;
        rotatedVector2[1] = vector2[1]*c+vector2[3]*s;
        rotatedVector2[2] = -vector2[0]*s+vector2[2]*c;
        rotatedVector2[3] = -vector2[1]*s+vector2[3]*c;
    }

    double MinimumTime2DController::getAccelerationForTimeY(const double (&initialState) [4], const double (&finalState)[4]
                                                           , const double &maxVelocityY, const double &tx,
                                                             double minAccel, double maxAccel, bool& searchResult) {
        double accel = (minAccel+maxAccel)*0.5;
        double ty = BVPsolution(initialState[3],maxVelocityY,finalState[3],accel,initialState[2],finalState[2]);
        double eps = 0.01;

        /// Error can be estimated by \frac{size}{2^N}, N = maxIterations.

        int maxIterations = 20;
        int k = 0;
        while(k++ < maxIterations)
        {
            if(tx > ty)
                maxAccel = accel;
            else minAccel = accel;
            accel = (maxAccel+minAccel)*0.5;
            ty = BVPsolution(initialState[3],maxVelocityY,finalState[3],accel,initialState[2],finalState[2]);
        }

        searchResult = (fabs(tx-ty)<eps);

        return accel;
    }

    double MinimumTime2DController::decoupledSynchronization(const double (&initialState) [4], const double (&finalState) [4], const double &maxAcceleration,
                                                             const double &maxVelocityX, const double &maxVelocityY,
                                                             double &maxAccelerationX, double &maxAccelerationY,
                                                             const double &beta) {
        int maxIterations = 20;
        double rotatedInitialState[4];
        double rotatedFinalState[4];

        getRotatedVector(initialState,finalState,rotatedInitialState,rotatedFinalState,beta);

        int i=0;
        double tx = 0.0;
        double txFound = 10000.0;
        double ty = 0.0;
        double alpha = M_PI_4;
        double lowerBound = 0.0;
        double upperBound = M_PI_2;
        double minDiff = 1000000;
        double alphaFound =  alpha;


        /// Dichotomic search

        while(i++<maxIterations)
        {
            maxAccelerationX = maxAcceleration*cos(alpha);
            maxAccelerationY = maxAcceleration*sin(alpha);
            tx = BVPsolution(rotatedInitialState[1],maxVelocityX,rotatedFinalState[1],maxAccelerationX,rotatedInitialState[0],rotatedFinalState[0]);
            ty = BVPsolution(rotatedInitialState[3],maxVelocityY,rotatedFinalState[3],maxAccelerationY,rotatedInitialState[2],rotatedFinalState[2]);

            if(ty<tx && minDiff > fabs(tx-ty))
            {
                alphaFound = alpha;
                txFound = tx;
                minDiff = fabs(tx-ty);
            }

            if(fabs(tx-ty) < epsilon)
            {
                return std::max(tx,ty);
            }

            else if(tx>ty)
                upperBound = alpha;
            else lowerBound = alpha;

            alpha = (upperBound+lowerBound)*0.5;
        }

        /// In case of discontinuities...

        maxAccelerationY = maxAcceleration*sin(alphaFound);
        maxAccelerationX = maxAcceleration*cos(alphaFound);
        bool searchResult;

        if(ty > 0)
        {
            maxAccelerationY = getAccelerationForTimeY(rotatedInitialState,rotatedFinalState,maxVelocityY,txFound,0,maxAccelerationY,searchResult);
        }

        /// you may also wanna check if tx and ty are indeed synchronized...
        if(searchResult)
            return txFound;
        return 10000;
    }

    double MinimumTime2DController::decoupledSynchronization(const double (&initialState) [4], const double (&finalState) [4], const double &maxAcceleration,
                                                             const double &maxVelocityX, const double &maxVelocityY, const double &beta) {
        int maxIterations = 20;
        double rotatedInitialState[4];
        double rotatedFinalState[4];

        getRotatedVector(initialState,finalState,rotatedInitialState,rotatedFinalState,beta);

        int i=0;
        double tx = 0.0;
        double txFound = 10000.0;
        double ty = 0.0;
        double alpha = M_PI_4;
        double lowerBound = 0.0;
        double upperBound = M_PI_2;
        double minDiff = 1000000;
        double alphaFound =  alpha;


        /// Dichotomic search

        while(i++<maxIterations)
        {
            tx = BVPsolution(rotatedInitialState[1],maxVelocityX,rotatedFinalState[1],maxAcceleration*fastCos(alpha),rotatedInitialState[0],rotatedFinalState[0]);
            ty = BVPsolution(rotatedInitialState[3],maxVelocityY,rotatedFinalState[3],maxAcceleration*fastSin(alpha),rotatedInitialState[2],rotatedFinalState[2]);

            if(ty<tx && minDiff > fabs(tx-ty))
            {
                alphaFound = alpha;
                txFound = tx;
                minDiff = fabs(tx-ty);
            }

            if(fabs(tx-ty) < epsilon)
                return std::max(tx,ty);
            else if(tx>ty)
                upperBound = alpha;
            else lowerBound = alpha;

            alpha = (upperBound+lowerBound)*0.5;
        }

        /// In case of discontinuities...

        double maxAccelerationY = maxAcceleration*fastSin(alphaFound);
        bool searchResult;

        if(ty > 0)
        {
            double accelY = getAccelerationForTimeY(rotatedInitialState,rotatedFinalState,maxVelocityY,txFound,0,maxAccelerationY,searchResult);
        }

        /// you may also wanna check if tx and ty are indeed synchronized...
        if(searchResult)
            return txFound;
        return 10000;
    }

    std::vector<double> MinimumTime2DController::getBestFrame() {

        /// Jump optimization if necessary
        if(!optimizingFrame)
            return getFrameInitialGuess();

        int i;
        int icount;
        int ifault;
        int kcount;
        int konvge;
        int n=2;
        int numres;
        double reqmin;
        std::vector<double> start;
        double* step;
        double* xmin;
        double ynewlo;
        std::vector<double> ans(n,0);


        // Converting std::vector to array
        start = getFrameInitialGuess();

        step = new double[n];
        xmin = new double[n];

        reqmin = 1.0E-03;

        step[0] = 1.0;
        step[1] = 1.0;

        konvge = 10;
        kcount = 100;

        //std::cout << "Initial guess: " << std::endl;
        //std::cout << start[0] << std::endl;
        //std::cout << start[1] << std::endl;


        ynewlo = getTimeForFrame ( &start[0] );


        auto getTimepartial = [this](double* angles) { return MinimumTime2DController::getTimeForFrame(angles); };
        //std::function<double(double[2])> func(std::bind(&MinimumTime2DController::getTimeForFrame, this, std::placeholders::_1));

        nelmin (getTimepartial, n, &start[0], xmin, &ynewlo, reqmin, step,
                 konvge, kcount, &icount, &numres, &ifault );


        delete [] step;

        ans[0] = xmin[0];
        ans[1] = xmin[1];

        delete[] xmin;


        return ans;

    }

    double MinimumTime2DController::getX0() const {
        return x0;
    }

    void MinimumTime2DController::setX0(double x0) {
        MinimumTime2DController::x0 = x0;
        initialState[0] = x0;
    }

    double MinimumTime2DController::getXf() const {
        return xf;
    }

    void MinimumTime2DController::setXf(double xf) {
        MinimumTime2DController::xf = xf;
        finalState[0] = xf;
    }

    double MinimumTime2DController::getVx0() const {
        return vx0;
    }

    void MinimumTime2DController::setVx0(double vx0) {
        MinimumTime2DController::vx0 = vx0;
        initialState[1] = vx0;
    }

    double MinimumTime2DController::getVxf() const {
        return vxf;
    }

    void MinimumTime2DController::setVxf(double vxf) {
        MinimumTime2DController::vxf = vxf;
        finalState[1] = vxf;
    }

    double MinimumTime2DController::getY0() const {
        return y0;
    }

    void MinimumTime2DController::setY0(double y0) {
        MinimumTime2DController::y0 = y0;
        initialState[2] = y0;
    }

    double MinimumTime2DController::getYf() const {
        return yf;
    }

    void MinimumTime2DController::setYf(double yf) {
        MinimumTime2DController::yf = yf;
        finalState[2] = yf;
    }

    double MinimumTime2DController::getVy0() const {
        return vy0;
    }

    void MinimumTime2DController::setVy0(double vy0) {
        MinimumTime2DController::vy0 = vy0;
        initialState[3] = vy0;
    }

    double MinimumTime2DController::getVyf() const {
        return vyf;
    }

    void MinimumTime2DController::setVyf(double vyf) {
        MinimumTime2DController::vyf = vyf;
        finalState[3] = vyf;
    }

    void MinimumTime2DController::setMaxVelocity(double maxVelocity) {
        MinimumTime2DController::maxVelocity = maxVelocity;
    }

    void MinimumTime2DController::setMaxAcceleration(double maxAcceleration) {
        MinimumTime2DController::maxAcceleration = maxAcceleration;
    }

    void MinimumTime2DController::setInitialState(const std::vector<double> &state) {
        for(int i=4;--i;)
            initialState[i] = state[i];

        x0 = state[0];
        vx0 = state[1];
        y0 = state[2];
        vy0 = state[3];
    }

    void MinimumTime2DController::setInitialState(double* state)
    {
        for(int i=4;i--;)
            initialState[i] = state[i];

        x0 = state[0];
        vx0 = state[1];
        y0 = state[2];
        vy0 = state[3];
    }

    void MinimumTime2DController::setFinalState(const std::vector<double>& state)
    {
        for(int i=4;i--;)
            finalState[i] = state[i];

        xf = state[0];
        vxf = state[1];
        yf = state[2];
        vyf = state[3];
    }

    void MinimumTime2DController::setFinalState(double* state)
    {
        for(int i=4;i--;)
            finalState[i] = state[i];

        xf = state[0];
        vxf = state[1];
        yf = state[2];
        vyf = state[3];
    }



    std::vector<double> MinimumTime2DController::getFrameInitialGuess() {
        std::vector<double> x0(2,0);
        /// Firstly we should handle corner cases
        if(initialState[3] == 0 && initialState[1] == 0){
            x0[0] = atan2(finalState[2]-initialState[2],finalState[0]-initialState[0]);
        }
        else{
            x0[0] = atan2(initialState[3],initialState[1]);
        }

        if(finalState[3] == 0 && finalState[1] == 0){
            x0[1] = atan2(finalState[2]-initialState[2],finalState[0]-initialState[0]);
        }
        else{
            x0[1] = atan2(finalState[3],finalState[1]);
        }

        return x0;
    }


    void MinimumTime2DController::cacheTrigonometricFunctions() {
        double angle = -M_PI;
        for(int i=0;i<trigonometricChunks;i++)
        {
            fastSinVector[i] = sin(angle);
            fastCosVector[i] = cos(angle);
            angle += chunkSize;
        }

    }


    double MinimumTime2DController::fastCos(const double& x, const double& chunkSize) {
        int position = (x+M_PI)/chunkSize;
        return fastCosVector[position];
    }

    double MinimumTime2DController::fastSin(const double& x, const double& chunkSize) {
        int position = (x+M_PI)/chunkSize;
        return fastSinVector[position];
    }

    double MinimumTime2DController::fastSin(const double &x) {
        int position = (x+M_PI)*chunkInverse;
        return *(fastSinVector+position);
    }

    double MinimumTime2DController::fastCos(const double &x) {
        int position = (x+M_PI)*chunkInverse;
        return fastCosVector[position];
    }

    double MinimumTime2DController::getMaxVelocity() const {
        return maxVelocity;
    }

    double MinimumTime2DController::getMaxAcceleration() const {
        return maxAcceleration;
    }

    void MinimumTime2DController::setFrameOptimization(bool frameOptimization) {
        this->optimizingFrame = frameOptimization;
    }


}