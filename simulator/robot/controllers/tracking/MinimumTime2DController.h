//
// Created by felipe on 12/09/20.
//

#ifndef SIMULATOR_MINIMUMTIME2DCONTROLLER_H
#define SIMULATOR_MINIMUMTIME2DCONTROLLER_H

#include <iostream>
#include <vector>
#include <math.h>

namespace controller {
    class MinimumTime2DController {
    private:
        /**
         * Compute time of arrival for single Degree of Freedom.
         * @param initialPosition
         * @param finalPosition
         * @param currentVelocity
         * @param finalVelocity
         * @param cruiseVelocity
         * @param maxAcceleration
         * @return time of arrival
         */
        double computeTimeDOF(double initialPosition, double finalPosition, double currentVelocity, double finalVelocity, double cruiseVelocity, double maxAcceleration);



        /**
         * Attempts to synchronize arrival times across different degrees of freedom.
         * @param start
         * @param goal
         * @param maxSpeed
         * @param maxAcceleration
         * @param maxIterations
         * @return return synchronization angle
         */
        double synchronizeSpeed(const std::vector<double>& start, const std::vector<double>& goal, const std::vector<double>& currentVelocity, const std::vector<double>& finalVelocity, double maxSpeed, double maxAcceleration,int maxIterations);


        /**
         * Trivial BVP front front
         * @param v0 current speed > 0
         * @param vc cruise speed > 0
         * @param vf final speed > 0
         * @param maxAcceleration > 0
         * @param x0
         * @param xf > x0
         * @return minimum time
         */
        double trivialBVPFF(double v0, double vc, double vf, double maxAcceleration, double x0, double xf);

        /**
         * Cache trigonometric functions according to number of chunks.
         */
        void cacheTrigonometricFunctions();

        /// Optimizing frame
        bool optimizingFrame = true;


        /// Epsilon
        double epsilon = 0.01;

        /// Initial BVP x position

        double x0;

        /// Final BVP x position

        double xf;

        /// Initial BVP x velocity

        double vx0;


        /// Final BVP x velocity

        double vxf;

        /// Initial BVP y position

        double y0;

        /// Final BVP y position

        double yf;

        /// Initial BVP y velocity

        double vy0;

        /// Final BVP y velocity

        double vyf;

        /// Initial state [x0 vx0 y0 vy0]^T

        double initialState[4];

        /// Final state [xf vxf yf vyf]^T

        double finalState[4];

        double maxVelocity;

        /// Maximum acceleration radius

        double maxAcceleration;

        /// Minimum epsilon to be at the same state

        double epsilonState = 0.0001;

        /// Number of chunks for fast sine and cosine

        static const int trigonometricChunks = 160000;

        static constexpr double chunkSize = 2.0*M_PI/trigonometricChunks;


        /// Evaluates fast sine function

        /**
         * Evaluates fast sine using first order Taylor approximation
         * @param x angle
         * @param chunkSize chunk size
         * @return sine evaluated
         */
        inline double fastSin(const double& x, const double& chunkSize );



        /**
        * Evaluates fast cosine using first order Taylor approximation
        * @param x angle
        * @param chunkSize chunk size
        * @return cosine evaluated
        */
        inline double fastCos(const double& x, const double& chunkSize);





    public:

        void setFrameOptimization(bool frameOptimization);

        double getMaxVelocity() const;

        double getMaxAcceleration() const;

        static constexpr double chunkInverse = 1.0/chunkSize;

        /// Fast sine vector

        double fastSinVector[trigonometricChunks];

        /// Fast cosine vector

        double fastCosVector[trigonometricChunks];


        /**
         * Evaluates sine with chunksize partial application
         * @param x angle
         * @return sine
         */
        double fastSin(const double& x);

        /**
         * Evaluates cosine with chunksize partial application
         * @param x angle
         * @return cosine
         */
        double fastCos(const double& x);

        /**
        * Evaluates arrival time
        * @param start
        * @param goal
        * @param maxSpeed
        * @param maxAcceleration
        * @param alpha cylinder angle
        * @return arrival time
        */
        double getArrivalTime(const std::vector<double>& start, const std::vector<double>& goal, const std::vector<double>& currentVelocity, const std::vector<double>& finalVelocity, double maxSpeed, double maxAcceleration, double alpha);

        /**
         * Returns minimum time of arrival of a given BVP
         * @param v0 initial speed
         * @param vc cruise speed
         * @param vf final speed
         * @param maxAcceleration maximum acceleration
         * @param x0 initial position
         * @param xf final position
         * @return minimum time
         */
        double BVPsolution(const double& v0,const double& vc,const double& vf,const double& maxAcceleration,const double& x0,const double& xf);

        /**
         * Given BVP, evaluates bang-singular-bang immediate control action
         * @param v0 initial speed
         * @param vc cruise speed
         * @param vf final speed
         * @param maxAcceleration maximum acceleration
         * @param x0 initial position
         * @param xf minimum time
         * @return control input
         */
        double BVPcontrolInput(const double& v0,const double& vc,const double& vf,const double& maxAcceleration,const double& x0,const double& xf);

        /**
         * Emulates BVP next step based on sampleTime given the current state.
         * @param v0 initial speed
         * @param vc cruise speed
         * @param vf final speed
         * @param maxAcceleration maximum acceleration
         * @param x0 initial position
         * @param xf minimum time
         */
        void BVPemulation(double& v0,
                          const double& vc,
                          const double& vf,
                          const double& maxAcceleration,
                          double& x0,
                          const double& xf,
                          const double& sampleTime);

        /**
        * Emulates BVP next step based on sampleTime given the current state. It prints the states on a file.
        * @param v0 initial speed
        * @param vc cruise speed
        * @param vf final speed
        * @param maxAcceleration maximum acceleration
        * @param x0 initial position
        * @param xf minimum time
        * @param output file to be printed
        */
        void BVPemulationVerbose(double& v0,
                                 const double& vc,
                                 const double& vf,
                                 const double& maxAcceleration,
                                 double& x0,
                                 const double& xf,
                                 const double& sampleTime,
                                 std::ofstream& output);

        /**
         * Gets arrival time of BVP solution to desired state.
         * @param x0 initial x position
         * @param xf final x position
         * @param vx0 initial x velocity
         * @param vxf final x velocity
         * @param y0 initial y position
         * @param yf final y position
         * @param vy0 initial y velocity
         * @param vyf final y velocity
         * @param maxAccel max acceleration
         * @param maxVelocity max velocity
         * @param beta beta angle decoupling speed and acceleration limits. The beta limits maxVelocity in x to maxVelocity*cos(beta) and
         * maxVelocity in y to maxVelocity*sin(beta). beta must lie inside (0,pi/2).
         * @param alpha alpha will be calculated inside this function and stored in variable alpha
         * @return arrival time
         */
        double getArrivalTimeDecoupled(const double& x0, const double& xf, const double& vx0, const double& vxf, const double& y0, const double& yf,
                                       const double& vy0, const double& vyf, const double& maxAccel, const double& maxVelocity, const double& beta, double& alpha);

        /**
         * Gets beta given initial and final states. Beta angle determines maximum velocity.
         * @param x0 initial x position
         * @param xf final x position
         * @param vx0 initial x velocity
         * @param vxf final x velocity
         * @param y0 initial y position
         * @param yf final y position
         * @param vy0 initial y velocity
         * @param vyf final y velocity
         * @param maxAccel max acceleration
         * @param maxVelocity max velocity
         * @return beta angle
         */
        double getBeta(const double& x0, const double& xf, const double& vx0, const double& vxf, const double& y0, const double& yf,
                                       const double& vy0, const double& vyf, const double& maxAccel, const double& maxVelocity);


        double getArrivalTime(double x0, double xf, double vx0, double vxf, double y0, double yf,
                              double vy0, double vyf, double maxAccel, double maxVelocity);

        /**
         * Class constructor
         * @param epsilon synchronization tolerance
         */
        MinimumTime2DController(double epsilon);


        /**
         * Gets arrival time of BVP solution to desired state.
         * @param x0 initial x position
         * @param xf final x position
         * @param vx0 initial x velocity
         * @param vxf final x velocity
         * @param y0 initial y position
         * @param yf final y position
         * @param vy0 initial y velocity
         * @param vyf final y velocity
         * @param maxAccel max acceleration
         * @param maxVelocity max velocity
         * @return arrival time
         */
        double getArrivalTime(double x0,double xf, double vx0, double vxf,double y0,double yf, double vy0, double vyf,double maxAccel,double maxVelocity) const;

        /**
         * Evaluate time to arrive at state s2 departing from s1. s is a 4x1 state, with positions and velocities [ x v_x y v_y]^T
         * @param s1 state 1
         * @param s2 state 2
         * @return time of arrival
         */
        double computeTime(std::vector<double> s1, std::vector<double> s2);

        /**
         * Get time for a given frame of angles defined as (\theta_i, \theta_j)
         * @param angles pair (\theta_i, \theta_j)
         * @return minimum time evaluated
         */
        double getTimeForFrame(double* angles);

        /**
         * Gets beta given a rectangle defined by angles array.
         * Beta defines the rotation to evaluate a given frame.
         * @param angles array of angles defininf the rectangle
         * @return beta value
         */
        double getBetaFromFrame(double* angles);

        /**
         * Gets theta given a rectangle defined by angles array.
         * Theta defines the maximum allowed velocity boundaries for a given rectangle.
         * @param angles array of angles defininf the rectangle
         * @return theta value
         */
        double getThetaFromFrame(double* angles);

        /**
         * Get time for a given frame of angles defined as (\theta_i, \theta_j)
         * @param angles pair (\theta_i, \theta_j)
         * @param maxAccelerationX will be assigned
         * @param maxAccelerationY will be assigned
         * @return minimum time evaluated
         */
        double getTimeForFrame(const std::vector<double>& angles, double& maxAccelerationX, double& maxAccelerationY);

        /**
         * Evaluates the initial guess using the bisector method.
         * @return initial guess for frame optimization
         */
        std::vector<double> getFrameInitialGuess();

        /**
         * Get sychronized time for a given BVP
         * @param initialState initial state
         * @param finalState final state
         * @param maxAcceleration acceleration cylinder radius
         * @param maxVelocityX max velocity x
         * @param maxVelocityY max velocity y
         * @param beta rotation angle
         * @return synchronized time
         */
        double decoupledSynchronization(const double (&initialState)[4], const double (&finalState)[4], const double& maxAcceleration, const double& maxVelocityX,
                                        const double& maxVelocityY, const double& beta);

        /**
         * Get sychronized time for a given BVP
         * @param initialState initial state
         * @param finalState final state
         * @param maxAcceleration acceleration cylinder radius
         * @param maxVelocityX max velocity x
         * @param maxVelocityY max velocity y
         * @param maxAccelerationX max acceleration x to be assigned
         * @param maxAccelerationY max acceleration y to be assigned
         * @param beta rotation angle
         * @return synchronized time
         */
        double decoupledSynchronization(const double (&initialState)[4], const double (&finalState)[4], const double& maxAcceleration, const double& maxVelocityX,
                                        const double& maxVelocityY,double& maxAccelerationX ,double& maxAccelerationY ,const double& beta);

        /**
         * Get acceleration to sychronize time
         * @param initialState initial state
         * @param finalState final state
         * @param maxVelocityY max velocity at y axis
         * @param tx time on axis x to be synchronized
         * @param minAccel minimum value of acceleration
         * @param maxAccel maximum value of acceleration
         * @param searchResult if true, the search is valid and the times are synchronized
         * @return acceleration to reach \finalState within given time tx
         */
        double getAccelerationForTimeY(const double (&initialState)[4],const double (&finalState)[4],const double& maxVelocityY,
                                       const double& tx, double minAccel, double maxAccel, bool& searchResult);

        /**
         * Get rotated vector given original and rotation angle
         * @param vector original vector
         * @param rotatedVector rotated vector
         * @param angle rotation angle
         */
        void getRotatedVector(const double (&vector)[4], double (&rotatedVector)[4], double angle);

        /**
         * Get rotated vector given original and rotation angle
         * @param vector original vector 1
         * @param vector original vector 2
         * @param rotatedVector1 rotated vector 1
         * @param rotatedVector2 rotated vector 2
         * @param angle rotation angle
         */
        void getRotatedVector(const double (&vector1)[4], const double (&vector2)[4], double (&rotatedVector1)[4], double (&rotatedVector2)[4] , double angle);

        /**
         * Gets best frame given intial and final boundary conditions;
         */
        std::vector<double> getBestFrame();

        std::vector<double> maximumAcceleration;

        double getX0() const;

        void setX0(double x0);

        double getXf() const;

        void setXf(double xf);

        double getVx0() const;

        void setVx0(double vx0);

        double getVxf() const;

        void setVxf(double vxf);

        double getY0() const;

        void setY0(double y0);

        double getYf() const;

        void setYf(double yf);

        double getVy0() const;

        void setVy0(double vy0);

        double getVyf() const;

        void setVyf(double vyf);

        void setMaxVelocity(double maxVelocity);

        void setMaxAcceleration(double maxAcceleration);

        void setInitialState(const std::vector<double>& state);

        void setInitialState(double* state);

        void setFinalState(const std::vector<double>& state);

        void setFinalState(double* state);
    };
}




#endif //SIMULATOR_MINIMUMTIME2DCONTROLLER_H
