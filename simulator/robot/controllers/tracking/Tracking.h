//
// Created by felipe on 01/04/20.
//

#ifndef SIMULATOR_TRACKING_H
#define SIMULATOR_TRACKING_H

#endif //SIMULATOR_TRACKING_H

#include "controllers/ControllerBase.h"

namespace controller
{
    /*
     * Class to encapsulate path trackers
     */
    class Tracker : public ControllerBase
    {

    public:
        /**
         * Constructor for class speed controller
         * @param gainMatrix gain matrix for control loop
         * @param sensorMatrix sensor Matrix
         * @param reference for controller
         */
        Tracker(const Eigen::MatrixXd& gainMatrix,
                const Eigen::MatrixXd& sensorMatrix,
                const Eigen::VectorXd& reference);

        /**
         * Sets control reference to controller
         * @param newReference
         */
        void setReference(const Eigen::VectorXd& newReference);

        /**
         * Gets control input given current state
         * @param currentState current system's state
         * @return system's control input
         */
        Eigen::VectorXd getControlInput(Eigen::VectorXd& currentState);


    private:

        /// Control loop gain matrix ``K''

        Eigen::MatrixXd gainMatrix;

        /// Control loop sensor matrix ``C''

        Eigen::MatrixXd sensorMatrix;

        /// Reference vector

        Eigen::VectorXd reference;

        /// Max speed for cylinder saturation

        double maxSpeed;


    };


}
