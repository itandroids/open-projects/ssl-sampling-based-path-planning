//
// Created by felipe on 26/07/20.
//

#ifndef SIMULATOR_DYNAMICWINDOWAPPROACH_H
#define SIMULATOR_DYNAMICWINDOWAPPROACH_H

#include <controllers/ControllerBase.h>
#include <vector>
#include <math.h>
#include <robot_model/RobotModelBase.h>
#include "world_model/RandomStateGenerator.h"

namespace controller {
    class DynamicWindowApproach : public ControllerBase {

    public:
        /**
         * Constructor for class DynamicWindowApproach. It relies on physical constraints.
         * For this class, we treat acceleration constraints as a circle.
         * @param speedConstraints
         * @param accelerationConstraints
         */
        DynamicWindowApproach(const Eigen::VectorXd& speedConstraints,
                              const Eigen::VectorXd& accelerationConstraints,
                              const std::vector<std::vector<double>>& obstacles);

        /**
         * Obstacles' setter.
         * @param obstacles list of obstacles' position.
         */
        void setObstacles(const std::vector<std::vector<double>>& obstacles);

        /**
         * Speed constraints' setter.
         * @param speedConstraints
         */
        void setSpeedConstraints(const Eigen::VectorXd& speedConstraints);

        /**
         * Acceleration constraints setter
         * @param accelerationConstraints
         */
        void setAccelerationConstraints(const Eigen::VectorXd& accelerationConstraints);

        /**
         * Obstacle radius setter
         * @param obstacleRadius
         */
        void setObstacleRadius(double obstacleRadius);


        /**
         * Uses a Monte Carlo approach to find best control input for the system.
         * @param currentState current system's state
         * @param waypoint waypoint to be followed
         * @param iterations number of MC iterations
         * @return control input vector
         */
        Eigen::VectorXd findOptimal(Eigen::VectorXd &currentState, Eigen::VectorXd &waypoint, int iterations);

        Eigen::VectorXd getControlInput(Eigen::VectorXd& currentState);

        void setReference(const Eigen::VectorXd& newReference);


    private:

        /**
         * Generates random control input
         */
        Eigen::VectorXd generateRandomInput();

        /**
         * Check line visibility, considering all obstacles on the field.
         * @param p line segment origin
         * @param q line segment end
         * @return true if line is observable
         */
        bool checkObsLine(const std::vector<double>& p,
                          const std::vector<double>& q);

        /**
         * Checks whether a given state [x y yaw \dot{x} \dot{y} \dot{yaw}]^T  is safe. This method relies on
         * maximum/minimum acceleration constraint. As the yaw coordinate does not interfere on collision checks,
         * this coordinate is disregarded from our analysis.
         * @param state full state coordinates [x y yaw \dot{x} \dot{y} \dot{yaw}]^T.
         * @param acceleration constraints, given by [max_accel_x max_accel_y max_accel_yaw]
         * @return
         */
        static bool checkStateSafety(const Eigen::VectorXd& state, const Eigen::VectorXd& accelerationConstraints);


        /**
         * Checks whether line segment pq intersects with circle (center o and radius r).
         * @param p line segment origin
         * @param q line segment end
         * @param o circle center
         * @param r circle's radius
         * @return true if line is observable
         */
        static bool checkObsLine(const std::vector<double>& p,
                                 const std::vector<double>& q,
                                 const std::vector<double>& o,
                                 double r);
        /**
         * Apply Bhaskara's formula to get roots from ax^2+bx+c=0 equation.
         * @param a
         * @param b
         * @param c
         * @return vector with size 2 and roots. In case delta < 0, it returns an empty vector.
         */
        static std::vector<double> getBhaskaraRoots(double a, double b, double c);

        /**
         * Cost function used in DWA.
         * @param controlInput sampled control input
         * @param robot robot model
         * @param waypoint coordinates
         * @return cost
         */
        double cost(Eigen::VectorXd &controlInput, Eigen::VectorXd &currentState, Eigen::VectorXd &waypoint);

        /**
         * Function to check if a given state is safe or not.
         * @param state sampled state
         * @param robot robot model
         * @return cost
         */
        bool safe(std::vector<double> state, const robot_model::RobotModelBase& robot);

        /**
         * Function to estimate position achieved after accelerating.
         * @param currentState [x y yaw v_x v_y \omega]^T
         * @param controlInput control input [v_x v_y \omega]^T
         * @return final state
         */
        Eigen::VectorXd finalPosition(Eigen::VectorXd& currentState, Eigen::VectorXd& controlInput);

        /**
         * Function to estimate position achieved after decelerating.
         * @param finalState state after previous acceleration
         * @param controlInput used to achieve previous position
         * @return state after complete decceleration
         */
        Eigen::VectorXd stopPosition(Eigen::VectorXd& finalState, Eigen::VectorXd& controlInput);

        /// List of obstacle's positions.

        std::vector<std::vector<double>> obstacles;

        /// Speed constraints

        Eigen::VectorXd speedConstraints;

        /// Acceleration constraints

        Eigen::VectorXd accelerationConstraints;

        /// Obstacle radius

        double obstacleRadius = 0.0;

        /// Random state generator

        random_state::RandomStateGenerator rsg;

        /// Number of Dynamic Window MC iterations

        double numberOfIterations = 2000;
    public:
        void setNumberOfIterations(double numberOfIterations);

    private:

        Eigen::VectorXd reference;

    };
}



#endif //SIMULATOR_DYNAMICWINDOWAPPROACH_H
