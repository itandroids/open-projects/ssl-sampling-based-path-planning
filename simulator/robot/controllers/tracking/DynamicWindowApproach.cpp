//
// Created by felipe on 26/07/20.
//

#include <math/MathUtils.h>
#include "DynamicWindowApproach.h"

namespace controller
{

    DynamicWindowApproach::DynamicWindowApproach(const Eigen::VectorXd& speedConstraints,
                                                 const Eigen::VectorXd& accelerationConstraints,
                                                 const std::vector<std::vector<double>>& obstacles): ControllerBase(),
                                                                                                     speedConstraints(speedConstraints),
                                                                                                     accelerationConstraints(accelerationConstraints),
                                                                                                     obstacles(obstacles),
                                                                                                     rsg(),
                                                                                                     reference(3){
        reference << 0,0,0;

    }

    void DynamicWindowApproach::setSpeedConstraints(const Eigen::VectorXd &speedConstraints) {
        this->speedConstraints = speedConstraints;
    }

    void DynamicWindowApproach::setAccelerationConstraints(const Eigen::VectorXd &accelerationConstraints) {
        this->accelerationConstraints = accelerationConstraints;
    }

    void DynamicWindowApproach::setObstacles(const std::vector<std::vector<double>> &obstacles) {
        this->obstacles = obstacles;
    }

    bool DynamicWindowApproach::checkObsLine(const std::vector<double> &p, const std::vector<double> &q,
                                             const std::vector<double> &o, double r)
    {
        /// Evaluating second degree equation parameters a,b and c

        double a = (q[0]-p[0])*(q[0]-p[0]) + (q[1]-p[1])*(q[1]-p[1]);
        double b = 2.0*((p[0]-o[0])*(q[0]-p[0])+(p[1]-o[1])*(q[1]-p[1]));
        double c = (p[0]-o[0])*(p[0]-o[0])+(p[1]-o[1])*(p[1]-o[1])-r*r;

        /// Getting analytic solution

        std::vector<double> roots = getBhaskaraRoots(a,b,c);

        /// Roots must be both lying outside of the interval [0,1]

        return (roots.empty()||((roots[0]>1||roots[0]<0)&&(roots[1]>1||roots[1]<0)));

    }

    bool DynamicWindowApproach::checkObsLine(const std::vector<double> &p, const std::vector<double> &q) {
        /// Checks visibility against all obstacles. Perhaps there is a cheaper way.
        for(std::vector<double> obstacle:obstacles){
            if(!checkObsLine(p,q,obstacle,obstacleRadius))
                return false;
        }
        return true;
    }

    std::vector<double> DynamicWindowApproach::getBhaskaraRoots(double a, double b, double c) {
        std::vector<double> roots;

        /// Evaluating delta

        double delta = b*b-4*a*c;

        if(delta >= 0)
        {
            roots = {0.0,0.0};

            /// Evaluating roots

            roots[0] = (b*b+sqrt(delta))/(2*a);
            roots[1] = -b/a-roots[0];
        }

        return roots;
    }

    void DynamicWindowApproach::setObstacleRadius(double obstacleRadius) {
        this->obstacleRadius = obstacleRadius;
    }

    Eigen::VectorXd DynamicWindowApproach::finalPosition(Eigen::VectorXd& currentState, Eigen::VectorXd& controlInput){
        Eigen::VectorXd currentSpeed = currentState.segment(3,3);
        Eigen::VectorXd error(3);
        error = controlInput-currentSpeed;
        double speedOrientation = atan2(error(1),error(0));
        double sign = 1.0;
        if(error(2) < 0)
            sign = -1.0;
        Eigen::VectorXd acceleration(3);
        acceleration << accelerationConstraints(0)*cos(speedOrientation),
                        accelerationConstraints(1)*sin(speedOrientation),
                        accelerationConstraints(2)*sign;
        double time = sqrt((error(0)*error(0) + error(1)*error(1))/(accelerationConstraints(0)*accelerationConstraints(0)));
        Eigen::VectorXd distance(3);
        distance = currentSpeed*time + acceleration*time*time/2.0;
        return currentState.segment(0,3)+distance;
    }

    Eigen::VectorXd DynamicWindowApproach::stopPosition(Eigen::VectorXd &finalState, Eigen::VectorXd &controlInput) {
        Eigen::VectorXd error(3);
        error = -controlInput;
        double speedOrientation = atan2(error(1),error(0));
        Eigen::VectorXd acceleration(3);
        double sign = 1.0;
        if(error(2) < 0)
            sign = -1.0;
        acceleration << accelerationConstraints(0)*cos(speedOrientation),
                accelerationConstraints(1)*sin(speedOrientation),
                accelerationConstraints(2)*sign;
        double time = sqrt((error(0)*error(0) + error(1)*error(1))/(accelerationConstraints(0)*accelerationConstraints(0)));
        Eigen::VectorXd distance(3);
        distance = controlInput*time + acceleration*time*time/2.0;
        return finalState+distance;
    }

    double DynamicWindowApproach::cost(Eigen::VectorXd &controlInput, Eigen::VectorXd &currentState, Eigen::VectorXd& waypoint){
        double cost =0;
        Eigen::VectorXd p1 = finalPosition(currentState,controlInput);
        Eigen::VectorXd p2 = stopPosition(p1,controlInput);
        Eigen::VectorXd waypointError(3);
        waypointError = p1-waypoint;
        waypointError(2) = itandroids_lib::math::MathUtils::wrapToPi(waypointError(2));

        bool safeAccel = checkObsLine({currentState(0),currentState(1)},{p1(0),p1(1)});
        bool safeStop = checkObsLine({p1(0),p1(1)},{p2(0),p2(1)});

        if(safeAccel && safeStop)
        {
            double controlCost = 0.01*controlInput.transpose()*controlInput;
            cost = (waypointError).transpose()*(waypointError);
        }
        else cost = 100000000;

        return cost;
    }

    Eigen::VectorXd DynamicWindowApproach::findOptimal(Eigen::VectorXd &currentState, Eigen::VectorXd &waypoint, int iterations) {
        Eigen::VectorXd u(3);
        Eigen::VectorXd uStar(3);
        double minCost;
        double inputCost;

        for(int i=0;i<iterations;i++)
        {
            u = generateRandomInput();
            inputCost = cost(u,currentState,waypoint);
            if(i == 0 || inputCost < minCost){
                uStar = u;
                minCost = inputCost;
            }
        }

        return uStar;
    }

    Eigen::VectorXd DynamicWindowApproach::generateRandomInput() {
        Eigen::VectorXd u(3);
        u << 0,0,0;

        double radius = rsg.generateUniformDistribution(0,1);
        double angle = rsg.generateUniformDistribution(0,2*M_PI);
        u(0) = radius*cos(angle)*speedConstraints(0);
        u(1) = radius*sin(angle)*speedConstraints(1);
        u(2) = rsg.generateUniformDistribution(-speedConstraints(2),speedConstraints(2));

        return u;

    }

    Eigen::VectorXd DynamicWindowApproach::getControlInput(Eigen::VectorXd &currentState) {
        return findOptimal(currentState,reference,numberOfIterations);
    }

    void DynamicWindowApproach::setReference(const Eigen::VectorXd &newReference) {
        /// Get waypoint reference

        this->reference = newReference;
    }

    void DynamicWindowApproach::setNumberOfIterations(double numberOfIterations) {
        this->numberOfIterations = numberOfIterations;
    }
}