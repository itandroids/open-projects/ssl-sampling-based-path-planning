//
// Created by felipe on 01/04/20.
//

#ifndef SIMULATOR_SPEEDCONTROLLER_H
#define SIMULATOR_SPEEDCONTROLLER_H


#include "controllers/ControllerBase.h"
#include "world_model/WorldModel.h"

namespace controller
{

    class SpeedController : public ControllerBase
    {

    public:
        /**
         * Constructor for class speed controller
         * @param gainMatrix gain matrix for control loop
         * @param sensorMatrix matrix to emulate state space C
         * @param reference vector to evaluate error
         */
        SpeedController(const Eigen::MatrixXd& gainMatrix,
        const Eigen::MatrixXd& sensorMatrix,
        const Eigen::VectorXd& reference);

        /**
         * Sets control reference to controller
         * @param newReference
         */
        void setReference(const Eigen::VectorXd& newReference);

        /**
         * Gets control input given current state
         * @param currentState current system's state
         * @return system's control input
         */
        Eigen::VectorXd getControlInput(Eigen::VectorXd& currentState);

    private:

        /// Control loop gain matrix ``K''

        Eigen::MatrixXd gainMatrix;

        /// Sensor matrix ``C''. Used to extract the observable information from states

        Eigen::MatrixXd sensorMatrix;

        /// Reference vector

        Eigen::VectorXd reference;

    };
}

#endif //SIMULATOR_SPEEDCONTROLLER_H