//
// Created by felipe on 27/06/20.
//

#include <controllers/speed_controller/SpeedController.h>
#include <iostream>


namespace controller {
    SpeedController::SpeedController(const Eigen::MatrixXd& gainMatrix,
                                     const Eigen::MatrixXd& sensorMatrix,
                                     const Eigen::VectorXd& reference)
    : gainMatrix(gainMatrix), reference(reference), sensorMatrix(sensorMatrix), ControllerBase()
    {
    }

    Eigen::VectorXd SpeedController::getControlInput(Eigen::VectorXd& currentState) {
        return (gainMatrix*(reference-sensorMatrix*currentState));
    }

    void SpeedController::setReference(const Eigen::VectorXd &newReference) {
        reference = newReference;
    }
}