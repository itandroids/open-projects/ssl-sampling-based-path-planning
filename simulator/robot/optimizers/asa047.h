//
// Created by felipe on 11/11/20.
//

#ifndef SIMULATOR_ASA047_H
#define SIMULATOR_ASA047_H


#include <functional>

void nelmin (double fn (double x[] ), int n, double start[], double xmin[],
             double *ynewlo, double reqmin, double step[], int konvge, int kcount,
             int *icount, int *numres, int *ifault );

void nelmin ( std::function<double(double[2])> fn, int n, double start[], double xmin[],
              double *ynewlo, double reqmin, double step[], int konvge, int kcount,
              int *icount, int *numres, int *ifault );
void timestamp ( );

#endif //SIMULATOR_ASA047_H
