//
// Created by felipe on 27/06/20.
//

#ifndef SIMULATOR_CAMERASYSTEM_H
#define SIMULATOR_CAMERASYSTEM_H

#include <emulator/Emulator.h>
#include <thread>
#include <chrono>
#include <controllers/speed_controller/SpeedController.h>
#include <controllers/tracking/Tracking.h>

namespace emulator {

    struct SystemAnalysis {
        SystemAnalysis()
        {
            planningTime = 0;
        }
        double planningTime;
    };

    typedef struct SystemAnalysis SystemAnalysis;

    class CameraSystem : public Emulator {
    public:
        /**
         * Constructor for Emulator class.
         * @param robot class to represent robot's model used
         * @param steps number of timesteps to be simulated
         * @param field shared pointer to gui
         * @param robots number of robots to be emulated
         * @param cameraFactor factor defined as  cameraSampleTime/robotSampleTime. Useful to emulate control loops.
         * @param visionDelayFactor factor defined as visionDelayTime/robotSampleTime. Useful to emulate control loops
         * @param initialState initial system state. 6x1 state, [x y rot \dot{x} \dot{y} \dot{yaw}]^T
         */
        CameraSystem(robot_model::RobotModelBase* robot, int steps,std::shared_ptr<itandroids_lib::gui::FieldWidget> field,
                     int robots, int cameraFactor, int visionDelayFactor, const std::vector<double>& initialState,
                     controller::ControllerBase& kinematicController);

        /**
         * Constructor for Emulator class with undefined number of timesteps.
         * @param robot class to represent robot's model used
         * @param field shared pointer to gui
         * @param robots number of robots to be emulated
         * @param cameraFactor factor defined as  cameraSampleTime/robotSampleTime. Useful to emulate control loops.
         * @param visionDelayFactor factor defined as visionDelayTime/robotSampleTime. Useful to emulate control loops
         * @param initialState initial system state. 6x1 state, [x y rot \dot{x} \dot{y} \dot{yaw}]^T
         */
        CameraSystem(robot_model::RobotModelBase* robot,std::shared_ptr<itandroids_lib::gui::FieldWidget> field,
                     int robots, int cameraFactor, int visionDelayFactor, const std::vector<double>& initialState,
                     controller::ControllerBase& kinematicController);

        /**
         * Starts SSL simulation. Starts a single thread for replanning and keeps the simulation going.
         * When the robot reaches the objective, the simulation ends.
         */
        void setup();

        /**
         * Set obstacles to emulator.
         * @param obstacles list of obstacle positions
         */
        void setObstacles(std::vector<std::vector<double>>& obstacles);

        void setStart(std::vector<double> start);

        void setGoal(std::vector<double> goal);

        /**
         * Function to enable replanning thread.
         * @param replanningTime fixed interval used for replanning in a regular time basis
         */
        void replanningThread(double replanningTime, std::vector<double>& start, std::vector<double>& goal);

        static std::shared_ptr<std::thread> startThread(CameraSystem& system,double planningTime, std::vector<double> &start,std::vector<double> &goal);

    private:

        void emulate(int k);

        bool arrived(double diskRadius, int k);

        /**
         * Function used to update obstacle position according to the vector of positions plus orientation
         * @param k simulation step
         */
        void drawObstacles(int k);

        /// cameraSampleTime/robotSampleTime factor

        int cameraFactor;

        /// list of obstacles

        std::vector<std::vector<double>> obstacles;

        /// visionDelayTime/robotSampleTime factor

        int visionDelayFactor;

        /// Robot sample time

        double sampleTime;

        /// Kinematic loop input

        Eigen::VectorXd kinematicLoopOutput;

        /// Robot speed loop input

        Eigen::VectorXd speedLoopInput;

        /// Kinematic controller for position loop

        controller::ControllerBase* kinematicController;

        /// Robot's current 6x1 state

        Eigen::VectorXd currentState;

        /// Robot's kinematic controller reference

        Eigen::VectorXd kinematicReference;

        /// Mutex to use in thread operations

        std::mutex mutex;

        /// Mutex to make start and goal operations safe

        std::mutex goalMutex;

        /// Clock to measure time spent on replanning thread

        tools::Clock clock;

        /**
         * Updates kinematic reference according to minimum disk
         * @param reference kinematic reference vector.
         * @param minDistance minimum distance value.
         * @return false in case waypoint queue is empty.
         */
        bool updateReferenceWaypoint(Eigen::VectorXd& reference, double minDistance);


        /// Tracking radius to make waypoint clearance

        const double trackingRadius = 0.5;

        /// Boolean value to allow continuous replanning

        std::atomic<bool> continueReplanning;

        /// Start spot

        std::vector<double> start;

        /// Goal spot

        std::vector<double> goal;

        /// Goal state

        Eigen::VectorXd goalState;

        /// Replanning thread

        std::shared_ptr<std::thread> replanning;

        /**
         * Ends system component behaviour.
         */
        void end();

        /// number of robots within the simulation
        int numberOfRobots;

        /// collision counter
        int collisions;

    };
}


#endif //SIMULATOR_CAMERASYSTEM_H
