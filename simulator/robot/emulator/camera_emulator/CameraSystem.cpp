//
// Created by felipe on 27/06/20.
//
#include <constants/Constants.h>
#include "CameraSystem.h"
#include <math.h>



namespace emulator {
    CameraSystem::CameraSystem(robot_model::RobotModelBase* robot, int steps,std::shared_ptr<itandroids_lib::gui::FieldWidget> field,
                               int robots,int cameraFactor, int visionDelayFactor, const std::vector<double>& initialState,
                               controller::ControllerBase& kinematicController): Emulator(robot, steps, field, robots), cameraFactor(cameraFactor), visionDelayFactor(visionDelayFactor), sampleTime(robot->getSampleTime()),
                                                                          currentState(6), kinematicReference(3), mutex(), goalMutex(), start(2,0), goal(2,0), kinematicLoopOutput(3,1), clock(), numberOfRobots(robots), obstacles(robots,{0,0}), goalState(6)
    {
        teamStates[0].teammates[0].setValues(initialState);
        teamStates[0].teammatesStates[0]
                << initialState[0], initialState[1], initialState[2], initialState[3], initialState[4],initialState[5];
        kinematicLoopOutput << 0.0, 0.0, 0.0;
        goalState << goal[0], goal[1], 0.0, 0.0 , 0.0, 0.0;
        this->kinematicController = &kinematicController;

    }


    CameraSystem::CameraSystem(robot_model::RobotModelBase *robot,
                               std::shared_ptr<itandroids_lib::gui::FieldWidget> field, int robots, int cameraFactor,
                               int visionDelayFactor, const std::vector<double> &initialState,
                               controller::ControllerBase &kinematicController): Emulator(robot, field, robots), cameraFactor(cameraFactor), visionDelayFactor(visionDelayFactor), sampleTime(robot->getSampleTime()),
                                                                                currentState(6), kinematicReference(3), mutex(), goalMutex(), start(2,0), goal(2,0), kinematicLoopOutput(3,1), clock(), numberOfRobots(robots), obstacles(robots,{0,0}), goalState(6){
        teamStates[0].teammates[0].setValues(initialState);
        teamStates[0].teammatesStates[0]
                << initialState[0], initialState[1], initialState[2], initialState[3], initialState[4],initialState[5];
        kinematicLoopOutput << 0.0, 0.0, 0.0;
        goalState << goal[0], goal[1], 0.0, 0.0 , 0.0, 0.0;
        this->kinematicController = &kinematicController;
    }

    bool CameraSystem::updateReferenceWaypoint(Eigen::VectorXd &reference, double minDistance) {

        /// If waypoint is within the minDistance disk, go to next waypoint

        std::vector<double> waypoint = robot->getPlanner().getTrackingWaypoint(mutex, robot->getRobotState(), trackingRadius);
        if(waypoint.empty())
            return false;
        reference << waypoint[0],waypoint[1],waypoint[2];
        return true;
    }

    void CameraSystem::emulate(int k) {

        /// Evaluate \dot{x}_{ref}, \dot{y}_{ref} and \dot{yaw}_{ref}.
        double timeSpent = 0.0;


        timeSpent = clock.getTime();
        /// Kinematic reference update
        if(cameraFactor != 0 && (k%cameraFactor) == 0 && k != 0 && updateReferenceWaypoint(kinematicReference,trackingRadius))
        {


            /// Input for kinematic loop is reference position x_{ref}, y_{ref} and yaw_{ref}.
            kinematicController->setReference(kinematicReference);
            kinematicLoopOutput = kinematicController->getControlInput(teamStates[k-1-visionDelayFactor].teammatesStates[0]);
            {
                std::lock_guard<std::mutex> guard(goalMutex);
                start[0] = teamStates[k-1-visionDelayFactor].teammatesStates[0](0);
                start[1] = teamStates[k-1-visionDelayFactor].teammatesStates[0](1);
            }

        }

        /// Emulates robot speed loop.
        robot->track(kinematicLoopOutput);

        /// Storing information to draw on gui

        teamStates[k+1].teammatesStates[0] = robot->getRobotState();

        teamStates[k+1].teammates[0].translation.x = teamStates[k].teammatesStates[0](0);
        teamStates[k+1].teammates[0].translation.y = teamStates[k].teammatesStates[0](1);
        teamStates[k+1].teammates[0].rotation = teamStates[k].teammatesStates[0](2);

        /// Check possible collision between states
        {
            std::vector<double> state = {robot->getRobotState()(0),robot->getRobotState()(1)};
            if(!robot->getPlanner().belongsToFree(state)){
                collisions++;
            }
        }

        /// Drawing obstacles for next timestep

        drawObstacles(k+1);

        timeSpent = clock.getTime()-timeSpent;
        if(sampleTime-timeSpent > 0)
            std::this_thread::sleep_for(std::chrono::microseconds((int)(sampleTime-timeSpent)*1000000));

    }

    std::shared_ptr<std::thread> CameraSystem::startThread(CameraSystem& system,double planningTime, std::vector<double> &start,std::vector<double> &goal)
    {
        return std::make_shared<std::thread>(&CameraSystem::replanningThread,&system,planningTime,std::ref(start),std::ref(goal));
    }

    void CameraSystem::setup() {
        /// Starts replanning...
        continueReplanning = true;
        collisions = 0; // initializing collision counter
        //replanning = std::make_shared<std::thread>(&CameraSystem::replanningThread,this,robot_constants::PLANNING_TIME,std::ref(start),std::ref(goal));
        replanning = startThread(*this,robot_constants::PLANNING_TIME,start,goal);
    }

    void CameraSystem::replanningThread(double replanningTime, std::vector<double> &start, std::vector<double> &goal) {
        /// Always replanning
        double timeSpent = 0.0;
        while(continueReplanning){
            timeSpent = clock.getTime();
            robot->getPlanner().setStartAndGoal(start,goal, goalMutex);
            robot->getPlanner().solveProblem(replanningTime,mutex);
            timeSpent = clock.getTime()-timeSpent;
            if(replanningTime-timeSpent > 0)
                std::this_thread::sleep_for(std::chrono::microseconds((int)(replanningTime-timeSpent)));
        }
        ;
    }

    void CameraSystem::end()
    {
        continueReplanning = false;
    }

    void CameraSystem::setStart(std::vector<double> start) {
        this->start = start;
    }

    void CameraSystem::setGoal(std::vector<double> goal) {
        this->goal = goal;
        if(goal.size() == 2)
            goalState << goal[0],goal[1],0.0,0.0,0.0,0.0;
        else if(goal.size()== 3)
            goalState << goal[0],goal[1],goal[2],0.0,0.0,0.0;
    }

    void CameraSystem::drawObstacles(int k) {
        for(int i=1; i<numberOfRobots;i++)
        {
            teamStates[k].teammates[i].translation.x = obstacles[i-1][0];
            teamStates[k].teammates[i].translation.y = obstacles[i-1][1];
            teamStates[k].teammates[i].rotation = obstacles[i-1][2];
        }
    }

    void CameraSystem::setObstacles(std::vector<std::vector<double>> &obstacles) {
        /// Caching obstacles reference inside class

        this->obstacles = obstacles;

        /// Set obstacles on path planner

        robot->getPlanner().setObstaclePositions(obstacles);

        /// Draw obstacles at timestep 0

        drawObstacles(0);
    }

    bool CameraSystem::arrived(double diskRadius, int k) {
        if(k < 1+visionDelayFactor)
            return false;
        Eigen::VectorXd currentState = teamStates[k-1-visionDelayFactor].teammatesStates[0];
        Eigen::VectorXd error = currentState-goalState;
        /// Angles cannot be included in euclidean distance
        double distance2 = error.transpose()*error - error(2)*error(2);
        /// Computing angle distance separately
        double currentAngle = currentState(2);
        double goalAngle = goalState(2);
        double angleDistance = (cos(currentAngle)-cos(goalAngle))*(cos(currentAngle)-cos(goalAngle))
                               + (sin(currentAngle)-sin(goalAngle))*(sin(currentAngle)-sin(goalAngle));
        distance2 += angleDistance;
        return (distance2 < diskRadius*diskRadius);
    }

}