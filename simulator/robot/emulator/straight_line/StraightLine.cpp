//
// Created by felipe on 14/06/20.
//

#include <gui/draw_request/DrawSSLTeamRequest.h>
#include "StraightLine.h"


namespace emulator {
    StraightLine::StraightLine(int steps, std::shared_ptr<itandroids_lib::gui::FieldWidget> field,
    const std::vector<double>& initialState) :
            Emulator(nullptr, steps, field, 1)
    {
        teamStates[0].teammates[0].setValues(initialState);
    }

    void StraightLine::emulate(int k)
    {
        teamStates[k+1].teammates[0].translation.x = teamStates[k].teammates[0].translation.x + epsilon;
    }

}