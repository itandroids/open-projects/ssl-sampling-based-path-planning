//
// Created by felipe on 14/06/20.
//

#ifndef SIMULATOR_STRAIGHTLINE_H
#define SIMULATOR_STRAIGHTLINE_H

#include <emulator/Emulator.h>
#include <world_model/Team.h>
#include <chrono>
#include "utils/Colors.h"
#include <thread>

namespace emulator {
    class StraightLine : public Emulator{

    private:
    void emulate(int k);

    double timestep;

    const double epsilon = 0.01; /// variation in x for each timestep

    public:
    /**
     * Constructor for class StraightLine
     * @param steps number of simulated timesteps
     * @param field Gui widget
     * @param initialState initial robot state
     */
    StraightLine(int steps, std::shared_ptr<itandroids_lib::gui::FieldWidget> field,
    const std::vector<double>& initialState);

    };
}



#endif //SIMULATOR_STRAIGHTLINE_H
