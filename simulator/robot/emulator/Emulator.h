//
// Created by felipe on 29/04/20.
//

#ifndef SIMULATOR_EMULATOR_H
#define SIMULATOR_EMULATOR_H

#include "robot_model/RobotModelBase.h"
#include <gui/FieldWidget.h>
#include <world_model/Team.h>
#include <gui/draw_request/DrawRequest.h>

namespace emulator {

    /**
     * Class to emulate the robot problem
     */
    class Emulator {
        public:
        /**
         * Constructor for Emulator class. Simulates a finite amount of timesteps.
         * @param robot class to represent robot's model used
         * @param steps number of timesteps to be simulated
         * @param field shared pointer to gui
         * @param robots number of robots to be emulated
         */
        Emulator(robot_model::RobotModelBase* robot, int steps,std::shared_ptr<itandroids_lib::gui::FieldWidget> field, int robots);

        /**
         * Constructor for Emulator class. Simulates an indefinite amount of timesteps.
         * @param robot class to represent robot's model used
         * @param field shared pointer to gui
         * @param robots number of robots to be emulated
         */
        Emulator(robot_model::RobotModelBase* robot, std::shared_ptr<itandroids_lib::gui::FieldWidget> field, int robots);

        /**
         * Starts simulation of physics and communication with gui.
         * @param sampleTime simulation sample time
         */
        void startSimulation(double sampleTime);

        /**
         * Starts simulation of physics and communication with gui (controlled by a boolean variable).
         * @param sampleTime system's sample time
         * @param showGui boolean to show gui
         */
        void startSimulation(double sampleTime, bool showGui);


        /**
         * Simulates arrival of robot at a given state
         * @param sampleTime system's sample time
         * @param showGui boolean to show gui
         * @param diskRadius goal clearance distance
         * @return number of timesteps necessary to arrive at a given objective
         */
        int simulateArrival(double sampleTime, bool showGui, double diskRadius);

        /**
         * Get statistics from given sequence of goals and starts. Main statistics are:
         * Average time of arrival (ATOA)
         * Number of collisions (NC)
         * @param startSequence sequence of start positions
         * @param goalSequence sequence of goal positions
         * @param sampleTime system's sample time
         * @param showGui boolean to enable/disable gui
         */
        void getStatistics(const std::vector<std::vector<double>>& startSequence,
                           const std::vector<std::vector<double>>& goalSequence,
                            double sampleTime,
                            bool showGui);

        /**
         * Function to check whether the robot has arrived close to the goal state
         * @param diskRadius radius within the function will return true
         * @param k emulation step
         * @return true in case the state has been reached
         */
        virtual bool arrived(double diskRadius, int k);

        /**
         * Set start position
         * @param start position
         */
        virtual void setStart(std::vector<double> start);

        /**
         * Set goal position
         * @param goal position
         */
        virtual void setGoal(std::vector<double> goal);

        const std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>> &getRequestsSequence() const;

        /**
         * Run Gui
         * @param sampleTime to perform animations
         */
        void runGui(double sampleTime);

        protected:

        /**
         * Sets up emulator configs. Its objective may change depending on the class considered.
         */
        virtual void setup();

        /**
         * Ends component behaviour.
         */
        virtual void end();

        int timesteps; /// simulation number of timesteps

        /// Number of robots considered in the simulation

        int numberOfRobots;

        robot_model::RobotModelBase* robot; /// robot with its model

        std::shared_ptr<itandroids_lib::gui::FieldWidget> field; /// field gui

        /**
         * Emulates robot's states at step k
         */
        virtual void emulate(int k) = 0;

        /**
         * Sends draw requests to Gui
         */
        void sendDrawRequestsToGui();

        std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>> requestsSequence; /// Sequence of requests to be drawn

        std::vector<world_model::Team> teamStates; /// team position states indexed in time


    };
}



#endif //SIMULATOR_EMULATOR_H
