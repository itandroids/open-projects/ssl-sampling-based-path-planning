//
// Created by felipe on 29/04/20.
//

#include "Emulator.h"
#include "robot_model/RobotModelBase.h"
#include <gui/draw_request/DrawSSLTeamRequest.h>

namespace emulator {
    Emulator::Emulator(robot_model::RobotModelBase* robot, int steps, std::shared_ptr<itandroids_lib::gui::FieldWidget> field, int robots):
        timesteps(steps), field(field), teamStates(steps, world_model::Team(robots, itandroids_lib::utils::Color::BLUE, itandroids_lib::utils::Color::WHITE)), numberOfRobots(robots)
    {
        this->robot = robot;
    }

    Emulator::Emulator(robot_model::RobotModelBase* robot, std::shared_ptr<itandroids_lib::gui::FieldWidget> field, int robots)
            :timesteps(0), field(field), teamStates(1, world_model::Team(robots, itandroids_lib::utils::Color::BLUE, itandroids_lib::utils::Color::WHITE)), numberOfRobots(robots)
    {
        this->robot = robot;
    }

    void Emulator::sendDrawRequestsToGui() {
        for(int i=0;i<timesteps;i++)
        {
            std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>> requests; /// draw requests
            requests.emplace_back(std::make_shared<itandroids_lib::gui::DrawSSLTeamRequest>(teamStates[i]));
            requestsSequence.emplace_back(requests);
        }
    }

    void Emulator::runGui(double sampleTime) {
        field->setFixedSize(1280, 720);
        field->fitToScreen();
        field->startAnimation(sampleTime, requestsSequence);
        field->show();
    }

    void Emulator::startSimulation(double sampleTime) {
        /// Start simulation showing on gui
        startSimulation(sampleTime,true);
    }

    int Emulator::simulateArrival(double sampleTime, bool showGui,double diskRadius) {
        int step = 0;
        /// robot simulation
        setup();
        while(!arrived(diskRadius,step))
        {
            teamStates.emplace_back(world_model::Team(numberOfRobots, itandroids_lib::utils::Color::BLUE, itandroids_lib::utils::Color::WHITE));
            emulate(step++);
        }
        end();
        timesteps = step;
        if(showGui)
        {
            sendDrawRequestsToGui();
            runGui(sampleTime);
        }

        return step;
    }

    bool Emulator::arrived(double diskRadius, int k) {
        return true;
    }

    void Emulator::startSimulation(double sampleTime, bool showGui) {
        /// robot simulation
        setup();
        for(int i=0;i<timesteps-1;i++){
            emulate(i);
        }
        end();
        if(showGui)
        {
            sendDrawRequestsToGui();
            runGui(sampleTime);
        }
    }

    void Emulator::getStatistics(const std::vector<std::vector<double>>& startSequence,
                                 const std::vector<std::vector<double>>& goalSequence,
                                double sampleTime, bool showGui) {
        if(startSequence.size() != goalSequence.size())
        {
            std::cout << "Vectors don't have the same size" << std::endl;
            return;
        }
        for(int i=0; i<startSequence.size();i++)
        {
            setStart(startSequence[i]);
            setGoal(goalSequence[i]);
            startSimulation(sampleTime,showGui);
        }

    }

    const std::vector<std::vector<std::shared_ptr<itandroids_lib::gui::DrawRequest>>> &
    Emulator::getRequestsSequence() const {
        return requestsSequence;
    }

    void Emulator::setup() {
        /// does nothing... derived classes may override.
    }

    void Emulator::end(){
        /// does nothing... derived classes may override.
    }

    void Emulator::setStart(std::vector<double> start) {
        /// does nothing... derived classes may override.
        throw;
    }

    void Emulator::setGoal(std::vector<double> goal) {
        /// does nothing... derived classes may override.
        throw;
    }
}