//
// Created by felipe on 20/05/20.
//

#ifndef SIMULATOR_RANDOMSTATEGENERATOR_H
#define SIMULATOR_RANDOMSTATEGENERATOR_H

#include <random>
#include <constants/Constants.h>

namespace random_state {

    class RandomStateGenerator{

    private:
        std::uniform_real_distribution<double> uniformDistribution;
        std::mt19937 generator; //Standard mersenne_twister_engine seeded with rd()

        /**
         * Evaluates L^2 norm of vector x1-x2
         * @param x1
         * @param x2
         * @return distance measure
         */
        double norm2(std::vector<double> x1, std::vector<double> x2);


    public:

        /**
         * RandomStateGenerator class constructor
         */
        RandomStateGenerator();

        /**
         * RandomStateGenerator class constructor
         * @param seed random seed
         */
        RandomStateGenerator(int seed);

        ~RandomStateGenerator()=default;

        /**
         * Returns random state given bounds
         * @param bounds
         * @return random state
         */
        std::vector<double> returnRandomState(const std::vector<double>& bounds);

        /**
         * Returns random list of obstacles given its radius. The obstacles must have no intersections.
         * @param obstacleRadius
         * @param numberOfObstacles
         * @param bounds
         * @return list of obstacles position over space
         */
        std::vector<std::vector<double>> generateRandomObstacles(double obstacleRadius, int numberOfObstacles, std::vector<double> bounds);

        /**
         * Generates uniform random number which lies within [min,max]
         * @param min
         * @param max
         * @return random number
         */
        double generateUniformDistribution(double min, double max);

        /**
         * Generates random kinodynamic state
         * @param state state to be sampled
         * @param maxVelocity maximum velocity allowed
         * @param maxX maximum x value allowed over the field
         * @param maxY maximum y value allowed over the field
         */
        void generateRandomKinodynamicState(double* state, const double& maxVelocity, const double& maxX, const double& maxY);

        /**
         * Generates random kinodynamic state
         * @param state state to be sampled
         * @param maxVelocity maximum velocity allowed
         * @param minX minimum x value allowed over the field
         * @param maxX maximum x value allowed over the field
         * @param minY minimum y value allowed over the field
         * @param maxY maximum y value allowed over the field
         * @param maxAcceleration maximum acceleration allowed
         * @param fieldLimits vector with maximum field limits [x_\text{max} y_\text{max}]^T
         */
        void generateRandomKinodynamicState(double* state, const double& maxVelocity, const double& minX, const double& maxX,const double& minY , const double& maxY,
                                                const double& maxAcceleration, const std::vector<double>& fieldLimits);

        /**
         * Generates random kinodynamic state
         * @param state state to be sampled
         * @param maxVelocity maximum velocity allowed
         * @param maxX maximum x value allowed over the field
         * @param maxY maximum y value allowed over the field
         */
        void generateRandomKinodynamicState(std::vector<double>& state, const double& maxVelocity, const double& maxX, const double& maxY);

        /**
         * Generate static obstacles
         * @param maxX max x position
         * @param maxY max y position
         * @param obstacles array of arrays with size corresponding to number of obstacles. Is filled.
         */
        void generateRandomKinodynamicStaticObstacles(const double& maxX, const double& maxY, std::vector<std::vector<double>>& obstacles);

        /**
         * @param maxX max x position
         * @param maxY max y position
         * @param obstacles array of arrays with size corresponding to number of obstacles. Is filled.
         * @param separation minimum distance between obstacles
         */
        void generateSeparatedRandomKinodynamicStaticObstacles(const double& maxX, const double& maxY, std::vector<std::vector<double>>& obstacles, double separation);
    };
}




#endif //SIMULATOR_RANDOMSTATEGENERATOR_H
