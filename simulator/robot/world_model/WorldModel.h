//
// Created by felipe on 01/04/20.
//

#ifndef SIMULATOR_WORLDMODEL_H
#define SIMULATOR_WORLDMODEL_H

#include <Eigen/Dense>

namespace world_model
{

    class WorldModelBase
    {
        WorldModelBase() = default;

        /**
         * Function to know if a given state is valid with no collisions
         * @return
         */
        virtual bool isValidState();

        /**
         * Add obstacles to Space
         */
        virtual void addObstacles();



    };
}

#endif //SIMULATOR_WORLDMODEL_H