//
// Created by felipe on 13/06/20.
//

#include "Team.h"

namespace world_model {
    Team::Team(int teamSize, const itandroids_lib::utils::Color &teamColor,
               const itandroids_lib::utils::Color &textColor) : teammates(teamSize,itandroids_lib::math::Pose2D()),
                                                                teammatesIds(teamSize,0), teamColor(teamColor), textColor(textColor),
                                                                teammatesStates(teamSize,Eigen::VectorXd(6))
    {
        for(int i=0;i < teamSize; i++)
        {
            teammatesIds[i] = i;
        }
    }

    const std::vector<int> &Team::getTeammatesIds() const {
        return teammatesIds;
    }

    const itandroids_lib::utils::Color &Team::getTeamColor() const {
        return teamColor;
    }

    const itandroids_lib::utils::Color &Team::getTextColor() const {
        return textColor;
    }


}