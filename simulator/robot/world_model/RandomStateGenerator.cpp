//
// Created by felipe on 20/05/20.
//

#include "RandomStateGenerator.h"
#include <math.h>


namespace random_state {
    RandomStateGenerator::RandomStateGenerator() : generator(std::random_device{}()), uniformDistribution(0,1)
    {
    }
    
    RandomStateGenerator::RandomStateGenerator(int seed): generator(seed), uniformDistribution(0,1){
    }

    double RandomStateGenerator::generateUniformDistribution(double min, double max)
    {
        return uniformDistribution(generator)*(max-min)+min;
    }

    double RandomStateGenerator::norm2(std::vector<double> x1, std::vector<double> x2)
    {
        double distance2 = 0.0;
        double diff2 = 0.0;

        for(int i=0;i<x1.size();i++)
        {
            diff2 = (x1[i]-x2[i]);
            distance2 += diff2*diff2;
        }

        return distance2;
    }

    std::vector<double> RandomStateGenerator::returnRandomState(const std::vector<double>& bounds) {
        std::vector<double> ans(bounds.size()/2);
        for(int i=0;i<ans.size();i++)
            ans[i] = generateUniformDistribution(bounds[2*i],bounds[2*i+1]);
        return ans;
    }

    std::vector<std::vector<double>> RandomStateGenerator::generateRandomObstacles(double obstacleRadius,
                                                                                   int numberOfObstacles,
                                                                                   std::vector<double> bounds) {
        std::vector<std::vector<double>> obstacles(0);
        std::vector<double> state;
        for(int i=0;i<numberOfObstacles;i++)
        {
            bool invalidState = true;
            while(invalidState)
            {
                state = returnRandomState(bounds);
                invalidState = false;
                for(int j=0; j<obstacles.size();j++)
                {
                    if(norm2(state,obstacles[j]) < obstacleRadius*obstacleRadius)
                        invalidState = true;
                }
            }
            obstacles.push_back(state);
        }
        return obstacles;
    }

    void RandomStateGenerator::generateRandomKinodynamicState(double *state, const double &maxVelocity,
                                                              const double &maxX, const double &maxY) {
        double speedAngle = generateUniformDistribution(0,2.0*M_PI);
        double speedAbs = maxVelocity*sqrt(generateUniformDistribution(0,1));
        state[0] =  generateUniformDistribution(-maxX,maxX);
        state[1] = speedAbs*cos(speedAngle);
        state[2] = generateUniformDistribution(-maxY,maxY);
        state[3] = speedAbs*sin(speedAngle);
    }

    void RandomStateGenerator::generateRandomKinodynamicState(double *state, const double &maxVelocity,const double&minX,
                                                              const double &maxX, const double& minY, const double &maxY,
                                                              const double &maxAcceleration, const std::vector<double>& fieldLimits) {

        bool isValid = false;
        double runOffDistanceX = 0.0;
        double runOffDistanceY = 0.0;
        double speedAngle = 0.0;
        double speedAbs = 0.0;
        while(!isValid)
        {
            speedAngle = generateUniformDistribution(0,2.0*M_PI);
            speedAbs = maxVelocity*sqrt(generateUniformDistribution(0,1));
            state[1] = speedAbs*cos(speedAngle);
            state[3] = speedAbs*sin(speedAngle);

            //runOffDistanceX = state[1]*state[1]/(2*maxAcceleration*0.707);
            //runOffDistanceY = state[3]*state[3]/(2*maxAcceleration*0.707);
            runOffDistanceX = state[1]*state[1]/(2*maxAcceleration*robot_constants::COS45);
            runOffDistanceY = state[3]*state[3]/(2*maxAcceleration*robot_constants::COS45);

            state[0] =  generateUniformDistribution(minX,maxX);
            state[2] = generateUniformDistribution(minY,maxY);

           isValid = (state[0] >= -fieldLimits[0] + runOffDistanceX && state[0] <= fieldLimits[0] - runOffDistanceX)
                   && (state[1] >= -fieldLimits[1] + runOffDistanceY && state[1] <= fieldLimits[1] - runOffDistanceY);
        }
    }

    void RandomStateGenerator::generateRandomKinodynamicState(std::vector<double>& state, const double &maxVelocity,
                                                              const double &maxX, const double &maxY) {
        generateRandomKinodynamicState(&state[0],maxVelocity,maxX,maxY);
    }

    void RandomStateGenerator::generateRandomKinodynamicStaticObstacles(const double &maxX, const double &maxY,
                                                                        std::vector<std::vector<double>> &obstacles) {
        for(int i=obstacles.size();i--;)
            generateRandomKinodynamicState(obstacles[i],0,maxX,maxY);
    }

    void RandomStateGenerator::generateSeparatedRandomKinodynamicStaticObstacles(const double &maxX, const double &maxY,
                                                                        std::vector<std::vector<double>> &obstacles, double separation){
        std::vector<double> obstacle(4,0); /// Obstacle should be a state with zero velocity
        bool found;
        double distance2 = 0.0;  
        double separation2 = separation*separation;                                                              
        for(int i=0;i<obstacles.size();i++)
        {
            found = false;
            while(!found)
            {
                generateRandomKinodynamicState(obstacle,0,maxX,maxY);
                found = true;
                for(int j = 0;j < i;j++)
                {
                    //std::cout << "Checking" << std::endl;
                    distance2 = (obstacles[j][0]-obstacle[0])*(obstacles[j][0]-obstacle[0]) + (obstacles[j][2]-obstacle[2])*(obstacles[j][2]-obstacle[2]);
                    if(distance2 < separation2)
                    {
                        found = false;
                        break;
                    }
                }
            }
            obstacles[i] = obstacle;

        }
                                                                        
                                                                        
    }

}