//
// Created by felipe on 13/06/20.
//

#ifndef SIMULATOR_TEAM_H
#define SIMULATOR_TEAM_H


#include <vector>
#include <utils/Colors.h>
#include "math/Pose2D.h"
#include <Eigen/Dense>

namespace world_model {
    class Team {
    public:
        /**
         * Constructor for Team class
         */
        Team() =  default;

        /**
         * Constructor for team class
         * @param teamSize number of teammates
         * @param teamColor T-shirt color
         * @param textColor
         */
        Team(int teamSize, const itandroids_lib::utils::Color &teamColor,
             const itandroids_lib::utils::Color &textColor);

        std::vector<itandroids_lib::math::Pose2D> teammates; /// teammates 2D-pose

        std::vector<Eigen::VectorXd> teammatesStates;

        const std::vector<int> &getTeammatesIds() const;

        /**
         * Get T-shirt color
         * @return color type
         */
        const itandroids_lib::utils::Color &getTeamColor() const;

        /**
         * Get text color
         * @return color type
         */
        const itandroids_lib::utils::Color &getTextColor() const;

    private:
        std::vector<int> teammatesIds;
        itandroids_lib::utils::Color teamColor;
        itandroids_lib::utils::Color textColor;

    };
}



#endif //SIMULATOR_TEAM_H
