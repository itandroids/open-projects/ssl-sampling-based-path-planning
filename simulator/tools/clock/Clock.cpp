/*
 * Clock.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: mmaximo
 */

#include "Clock.h"

#include <sys/time.h>

namespace tools {
    double Clock::getTime() {
        struct timeval tv;
        struct timezone tz;

        gettimeofday(&tv, &tz);
        return (tv.tv_sec + tv.tv_usec / 1000000.0);
    }
} /* namespace utils */
