/*
 * Clock.h
 *
 *  Created on: Oct 28, 2015
 *      Author: mmaximo
 */

#ifndef ITANDROIDS_LIB_UTILS_CLOCK_H_
#define ITANDROIDS_LIB_UTILS_CLOCK_H_

namespace tools {
    class Clock {
    public:
        Clock() = default;

        virtual ~Clock() = default;

        /**
         * The number of seconds elapsed since the epoch
         * (1/1/1970 00:00:00 UTC) until logging event was created. */
        static double getTime();
    };
}

#endif /* ITANDROIDS_LIB_UTILS_CLOCK_H_ */
