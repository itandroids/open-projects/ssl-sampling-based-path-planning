file(GLOB_RECURSE SRCS *.cpp *.h)

add_library( tools
        ${SRCS})

target_include_directories(tools PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(tools)

