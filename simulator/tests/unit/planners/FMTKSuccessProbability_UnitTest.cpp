//
// Created by felipe on 03/12/20.
//


#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <world_model/RandomStateGenerator.h>

#include "gtest/gtest.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;






TEST(FMTKMonteCarloTest, planning)
{

    auto linspace = [](std::vector<double>& vector, double begin, int spaces, double end)
    {
        double chunks = (end-begin)/(spaces-1.0);
        for(int i=0;i<spaces;i++)
        {
            vector[i] = begin+i*chunks;
        }
    };

    ompl::msg::noOutputHandler();
    auto allocMyValidStateSampler = [](const ompl::base::SpaceInformation *si)
    {
        return std::make_shared<dss::KinematicStateSampler>(si);
    };

    namespace ob = ompl::base;
    namespace og = ompl::geometric;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>(1));

    std::ofstream outputFile;
    outputFile.open("FMTKSuccessProbability.txt");

    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double emulationSampleTime = 0.03;
    int numberOfObstacles = 10;

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(numberOfObstacles,std::vector<double>(4,0));
    rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));
    bvpSolver->setFrameOptimization(false);

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);

    // set state validity checking for this space
    si->setStateValidityChecker(dsc);

    // set motion validity checking for this space
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));
    dvc->setMaxAcceleration(maxAcceleration);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setEmulationSampleTime(emulationSampleTime);


    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    start = std::vector<double> {-3.5,0,0,0};

    // create a random goal state
    ob::ScopedState<> goal(space);
    goal = std::vector<double> {3.5,0.0,0.0,0.0};


    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    std::cout << std::endl;

    // create a planner for the defined space
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));

    /// Loop to evaluate success probability
    int numberOfSuccesses = 0;
    double successProbability = 0.0;
    int iterations = 10;
    int i=0;
    int size = 1;
    double begin = 0.0;
    double end = 1.0;
    //std::vector<double> planningTimes(size,0);
    //linspace(planningTimes,begin,size,end);
    std::vector<double> planningTimes = {1.0};
    
    /// Loop to get success probability
    for(int i=0;i<size;i++)
    {
        for(int j = 0; j<iterations;j++)
        {
            // create a problem instance
            auto pdef(std::make_shared<ob::ProblemDefinition>(si));

            // set the start and goal states
            pdef->setStartAndGoalStates(start, goal);

            // clear planner's datastructures
            planner->clear();

            // perform setup steps for the planner
            planner->setup();

            // set the problem we are trying to solve for the planner
            planner->setProblemDefinition(pdef);


            planner->setNumSamples(50);



            //planner->setGoalBias(0.7);

            ob::PlannerStatus solved = planner->ob::Planner::solve(planningTimes[i]);

            if(solved == ob::PlannerStatus::EXACT_SOLUTION)
                numberOfSuccesses++;
        }

        successProbability = ((double)numberOfSuccesses)/((double)iterations);
        std::cout << "Number of Successes: " << numberOfSuccesses << std::endl;
        outputFile <<  successProbability << " " << planningTimes[i] << std::endl;
        std::cout << "Case of planning time (" << i+1 << "/" << size << ") ended with probability " << successProbability
                                                                                                    << " for " << planningTimes[i] << " seconds." << std::endl;

        /// Emptying success number
        numberOfSuccesses = 0;
    }

}






