//
// Created by felipe on 30/11/20.
//

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
#include <clock/Clock.h>

#include <controllers/tracking/MinimumTime2DController.h>
#include <memory>
#include <world_model/RandomStateGenerator.h>
#include "gtest/gtest.h"
# include "optimizers/asa047.h"
#include <fstream>

/**
 * Function to emulate trajectory from initial to final state, returning the remaining squared error
 * @param bvpSolver shared pointer to boundary value problem solver
 * @param initialState initial trajectory state
 * @param finalState final trajectory state
 * @param maxVelocity maximum velocity allowed
 * @param maxAcceleration maximum acceleration allowed
 * @param sampleTime emulation sample time
 * @return final squared error
 */
double emulation(std::shared_ptr<controller::MinimumTime2DController> bvpSolver,
                 const std::vector<double>& initialState,
                 const std::vector<double>& finalState,
                 const double& maxVelocity,
                 const double& maxAcceleration,
                 const double& sampleTime,
                 std::ofstream& output)
{
    std::vector<double> transientState = initialState;
    double totalTime = bvpSolver->BVPsolution(initialState[1],maxVelocity,finalState[1],maxAcceleration,initialState[0],finalState[0]);
    int timeSteps = ceil(totalTime/sampleTime);
    output.open("bvpSolution.txt");
    for(int i=0;i<timeSteps;i++)
    {
        bvpSolver->BVPemulation(transientState[1],maxVelocity,finalState[1], maxAcceleration,transientState[0],finalState[0],sampleTime);
    }
    output.close();
    double error = sqrt((transientState[0]-finalState[0])*(transientState[0]-finalState[0]) + (transientState[1]-finalState[1])*(transientState[1]-finalState[1]));

    return error;
}

TEST(MonteCarloBVPTest, bvp)
{
    /**
     * Output file
     */
    std::ofstream output;

    double epsilon = 0.001;
    auto bvpSolver = std::make_shared<controller::MinimumTime2DController>(epsilon);
    auto clk = std::make_shared<tools::Clock>();
    auto rsg = std::make_shared<random_state::RandomStateGenerator>();
    double maxVelocity=3.0;
    double maxAcceleration= 3.0;
    double maxX = 4.5;
    double maxY = 3.0;

    std::cout << "\n";
    std::cout << "BVP Optimization test" << std::endl;
    std::cout << "Generating random initial and final states, and computing time..." << std::endl;
    bvpSolver->setMaxVelocity(maxVelocity);
    bvpSolver->setMaxAcceleration(maxAcceleration);

    double maxPositionX = 4.0;

    std::vector<double> initialState;
    std::vector<double> finalState;
    std::vector<double> stateBounds = {-maxPositionX,maxPositionX,-maxVelocity,maxVelocity};


    int N = 500; // Number of Samples
    double error  = 0.0;
    double sampleTime = 0.0001;
    double totalTime;

    /// Test parameters
    double tolerance = 0.1;
    double expectedError = 0.0;

    for(int i=1;i<=N;i++)
    {
        initialState = rsg->returnRandomState(stateBounds);
        finalState = rsg->returnRandomState(stateBounds);
        //bvpSolver->setInitialState(initialState);
        //bvpSolver->setFinalState(finalState);
        totalTime = bvpSolver->BVPsolution(initialState[1],maxVelocity,finalState[1],maxAcceleration,initialState[0],finalState[0]);
        error = emulation(bvpSolver,initialState,finalState,maxVelocity,maxAcceleration,sampleTime,output);
        if(error > tolerance)
        {
            std::cout << "Test failed on following states: " << std::endl;
            std::cout << initialState[0] << " " << initialState[1] << std::endl;
            std::cout << finalState[0] << " " << finalState[1] << std::endl;
            std::cout << "Time for convergence: " << totalTime << std::endl;
        }
        ASSERT_NEAR(error,expectedError,tolerance);
        std::cout << "Passed test case number (" << i << "/" << N  << ") with error " << error << std::endl;
    }

}

