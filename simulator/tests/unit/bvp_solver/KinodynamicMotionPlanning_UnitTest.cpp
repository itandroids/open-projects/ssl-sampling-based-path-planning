//
// Created by felipe on 29/09/20.
//


#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>

#include "gtest/gtest.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

TEST(MonteCarloMotionTest, emulation)
{
    ompl::msg::noOutputHandler();
    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.3;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(1,std::vector<double>(4,0));

    obstacles[0][0] = 0.5;
    obstacles[0][1] = 0.0;
    obstacles[0][2] = 0.5;
    obstacles[0][3] = 0.0;


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));

    // Setting number of samples to be analyzed on narrow phase
    int numberOfSamples = 10;
    double sampleTime= 0.01;
    dvc->setEmulationSampleTime(sampleTime);

    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    start = std::vector<double> {-0.40524, -0.0309386, 2.03978, -2.46358};
    ob::State* state1 = start->as<ob::State>();

    // create a random goal state
    ob::ScopedState<> goal(space);
    //goal = std::vector<double> {1.0,2.0,1.0,0.5};
    goal = std::vector<double> {1.31095, 0.580837, 0.938715, -1.6686};
    ob::State* state2 = goal->as<ob::State>();

    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal);
    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    //si->printSettings(std::cout);

    // print the problem settings
    //pdef->print(std::cout);

    // attempt to solve the problem within one second of planning time
    double maxPlanningTime = 100.0;
    planner->setGoalBias(0.7);

    /// Printing random number in variable numberOfStates, since it doesn't matter at all

    int numberOfStates = 1000;

    /// Printing [number of waypoints; number of obstacles; obstacle radius; 0] ^T

    std::string initialMessage = std::to_string(numberOfStates) + " " + std::to_string(obstacles.size()) + " " + std::to_string(obstacleRadius) +  " 0";
    dvc->printToFile(initialMessage);

    /// Printing obstacles to file




    int numberOfTests = 1000;
    double expectedError = 0.0;
    double tolerance = 0.5;
    double error = 0.0;
    double maximumTime = 10.0;
    double time = 0.0;
    double emulationSampleTime = 0.01;

    auto stateSampler(std::make_shared<dss::KinematicStateSampler>(si.get()));

    int testNumber = 0;
    dvc->setEmulationSampleTime(emulationSampleTime);

    std::cout << std::endl;

    while(testNumber < numberOfTests)
    {
        stateSampler->sample(state1);
        stateSampler->sample(state2);
        ompl::base::RealVectorStateSpace::StateType* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>();
        ompl::base::RealVectorStateSpace::StateType* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>();
        time = dvc->getTimeForTrajectory(state1,state2);
        if( time < maximumTime){
            error  = dvc->emulateTrajectory(state1,state2,sampleTime);
            if(error > tolerance)
            {
                std::cout << "Test failed for states: " << std::endl;
                std::cout << s1->values[0] << " " << s1->values[1] << " " << s1->values[2] << " " << s1->values[3] << std::endl;
                std::cout << s2->values[0] << " " << s2->values[1] << " " << s2->values[2] << " " << s2->values[3] << std::endl;
                std::cout << "It would need a time of " << time << " seconds." << std::endl;
            }
            dvc->printObstaclesToFile();

            ASSERT_NEAR(error, expectedError , tolerance);
            std::cout << "Passed test case number (" << testNumber+1 << "/" << numberOfTests <<  ") passed with error " << error << "." << std::endl;
            testNumber++;
        }


    }
}




