//
// Created by felipe on 29/12/20.
//

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
#include <clock/Clock.h>
#include <fstream>
#include <geometry/KDTree.h>
#include <random>
#include "gtest/gtest.h"


std::vector<double> findClosest(std::vector<double>& target,std::vector<std::vector<double>>& points){

    double minDist = 100000000;
    double currDist;
    std::vector<double> ans;
    for(int i=0;i<points.size();i++)
    {
        currDist = (target[0]-points[i][0])*(target[0]-points[i][0]) + (target[1]-points[i][1])*(target[1]-points[i][1]);
        if(minDist > currDist)
        {
            minDist = currDist;
            ans = points[i];
        }
    }

    return ans;
}

std::vector<double> findClosestOffset(std::vector<double>& target,std::vector<std::vector<double>>& points){

    double minDist = 100000000;
    double currDist;
    std::vector<double> ans;
    for(int i=0;i<points.size();i++)
    {
        currDist = (target[0]-points[i][0])*(target[0]-points[i][0]) + (target[2]-points[i][2])*(target[2]-points[i][2]);
        if(minDist > currDist)
        {
            minDist = currDist;
            ans = points[i];
        }
    }

    return ans;
}



TEST(KDTreeTest, nn)
{

    double max = 10.0;
    double min = -10.0;

    // First create an instance of an engine.
    std::random_device rnd_device;
    // Specify the engine and distribution.
    std::mt19937 mersenne_engine {rnd_device()};  // Generates random integers
    std::uniform_int_distribution<int> dist {1, 50};
    std::uniform_real_distribution<double> realDist {min,max};
    int numberOfTests = 100;
    std::cout << std::endl;
    for(int j = 0;j < numberOfTests; j++)
    {
        int randomSize = dist(mersenne_engine);
        std::vector<std::vector<double>> obstacles(randomSize,std::vector<double>(2,0));




        for(int i=randomSize;i--;)
        {
            obstacles[i][0] = realDist(mersenne_engine);
            obstacles[i][1] = realDist(mersenne_engine);
        }


        geometry::KDTree kd(2);

        for(int i=obstacles.size();i--;)
        {
            kd.insert(obstacles[i]);
        }
        std::vector<double> target = {1,1};

        std::vector<double> nn = kd.nearestNeighbor(target);
        std::vector<double> grountTruth = findClosest(target,obstacles);

        ASSERT_NEAR(nn[0],grountTruth[0],0.0001);
        ASSERT_NEAR(nn[1],grountTruth[1],0.0001);
        std::cout << "Passed test case (" << j+1 << "/" << numberOfTests << ")" << std::endl;
    }
}

TEST(KDTreeTest, offset)
{

    double max = 10.0;
    double min = -10.0;

    // First create an instance of an engine.
    std::random_device rnd_device;
    // Specify the engine and distribution.
    std::mt19937 mersenne_engine {1};  // Generates random integers
    std::uniform_int_distribution<int> dist {1, 50};
    std::uniform_real_distribution<double> realDist {min,max};
    int numberOfTests = 100;
    int stateSize = 4;
    std::cout << std::endl;
    for(int j = 0;j < numberOfTests; j++)
    {
        int randomSize = dist(mersenne_engine);
        std::vector<std::vector<double>> obstacles(randomSize,std::vector<double>(stateSize,0));

        for(int i=randomSize;i--;)
        {
            for(int j=0;j<stateSize;j++)
            {
                obstacles[i][j] = realDist(mersenne_engine);
            }
        }


        geometry::KDTree kd(stateSize);
        kd.setOffset(2);

        for(int i=obstacles.size();i--;)
        {
            kd.insert(obstacles[i]);
        }
        std::vector<double> target = {1,1,1,1};

        std::vector<double> nn = kd.nearestNeighbor(target);
        std::vector<double> groundTruth = findClosestOffset(target,obstacles);


        ASSERT_NEAR(nn[0],groundTruth[0],0.0001);
        ASSERT_NEAR(nn[1],groundTruth[1],0.0001);
        std::cout << "Passed test case (" << j+1 << "/" << numberOfTests << ")" << std::endl;
    }
}

