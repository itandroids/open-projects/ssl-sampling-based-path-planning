//
// Created by felipe on 29/12/20.
//

#include <geometry/KDTree.h>

std::vector<double> findClosest(std::vector<double>& target,std::vector<std::vector<double>>& points){

    double minDist = 100000000;
    double currDist;
    std::vector<double> ans;
    for(int i=0;i<points.size();i++)
    {
        currDist = (target[0]-points[i][0])*(target[0]-points[i][0]) + (target[1]-points[i][1])*(target[1]-points[i][1]);
        if(minDist > currDist)
        {
            minDist = currDist;
            ans = points[i];
        }
    }

    return ans;
}


int main()
{
    std::vector<std::vector<double>> obstacles = {{2,4},{3,4},{-1,2},{10,5},{2,2},{4,5},{9,1},{-3,4}};
    geometry::KDTree kd(2);

    for(int i=obstacles.size();i--;)
    {
        kd.insert(obstacles[i]);
    }
    std::vector<double> target = {1,1};

    std::vector<double> nn = kd.nearestNeighbor(target);
    std::cout << nn[0] << " " << nn[1] << std::endl;
    nn = findClosest(target,obstacles);
    std::cout << nn[0] << " " << nn[1] << std::endl;

    return 0;
}