//
// Created by felipe on 25/06/20.
//

#include <Eigen/Dense>
#include <iostream>
#include <vector>

Eigen::VectorXd printVector(Eigen::VectorXd v)
{
    std::cout << "Printing slice" << std::endl;
    std::cout << v << std::endl;
    v = v*2;

    return v;

}


int main(){
    Eigen::MatrixXd m(4,4);
    m << 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;
    //std::cout << m << std::endl;
    //std::cout << m.block(1,1,2,2) << std::endl;
    m.block(1,1,2,2) *= 2;
    //std::cout << m << std::endl;

    Eigen::VectorXd state(6);
    //state << 1,2,3,4,5,6;
    //std::cout << state << std::endl;
    //std::cout << state.segment(1,2) << std::endl;
    //std::cout << m.block(1,1,2,2)*state.segment(1,2) << std::endl;

    Eigen::VectorXd v(6);
    v << 1,2,3,4,5,6;
    std::cout << v.segment(3,3) << std::endl;

    v = printVector(v.segment(1,4));

    std::cout << v << std::endl;

    Eigen::VectorXd v1(2);
    v1 << 2,3;
    Eigen::MatrixXd Q(2,2);
    Q << 1,0,
        0,4;

    double innerProduct = v1.transpose()*Q*v1;
    std::cout << "Inner product" << std::endl;
    std::cout << innerProduct << std::endl;


    return 0;
}