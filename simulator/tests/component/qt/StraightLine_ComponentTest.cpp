//
// Created by felipe on 15/06/20.
//

#include <QApplication>

#include "gui/FieldWidget.h"
#include "gui/draw_request/DrawBallsRequest.h"
#include "gui/draw_request/DrawSSLTeamRequest.h"
#include "utils/Colors.h"
#include <constants/Constants.h>
#include <world_model/Team.h>
#include <emulator/straight_line/StraightLine.h>

int testSSLBField(int argc, char *argv[]) {
    using itandroids_lib::gui::FieldWidget;
    using itandroids_lib::gui::FieldDrawingParams;
    using itandroids_lib::gui::FieldGeometryParams;
    using itandroids_lib::geometry::Line2D;
    using itandroids_lib::math::Vector2d;
    using itandroids_lib::math::Pose2D;
    using itandroids_lib::gui::DrawRequest;
    using itandroids_lib::gui::DrawBallsRequest;
    using itandroids_lib::gui::DrawSSLTeamRequest;
    using itandroids_lib::utils::Color;

    // Division B dimensions
    const double FIELD_LENGTH = robot_constants::FIELD_SIZE_X;
    const double FIELD_WIDTH = robot_constants::FIELD_SIZE_Y;
    const double BOUNDARY_LENGTH = 9.0 + 0.6;
    const double BOUNDARY_WIDTH = 6.0 + 0.6;
    const double TOTAL_LENGTH = 10.4;
    const double TOTAL_WIDTH = 7.0;
    const double GOAL_AREA_LENGTH = 1.0;
    const double GOAL_AREA_WIDTH = 2.0;
    const double GOAL_DEPTH = 0.18;
    const double GOAL_WIDTH = 1.0;

    FieldDrawingParams drawingParams = FieldDrawingParams::getSLLParams();
    FieldGeometryParams geometryParams = FieldGeometryParams::getSSLParams();
    std::vector<Line2D> fieldLines;
    // Field border
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    // Left goal area
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    // Right goal area
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    // Lines at the middle of the field
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, 0.0), Vector2d(FIELD_LENGTH / 2.0, 0.0)));
    fieldLines.emplace_back(Line2D(Vector2d(0.0, -FIELD_WIDTH / 2.0), Vector2d(0.0, FIELD_WIDTH / 2.0)));

    std::vector<Line2D> leftGoalLines;
    // Left goal
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> rightGoalLines;
    // Right goal
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> boundaryLines;
    // Boundary border
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));

    std::vector<Vector2d> balls;
    balls.emplace_back(Vector2d(0.0, 0.0));

    int teamsize = 1;



    auto team(std::make_shared<world_model::Team>(teamsize,Color::BLUE, Color::WHITE));
    std::vector<double> initialPose = {0.0,0.0,0.0};
    team->teammates[0].setValues(initialPose);


    QApplication application(argc, argv);

    auto fieldWidget(std::make_shared<FieldWidget>(drawingParams, geometryParams, fieldLines, leftGoalLines, rightGoalLines, boundaryLines));
    int simulationSteps = 100;
    double sampleTime = 0.1;
    std::vector<double> initialState = {0.0,0.0,0.0};

    emulator::StraightLine test(simulationSteps,fieldWidget,initialState);

    test.startSimulation(sampleTime);

    return application.exec();
}

int main (int argc, char *argv[]) {
    return testSSLBField(argc, argv);
}