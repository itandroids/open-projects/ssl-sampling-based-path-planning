#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <memory>
#include <thread>
#include <iostream>
#include <chrono>
#include <thread>

/*
Prereqs:
-basic knowledge of c++ (pointers and memory management)
-VERY basic knowledge of Qt (widgets)
Tutorial Topics:
-QGraphicsScene
-QGraphicsItem (QGraphicsRectItem)
-QGraphicsView
*/

std::shared_ptr<QApplication> app;
std::shared_ptr<QGraphicsScene> scene;
std::shared_ptr<QGraphicsView> view;
std::shared_ptr<QGraphicsRectItem> rect;




int main(int argc, char *argv[])
{
    app = std::make_shared<QApplication>(argc,argv);

    QGraphicsRectItem sceneRect(0,0,1000,1000);

    // create a scene
    scene = std::make_shared<QGraphicsScene>(0,0,100,100);

    // create an rectangle item to put into the scene
    rect = std::make_shared<QGraphicsRectItem>();
    rect->setRect(0,0,10,10);

    // create an ellipse item to put into the scene
    auto circle(std::make_shared<QGraphicsEllipseItem>());
    circle->setRect(20,20, 10, 10);

    // add the item to the scene
    scene->addItem(rect.get());
    scene->addItem(circle.get());



    // add a view to visualize the scene
    view = std::make_shared<QGraphicsView>(scene.get());

    // show scene
    view->show();


    app->exec();

    return 0;
}