//
// Created by felipe on 17/11/20.
//

// This program shows the basics of using TBB in C++
// By: Nick from CoffeeBeforeArch

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
#include <vector>

#include "tbb/parallel_invoke.h"

// Helper function for getting the current time for profiling
auto get_time() { return std::chrono::high_resolution_clock::now(); }

int main() {
    // Size of the vector
    constexpr int N = 1 << 25;

    // Create our two vectors
    std::vector<int> v1(N);
    std::vector<int> v2(N);
    std::vector<int> v3(N);
    std::vector<int> v4(N);
    std::vector<int> v5(N);

    // Create our random number generators
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<int> dist(0, 255);

    // Profile parallel invoke
    auto start = get_time();

    // Generate our random inputs
    tbb::parallel_invoke([&] { std::generate(begin(v1), end(v1), [&]() { return dist(rng); }); },
                         [&] { std::generate(begin(v2), end(v2), [&]() { return dist(rng); }); },
                         [&] { std::generate(begin(v3), end(v3), [&]() { return dist(rng); }); },
                         [&] { std::generate(begin(v4), end(v4), [&]() { return dist(rng); }); },
                         [&] { std::generate(begin(v5), end(v5), [&]() { return dist(rng); }); });


    tbb::parallel_invoke([&] { std::sort(begin(v1), end(v1)); },
                         [&] { std::sort(begin(v2), end(v2)); },
                         [&] { std::sort(begin(v3), end(v3)); },
                         [&] { std::sort(begin(v4), end(v4)); },
                         [&] { std::sort(begin(v5), end(v5)); });
    auto finish = get_time();

    // Print out the execution time
    auto duration =
            std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
    std::cout << "Elapsed time = " << duration.count() << " ms\n";

    return 0;
}
