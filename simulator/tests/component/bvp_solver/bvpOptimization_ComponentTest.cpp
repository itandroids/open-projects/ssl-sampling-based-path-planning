//
// Created by felipe on 13/11/20.
//

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
#include <clock/Clock.h>

#include <controllers/tracking/MinimumTime2DController.h>
#include <memory>
#include <world_model/RandomStateGenerator.h>
# include "optimizers/asa047.h"

void test ( );
double epsilon = 0.001;
auto bvpSolver = std::make_shared<controller::MinimumTime2DController>(epsilon);
auto clk = std::make_shared<tools::Clock>();
auto rsg = std::make_shared<random_state::RandomStateGenerator>();
double maxVelocity=3.8651;
double maxAcceleration=2.7747;
double maxX = 4.5;
double maxY = 3.0;

int main ( )
{
    timestamp ( );
    std::cout << "\n";
    std::cout << "BVP Optimization test" << std::endl;
    std::cout << "Generating random initial and final states, and computing time..." << std::endl;
    bvpSolver->setMaxVelocity(maxVelocity);
    bvpSolver->setMaxAcceleration(maxAcceleration);

    double initialState[4] = {1.0,1.0,2.0,1.0};
    double finalState[4] = {1.0,1.0,2.0,1.0};

    double averageTime;

    int N = 5000; // Number of Samples
    double randomAngle;
    std::vector<double> angles;

    for(int i=0;i<N;i++)
    {

        rsg->generateRandomKinodynamicState(initialState,maxVelocity,maxX,maxY);
        rsg->generateRandomKinodynamicState(finalState,maxVelocity,maxX,maxY);
        randomAngle = rsg->generateUniformDistribution(-M_PI,M_PI);


        bvpSolver->setInitialState(initialState);
        bvpSolver->setFinalState(finalState);

        //std::cout << sin(randomAngle) - bvpSolver->fastSin(randomAngle) << std::endl;



        angles = bvpSolver->getFrameInitialGuess();


        double tic = clk->getTime();
        angles = bvpSolver->getBestFrame();
        double time = bvpSolver->getTimeForFrame(&angles[0]);
        double toc = clk->getTime();
        averageTime += toc-tic;
    }

    averageTime /= N;

    std::cout << "Average time: " << averageTime << std::endl;


    /// Angle found
    //std::cout << "Angles used: " << std::endl;
    //for(int i=0;i<2;i++)
    //    std::cout << angles[i] << std::endl;

//
//  Terminate.
//

    timestamp ( );


    return 0;
}

