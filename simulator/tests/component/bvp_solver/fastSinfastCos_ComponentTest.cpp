//
// Created by felipe on 13/11/20.
//

# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <cmath>
#include <clock/Clock.h>

#include <controllers/tracking/MinimumTime2DController.h>
#include <memory>
#include <world_model/RandomStateGenerator.h>
# include "optimizers/asa047.h"

double epsilon = 0.001;
auto bvpSolver = std::make_shared<controller::MinimumTime2DController>(epsilon);
auto clk = std::make_shared<tools::Clock>();
auto rsg = std::make_shared<random_state::RandomStateGenerator>();



int main ( )
{
    timestamp ( );
    std::cout << "\n";
    std::cout << "Fast sin and cos component test" << std::endl;
    std::cout << "Generating random angles" << std::endl;

    double averageTimeSin = 0;
    double averageTimeFastSin = 0;

    int N = 1000000; // Number of Samples
    double randomAngle;
    double tic;
    double toc;
    double val;
    int position;

    for(int i=0;i<N;i++)
    {
        randomAngle = rsg->generateUniformDistribution(-M_PI,M_PI);



        tic = clk->getTime();
        val = sin(randomAngle);
        toc = clk->getTime();
        averageTimeSin += toc-tic;

        tic = clk->getTime();
        val = bvpSolver->fastSin(randomAngle);
        toc = clk->getTime();
        averageTimeFastSin += toc-tic;
    }

    averageTimeSin /= N;
    averageTimeFastSin /= N;

    std::cout << "Average time sin original: " << averageTimeSin << std::endl;
    std::cout << "Average time fast sin: " << averageTimeFastSin << std::endl;


    /// Angle found
    //std::cout << "Angles used: " << std::endl;
    //for(int i=0;i<2;i++)
    //    std::cout << angles[i] << std::endl;

//
//  Terminate.
//
    std::cout << "\n";
    std::cout << "ASA047_TEST:\n";
    std::cout << "  Normal end of execution.\n";
    std::cout << "\n";
    timestamp ( );

    return 0;
}

