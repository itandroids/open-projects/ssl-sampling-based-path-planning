//
// Created by felipe on 24/07/20.
//

//
// Created by felipe on 06/07/20.
//

#include <robot_model/RobotModelBase.h>
#include <robot_model/SingleIntegrator.h>
#include <boost/shared_ptr.hpp>
#include <Eigen/Dense>
#include <iostream>
#include <Eigen/Core>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/bitstar/BITstar.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <path_planning/OMPLPlanning.h>
#include "path_planning_analysis/PlanningAnalysis.h"
#include <ompl/base/samplers/ObstacleBasedValidStateSampler.h>
#include <constants/Constants.h>
#include <emulator/camera_emulator/CameraSystem.h>
#include <QApplication>
#include "gui/FieldWidget.h"
#include "gui/draw_request/DrawBallsRequest.h"
#include "gui/draw_request/DrawSSLTeamRequest.h"
#include "utils/Colors.h"
#include <constants/Constants.h>


int startCameraTest(int argc, char *argv[])
{
    /**
     * Initializing path planner
     */
    int dimensions = 2;

    auto stateSpace(std::make_shared<ompl::base::RealVectorStateSpace>(dimensions));

    // set the bounds
    int lowerBound = -4.5;
    int upperBound = 4.5;
    ompl::base::RealVectorBounds bounds(dimensions);
    for(int i=0;i<dimensions;i++)
    {
        bounds.setLow(i,lowerBound);
        bounds.setHigh(i,upperBound);
    }


    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    //auto planner(std::make_shared<ompl::geometric::RRT>(spaceInfo));
    auto planner(std::make_shared<ompl::geometric::BITstar>(spaceInfo));

    //planner->setRange(0.2);
    //planner->setGoalBias(0.5);

    auto omplInterface(std::make_shared<path_planning::OmplPlanning>(planner,spaceInfo));

    std::vector<double> start(2);
    start[0] = 0.0; start[1] = 0.0;

    std::vector<double> goal(2);
    goal[0] = 0.0; goal[1] = 10.0;


    /**
     * Initalize speed controller
     */
    Eigen::MatrixXd speedControllerGain(3,3);
    Eigen::MatrixXd sensorMatrix(3,6);
    Eigen::VectorXd reference(3,1);

    double proportionalSpeedGain = 4.0;
    speedControllerGain << proportionalSpeedGain, 0.0, 0.0,
            0.0, proportionalSpeedGain, 0.0,
            0.0, 0.0, proportionalSpeedGain;

    // Getting \dot{x}, \dot{y} and \dot{yaw} from given state

    sensorMatrix << 0.0,0.0,0.0,1.0,0.0,0.0,
            0.0,0.0,0.0,0.0,1.0,0.0,
            0.0,0.0,0.0,0.0,0.0,1.0,

            // Reference at zero

            reference << 0,0,0;

    controller::SpeedController speedController(speedControllerGain,sensorMatrix,reference);

    /**
     * Creating bounds for randomStart and randomGoal
     */

    random_state::RandomStateGenerator randomStateGen;

    std::vector<double> startBounds = {robot_constants::START_BEGIN_X,
                                       robot_constants::START_END_X,
                                       robot_constants::START_BEGIN_Y,
                                       robot_constants::START_END_Y};
    std::vector<double> goalBounds = {robot_constants::GOAL_BEGIN_X,
                                      robot_constants::GOAL_END_X,
                                      robot_constants::GOAL_BEGIN_Y,
                                      robot_constants::GOAL_END_Y,
                                      robot_constants::GOAL_BEGIN_YAW,
                                      robot_constants::GOAL_END_YAW};

    std::vector<double> randomStart = randomStateGen.returnRandomState(startBounds);
    std::vector<double> randomGoal = randomStateGen.returnRandomState(goalBounds);


    /**
     * Initialize robot
     */
    double sampleTime = 0.005;
    Eigen::VectorXd initialState(6);
    Eigen::VectorXd initialControllerState(6);
    Eigen::VectorXd accelerationConstraints(3);
    Eigen::VectorXd speedConstraints(3);

    initialState << randomStart[0], randomStart[1], 0.0, 0.0, 0.0, 0.0;
    initialControllerState << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    accelerationConstraints << 2.7747, 2.7747, 27.2595;
    speedConstraints << 3.8651, 3.8651, 26.4807;
    double wn = 164.17;
    double csi = 0.6;

    auto robot(std::make_shared<robot_model::SingleIntegrator>(initialState,
                                                               initialControllerState,
                                                               omplInterface.get(),
                                                               accelerationConstraints,
                                                               speedConstraints,
                                                               sampleTime,
                                                               wn,
                                                               csi));


    /**
     * Initialize FieldWidget
     */

    using itandroids_lib::gui::FieldWidget;
    using itandroids_lib::gui::FieldDrawingParams;
    using itandroids_lib::gui::FieldGeometryParams;
    using itandroids_lib::geometry::Line2D;
    using itandroids_lib::math::Vector2d;
    using itandroids_lib::math::Pose2D;
    using itandroids_lib::gui::DrawRequest;
    using itandroids_lib::gui::DrawBallsRequest;
    using itandroids_lib::gui::DrawSSLTeamRequest;
    using itandroids_lib::utils::Color;

    // Division B dimensions
    const double FIELD_LENGTH = robot_constants::FIELD_SIZE_X;
    const double FIELD_WIDTH = robot_constants::FIELD_SIZE_Y;
    const double BOUNDARY_LENGTH = 9.0 + 0.6;
    const double BOUNDARY_WIDTH = 6.0 + 0.6;
    const double TOTAL_LENGTH = 10.4;
    const double TOTAL_WIDTH = 7.0;
    const double GOAL_AREA_LENGTH = 1.0;
    const double GOAL_AREA_WIDTH = 2.0;
    const double GOAL_DEPTH = 0.18;
    const double GOAL_WIDTH = 1.0;

    FieldDrawingParams drawingParams = FieldDrawingParams::getSLLParams();
    FieldGeometryParams geometryParams = FieldGeometryParams::getSSLParams();
    std::vector<Line2D> fieldLines;
    // Field border
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    // Left goal area
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    // Right goal area
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    // Lines at the middle of the field
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, 0.0), Vector2d(FIELD_LENGTH / 2.0, 0.0)));
    fieldLines.emplace_back(Line2D(Vector2d(0.0, -FIELD_WIDTH / 2.0), Vector2d(0.0, FIELD_WIDTH / 2.0)));

    std::vector<Line2D> leftGoalLines;
    // Left goal
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> rightGoalLines;
    // Right goal
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> boundaryLines;
    // Boundary border
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));

    std::vector<Vector2d> balls;
    balls.emplace_back(Vector2d(0.0, 0.0));

//    dReal teamPosX[MAX_ROBOT_COUNT] = {2.2, 1.0, 1.0, 1.0, 0.33, 1.22,
//                                       3, 3.2, 3.4, 3.6, 3.8, 4.0};
//    dReal teamPosY[MAX_ROBOT_COUNT] = {0.0, -0.75, 0.0, 0.75, 0.25, 0.0,
//                                       1, 1, 1, 1, 1, 1};
    std::vector<double> pxVector = {2.2, 1.0, 1.0, 1.0, 0.33, 1.22};
    std::vector<double> pyVector = {0.0, -0.75, 0.0, 0.75, 0.25, 0.0};

    std::vector<Pose2D> teammates;
    std::vector<int> teammateIds;
    for (int i = 0; i < pxVector.size(); ++i) {
        double px = -pxVector[i];
        double py = pyVector[i];
        double angle = std::atan2(-py, -px);
        teammates.emplace_back(Pose2D(angle, px, py));
        teammateIds.emplace_back(i);
    }

    std::vector<Pose2D> opponents;
    std::vector<int> opponentIds;
    for (int i = 0; i < pxVector.size(); ++i) {
        double px = pxVector[i];
        double py = pyVector[i];
        double angle = std::atan2(-py, -px);
        opponents.emplace_back(Pose2D(angle, px, py));
        opponentIds.emplace_back(i);
    }

    std::vector<std::shared_ptr<DrawRequest>> requests;

    /**
     * No need to draw ball
     */

    //requests.emplace_back(std::make_shared<DrawBallsRequest>(balls, 0.043 / 2.0, Color::ORANGE));
    requests.emplace_back(std::make_shared<DrawSSLTeamRequest>(teammates, teammateIds, Color::BLUE, Color::WHITE));
    requests.emplace_back(std::make_shared<DrawSSLTeamRequest>(opponents, opponentIds, Color::YELLOW, Color::BLACK));

    QApplication application(argc, argv);

    auto fieldWidget(std::make_shared<FieldWidget>(drawingParams, geometryParams, fieldLines, leftGoalLines, rightGoalLines, boundaryLines));
    /**
     * Defining kinematic controller
     */

    Eigen::MatrixXd kinematicControllerGain(3,3);
    Eigen::MatrixXd derivativeKinematicMatrix(3,6);
    Eigen::VectorXd referencePosition(3,1);

    double proportionalGainWheels = 3.0;
    double proportionalGainAngular = 1.0;
    kinematicControllerGain << proportionalGainWheels, 0.0, 0.0,
            0.0, proportionalGainWheels, 0.0,
            0.0, 0.0, proportionalGainAngular;

    // Getting \dot{x}, \dot{y} and \dot{yaw} from given state

    double derivativeKinematicGain = 0.5;
    derivativeKinematicMatrix << 1.0,0.0,0.0,derivativeKinematicGain,0.0,0.0,
            0.0,1.0,0.0,0.0,derivativeKinematicGain,0.0,
            0.0,0.0,1.0,0.0,0.0,derivativeKinematicGain;

    // Reference at zero

    referencePosition << 0,0,0;

    controller::Tracker kinematicController(kinematicControllerGain, derivativeKinematicMatrix, referencePosition);


    /**
     * Creating obstacles
     */

    // Initializing obstacles vector and obstacle bounds
    std::vector<double> obstacleBounds(6,0.0);
    obstacleBounds[0] = -robot_constants::OBSTACLES_LIMIT_REAL_X;
    obstacleBounds[1] = robot_constants::OBSTACLES_LIMIT_REAL_X;
    obstacleBounds[2] = -robot_constants::OBSTACLES_LIMIT_REAL_Y;
    obstacleBounds[3] = robot_constants::OBSTACLES_LIMIT_REAL_Y;
    obstacleBounds[4] = 0;
    obstacleBounds[5] = 2.0*M_PI;

    double obstacleRadius = robot_constants::OBSTACLE_RADIUS;
    int numberOfObstacles = 9;

    std::vector<std::vector<double>> obstacles = randomStateGen.generateRandomObstacles(obstacleRadius,numberOfObstacles,obstacleBounds);


    omplInterface->setObstaclePositions(obstacles);

    /**
     * Defining emulator and timestep configuration
     */
    int numberOfRobots = numberOfObstacles+1;
    int cameraFactor = 3;
    int visionDelay = 0;
    bool showGui = true;
    double diskRadius = 0.05;
    std::vector<double> initialConfig(6,0);

    initialConfig[0] = randomStart[0];
    initialConfig[1] = randomStart[1];

    auto emulator(std::make_shared<emulator::CameraSystem>(robot.get(),fieldWidget, numberOfRobots, cameraFactor, visionDelay, initialConfig, kinematicController));
    emulator->setGoal(randomGoal);
    emulator->setStart(randomStart);
    emulator->setObstacles(obstacles);

    int timesteps = emulator->simulateArrival(sampleTime,showGui,diskRadius);


    return application.exec();

}

int main(int argc, char *argv[])
{
    startCameraTest(argc, argv);
    return 0;
}