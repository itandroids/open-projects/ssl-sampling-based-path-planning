//
// Created by felipe on 08/12/20.
//

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/base/PlannerTerminationCondition.h>

int main() {
    ompl::msg::noOutputHandler();
    auto allocMyValidStateSampler = [](const ompl::base::SpaceInformation *si)
    {
        return std::make_shared<dss::KinematicStateSampler>(si);
    };

    namespace ob = ompl::base;
    namespace og = ompl::geometric;

    std::ofstream outputFile;
    outputFile.open("KFMTConvergenceTest.txt");

    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.30;
    double emulationSampleTime = 0.03;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(1,std::vector<double>(4,0));

    obstacles[0][0] = 0.5;
    obstacles[0][1] = 0.0;
    obstacles[0][2] = 0.5;
    obstacles[0][3] = 0.0;


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);

    // set state validity checking for this space
    si->setStateValidityChecker(dsc);

    // set motion validity checking for this space
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));
    dvc->setMaxAcceleration(maxAcceleration);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setEmulationSampleTime(emulationSampleTime);


    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    start = std::vector<double> {0,0,0,0};

    // create a random goal state
    ob::ScopedState<> goal(space);
    goal = std::vector<double> {1.0,2.0,1.0,0.5};

    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal);
    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    std::cout << std::endl;

    // create a planner for the defined space
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));

    /// Loop to evaluate success probability
    int numberOfSuccesses = 0;
    double successProbability = 0.0;
    int iterations = 10;
    int i=0;

    std::vector<int> samplesVector = {1,3,10,30,100,300,1000,3000,10000};
    double avgLength =0.0;
    double variance = 0.0;

    double planningTimeTest = 1000;
    ob::PlannerTerminationCondition ptc(ob::timedPlannerTerminationCondition(0.0));

    /// Loop to get solution after convergence
    for(int i=0;i<samplesVector.size();i++,avgLength = 0.0,variance = 0.0, successProbability = 0.0)
    {
        for(int j = 0; j<iterations;j++)
        {
            // clear planner's datastructures
            planner->clear();

            // perform setup steps for the planner
            planner->setup();

            // set the problem we are trying to solve for the planner
            planner->setProblemDefinition(pdef);

            planner->setNumSamples(samplesVector[i]);


            ptc = ob::timedPlannerTerminationCondition(planningTimeTest);
            //planner->setGoalBias(0.7);

            ob::PlannerStatus solved = planner->solve(ptc);
            if(solved == ob::PlannerStatus::EXACT_SOLUTION)
            {
                successProbability += 1.0;
                avgLength += pdef->getSolutionPath()->length();
                variance += pdef->getSolutionPath()->length()*pdef->getSolutionPath()->length();
            }

        }
        successProbability /= iterations;
        avgLength /= iterations;
        variance = (variance/iterations-avgLength*avgLength)*(iterations)/(iterations-1);

        outputFile <<  avgLength <<  " " << variance <<" " << successProbability << " " << samplesVector[i] << std::endl;
        std::cout << "Case of planning time (" << i+1 << "/" << samplesVector.size() << ") ended with average length of " << avgLength << std::endl;

        /// Emptying success number
        numberOfSuccesses = 0;
    }

    return 0;
}