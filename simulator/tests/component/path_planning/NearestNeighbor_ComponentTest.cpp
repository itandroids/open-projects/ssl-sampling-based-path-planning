//
// Created by felipe on 24/09/20.
//
#include <iostream>
#include <ompl/datastructures/NearestNeighborsGNATNoThreadSafety.h>
#include <ompl/datastructures/NearestNeighborsSqrtApprox.h>
#include "math.h"

double distanceFunction(double a, double b){
    return fabs(a-b);
}
int main() {

    auto nn = std::make_shared<ompl::NearestNeighborsGNATNoThreadSafety<double>>();
    //auto nn = std::make_shared<ompl::NearestNeighborsSqrtApprox<double>>();
    nn->setDistanceFunction([](double a, double b) { return distanceFunction(a, b);});
    nn->clear();
    double a = 0.0;
    double b = 1.0;
    double e = 2.0;
    double f = 3.0;
    double k = 12.0;
    double h = 13.0;
    double g = 1.1;
    nn->add(a);
    nn->add(b);
    nn->add(e);
    nn->add(f);
    nn->add(k);
    nn->add(h);
    double p = nn->nearest(g);
    std::cout << p << std::endl;

    return 0;
}