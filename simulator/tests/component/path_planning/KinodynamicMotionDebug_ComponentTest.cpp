//
// Created by felipe on 29/09/20.
//


#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl_planners/FMTK.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;

bool isStateValid(const ob::State *state)
{
    // cast the abstract state type to the type we expect
    const auto *se3state = state->as<ob::SE3StateSpace::StateType>();

    // extract the first component of the state and cast it to what we expect
    const auto *pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);

    // extract the second component of the state and cast it to what we expect
    const auto *rot = se3state->as<ob::SO3StateSpace::StateType>(1);

    // check validity of state defined by pos & rot


    // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
    return (const void*)rot != (const void*)pos;
}

ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

void plan()
{


    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.7;
    double sampleTime = 0.01;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(1,std::vector<double>(4,0));

    obstacles[0][0] = 0.5;
    obstacles[0][1] = 0.0;
    obstacles[0][2] = 0.5;
    obstacles[0][3] = 0.0;


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    start = std::vector<double> {0,0,0,0};

    // create a random goal state
    ob::ScopedState<> goal(space);
    goal = std::vector<double> {1.3,-1.0,0.5,-0.5};

    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal);
    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    //si->printSettings(std::cout);

    // print the problem settings
    //pdef->print(std::cout);

    // attempt to solve the problem within one second of planning time
    double maxPlanningTime = 5.0;
    //planner->setGoalBias(0.3);
    planner->setNumSamples(100);
    ob::PlannerStatus solved = planner->ob::Planner::solve(maxPlanningTime);
    ob::PathPtr path = pdef->getSolutionPath();

    if (solved == ob::PlannerStatus::EXACT_SOLUTION)
    {
        std::cout << "Exact solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        ob::PathPtr path = pdef->getSolutionPath();
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else if (solved == ob::PlannerStatus::APPROXIMATE_SOLUTION)
    {
        std::cout << "Approximate solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        ob::PathPtr path = pdef->getSolutionPath();
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else
        std::cout << "No solution found" << std::endl;

    auto pathG(std::make_shared<ompl::geometric::PathGeometric>(*(path->as<ompl::geometric::PathGeometric>())));

    /// Printing [number of waypoints; number of obstacles; obstacle radius; 0] ^T

    int numberOfStates = pathG->getStateCount();
    std::string initialMessage = std::to_string(numberOfStates) + " " + std::to_string(obstacles.size()) + " " + std::to_string(obstacleRadius) +  " 0";
    dvc->printToFile(initialMessage);

    /// Printing obstacles to file

    dvc->printObstaclesToFile();


    for(int i=0;i<numberOfStates-1;i++)
    {
        ompl::base::State* state1 = pathG->getState(i);
        ompl::base::State* state2 = pathG->getState(i+1);
        ompl::base::RealVectorStateSpace::StateType* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>();
        ompl::base::RealVectorStateSpace::StateType* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>();
        std::cout << s1->values[0] << " " << s1->values[1] << " " << s1->values[2] << " " << s1->values[3] << std::endl;
        std::cout << s2->values[0] << " " << s2->values[1] << " " << s2->values[2] << " " << s2->values[3] << std::endl;
        dvc->emulateTrajectory(state1,state2,sampleTime);
    }


}

int main(int /*argc*/, char ** /*argv*/)
{
    //std::cout << "OMPL version: " << OMPL_VERSION << std::endl;

    ompl::msg::noOutputHandler();
    plan();

    std::cout << std::endl << std::endl;


    return 0;
}
