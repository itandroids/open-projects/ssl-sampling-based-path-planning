//
// Created by felipe on 29/09/20.
//

#include <controllers/tracking/MinimumTime2DController.h>
#include <memory>

int main()
{
    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;

    // set the bounds for the R^4
    int dimensions = 4;
    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    std::vector<double> start = {-3.85862, 1.76404, -1.84721, 1.47269};
    std::vector<double> goal = {-3.90772, 0.918184, -0.616304, 1.56635};

    double alpha = 0.0;

    double beta = bvpSolver->getBeta(start[0], goal[0], start[1], goal[1], start[2], goal[2], start[3], goal[3],maxAcceleration, maxVelocity);
    std::cout << beta << std::endl;
    std::cout <<
              bvpSolver->getArrivalTimeDecoupled(start[0], goal[0], start[1], goal[1], start[2], goal[2], start[3], goal[3],maxAcceleration, maxVelocity, beta, alpha)
              << std::endl;
    return 0;
}