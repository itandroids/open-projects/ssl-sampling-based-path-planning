//
// Created by felipe on 29/09/20.
//


  
 /* Author: Ioan Sucan */
  
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>

namespace ob = ompl::base;
 namespace og = ompl::geometric;
  
 bool isStateValid(const ob::State *state)
 {
     // cast the abstract state type to the type we expect
     const auto *se3state = state->as<ob::SE3StateSpace::StateType>();
  
     // extract the first component of the state and cast it to what we expect
     const auto *pos = se3state->as<ob::RealVectorStateSpace::StateType>(0);
  
     // extract the second component of the state and cast it to what we expect
     const auto *rot = se3state->as<ob::SO3StateSpace::StateType>(1);
  
     // check validity of state defined by pos & rot
  
  
     // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
     return (const void*)rot != (const void*)pos;
 }

ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}
  
 void plan()
 {


     const double maxAcceleration = 3.0;
     const double maxVelocity = 3.0;
     double epsilon = 0.01;
  
     // set the bounds for the R^4
     int dimensions = 4;
     ob::RealVectorBounds bounds(dimensions);

     /// Single obstacle configuration
     std::vector<std::vector<double>> obstacles(1,std::vector<double>(2,0));


     auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

     // construct the state space we are planning in
     auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

     /**
      * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
      */
     bounds.setHigh(0,robot_constants::HALF_SIZE_X);
     bounds.setLow(0,-robot_constants::HALF_SIZE_X);
     bounds.setHigh(1,2*maxVelocity);
     bounds.setLow(1,-2*maxVelocity);
     bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
     bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
     bounds.setHigh(3,2*maxVelocity);
     bounds.setLow(3,-2*maxVelocity);
  
     space->setBounds(bounds);
     space->setMaxAcceleration(maxAcceleration);
     space->setMaxVelocity(maxVelocity);


  
     // construct an instance of  space information from this state space
     auto si(std::make_shared<ob::SpaceInformation>(space));
  
     // set state validity checking for this space
     si->setStateValidityChecker(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));

     // set motion validity checking for this space
     si->setMotionValidator(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));

     // set state sampler
     si->setValidStateSamplerAllocator(allocMyValidStateSampler);
  
     // create a random start state
     ob::ScopedState<> start(space);
     start = std::vector<double> {0,0,0,0};
  
     // create a random goal state
     ob::ScopedState<> goal(space);
     goal = std::vector<double> {1.0,2.0,1.0,0.5};
  
     // create a problem instance
     auto pdef(std::make_shared<ob::ProblemDefinition>(si));
  
     // set the start and goal states
     pdef->setStartAndGoalStates(start, goal);
     std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
     std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;
  
     // create a planner for the defined space
     auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
  
     // set the problem we are trying to solve for the planner
     planner->setProblemDefinition(pdef);
  
     // perform setup steps for the planner
     planner->setup();
  
  
     // print the settings for this space
     //si->printSettings(std::cout);
  
     // print the problem settings
     //pdef->print(std::cout);
  
     // attempt to solve the problem within one second of planning time
     double maxPlanningTime = 0.01;
     planner->setGoalBias(0.7);
     ob::PlannerStatus solved = planner->ob::Planner::solve(maxPlanningTime);
  
     if (solved == ob::PlannerStatus::EXACT_SOLUTION)
     {
         std::cout << "Exact solution!" << std::endl;
         // get the goal representation from the problem definition (not the same as the goal state)
         // and inquire about the found path
         ob::PathPtr path = pdef->getSolutionPath();
         std::cout << "Found solution:" << std::endl;
  
         // print the path to screen
         path->print(std::cout);

         std::cout << "Path length: " << path->length() << std::endl;
     }
     else if (solved == ob::PlannerStatus::APPROXIMATE_SOLUTION)
     {
         std::cout << "Exact solution!" << std::endl;
         // get the goal representation from the problem definition (not the same as the goal state)
         // and inquire about the found path
         ob::PathPtr path = pdef->getSolutionPath();
         std::cout << "Found solution:" << std::endl;

         // print the path to screen
         path->print(std::cout);

         std::cout << "Path length: " << path->length() << std::endl;
     }
     else
         std::cout << "No solution found" << std::endl;
 }

 int main(int /*argc*/, char ** /*argv*/)
 {
     //std::cout << "OMPL version: " << OMPL_VERSION << std::endl;

     ompl::msg::noOutputHandler();
     plan();
  
     std::cout << std::endl << std::endl;

  
     return 0;
 }
