//
// Created by felipe on 22/05/20.
//

#include <iostream>
#include <Eigen/Core>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <path_planning/OMPLPlanning.h>
#include <ompl/base/samplers/UniformValidStateSampler.h>


namespace ob = ompl::base;
namespace og = ompl::geometric;

ompl::base::ValidStateSamplerPtr allocValidStateSampler(const ompl::base::SpaceInformation *si)
{
    // we can perform any additional setup / configuration of a sampler here,
    // but there is nothing to tweak in case of the ObstacleBasedValidStateSampler.
    return std::make_shared<ompl::base::UniformValidStateSampler>(si);
}

int main()
{
    auto stateSpace(std::make_shared<ompl::base::SE2StateSpace>());

    // set the bounds
    ompl::base::RealVectorBounds bounds(2);
    bounds.setLow(0,-100.0);
    bounds.setHigh(0,100.0);
    bounds.setLow(1,-100.0);
    bounds.setHigh(1,100.0);
    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    ompl::base::State* state;

    spaceInfo->setValidStateSamplerAllocator(allocValidStateSampler);

    ompl::base::ValidStateSamplerPtr stateSampler = spaceInfo->allocValidStateSampler();



    for(int i=0;i<100;i++)
    {
        if(stateSampler->sample(state))
            spaceInfo->printState(state,std::cout);
    }
}
