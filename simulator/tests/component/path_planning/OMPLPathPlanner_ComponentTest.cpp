//
// Created by felipe on 08/05/20.
//

#include <iostream>
#include <Eigen/Core>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <path_planning/OMPLPlanning.h>

void testOMPLPlanningSE2()
{
    auto stateSpace(std::make_shared<ompl::base::SE2StateSpace>());

    // set the bounds
    ompl::base::RealVectorBounds bounds(2);
    bounds.setLow(0,-100.0);
    bounds.setHigh(0,100.0);
    bounds.setLow(1,-100.0);
    bounds.setHigh(1,100.0);
    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    auto planner(std::make_shared<ompl::geometric::RRT>(spaceInfo));

    planner->setRange(0.3);

    auto omplInterface(std::make_shared<path_planning::OmplPlanning>(planner,spaceInfo));

    std::vector<double> start(2);
    start[0] = 0.0; start[1] = 0.0;

    std::vector<double> goal(2);
    goal[0] = 0.0; goal[1] = 10.0;

    omplInterface->setStartAndGoal(start,goal);
    omplInterface->solveProblem(10.0);
    omplInterface->getProblem()->getSolutionPath()->print(std::cout);

    std::cout << "Printing states: ";
    for(int i=0;i<omplInterface->getPathSize();i++)
    {

        std::vector<std::vector<double>> waypoint = omplInterface->getWaypoint(i);
        std::cout << "Waypoint " << i << ": " << std::endl;
        std::cout << "Real coordinates:";
        for(int r =0;r < waypoint[0].size(); r++)
            std::cout << waypoint[0][r] << " ";
        std::cout << std::endl;
        std::cout << "SO2 coordinates: ";
        for(int s =0;s < waypoint[1].size(); s++)
            std::cout << waypoint[1][s] << " ";
        std::cout << std::endl;
    }


}

void testOMPLPlanningReal()
{
    int dimensions = 2;

    auto stateSpace(std::make_shared<ompl::base::RealVectorStateSpace>(dimensions));

    // set the bounds
    int lowerBound = -100;
    int upperBound = 100;
    ompl::base::RealVectorBounds bounds(dimensions);
    for(int i=0;i<dimensions;i++)
    {
        bounds.setLow(i,lowerBound);
        bounds.setHigh(i,upperBound);
    }


    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    auto planner(std::make_shared<ompl::geometric::RRT>(spaceInfo));

    planner->setRange(0.3);

    auto omplInterface(std::make_shared<path_planning::OmplPlanning>(planner,spaceInfo));

    std::vector<double> start(2);
    start[0] = 0.0; start[1] = 0.0;

    std::vector<double> goal(2);
    goal[0] = 0.0; goal[1] = 10.0;

    omplInterface->setStartAndGoal(start,goal);
    omplInterface->solveProblem(10.0);
    omplInterface->getProblem()->getSolutionPath()->print(std::cout);

    std::cout << "Printing states: ";
    for(int i=0;i<omplInterface->getPathSize();i++)
    {

        std::vector<std::vector<double>> waypoint = omplInterface->getWaypoint(i);
        std::cout << "Waypoint " << i << ": " << std::endl;
        std::cout << "Real coordinates:";
        for(int r =0;r < waypoint[0].size(); r++)
            std::cout << waypoint[0][r] << " ";
        std::cout << std::endl;
        std::cout << "SO2 coordinates: ";
        for(int s =0;s < waypoint[1].size(); s++)
            std::cout << waypoint[1][s] << " ";
        std::cout << std::endl;
    }


}

int main()
{
    testOMPLPlanningSE2();
    testOMPLPlanningReal();
    return 0;
}