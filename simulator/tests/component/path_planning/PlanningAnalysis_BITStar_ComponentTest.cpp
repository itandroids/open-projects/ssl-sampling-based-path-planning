//
// Created by felipe on 20/05/20.
//

#include <iostream>
#include <Eigen/Core>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/bitstar/BITstar.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <path_planning/OMPLPlanning.h>
#include "path_planning_analysis/PlanningAnalysis.h"
#include <ompl/base/samplers/ObstacleBasedValidStateSampler.h>
#include <constants/Constants.h>
#include <fstream>

ompl::base::ValidStateSamplerPtr allocOBValidStateSampler(const ompl::base::SpaceInformation *si)
{
    // we can perform any additional setup / configuration of a sampler here,
    // but there is nothing to tweak in case of the ObstacleBasedValidStateSampler.
    return std::make_shared<ompl::base::ObstacleBasedValidStateSampler>(si);
}

int main()
{
    /// Creating file
    std::ofstream bestCost;
    bestCost.open(robot_constants::OUTPUT_PATH + "resultsBestCostBITstar.txt");
    std::ofstream sucProb;
    sucProb.open(robot_constants::OUTPUT_PATH + "resultsSuccessProbabilityBITstar.txt");


    int dimensions = 2;

    auto stateSpace(std::make_shared<ompl::base::RealVectorStateSpace>(dimensions));

    // set the bounds
    ompl::base::RealVectorBounds bounds(dimensions);

    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(1,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(1,robot_constants::HALF_SIZE_Y);



    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    spaceInfo->setValidStateSamplerAllocator(allocOBValidStateSampler);

    auto planner(std::make_shared<ompl::geometric::BITstar>(spaceInfo));


    //planner->setUseKNearest(false);

    //planner->setRange(0.001);

    auto omplInterface(std::make_shared<path_planning::OmplPlanning>(planner,spaceInfo));

    std::vector<double> boundsStart{robot_constants::START_BEGIN_X,robot_constants::START_END_X,robot_constants::START_BEGIN_Y,robot_constants::START_END_Y};
    std::vector<double> boundsGoal{robot_constants::GOAL_BEGIN_X,robot_constants::GOAL_END_X,robot_constants::GOAL_BEGIN_Y,robot_constants::GOAL_END_Y};
    int seed = 1;
    planning_analysis::PlanningAnalysis planAnalysis(boundsStart, boundsGoal, seed);
    std::vector<double> amountOfSamples = planAnalysis.linearSpacedVector(10,800,80);


    // Performs iterations to evaluate average time

    planner->setSamplesPerBatch(100);
    int monteCarloIterations = 10000;
    planning_analysis::AnalysisResult result = planAnalysis.MonteCarloTimingSimulation(*dynamic_cast<path_planning::PathPlannerBase*>(omplInterface.get()),
    monteCarloIterations);

    for(int i=0;i < result.computationTimeSequence.size() ; i++)
    {
        sucProb << result.computationTimeSequence[i] << " " << result.successProbability[i] << " " << result.bestCost[i] << " " << result.avgCost[i] << " " << result.stdDev[i] << std::endl;

    }

    std::vector<double> avgLengthResult(2,0);
    int sampleBatchesIterations = monteCarloIterations*2;
    for(int i=0; i<amountOfSamples.size() ;i++)
    {
        planner->setSamplesPerBatch(amountOfSamples[i]);
        avgLengthResult = planAnalysis.MonteCarloAverageLength(*dynamic_cast<path_planning::PathPlannerBase*>(omplInterface.get()),
                                                         sampleBatchesIterations);
        bestCost << avgLengthResult[0] << " " << avgLengthResult[1] << " "<< amountOfSamples[i] << std::endl;
    }

    sucProb.close();
    bestCost.close();
    return 0;
}