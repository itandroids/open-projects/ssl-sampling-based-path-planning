//
// Created by felipe on 08/12/20.
//

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <world_model/RandomStateGenerator.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/base/PlannerTerminationCondition.h>
#include <clock/Clock.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>


#define PBSTR ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
#define PBWIDTH 60

void printProgress(double percentage) {
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\rCurrent test progress:");
    printf("%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}

ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

void visualizationTest()
{
    namespace ob = ompl::base;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>());

    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double sampleTime = 0.01;
    double finalEmulationSampleTime = 0.0001;


    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(10,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;

    rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);



    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,maxVelocity);
    bounds.setLow(1,-maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,maxVelocity);
    bounds.setLow(3,-maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    std::vector<double> startVector(4,0);
    rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,maximumStartX,maximumStartY);
    startVector[0] += startXBias;
    start = startVector;



    // create a random goal state
    ob::ScopedState<> goal(space);
    std::vector<double> goalVector(4,0);
    rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,maximumGoalX,maximumGoalY);
    goalVector[0] += goalXBias;
    goal = goalVector;


    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal);
    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    //si->printSettings(std::cout);

    // print the problem settings
    //pdef->print(std::cout);

    // attempt to solve the problem within one second of planning time
    double maxPlanningTime = 10.0;
    //planner->setGoalBias(0.3);
    planner->setNumSamples(100);

    ompl::base::PlannerTerminationCondition ptc(ompl::base::exactSolnPlannerTerminationCondition(pdef));

    ob::PathPtr path = pdef->isStraightLinePathValid();


    ob::PlannerStatus solved;

    if(!path)
    {
        solved = planner->solve(ptc);
        path = pdef->getSolutionPath();
    }
    else
    {
        solved = ob::PlannerStatus::EXACT_SOLUTION;
    }


    if (solved == ob::PlannerStatus::EXACT_SOLUTION)
    {
        std::cout << "Exact solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else if (solved == ob::PlannerStatus::APPROXIMATE_SOLUTION)
    {
        std::cout << "Approximate solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else
        std::cout << "No solution found" << std::endl;

    auto pathG(std::make_shared<ompl::geometric::PathGeometric>(*(path->as<ompl::geometric::PathGeometric>())));

    /// Printing [number of waypoints; number of obstacles; obstacle radius; 0] ^T

    int numberOfStates = pathG->getStateCount();
    std::string initialMessage = std::to_string(numberOfStates) + " " + std::to_string(obstacles.size()) + " " + std::to_string(obstacleRadius) +  " 0";
    dvc->printToFile(initialMessage);

    /// Printing obstacles to file

    dvc->printObstaclesToFile();


    for(int i=0;i<numberOfStates-1;i++)
    {
        ompl::base::State* state1 = pathG->getState(i);
        ompl::base::State* state2 = pathG->getState(i+1);
        ompl::base::RealVectorStateSpace::StateType* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>();
        ompl::base::RealVectorStateSpace::StateType* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>();
        std::cout << s1->values[0] << " " << s1->values[1] << " " << s1->values[2] << " " << s1->values[3] << std::endl;
        std::cout << s2->values[0] << " " << s2->values[1] << " " << s2->values[2] << " " << s2->values[3] << std::endl;
        dvc->emulateTrajectory(state1,state2,finalEmulationSampleTime);
    }

}

void convergenceTest()
{
    ompl::msg::noOutputHandler();
    namespace ob = ompl::base;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>());

    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double sampleTime = 0.03;
    double finalEmulationSampleTime = 0.0001;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Creating file with time results for bootstrapping analysis

    std::ofstream output;
    output.open("bootstrappSpeedTest.txt");

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(10,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;

    rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,maxVelocity);
    bounds.setLow(1,-maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,maxVelocity);
    bounds.setLow(3,-maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    std::vector<double> startVector(4,0);


    // create a random goal state
    ob::ScopedState<> goal(space);
    std::vector<double> goalVector(4,0);


    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));



    int numberOfSimulations = 10000;
    planner->setNumSamples(100);
    auto lengthObj(std::make_shared<ompl::base::PathLengthOptimizationObjective>(si));
    ompl::base::PlannerTerminationCondition ptc(ompl::base::exactSolnPlannerTerminationCondition(pdef));

    /// Set very high threshold to stop ASAP.
    lengthObj->setCostThreshold(ompl::base::Cost(1000000.0));



    auto clk(std::make_shared<tools::Clock>());
    ob::PathPtr path;
    ob::PlannerStatus solved;
    int numberOfSuccesses = 0.0;
    double successProbability;
    int it = 0;
    double averageTime = 0.0;


    while(it++<numberOfSimulations)
    {
        // create a random start state
        rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,maximumStartX,maximumStartY);
        startVector[0] += startXBias;
        start = startVector;


        // create a random goal state
        rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,maximumGoalX,maximumGoalY);
        goalVector[0] += goalXBias;
        goal = goalVector;



        // set the start and goal states
        pdef->clearSolutionPaths();
        pdef->setStartAndGoalStates(start, goal);
        pdef->setOptimizationObjective(lengthObj);

        planner->clear();
        planner->setProblemDefinition(pdef);

        planner->setup();


        double tic = clk->getTime();
        path = pdef->isStraightLinePathValid();
        if(!path)
        {
            solved = planner->solve(ptc);
            if(pdef->hasExactSolution())
            {
                path = pdef->getSolutionPath();
                numberOfSuccesses++;
            }
        }
        else
        {
            solved = ob::PlannerStatus::EXACT_SOLUTION;
            numberOfSuccesses++;
        }
        double toc = clk->getTime();

        output << toc-tic << std::endl;
        averageTime += toc-tic;
    }

    averageTime /= numberOfSimulations;
    successProbability = ((double)numberOfSuccesses)/((double)numberOfSimulations);

    std::cout << "Results of speed test below.." << std::endl;
    std::cout << "Success probability: " << successProbability << std::endl;
    std::cout << "Average time: " << averageTime << std::endl;
    output.close();


}

void speedTest()
{
    ompl::msg::noOutputHandler();
    namespace ob = ompl::base;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>());

    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double sampleTime = 0.1;
    double finalEmulationSampleTime = 0.0001;
    int numberOfObstacles = 10;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Creating file with time results for bootstrapping analysis

    std::ofstream output;
    output.open("bootstrappSpeedTest.txt");

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(numberOfObstacles,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;

    rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,maxVelocity);
    bounds.setLow(1,-maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,maxVelocity);
    bounds.setLow(3,-maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    std::vector<double> startVector(4,0);


    // create a random goal state
    ob::ScopedState<> goal(space);
    std::vector<double> goalVector(4,0);


    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::FMTK>(si));



    int numberOfSimulations = 1000;
    planner->setNumSamples(100);
    auto lengthObj(std::make_shared<ompl::base::PathLengthOptimizationObjective>(si));
    ompl::base::PlannerTerminationCondition ptc(ompl::base::exactSolnPlannerTerminationCondition(pdef));

    /// Set very high threshold to stop ASAP.
    lengthObj->setCostThreshold(ompl::base::Cost(1000000.0));



    auto clk(std::make_shared<tools::Clock>());
    ob::PathPtr path;
    ob::PlannerStatus solved;
    int numberOfSuccesses = 0.0;
    double successProbability;
    int it = 0;
    double averageTime = 0.0;
    double percentage = 0.0;


    while(it++<numberOfSimulations)
    {
        percentage = (double)(it-1)/numberOfSimulations;
        printProgress(percentage);

        // create a random start state
        rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,maximumStartX,maximumStartY);
        startVector[0] += startXBias;
        start = startVector;


        // create a random goal state
        rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,maximumGoalX,maximumGoalY);
        goalVector[0] += goalXBias;
        goal = goalVector;



        // set the start and goal states
        pdef->clearSolutionPaths();
        pdef->setStartAndGoalStates(start, goal);
        pdef->setOptimizationObjective(lengthObj);

        planner->clear();
        planner->setProblemDefinition(pdef);

        planner->setup();


        double tic = clk->getTime();
        path = pdef->isStraightLinePathValid();
        if(!path)
        {
            solved = planner->solve(ptc);
            if(pdef->hasExactSolution())
            {
                path = pdef->getSolutionPath();
                numberOfSuccesses++;
            }
        }
        else
        {
            solved = ob::PlannerStatus::EXACT_SOLUTION;
            numberOfSuccesses++;
        }
        double toc = clk->getTime();

        output << toc-tic << std::endl;
        averageTime += toc-tic;
    }

    averageTime /= numberOfSimulations;
    successProbability = ((double)numberOfSuccesses)/((double)numberOfSimulations);

    std::cout << "Results of speed test below.." << std::endl;
    std::cout << "Success probability: " << successProbability << std::endl;
    std::cout << "Average time: " << averageTime << std::endl;
    output.close();

}

int main()
{


    /// Runs speed test
    speedTest();

    /// Runs convergence test
    //convergenceTest();

    /// Runs visualization test
    visualizationTest();

    return 0;
}
