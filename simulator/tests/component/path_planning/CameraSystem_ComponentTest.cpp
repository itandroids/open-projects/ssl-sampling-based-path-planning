//
// Created by felipe on 06/07/20.
//

#include <robot_model/RobotModelBase.h>
#include <robot_model/DoubleIntegrator.h>
#include <boost/shared_ptr.hpp>
#include <Eigen/Dense>
#include <iostream>
#include <Eigen/Core>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/bitstar/BITstar.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <path_planning/OMPLPlanning.h>
#include "path_planning_analysis/PlanningAnalysis.h"
#include <ompl/base/samplers/ObstacleBasedValidStateSampler.h>
#include <constants/Constants.h>
#include <emulator/camera_emulator/CameraSystem.h>
#include <QApplication>
#include "gui/FieldWidget.h"
#include "gui/draw_request/DrawBallsRequest.h"
#include "gui/draw_request/DrawSSLTeamRequest.h"
#include "utils/Colors.h"
#include <constants/Constants.h>


int startCameraTest(int argc, char *argv[])
{
    /**
     * Initializing path planner
     */
    int dimensions = 2;

    auto stateSpace(std::make_shared<ompl::base::RealVectorStateSpace>(dimensions));

    // set the bounds
    int lowerBound = -4.5;
    int upperBound = 4.5;
    ompl::base::RealVectorBounds bounds(dimensions);
    for(int i=0;i<dimensions;i++)
    {
        bounds.setLow(i,lowerBound);
        bounds.setHigh(i,upperBound);
    }


    stateSpace->setBounds(bounds);

    auto spaceInfo(std::make_shared<ompl::base::SpaceInformation>(stateSpace));

    auto planner(std::make_shared<ompl::geometric::RRT>(spaceInfo));

    planner->setRange(0.3);

    auto omplInterface(std::make_shared<path_planning::OmplPlanning>(planner,spaceInfo));

    std::vector<double> start(2);
    start[0] = 0.0; start[1] = 0.0;

    std::vector<double> goal(2);
    goal[0] = 0.0; goal[1] = 10.0;


    /**
     * Initalize speed controller
     */
    Eigen::MatrixXd speedControllerGain(3,3);
    Eigen::MatrixXd sensorMatrix(3,6);
    Eigen::VectorXd reference(3,1);

    double proportionalSpeedGain = 4.0;
    speedControllerGain << proportionalSpeedGain, 0.0, 0.0,
                           0.0, proportionalSpeedGain, 0.0,
                           0.0, 0.0, proportionalSpeedGain;

    // Getting \dot{x}, \dot{y} and \dot{yaw} from given state

    sensorMatrix << 0.0,0.0,0.0,1.0,0.0,0.0,
                    0.0,0.0,0.0,0.0,1.0,0.0,
                    0.0,0.0,0.0,0.0,0.0,1.0,

    // Reference at zero

    reference << 0,0,0;

    controller::SpeedController speedController(speedControllerGain,sensorMatrix,reference);


    /**
     * Initialize robot
     */
    double sampleTime = 0.005;
    Eigen::VectorXd initialState(6,1);
    Eigen::VectorXd accelerationConstraints(3,1);
    Eigen::VectorXd speedConstraints(3,1);

    initialState << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    accelerationConstraints << 2.7747, 2.7747, 27.2595;
    speedConstraints << 3.8651, 3.8651, 26.4807;

    auto robot(std::make_shared<robot_model::DoubleIntegrator>(initialState, omplInterface.get(), accelerationConstraints, speedConstraints,
                speedController, sampleTime));


    /**
     * Initialize FieldWidget
     */

    using itandroids_lib::gui::FieldWidget;
    using itandroids_lib::gui::FieldDrawingParams;
    using itandroids_lib::gui::FieldGeometryParams;
    using itandroids_lib::geometry::Line2D;
    using itandroids_lib::math::Vector2d;
    using itandroids_lib::math::Pose2D;
    using itandroids_lib::gui::DrawRequest;
    using itandroids_lib::gui::DrawBallsRequest;
    using itandroids_lib::gui::DrawSSLTeamRequest;
    using itandroids_lib::utils::Color;

    // Division B dimensions
    const double FIELD_LENGTH = robot_constants::FIELD_SIZE_X;
    const double FIELD_WIDTH = robot_constants::FIELD_SIZE_Y;
    const double BOUNDARY_LENGTH = 9.0 + 0.6;
    const double BOUNDARY_WIDTH = 6.0 + 0.6;
    const double TOTAL_LENGTH = 10.4;
    const double TOTAL_WIDTH = 7.0;
    const double GOAL_AREA_LENGTH = 1.0;
    const double GOAL_AREA_WIDTH = 2.0;
    const double GOAL_DEPTH = 0.18;
    const double GOAL_WIDTH = 1.0;

    FieldDrawingParams drawingParams = FieldDrawingParams::getSLLParams();
    FieldGeometryParams geometryParams = FieldGeometryParams::getSSLParams();
    std::vector<Line2D> fieldLines;
    // Field border
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, FIELD_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -FIELD_WIDTH / 2.0)));
    // Left goal area
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 + GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    // Right goal area
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0)));
    fieldLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, GOAL_AREA_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 - GOAL_AREA_LENGTH, -GOAL_AREA_WIDTH / 2.0)));
    // Lines at the middle of the field
    fieldLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, 0.0), Vector2d(FIELD_LENGTH / 2.0, 0.0)));
    fieldLines.emplace_back(Line2D(Vector2d(0.0, -FIELD_WIDTH / 2.0), Vector2d(0.0, FIELD_WIDTH / 2.0)));

    std::vector<Line2D> leftGoalLines;
    // Left goal
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    leftGoalLines.emplace_back(Line2D(Vector2d(-FIELD_LENGTH / 2.0 - GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(-FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> rightGoalLines;
    // Right goal
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, -GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0)));
    rightGoalLines.emplace_back(Line2D(Vector2d(FIELD_LENGTH / 2.0 + GOAL_DEPTH, GOAL_WIDTH / 2.0), Vector2d(FIELD_LENGTH / 2.0, GOAL_WIDTH / 2.0)));

    std::vector<Line2D> boundaryLines;
    // Boundary border
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0), Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0)));
    boundaryLines.emplace_back(Line2D(Vector2d(-BOUNDARY_LENGTH / 2.0, BOUNDARY_WIDTH / 2.0), Vector2d(-BOUNDARY_LENGTH / 2.0, -BOUNDARY_WIDTH / 2.0)));

    std::vector<Vector2d> balls;
    balls.emplace_back(Vector2d(0.0, 0.0));

//    dReal teamPosX[MAX_ROBOT_COUNT] = {2.2, 1.0, 1.0, 1.0, 0.33, 1.22,
//                                       3, 3.2, 3.4, 3.6, 3.8, 4.0};
//    dReal teamPosY[MAX_ROBOT_COUNT] = {0.0, -0.75, 0.0, 0.75, 0.25, 0.0,
//                                       1, 1, 1, 1, 1, 1};
    std::vector<double> pxVector = {2.2, 1.0, 1.0, 1.0, 0.33, 1.22};
    std::vector<double> pyVector = {0.0, -0.75, 0.0, 0.75, 0.25, 0.0};

    std::vector<Pose2D> teammates;
    std::vector<int> teammateIds;
    for (int i = 0; i < pxVector.size(); ++i) {
        double px = -pxVector[i];
        double py = pyVector[i];
        double angle = std::atan2(-py, -px);
        teammates.emplace_back(Pose2D(angle, px, py));
        teammateIds.emplace_back(i);
    }

    std::vector<Pose2D> opponents;
    std::vector<int> opponentIds;
    for (int i = 0; i < pxVector.size(); ++i) {
        double px = pxVector[i];
        double py = pyVector[i];
        double angle = std::atan2(-py, -px);
        opponents.emplace_back(Pose2D(angle, px, py));
        opponentIds.emplace_back(i);
    }

    std::vector<std::shared_ptr<DrawRequest>> requests;

    /**
     * No need to draw ball
     */

    //requests.emplace_back(std::make_shared<DrawBallsRequest>(balls, 0.043 / 2.0, Color::ORANGE));
    requests.emplace_back(std::make_shared<DrawSSLTeamRequest>(teammates, teammateIds, Color::BLUE, Color::WHITE));
    requests.emplace_back(std::make_shared<DrawSSLTeamRequest>(opponents, opponentIds, Color::YELLOW, Color::BLACK));

    QApplication application(argc, argv);

    auto fieldWidget(std::make_shared<FieldWidget>(drawingParams, geometryParams, fieldLines, leftGoalLines, rightGoalLines, boundaryLines));
    /**
     * Defining kinematic controller
     */

    Eigen::MatrixXd kinematicControllerGain(3,3);
    Eigen::MatrixXd derivativeKinematicMatrix(3,6);
    Eigen::VectorXd referencePosition(3,1);

    double proportionalGain = 1.0;
    kinematicControllerGain << proportionalGain, 0.0, 0.0,
            0.0, proportionalGain, 0.0,
            0.0, 0.0, proportionalGain;

    // Getting \dot{x}, \dot{y} and \dot{yaw} from given state

    double derivativeKinematicGain = 0.0;
    derivativeKinematicMatrix << 1.0,0.0,0.0,derivativeKinematicGain,0.0,0.0,
                        0.0,1.0,0.0,0.0,derivativeKinematicGain,0.0,
                        0.0,0.0,1.0,0.0,0.0,derivativeKinematicGain;

    // Reference at zero

    referencePosition << 0,0,0;

    controller::Tracker kinematicController(kinematicControllerGain, derivativeKinematicMatrix, referencePosition);


    /**
     * Defining emulator and timestep configuration
     */

    int steps = 4000;
    int numberOfRobots = 10;
    int cameraFactor = 16;
    int visionDelay = 0;
    std::vector<double> initialConfig(6,0);

    auto emulator(std::make_shared<emulator::CameraSystem>(robot.get(),steps, fieldWidget, numberOfRobots, cameraFactor, visionDelay, initialConfig, kinematicController));
    emulator->setGoal({2,2});
    emulator->setStart({initialConfig[0],initialConfig[1]});

    emulator->startSimulation(sampleTime);

    return application.exec();

}

int main(int argc, char *argv[])
{
    startCameraTest(argc, argv);
    return 0;
}