//
// Created by felipe on 05/06/20.
//

#include <world_model/RandomStateGenerator.h>

int main()
{
    random_state::RandomStateGenerator rand(1);
    for(int i=0;i<10;i++)
        std::cout << rand.generateUniformDistribution(0,1) << std::endl;
    return 0;
}