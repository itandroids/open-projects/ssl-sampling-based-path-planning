//
// Created by felipe on 09/01/21.
//


/// Performance evaluation with multiple obstacles
/// Convergence performance

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/base/PlannerTerminationCondition.h>
#include <world_model/RandomStateGenerator.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <clock/Clock.h>
#include <string.h>


#define PBSTR ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
#define PBWIDTH 60


void printProgress(double percentage) {
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    //printf("\rCurrent test progress:");
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}


ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

std::vector<double> linspace(double begin, int spaces, double end)
{
    double space = (end-begin)/(spaces-1);
    std::vector<double> ans(spaces,0);
    int it = 0;
    double curr = begin;
    for(int i=0;i<spaces;i++)
    {
        ans[i] = curr;
        curr+=space;
    }

    return ans;
}


void successProbabilityTest(bool optimization, bool withObstacles){
    ompl::msg::noOutputHandler();
    namespace ob = ompl::base;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>(1));

    const double maxAcceleration = robot_constants::ROBOT_MAX_ACCELERATION;
    const double maxVelocity = robot_constants::ROBOT_MAX_VELOCITY;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double sampleTime = 0.1;
    double finalEmulationSampleTime = 0.0001;
    int numberOfObstacles = 0;

    std::vector<double> fieldLimits = {robot_constants::MAX_ALLOWED_X,robot_constants::MAX_ALLOWED_Y};

    if(withObstacles)
        numberOfObstacles = 10;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Creating file with time results for bootstrapping analysis

    std::ofstream output;

    std::string fileName("results_analysis/successProbabilityRRTK");

    if(optimization)
        fileName += "_optimizing";
    if(withObstacles)
        fileName += "_WithObstacles";

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(numberOfObstacles,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    bvpSolver->setFrameOptimization(optimization);

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    bounds.setHigh(0,robot_constants::MAX_ALLOWED_X);
    bounds.setLow(0,-robot_constants::MAX_ALLOWED_X);
    bounds.setHigh(1,maxVelocity);
    bounds.setLow(1,-maxVelocity);
    bounds.setHigh(2,robot_constants::MAX_ALLOWED_Y);
    bounds.setLow(2,-robot_constants::MAX_ALLOWED_Y);
    bounds.setHigh(3,maxVelocity);
    bounds.setLow(3,-maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    std::vector<double> startVector(4,0);


    // create a random goal state
    ob::ScopedState<> goal(space);
    std::vector<double> goalVector(4,0);


    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));



    int numberOfSimulations = 2000;
    planner->setGoalBias(0.7);
    auto lengthObj(std::make_shared<ompl::base::PathLengthOptimizationObjective>(si));





    auto clk(std::make_shared<tools::Clock>());
    ob::PathPtr path;
    ob::PlannerStatus solved;
    int numberOfSuccesses;
    double successProbability;
    int it = 0;
    double averageTime = 0.0;
    double percentage = 0.0;
    double maximumAllowedTime = 0.5;

    std::vector<double> allowedTime = linspace(0.01,21,0.99);

    output.open(fileName +".txt");
    for(int j=0;j<allowedTime.size();j++,it = 0)
    {

        numberOfSuccesses = 0.0;
        while(it++<numberOfSimulations)
        {
            rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);
            //percentage = (double)(it-1)/numberOfSimulations;

            // create a problem instance
            auto pdef(std::make_shared<ob::ProblemDefinition>(si));


            dsc->setObstacles(obstacles);
            dvc->setObstacles(obstacles);
            //printProgress(percentage);

            do{
                // create a random start state
                rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,
                                                    -maximumStartX + startXBias,maximumStartX + startXBias,-maximumStartY,maximumStartY,maxAcceleration,fieldLimits);
                start = startVector;
            }while(!dsc->isValid(start->as<ompl::base::State>()));

            do{
                // create a random goal state
                rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,-maximumGoalX+goalXBias,maximumGoalX+goalXBias,-maximumGoalY,maximumGoalY,maxAcceleration,fieldLimits);
                goal = goalVector;
            }while(!dsc->isValid(goal->as<ompl::base::State>()));



            // set the start and goal states
            pdef->setStartAndGoalStates(start, goal);

            planner->clear();
            planner->setProblemDefinition(pdef);

            planner->setup();

            solved = planner->ob::Planner::solve(allowedTime[j]);
            if(pdef->hasExactSolution())
            {
                path = pdef->getSolutionPath();
                numberOfSuccesses++;
            }

        }
        averageTime /= numberOfSimulations;
        successProbability = ((double)numberOfSuccesses)/((double)numberOfSimulations);

        output << allowedTime[j] << " " <<  numberOfSuccesses  << " " << numberOfSimulations << std::endl;
    }

    output.close();
}



int main()
{
    std::cout << "Running success probability test w/o obstacles and w/o optimization..." << std::endl;
    successProbabilityTest(false,false);
    std::cout << "Running success probability test w/o obstacles and with optimization..." << std::endl;
    successProbabilityTest(true, false);
    std::cout << "Running success probability test with obstacles and w/o optimization..." << std::endl;
    successProbabilityTest(false, true);
    std::cout << "Running success probability test with obstacles and with optimization..." << std::endl;
    successProbabilityTest(true, true);

    return 0;
}