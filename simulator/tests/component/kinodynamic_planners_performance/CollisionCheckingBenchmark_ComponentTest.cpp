//
// Created by felipe on 29/09/20.
//


#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <clock/Clock.h>
#include <world_model/RandomStateGenerator.h>


ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

double evaluateCollisionChecking(bool withGnat)
{

    namespace ob = ompl::base;
    namespace og = ompl::geometric;

    ompl::msg::noOutputHandler();

    auto clk = std::make_shared<tools::Clock>();


    const double maxAcceleration = 3.0;
    const double maxVelocity = 3.0;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>(1));

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(10,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;

    rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::HALF_SIZE_X);
    bounds.setLow(0,-robot_constants::HALF_SIZE_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::HALF_SIZE_Y);
    bounds.setLow(2,-robot_constants::HALF_SIZE_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));

    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));

    // Setting number of samples to be analyzed on narrow phase
    double emulationSampleTime = 0.01;
    dvc->setEmulationSampleTime(emulationSampleTime);

    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);
    dvc->setGnatCollisionChecking(withGnat);
    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);


    // Declare start and goal
    std::vector<double> startVector(4,0);
    ob::ScopedState<> start(space);
    std::vector<double> goalVector(4,0);
    ob::ScopedState<> goal(space);


    int numberOfTests = 10000;
    int testNumber = 0;
    dvc->setEmulationSampleTime(emulationSampleTime);

    std::cout << std::endl;

    double tic,toc;
    double emulationTime = 0.0,timeEstimationTime = 0.0;
    double averageTime = 0.0;

    while(testNumber++ < numberOfTests)
    {
        // create a random start state

        rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,maximumStartX,maximumStartY);
        startVector[0] += startXBias;
        start = startVector;

        // create a random goal state

        rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,maximumGoalX,maximumGoalY);
        goalVector[0] += goalXBias;
        goal = goalVector;

        tic = clk->getTime();
        dvc->evaluateNarrowPhase(start->as<ompl::base::State>(),goal->as<ompl::base::State>());
        toc = clk->getTime();

        averageTime += toc-tic;
    }

    averageTime /= (double)numberOfTests;

    return averageTime;
}


int main()
{
    std::cout << "Average time with GNAT: " << evaluateCollisionChecking(true) << std::endl;
    std::cout << "Average time with brute force: " << evaluateCollisionChecking(false) << std::endl;

    return 0;
}




