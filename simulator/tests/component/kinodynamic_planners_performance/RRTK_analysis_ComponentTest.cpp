//
// Created by felipe on 15/12/20.
//

/// Performance evaluation with multiple obstacles
/// Convergence performance


//
// Created by felipe on 08/12/20.
//

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/base/PlannerTerminationCondition.h>
#include <world_model/RandomStateGenerator.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <clock/Clock.h>
#include <string.h>


#define PBSTR ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
#define PBWIDTH 60

void printProgress(double percentage) {
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    //printf("\rCurrent test progress:");
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}


ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

std::vector<double> linspace(double begin, int spaces, double end)
{
    double space = (end-begin)/(spaces-1);
    std::vector<double> ans(spaces,0);
    int it = 0;
    double curr = begin;
    for(int i=0;i<spaces;i++)
    {
        ans[i] = curr;
        curr+=space;
    }

    return ans;
}


void visualizationTest(const std::vector<double>& startVector, const std::vector<double>& goalVector, std::string filename)
{
    namespace ob = ompl::base;
    namespace og = ompl::geometric;

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;
    double obstacleRadius = 0.18;
    int numberOfObstacles = 10;
    std::vector<std::vector<double>> obstacles(numberOfObstacles,std::vector<double>(4,0));

    ompl::msg::noOutputHandler();

    auto clk = std::make_shared<tools::Clock>();

    auto rsg(std::make_shared<random_state::RandomStateGenerator>(1));

    rsg->generateSeparatedRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles,obstacleRadius);


    const double maxAcceleration = robot_constants::ROBOT_MAX_ACCELERATION;
    const double maxVelocity = robot_constants::ROBOT_MAX_VELOCITY;
    double epsilon = 0.01;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    /**
     * It is necessary to set velocity bounds duplicated. The circle must fit inside the square.
     */
    bounds.setHigh(0,robot_constants::MAX_ALLOWED_X);
    bounds.setLow(0,-robot_constants::MAX_ALLOWED_X);
    bounds.setHigh(1,2*maxVelocity);
    bounds.setLow(1,-2*maxVelocity);
    bounds.setHigh(2,robot_constants::MAX_ALLOWED_Y);
    bounds.setLow(2,-robot_constants::MAX_ALLOWED_Y);
    bounds.setHigh(3,2*maxVelocity);
    bounds.setLow(3,-2*maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacles(obstacles);
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver,filename));

    // Setting number of samples to be analyzed on narrow phase
    double emulationSampleTime = 0.05;
    double motionSampleTime = 0.001;
    dvc->setEmulationSampleTime(emulationSampleTime);

    dvc->setObstacles(obstacles);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    //start = std::vector<double> {-3.0, 1.0, 0.0, 1.0};
    start = startVector;
    ob::State* state1 = start->as<ob::State>();

    // create a random goal state
    ob::ScopedState<> goal(space);
    //goal = std::vector<double> {1.0,2.0,1.0,0.5};
    //goal = std::vector<double> {3.0, 1.0, 1.0, 1.0};
    goal = goalVector;
    ob::State* state2 = goal->as<ob::State>();

    // create a problem instance
    auto pdef(std::make_shared<ob::ProblemDefinition>(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal);
    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    //si->printSettings(std::cout);

    // print the problem settings
    //pdef->print(std::cout);

    // attempt to solve the problem within one second of planning time
    double maxPlanningTime = 10.0;

    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    //si->printSettings(std::cout);


    planner->setGoalBias(0.7);
    ob::PlannerStatus solved = planner->ob::Planner::solve(maxPlanningTime);
    ob::PathPtr path = pdef->getSolutionPath();

    if (solved == ob::PlannerStatus::EXACT_SOLUTION)
    {
        std::cout << "Exact solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        ob::PathPtr path = pdef->getSolutionPath();
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else if (solved == ob::PlannerStatus::APPROXIMATE_SOLUTION)
    {
        std::cout << "Approximate solution!" << std::endl;
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        ob::PathPtr path = pdef->getSolutionPath();
        std::cout << "Found solution:" << std::endl;

        // print the path to screen
        path->print(std::cout);

        std::cout << "Path length: " << path->length() << std::endl;
    }
    else
        std::cout << "No solution found" << std::endl;

    auto pathG(std::make_shared<ompl::geometric::PathGeometric>(*(path->as<ompl::geometric::PathGeometric>())));

    /// Printing [number of waypoints; number of obstacles; obstacle radius; 0] ^T

    int numberOfStates = pathG->getStateCount();
    std::string initialMessage = std::to_string(numberOfStates) + " " + std::to_string(obstacles.size()) + " " + std::to_string(obstacleRadius) +  " 0";
    dvc->printToFile(initialMessage);

    /// Printing obstacles to file

    dvc->printObstaclesToFile();


    for(int i=0;i<numberOfStates-1;i++)
    {
        ompl::base::State* state1 = pathG->getState(i);
        ompl::base::State* state2 = pathG->getState(i+1);
        ompl::base::RealVectorStateSpace::StateType* s1 = state1->as<ompl::base::RealVectorStateSpace::StateType>();
        ompl::base::RealVectorStateSpace::StateType* s2 = state2->as<ompl::base::RealVectorStateSpace::StateType>();
        std::cout << s1->values[0] << " " << s1->values[1] << " " << s1->values[2] << " " << s1->values[3] << std::endl;
        std::cout << s2->values[0] << " " << s2->values[1] << " " << s2->values[2] << " " << s2->values[3] << std::endl;
        dvc->emulateTrajectory(state1,state2,motionSampleTime);
    }

}

int main()
{
    //std::cout << "Running success probability test w/o obstacles and w/o optimization..." << std::endl;
    //successProbabilityTest(false,false);
    //std::cout << "Running success probability test w/o obstacles and with optimization..." << std::endl;
    //successProbabilityTest(true, false);
    //std::cout << "Running success probability test with obstacles and w/o optimization..." << std::endl;
    //successProbabilityTest(false, true);
    //std::cout << "Running success probability test with obstacles and with optimization..." << std::endl;
    //successProbabilityTest(true, true);

    //std::cout << "Running speed test w/o obstacles and w/o optimization..." << std::endl;
    //speedTest(false, false);
    //std::cout << "Running speed test w/o obstacles and with optimization..." << std::endl;
    //speedTest(true, false);
    //std::cout << "Running speed test with obstacles and w/o optimization..." << std::endl;
    //speedTest(false, true);
    //std::cout << "Running speed test with obstacles and with optimization..." << std::endl;
    //speedTest(true, true);

    //std::cout << "Convergence test w/o obstacles and w/o optimization..." << std::endl;
    //convergenceTest(false, false);
    //std::cout << "Convergence test w/o obstacles and with optimization..." << std::endl;
    //convergenceTest(true, false);
    //std::cout << "Convergence test with obstacles and w/o optimization..." << std::endl;
    //convergenceTest(false, true);
    //std::cout << "Convergence test with obstacles and with optimization..." << std::endl;
    //convergenceTest(true, true);

    visualizationTest({-3.0, 1.0, 0.0, 1.0},{3.0, 1.0, 1.0, 1.0},"RRTKnarrowPhaseAnalysis_1.txt");
    visualizationTest({-3.0, 0.0, 0.0, 0.0},{3.0, 3.0, 0.0, 0.0},"RRTKnarrowPhaseAnalysis_2.txt");
    visualizationTest({-3.0, 0.0, 0.0, 1.0},{3.0, 0.0, 1.0, 0.0},"RRTKnarrowPhaseAnalysis_3.txt");

    return 0;
}
