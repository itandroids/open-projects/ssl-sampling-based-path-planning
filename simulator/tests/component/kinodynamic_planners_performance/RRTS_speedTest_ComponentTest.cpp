//
// Created by felipe on 09/01/21.
//


#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <constants/Constants.h>
#include <ompl/geometric/PathGeometric.h>

#include <ompl/config.h>
#include <iostream>
#include <ompl_planners/KinematicStateSpace.h>
#include <ompl_planners/RRTXVG.h>
#include <ompl_planners/FMTK.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/base/PlannerTerminationCondition.h>
#include <world_model/RandomStateGenerator.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <clock/Clock.h>
#include <string.h>
#include <ompl_planners/RRTsteer.h>
#include <ompl_planners/BVPRRT.h>


#define PBSTR ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
#define PBWIDTH 60


void printProgress(double percentage) {
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    //printf("\rCurrent test progress:");
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}


ompl::base::ValidStateSamplerPtr allocMyValidStateSampler(const ompl::base::SpaceInformation *si)
{
    return std::make_shared<dss::KinematicStateSampler>(si);
}

std::vector<double> linspace(double begin, int spaces, double end)
{
    double space = (end-begin)/(spaces-1);
    std::vector<double> ans(spaces,0);
    int it = 0;
    double curr = begin;
    for(int i=0;i<spaces;i++)
    {
        ans[i] = curr;
        curr+=space;
    }

    return ans;
}


void speedTest(bool optimization, bool withObstacles)
{
    ompl::msg::noOutputHandler();
    namespace ob = ompl::base;

    auto rsg(std::make_shared<random_state::RandomStateGenerator>(1));

    const double maxAcceleration = robot_constants::ROBOT_MAX_ACCELERATION;
    const double maxVelocity = robot_constants::ROBOT_MAX_VELOCITY;
    double epsilon = 0.01;
    double obstacleRadius = 0.18;
    double sampleTime = 0.2;
    double finalEmulationSampleTime = 0.0001;
    int numberOfObstacles = 0;

    if(withObstacles)
        numberOfObstacles = 10;

    // set the bounds for the R^4
    int dimensions = 4;
    ob::RealVectorBounds bounds(dimensions);

    /// Creating file with time results for bootstrapping analysis

    std::ofstream output;

    std::string fileName("results_analysis/performanceTestRRTK");

    if(optimization)
        fileName += "_optimizing";
    if(withObstacles)
        fileName += "_WithObstacles";
    output.open(fileName + ".txt");

    /// Single obstacle configuration
    std::vector<std::vector<double>> obstacles(numberOfObstacles,std::vector<double>(4,0));

    double maximumObstacleX = 1.0;
    double maximumObstacleY = 2.5;
    double maximumStartX = 0.5;
    double maximumStartY = 2.5;
    double maximumGoalX = 0.5;
    double maximumGoalY = 2.5;

    double startXBias = -3.5;
    double goalXBias = 3.5;


    auto bvpSolver(std::make_shared<controller::MinimumTime2DController>(epsilon));

    bvpSolver->setFrameOptimization(optimization);

    // construct the state space we are planning in
    auto space(std::make_shared<dss::KinematicStateSpace>(bvpSolver,dimensions));

    bounds.setHigh(0,robot_constants::MAX_ALLOWED_X);
    bounds.setLow(0,-robot_constants::MAX_ALLOWED_X);
    bounds.setHigh(1,maxVelocity);
    bounds.setLow(1,-maxVelocity);
    bounds.setHigh(2,robot_constants::MAX_ALLOWED_Y);
    bounds.setLow(2,-robot_constants::MAX_ALLOWED_Y);
    bounds.setHigh(3,maxVelocity);
    bounds.setLow(3,-maxVelocity);

    space->setBounds(bounds);
    space->setMaxAcceleration(maxAcceleration);
    space->setMaxVelocity(maxVelocity);



    // construct an instance of  space information from this state space
    auto si(std::make_shared<ob::SpaceInformation>(space));


    // Configuring dynamic state validity checker
    auto dsc(std::make_shared<dss::DynamicStateValidityChecker>(si,dimensions));
    dsc->setObstacleRadius(obstacleRadius);
    si->setStateValidityChecker(dsc);


    // Configuring dynamic validity checker
    auto dvc(std::make_shared<dss::DynamicMotionValidityChecker>(si,bvpSolver));


    dvc->setEmulationSampleTime(sampleTime);
    dvc->setObstacleRadius(obstacleRadius);
    dvc->setMaxVelocity(maxVelocity);
    dvc->setMaxAcceleration(maxAcceleration);

    // set motion validity checking for this space
    si->setMotionValidator(dvc);

    // set state sampler
    si->setValidStateSamplerAllocator(allocMyValidStateSampler);

    // create a random start state
    ob::ScopedState<> start(space);
    std::vector<double> startVector(4,0);


    // create a random goal state
    ob::ScopedState<> goal(space);
    std::vector<double> goalVector(4,0);



    //std::cout << si->isValid(start->as<ompl::base::State>()) << std::endl;
    //std::cout << si->isValid(goal->as<ompl::base::State>()) << std::endl;

    // create a planner for the defined space
    //auto planner(std::make_shared<ompl::geometric::RRTXVG>(si));
    auto planner(std::make_shared<ompl::geometric::BVPRRT>(si,dvc));



    int numberOfSimulations = 100;
    planner->setGoalBias(0.5);
    planner->setRange(sampleTime);
    auto lengthObj(std::make_shared<ompl::base::PathLengthOptimizationObjective>(si));

    /// Set very high threshold to stop ASAP.
    lengthObj->setCostThreshold(ompl::base::Cost(1000000.0));



    auto clk(std::make_shared<tools::Clock>());
    ob::PathPtr path;
    ob::PlannerStatus solved;
    int numberOfSuccesses = 0.0;
    double successProbability;
    int it = 0;
    double averageTime = 0.0;
    double percentage = 0.0;
    double maximumAllowedTime = 0.5;


    while(it++<numberOfSimulations)
    {
        rsg->generateRandomKinodynamicStaticObstacles(maximumObstacleX,maximumObstacleY,obstacles);
        percentage = (double)(it-1)/numberOfSimulations;

        dsc->setObstacles(obstacles);
        dvc->setObstacles(obstacles);
        //printProgress(percentage);

        // create a random start state
        rsg->generateRandomKinodynamicState(&startVector[0],maxVelocity,maximumStartX,maximumStartY);
        startVector[0] += startXBias;
        //startVector[1] = 0.0;
        //startVector[3] = 0.0;
        start = startVector;


        // create a random goal state
        rsg->generateRandomKinodynamicState(&goalVector[0],maxVelocity,maximumGoalX,maximumGoalY);
        goalVector[0] += goalXBias;
        //goalVector[1] = 0.0;
        //goalVector[3] = 0.0;
        goal = goalVector;



        // set the start and goal states
        auto pdef(std::make_shared<ompl::base::ProblemDefinition>(si));

        pdef->clearSolutionPaths();
        pdef->setStartAndGoalStates(start, goal);
        pdef->setOptimizationObjective(lengthObj);

        planner->clear();
        planner->setProblemDefinition(pdef);

        planner->setup();


        double tic = clk->getTime();
        double toc;
        path = pdef->isStraightLinePathValid();
        if(!path)
        {
            solved = planner->ob::Planner::solve(maximumAllowedTime);
            if(pdef->hasExactSolution())
            {
                path = pdef->getSolutionPath();
                numberOfSuccesses++;
                toc = clk->getTime();
                output << toc-tic << std::endl;
                averageTime += toc-tic;
            }

        }
        else
        {
            solved = ob::PlannerStatus::EXACT_SOLUTION;
            numberOfSuccesses++;
            toc = clk->getTime();
            output << toc-tic << std::endl;
            averageTime += toc-tic;
        }
    }

    averageTime /= numberOfSuccesses;
    successProbability = ((double)numberOfSuccesses)/((double)numberOfSimulations);

    std::cout << "Results of speed test below.." << std::endl;
    std::cout << "Success probability: " << successProbability << std::endl;
    std::cout << "Average time: " << averageTime << std::endl;
    output.close();

}



int main()
{

    std::cout << "Running speed test w/o obstacles and w/o optimization..." << std::endl;
    speedTest(false, false);
    std::cout << "Running speed test w/o obstacles and with optimization..." << std::endl;
    speedTest(true, false);
    std::cout << "Running speed test with obstacles and w/o optimization..." << std::endl;
    speedTest(false, true);
    std::cout << "Running speed test with obstacles and with optimization..." << std::endl;
    speedTest(true, true);

    return 0;
}