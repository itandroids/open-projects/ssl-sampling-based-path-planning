clc;
clear all;

%Theta angle
theta = sym('the');

%Robot radius
radius = sym('r');

%Robot center of mass height
height = sym('h');

%Friction forces
friction = sym('f',[4 1]);

%Normal forces
normal = sym('n', [4 1]);

%Create forces
F1 = [-friction(1)*sin(theta), friction(1)*cos(theta), normal(1)];
F2 = [-friction(2)*sin(theta), -friction(2)*cos(theta), normal(2)];
F3 = [friction(3)*sin(theta), -friction(3)*cos(theta), normal(3)];
F4 = [friction(4)*sin(theta), friction(4)*cos(theta), normal(4)];

%Calculating wheel's positions
r1 = [radius*cos(theta), radius*sin(theta), -height];
r2 = [-radius*cos(theta), radius*sin(theta), -height];
r3 = [-radius*cos(theta), -radius*sin(theta), -height];
r4 = [radius*cos(theta), -radius*sin(theta), -height];

%Calculating momentum
M1 = cross(r1,F1);
M2 = cross(r2,F2);
M3 = cross(r3,F3);
M4 = cross(r4,F4);

%Newton laws
Fr = F1+F2+F3+F4;
Mr = simplify(M1+M2+M3+M4);

%Calculating controllability Gramian

A = [0 0 1 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
B = [0 0; 0 0; 1 0; 0 1];
R = eye(2);
t = sym('t');
t_ = sym('t_');

Gr = int(expm(A*(t-t_))*B*inv(R)*B'*expm(A'*(t-t_)),t_,0,t);
test = int(t_,t_,0,t);
